-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: web1
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_detail`
--

DROP TABLE IF EXISTS `account_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_detail` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `roleId` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `addressId` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_tji1c2srb5ikrmv7mgjnxflpd` (`roleId`),
  KEY `FK_ao4njohoefoh9522smkiv17t8` (`addressId`),
  CONSTRAINT `FK_ao4njohoefoh9522smkiv17t8` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_tji1c2srb5ikrmv7mgjnxflpd` FOREIGN KEY (`roleId`) REFERENCES `account_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_detail`
--

LOCK TABLES `account_detail` WRITE;
/*!40000 ALTER TABLE `account_detail` DISABLE KEYS */;
INSERT INTO `account_detail` VALUES (3,'lamthanh1451999@gmail.com',NULL,'yTH1GEXE+60=','0964774964','Nguyen Lam Thanh',2,'activated',6),(4,'ltttrang@gmail.com',NULL,'yTH1GEXE+60=','0964774964','Le Thuy Thuy Trang',3,'activated',5),(22,'thanhlam1451999@gmail.com',NULL,'yTH1GEXE+60=','0964774964','Nguyen Lam Thanh Michael',21,'activated',1),(27,'nlthanhititiu17038@gmail.com',NULL,'yTH1GEXE+60=','09xxxxxxxx','Nguyen Lam Thanh',26,'activated',NULL);
/*!40000 ALTER TABLE `account_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_role`
--

DROP TABLE IF EXISTS `account_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_role`
--

LOCK TABLES `account_role` WRITE;
/*!40000 ALTER TABLE `account_role` DISABLE KEYS */;
INSERT INTO `account_role` VALUES (2,'user'),(3,'admin'),(21,'user'),(26,'user');
/*!40000 ALTER TABLE `account_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(20) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Thành phố Hà Nội','Quận Ba Đình','Số 60, Ấp Phú Hưng, Xã Phú Hựu'),(2,'Tỉnh Đồng Tháp','Huyện Châu Thành','Số 60'),(5,'Tỉnh Lào Cai','Huyện Bảo Thắng','More'),(6,'Tỉnh Đồng Tháp','Huyện Châu Thành','Số 60, Ấp Phú Hưng, Xã Phú Hựu');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bill` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `due_date` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `order_date` varchar(255) NOT NULL,
  `paymentMode` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `staus` varchar(255) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `FK_3ac38309d899hoderihd5mh9j` (`user_id`),
  CONSTRAINT `FK_3ac38309d899hoderihd5mh9j` FOREIGN KEY (`user_id`) REFERENCES `account_detail` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (7,'TÃ¡Â»Ânh Cao BÃ¡ÂºÂ±ng-HuyÃ¡Â»Ân TrÃÂ  LÃÂ©nh','2019-05-02','6','6 6','2019-04-30','MasterCart/Visa','6','Canceled',60000,3),(8,'Tỉnh Điện Biên-Huyện Tủa Chùa','2019-05-02','nlthanhititiu17038@gmail.com','Test Buy without logging in','2019-04-30','MasterCart/Visa','0964774964','Finish',4000,NULL),(9,'Tá»nh HoÃ  BÃ¬nh-Huyá»n LÆ°Æ¡ng SÆ¡n','2019-05-05','nlthanhititiu17038@gmail.com','Nguyen Lam Thanh Test','2019-05-03','MasterCart/Visa','0964774964','Finish',15000,NULL),(10,'Äá»ng ThÃ¡p-ChÃ¢u ThÃ nh','2019-05-05','nlthanhititiu17038@gmail.com','BÃ¡n NhÃ  Äá» Mua','2019-05-03','ATM','0964774964','Canceled',384887,3),(11,'Tỉnh Quảng Ninh-Huyện Bình Liêu','2019-05-05','lamthanh1451999@gmail.com','1 1','2019-05-03','ATM','0898489101','Canceled',269000,3),(12,'Tỉnh Hà Giang-Huyện Vị Xuyên','2019-05-05','nlthanhititiu17038@gmail.com','Nguyễn Lâm Thành','2019-05-03','MasterCart/Visa','0964774964','Canceled',12000,3),(13,'Tỉnh Sơn La-Huyện Mường La','2019-05-05','nlthanhititiu17038@gmail.com','Nguyễn Lâm Thành','2019-05-03','ATM','0964774964','Canceled',45000,NULL),(15,'Tỉnh Sơn La-Huyện Phù Yên','2019-05-05','lamthanh1451999@gmail.com','Le Thuy Thuy Trang','2019-05-03','ATM','0898489101','Canceled',45000,NULL),(27,'Tỉnh Đồng Tháp-Huyện Châu Thành','2019-05-06','nlthanhititiu17038@gmail.com','Nguyễn Lâm Thành','2019-05-04','ATM','0964774964','Waiting',11774,NULL),(28,'Tỉnh Bắc Giang-Huyện Yên DũngMore address detail area, more detail','2019-05-06','nlthanhititiu17038@gmail.com','Lê Thụy Thùy Trang','2019-05-04','MasterCart/Visa','0964774964','Waiting',8000,NULL),(29,'87-877-Số 60, Ấp Phú Hưng, Xã Phú Hựu','2019-05-06','nlthanhititiu17038@gmail.com','Nguyễn Lâm Thành','2019-05-04','ATM','0898489101','Waiting',15799,3),(30,'Tỉnh Lai Châu-Huyện Tam Đường-More detail, số nhà','2019-05-06','nlthanhititiu17038@gmail.com','Phạm Càng Long','2019-05-04','MasterCart/Visa','0964774964','Canceled',60000,3),(31,'Tỉnh Lạng Sơn-Huyện Văn Quan-More detail','2019-05-06','nlthanhititiu17038@gmail.com','Nguyen Lam Thanh','2019-05-04','MasterCart/Visa','0964774964','Waiting',2397,NULL),(32,'Tá»nh ThÃ¡i NguyÃªn-ThÃ nh phá» ThÃ¡i NguyÃªn-mORE HEARE','2019-05-06','nlthanhititiu17038@gmail.com','Nguyen Lam Thanh','2019-05-04','MasterCart/Visa','0964774964','Canceled',4000,NULL),(33,'Tỉnh Quảng Ninh-Thành phố Cẩm Phả-Số 60, Ấp Phú Hưng, Xã Phú Hựu','2019-05-08','nlthanhititiu17038@gmail.com','Test Last Time Time','2019-05-06','MasterCart/Visa','0964774964','Waiting',30000,3),(34,'Tỉnh Hoà Bình-Huyện Lương Sơn-Thông tin thêm','2019-05-08','nlthanhititiu17038@gmail.com','Test mua hang Khong dang nhap','2019-05-06','MasterCart/Visa','0964774964','Waiting',180000,NULL),(35,'Tỉnh Bắc Giang-Huyện Lạng Giang-sdfuysgfusfdsf','2019-05-09','nhthau99@gmail.com','Thao Dien','2019-05-07','MasterCart/Visa','0964774964','Waiting',2000,NULL);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billdetail`
--

DROP TABLE IF EXISTS `billdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `billdetail` (
  `bill_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`,`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billdetail`
--

LOCK TABLES `billdetail` WRITE;
/*!40000 ALTER TABLE `billdetail` DISABLE KEYS */;
INSERT INTO `billdetail` VALUES (3,22,0,0),(4,22,0,0),(5,22,0,0),(6,22,0,0),(7,22,3,0),(8,37,0,0),(9,46,0,0),(10,1,1,0),(10,3,1,0),(10,6,1,0),(10,7,1,0),(11,12,1,0),(11,13,3,0),(11,32,1,0),(11,35,3,0),(11,36,2,0),(11,37,1,0),(11,49,2,0),(11,52,11,0),(12,36,6,0),(13,48,0,0),(15,46,3,0),(16,18,13,0),(17,18,13,0),(18,18,13,0),(19,18,13,0),(20,18,13,0),(21,18,13,0),(22,18,13,0),(23,18,13,0),(24,18,13,0),(25,18,13,0),(26,18,13,0),(26,47,0,0),(27,16,6,0),(27,38,4,0),(28,35,4,8000),(29,15,1,799),(29,48,1,15000),(30,9,4,60000),(31,15,3,2397),(32,28,2,4000),(33,18,2,30000),(34,46,11,165000),(34,53,1,15000),(35,35,1,2000);
/*!40000 ALTER TABLE `billdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cart` (
  `pro_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`pro_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,1,50),(1,2,50),(1,3,1),(1,7,50),(2,1,50),(2,2,50),(2,7,50),(3,1,50),(3,2,50),(3,7,50),(4,1,50),(4,2,50),(4,3,1),(4,7,50),(5,1,50),(5,2,50),(5,7,50),(7,3,1),(8,3,1),(9,3,1),(13,3,1),(14,3,2),(18,27,3),(37,0,2),(46,0,3),(48,3,10),(53,0,1);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Books','activated'),(2,'Smartphone','activated'),(3,'Fashion','blocked'),(4,'Home','blocked'),(8,'Test2','activated');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(130) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Thành phố Hà Nội'),(2,'Tỉnh Hà Giang'),(4,'Tỉnh Cao Bằng'),(6,'Tỉnh Bắc Kạn'),(8,'Tỉnh Tuyên Quang'),(10,'Tỉnh Lào Cai'),(11,'Tỉnh Điện Biên'),(12,'Tỉnh Lai Châu'),(14,'Tỉnh Sơn La'),(15,'Tỉnh Yên Bái'),(17,'Tỉnh Hoà Bình'),(19,'Tỉnh Thái Nguyên'),(20,'Tỉnh Lạng Sơn'),(22,'Tỉnh Quảng Ninh'),(24,'Tỉnh Bắc Giang'),(25,'Tỉnh Phú Thọ'),(26,'Tỉnh Vĩnh Phúc'),(27,'Tỉnh Bắc Ninh'),(30,'Tỉnh Hải Dương'),(31,'Thành phố Hải Phòng'),(33,'Tỉnh Hưng Yên'),(34,'Tỉnh Thái Bình'),(35,'Tỉnh Hà Nam'),(36,'Tỉnh Nam Định'),(37,'Tỉnh Ninh Bình'),(38,'Tỉnh Thanh Hóa'),(40,'Tỉnh Nghệ An'),(42,'Tỉnh Hà Tĩnh'),(44,'Tỉnh Quảng Bình'),(45,'Tỉnh Quảng Trị'),(46,'Tỉnh Thừa Thiên Huế'),(48,'Thành phố Đà Nẵng'),(49,'Tỉnh Quảng Nam'),(51,'Tỉnh Quảng Ngãi'),(52,'Tỉnh Bình Định'),(54,'Tỉnh Phú Yên'),(56,'Tỉnh Khánh Hòa'),(58,'Tỉnh Ninh Thuận'),(60,'Tỉnh Bình Thuận'),(62,'Tỉnh Kon Tum'),(64,'Tỉnh Gia Lai'),(66,'Tỉnh Đắk Lắk'),(67,'Tỉnh Đắk Nông'),(68,'Tỉnh Lâm Đồng'),(70,'Tỉnh Bình Phước'),(72,'Tỉnh Tây Ninh'),(74,'Tỉnh Bình Dương'),(75,'Tỉnh Đồng Nai'),(77,'Tỉnh Bà Rịa - Vũng Tàu'),(79,'Thành phố Hồ Chí Minh'),(80,'Tỉnh Long An'),(82,'Tỉnh Tiền Giang'),(83,'Tỉnh Bến Tre'),(84,'Tỉnh Trà Vinh'),(86,'Tỉnh Vĩnh Long'),(87,'Tỉnh Đồng Tháp'),(89,'Tỉnh An Giang'),(91,'Tỉnh Kiên Giang'),(92,'Thành phố Cần Thơ'),(93,'Tỉnh Hậu Giang'),(94,'Tỉnh Sóc Trăng'),(95,'Tỉnh Bạc Liêu'),(96,'Tỉnh Cà Mau');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_84i2517bdq06krr4qq0j4jwd3` (`city_id`),
  CONSTRAINT `FK_84i2517bdq06krr4qq0j4jwd3` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=974 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district`
--

LOCK TABLES `district` WRITE;
/*!40000 ALTER TABLE `district` DISABLE KEYS */;
INSERT INTO `district` VALUES (1,'Quận Ba Đình',1),(2,'Quận Hoàn Kiếm',1),(3,'Quận Tây Hồ',1),(4,'Quận Long Biên',1),(5,'Quận Cầu Giấy',1),(6,'Quận Đống Đa',1),(7,'Quận Hai Bà Trưng',1),(8,'Quận Hoàng Mai',1),(9,'Quận Thanh Xuân',1),(16,'Huyện Sóc Sơn',1),(17,'Huyện Đông Anh',1),(18,'Huyện Gia Lâm',1),(19,'Quận Nam Từ Liêm',1),(20,'Huyện Thanh Trì',1),(21,'Quận Bắc Từ Liêm',1),(24,'Thành phố Hà Giang',2),(26,'Huyện Đồng Văn',2),(27,'Huyện Mèo Vạc',2),(28,'Huyện Yên Minh',2),(29,'Huyện Quản Bạ',2),(30,'Huyện Vị Xuyên',2),(31,'Huyện Bắc Mê',2),(32,'Huyện Hoàng Su Phì',2),(33,'Huyện Xín Mần',2),(34,'Huyện Bắc Quang',2),(35,'Huyện Quang Bình',2),(40,'Thành phố Cao Bằng',4),(42,'Huyện Bảo Lâm',4),(43,'Huyện Bảo Lạc',4),(44,'Huyện Thông Nông',4),(45,'Huyện Hà Quảng',4),(46,'Huyện Trà Lĩnh',4),(47,'Huyện Trùng Khánh',4),(48,'Huyện Hạ Lang',4),(49,'Huyện Quảng Uyên',4),(50,'Huyện Phục Hoà',4),(51,'Huyện Hoà An',4),(52,'Huyện Nguyên Bình',4),(53,'Huyện Thạch An',4),(58,'Thành Phố Bắc Kạn',6),(60,'Huyện Pác Nặm',6),(61,'Huyện Ba Bể',6),(62,'Huyện Ngân Sơn',6),(63,'Huyện Bạch Thông',6),(64,'Huyện Chợ Đồn',6),(65,'Huyện Chợ Mới',6),(66,'Huyện Na Rì',6),(70,'Thành phố Tuyên Quang',8),(71,'Huyện Lâm Bình',8),(72,'Huyện Nà Hang',8),(73,'Huyện Chiêm Hóa',8),(74,'Huyện Hàm Yên',8),(75,'Huyện Yên Sơn',8),(76,'Huyện Sơn Dương',8),(80,'Thành phố Lào Cai',10),(82,'Huyện Bát Xát',10),(83,'Huyện Mường Khương',10),(84,'Huyện Si Ma Cai',10),(85,'Huyện Bắc Hà',10),(86,'Huyện Bảo Thắng',10),(87,'Huyện Bảo Yên',10),(88,'Huyện Sa Pa',10),(89,'Huyện Văn Bàn',10),(94,'Thành phố Điện Biên Phủ',11),(95,'Thị Xã Mường Lay',11),(96,'Huyện Mường Nhé',11),(97,'Huyện Mường Chà',11),(98,'Huyện Tủa Chùa',11),(99,'Huyện Tuần Giáo',11),(100,'Huyện Điện Biên',11),(101,'Huyện Điện Biên Đông',11),(102,'Huyện Mường Ảng',11),(103,'Huyện Nậm Pồ',11),(105,'Thành phố Lai Châu',12),(106,'Huyện Tam Đường',12),(107,'Huyện Mường Tè',12),(108,'Huyện Sìn Hồ',12),(109,'Huyện Phong Thổ',12),(110,'Huyện Than Uyên',12),(111,'Huyện Tân Uyên',12),(112,'Huyện Nậm Nhùn',12),(116,'Thành phố Sơn La',14),(118,'Huyện Quỳnh Nhai',14),(119,'Huyện Thuận Châu',14),(120,'Huyện Mường La',14),(121,'Huyện Bắc Yên',14),(122,'Huyện Phù Yên',14),(123,'Huyện Mộc Châu',14),(124,'Huyện Yên Châu',14),(125,'Huyện Mai Sơn',14),(126,'Huyện Sông Mã',14),(127,'Huyện Sốp Cộp',14),(128,'Huyện Vân Hồ',14),(132,'Thành phố Yên Bái',15),(133,'Thị xã Nghĩa Lộ',15),(135,'Huyện Lục Yên',15),(136,'Huyện Văn Yên',15),(137,'Huyện Mù Căng Chải',15),(138,'Huyện Trấn Yên',15),(139,'Huyện Trạm Tấu',15),(140,'Huyện Văn Chấn',15),(141,'Huyện Yên Bình',15),(148,'Thành phố Hòa Bình',17),(150,'Huyện Đà Bắc',17),(151,'Huyện Kỳ Sơn',17),(152,'Huyện Lương Sơn',17),(153,'Huyện Kim Bôi',17),(154,'Huyện Cao Phong',17),(155,'Huyện Tân Lạc',17),(156,'Huyện Mai Châu',17),(157,'Huyện Lạc Sơn',17),(158,'Huyện Yên Thủy',17),(159,'Huyện Lạc Thủy',17),(164,'Thành phố Thái Nguyên',19),(165,'Thành phố Sông Công',19),(167,'Huyện Định Hóa',19),(168,'Huyện Phú Lương',19),(169,'Huyện Đồng Hỷ',19),(170,'Huyện Võ Nhai',19),(171,'Huyện Đại Từ',19),(172,'Thị xã Phổ Yên',19),(173,'Huyện Phú Bình',19),(178,'Thành phố Lạng Sơn',20),(180,'Huyện Tràng Định',20),(181,'Huyện Bình Gia',20),(182,'Huyện Văn Lãng',20),(183,'Huyện Cao Lộc',20),(184,'Huyện Văn Quan',20),(185,'Huyện Bắc Sơn',20),(186,'Huyện Hữu Lũng',20),(187,'Huyện Chi Lăng',20),(188,'Huyện Lộc Bình',20),(189,'Huyện Đình Lập',20),(193,'Thành phố Hạ Long',22),(194,'Thành phố Móng Cái',22),(195,'Thành phố Cẩm Phả',22),(196,'Thành phố Uông Bí',22),(198,'Huyện Bình Liêu',22),(199,'Huyện Tiên Yên',22),(200,'Huyện Đầm Hà',22),(201,'Huyện Hải Hà',22),(202,'Huyện Ba Chẽ',22),(203,'Huyện Vân Đồn',22),(204,'Huyện Hoành Bồ',22),(205,'Thị xã Đông Triều',22),(206,'Thị xã Quảng Yên',22),(207,'Huyện Cô Tô',22),(213,'Thành phố Bắc Giang',24),(215,'Huyện Yên Thế',24),(216,'Huyện Tân Yên',24),(217,'Huyện Lạng Giang',24),(218,'Huyện Lục Nam',24),(219,'Huyện Lục Ngạn',24),(220,'Huyện Sơn Động',24),(221,'Huyện Yên Dũng',24),(222,'Huyện Việt Yên',24),(223,'Huyện Hiệp Hòa',24),(227,'Thành phố Việt Trì',25),(228,'Thị xã Phú Thọ',25),(230,'Huyện Đoan Hùng',25),(231,'Huyện Hạ Hoà',25),(232,'Huyện Thanh Ba',25),(233,'Huyện Phù Ninh',25),(234,'Huyện Yên Lập',25),(235,'Huyện Cẩm Khê',25),(236,'Huyện Tam Nông',25),(237,'Huyện Lâm Thao',25),(238,'Huyện Thanh Sơn',25),(239,'Huyện Thanh Thuỷ',25),(240,'Huyện Tân Sơn',25),(243,'Thành phố Vĩnh Yên',26),(244,'Thị xã Phúc Yên',26),(246,'Huyện Lập Thạch',26),(247,'Huyện Tam Dương',26),(248,'Huyện Tam Đảo',26),(249,'Huyện Bình Xuyên',26),(250,'Huyện Mê Linh',1),(251,'Huyện Yên Lạc',26),(252,'Huyện Vĩnh Tường',26),(253,'Huyện Sông Lô',26),(256,'Thành phố Bắc Ninh',27),(258,'Huyện Yên Phong',27),(259,'Huyện Quế Võ',27),(260,'Huyện Tiên Du',27),(261,'Thị xã Từ Sơn',27),(262,'Huyện Thuận Thành',27),(263,'Huyện Gia Bình',27),(264,'Huyện Lương Tài',27),(268,'Quận Hà Đông',1),(269,'Thị xã Sơn Tây',1),(271,'Huyện Ba Vì',1),(272,'Huyện Phúc Thọ',1),(273,'Huyện Đan Phượng',1),(274,'Huyện Hoài Đức',1),(275,'Huyện Quốc Oai',1),(276,'Huyện Thạch Thất',1),(277,'Huyện Chương Mỹ',1),(278,'Huyện Thanh Oai',1),(279,'Huyện Thường Tín',1),(280,'Huyện Phú Xuyên',1),(281,'Huyện Ứng Hòa',1),(282,'Huyện Mỹ Đức',1),(288,'Thành phố Hải Dương',30),(290,'Thị xã Chí Linh',30),(291,'Huyện Nam Sách',30),(292,'Huyện Kinh Môn',30),(293,'Huyện Kim Thành',30),(294,'Huyện Thanh Hà',30),(295,'Huyện Cẩm Giàng',30),(296,'Huyện Bình Giang',30),(297,'Huyện Gia Lộc',30),(298,'Huyện Tứ Kỳ',30),(299,'Huyện Ninh Giang',30),(300,'Huyện Thanh Miện',30),(303,'Quận Hồng Bàng',31),(304,'Quận Ngô Quyền',31),(305,'Quận Lê Chân',31),(306,'Quận Hải An',31),(307,'Quận Kiến An',31),(308,'Quận Đồ Sơn',31),(309,'Quận Dương Kinh',31),(311,'Huyện Thuỷ Nguyên',31),(312,'Huyện An Dương',31),(313,'Huyện An Lão',31),(314,'Huyện Kiến Thuỵ',31),(315,'Huyện Tiên Lãng',31),(316,'Huyện Vĩnh Bảo',31),(317,'Huyện Cát Hải',31),(318,'Huyện Bạch Long Vĩ',31),(323,'Thành phố Hưng Yên',33),(325,'Huyện Văn Lâm',33),(326,'Huyện Văn Giang',33),(327,'Huyện Yên Mỹ',33),(328,'Huyện Mỹ Hào',33),(329,'Huyện Ân Thi',33),(330,'Huyện Khoái Châu',33),(331,'Huyện Kim Động',33),(332,'Huyện Tiên Lữ',33),(333,'Huyện Phù Cừ',33),(336,'Thành phố Thái Bình',34),(338,'Huyện Quỳnh Phụ',34),(339,'Huyện Hưng Hà',34),(340,'Huyện Đông Hưng',34),(341,'Huyện Thái Thụy',34),(342,'Huyện Tiền Hải',34),(343,'Huyện Kiến Xương',34),(344,'Huyện Vũ Thư',34),(347,'Thành phố Phủ Lý',35),(349,'Huyện Duy Tiên',35),(350,'Huyện Kim Bảng',35),(351,'Huyện Thanh Liêm',35),(352,'Huyện Bình Lục',35),(353,'Huyện Lý Nhân',35),(356,'Thành phố Nam Định',36),(358,'Huyện Mỹ Lộc',36),(359,'Huyện Vụ Bản',36),(360,'Huyện Ý Yên',36),(361,'Huyện Nghĩa Hưng',36),(362,'Huyện Nam Trực',36),(363,'Huyện Trực Ninh',36),(364,'Huyện Xuân Trường',36),(365,'Huyện Giao Thủy',36),(366,'Huyện Hải Hậu',36),(369,'Thành phố Ninh Bình',37),(370,'Thành phố Tam Điệp',37),(372,'Huyện Nho Quan',37),(373,'Huyện Gia Viễn',37),(374,'Huyện Hoa Lư',37),(375,'Huyện Yên Khánh',37),(376,'Huyện Kim Sơn',37),(377,'Huyện Yên Mô',37),(380,'Thành phố Thanh Hóa',38),(381,'Thị xã Bỉm Sơn',38),(382,'Thị xã Sầm Sơn',38),(384,'Huyện Mường Lát',38),(385,'Huyện Quan Hóa',38),(386,'Huyện Bá Thước',38),(387,'Huyện Quan Sơn',38),(388,'Huyện Lang Chánh',38),(389,'Huyện Ngọc Lặc',38),(390,'Huyện Cẩm Thủy',38),(391,'Huyện Thạch Thành',38),(392,'Huyện Hà Trung',38),(393,'Huyện Vĩnh Lộc',38),(394,'Huyện Yên Định',38),(395,'Huyện Thọ Xuân',38),(396,'Huyện Thường Xuân',38),(397,'Huyện Triệu Sơn',38),(398,'Huyện Thiệu Hóa',38),(399,'Huyện Hoằng Hóa',38),(400,'Huyện Hậu Lộc',38),(401,'Huyện Nga Sơn',38),(402,'Huyện Như Xuân',38),(403,'Huyện Như Thanh',38),(404,'Huyện Nông Cống',38),(405,'Huyện Đông Sơn',38),(406,'Huyện Quảng Xương',38),(407,'Huyện Tĩnh Gia',38),(412,'Thành phố Vinh',40),(413,'Thị xã Cửa Lò',40),(414,'Thị xã Thái Hoà',40),(415,'Huyện Quế Phong',40),(416,'Huyện Quỳ Châu',40),(417,'Huyện Kỳ Sơn',40),(418,'Huyện Tương Dương',40),(419,'Huyện Nghĩa Đàn',40),(420,'Huyện Quỳ Hợp',40),(421,'Huyện Quỳnh Lưu',40),(422,'Huyện Con Cuông',40),(423,'Huyện Tân Kỳ',40),(424,'Huyện Anh Sơn',40),(425,'Huyện Diễn Châu',40),(426,'Huyện Yên Thành',40),(427,'Huyện Đô Lương',40),(428,'Huyện Thanh Chương',40),(429,'Huyện Nghi Lộc',40),(430,'Huyện Nam Đàn',40),(431,'Huyện Hưng Nguyên',40),(432,'Thị xã Hoàng Mai',40),(436,'Thành phố Hà Tĩnh',42),(437,'Thị xã Hồng Lĩnh',42),(439,'Huyện Hương Sơn',42),(440,'Huyện Đức Thọ',42),(441,'Huyện Vũ Quang',42),(442,'Huyện Nghi Xuân',42),(443,'Huyện Can Lộc',42),(444,'Huyện Hương Khê',42),(445,'Huyện Thạch Hà',42),(446,'Huyện Cẩm Xuyên',42),(447,'Huyện Kỳ Anh',42),(448,'Huyện Lộc Hà',42),(449,'Thị xã Kỳ Anh',42),(450,'Thành Phố Đồng Hới',44),(452,'Huyện Minh Hóa',44),(453,'Huyện Tuyên Hóa',44),(454,'Huyện Quảng Trạch',44),(455,'Huyện Bố Trạch',44),(456,'Huyện Quảng Ninh',44),(457,'Huyện Lệ Thủy',44),(458,'Thị xã Ba Đồn',44),(461,'Thành phố Đông Hà',45),(462,'Thị xã Quảng Trị',45),(464,'Huyện Vĩnh Linh',45),(465,'Huyện Hướng Hóa',45),(466,'Huyện Gio Linh',45),(467,'Huyện Đa Krông',45),(468,'Huyện Cam Lộ',45),(469,'Huyện Triệu Phong',45),(470,'Huyện Hải Lăng',45),(471,'Huyện Cồn Cỏ',45),(474,'Thành phố Huế',46),(476,'Huyện Phong Điền',46),(477,'Huyện Quảng Điền',46),(478,'Huyện Phú Vang',46),(479,'Thị xã Hương Thủy',46),(480,'Thị xã Hương Trà',46),(481,'Huyện A Lưới',46),(482,'Huyện Phú Lộc',46),(483,'Huyện Nam Đông',46),(490,'Quận Liên Chiểu',48),(491,'Quận Thanh Khê',48),(492,'Quận Hải Châu',48),(493,'Quận Sơn Trà',48),(494,'Quận Ngũ Hành Sơn',48),(495,'Quận Cẩm Lệ',48),(497,'Huyện Hòa Vang',48),(498,'Huyện Hoàng Sa',48),(502,'Thành phố Tam Kỳ',49),(503,'Thành phố Hội An',49),(504,'Huyện Tây Giang',49),(505,'Huyện Đông Giang',49),(506,'Huyện Đại Lộc',49),(507,'Thị xã Điện Bàn',49),(508,'Huyện Duy Xuyên',49),(509,'Huyện Quế Sơn',49),(510,'Huyện Nam Giang',49),(511,'Huyện Phước Sơn',49),(512,'Huyện Hiệp Đức',49),(513,'Huyện Thăng Bình',49),(514,'Huyện Tiên Phước',49),(515,'Huyện Bắc Trà My',49),(516,'Huyện Nam Trà My',49),(517,'Huyện Núi Thành',49),(518,'Huyện Phú Ninh',49),(519,'Huyện Nông Sơn',49),(522,'Thành phố Quảng Ngãi',51),(524,'Huyện Bình Sơn',51),(525,'Huyện Trà Bồng',51),(526,'Huyện Tây Trà',51),(527,'Huyện Sơn Tịnh',51),(528,'Huyện Tư Nghĩa',51),(529,'Huyện Sơn Hà',51),(530,'Huyện Sơn Tây',51),(531,'Huyện Minh Long',51),(532,'Huyện Nghĩa Hành',51),(533,'Huyện Mộ Đức',51),(534,'Huyện Đức Phổ',51),(535,'Huyện Ba Tơ',51),(536,'Huyện Lý Sơn',51),(540,'Thành phố Qui Nhơn',52),(542,'Huyện An Lão',52),(543,'Huyện Hoài Nhơn',52),(544,'Huyện Hoài Ân',52),(545,'Huyện Phù Mỹ',52),(546,'Huyện Vĩnh Thạnh',52),(547,'Huyện Tây Sơn',52),(548,'Huyện Phù Cát',52),(549,'Thị xã An Nhơn',52),(550,'Huyện Tuy Phước',52),(551,'Huyện Vân Canh',52),(555,'Thành phố Tuy Hoà',54),(557,'Thị xã Sông Cầu',54),(558,'Huyện Đồng Xuân',54),(559,'Huyện Tuy An',54),(560,'Huyện Sơn Hòa',54),(561,'Huyện Sông Hinh',54),(562,'Huyện Tây Hoà',54),(563,'Huyện Phú Hoà',54),(564,'Huyện Đông Hòa',54),(568,'Thành phố Nha Trang',56),(569,'Thành phố Cam Ranh',56),(570,'Huyện Cam Lâm',56),(571,'Huyện Vạn Ninh',56),(572,'Thị xã Ninh Hòa',56),(573,'Huyện Khánh Vĩnh',56),(574,'Huyện Diên Khánh',56),(575,'Huyện Khánh Sơn',56),(576,'Huyện Trường Sa',56),(582,'Thành phố Phan Rang-Tháp Chàm',58),(584,'Huyện Bác Ái',58),(585,'Huyện Ninh Sơn',58),(586,'Huyện Ninh Hải',58),(587,'Huyện Ninh Phước',58),(588,'Huyện Thuận Bắc',58),(589,'Huyện Thuận Nam',58),(593,'Thành phố Phan Thiết',60),(594,'Thị xã La Gi',60),(595,'Huyện Tuy Phong',60),(596,'Huyện Bắc Bình',60),(597,'Huyện Hàm Thuận Bắc',60),(598,'Huyện Hàm Thuận Nam',60),(599,'Huyện Tánh Linh',60),(600,'Huyện Đức Linh',60),(601,'Huyện Hàm Tân',60),(602,'Huyện Phú Quí',60),(608,'Thành phố Kon Tum',62),(610,'Huyện Đắk Glei',62),(611,'Huyện Ngọc Hồi',62),(612,'Huyện Đắk Tô',62),(613,'Huyện Kon Plông',62),(614,'Huyện Kon Rẫy',62),(615,'Huyện Đắk Hà',62),(616,'Huyện Sa Thầy',62),(617,'Huyện Tu Mơ Rông',62),(618,'Huyện Ia H\' Drai',62),(622,'Thành phố Pleiku',64),(623,'Thị xã An Khê',64),(624,'Thị xã Ayun Pa',64),(625,'Huyện KBang',64),(626,'Huyện Đăk Đoa',64),(627,'Huyện Chư Păh',64),(628,'Huyện Ia Grai',64),(629,'Huyện Mang Yang',64),(630,'Huyện Kông Chro',64),(631,'Huyện Đức Cơ',64),(632,'Huyện Chư Prông',64),(633,'Huyện Chư Sê',64),(634,'Huyện Đăk Pơ',64),(635,'Huyện Ia Pa',64),(637,'Huyện Krông Pa',64),(638,'Huyện Phú Thiện',64),(639,'Huyện Chư Pưh',64),(643,'Thành phố Buôn Ma Thuột',66),(644,'Thị Xã Buôn Hồ',66),(645,'Huyện Ea H\'leo',66),(646,'Huyện Ea Súp',66),(647,'Huyện Buôn Đôn',66),(648,'Huyện Cư M\'gar',66),(649,'Huyện Krông Búk',66),(650,'Huyện Krông Năng',66),(651,'Huyện Ea Kar',66),(652,'Huyện M\'Đrắk',66),(653,'Huyện Krông Bông',66),(654,'Huyện Krông Pắc',66),(655,'Huyện Krông A Na',66),(656,'Huyện Lắk',66),(657,'Huyện Cư Kuin',66),(660,'Thị xã Gia Nghĩa',67),(661,'Huyện Đăk Glong',67),(662,'Huyện Cư Jút',67),(663,'Huyện Đắk Mil',67),(664,'Huyện Krông Nô',67),(665,'Huyện Đắk Song',67),(666,'Huyện Đắk R\'Lấp',67),(667,'Huyện Tuy Đức',67),(672,'Thành phố Đà Lạt',68),(673,'Thành phố Bảo Lộc',68),(674,'Huyện Đam Rông',68),(675,'Huyện Lạc Dương',68),(676,'Huyện Lâm Hà',68),(677,'Huyện Đơn Dương',68),(678,'Huyện Đức Trọng',68),(679,'Huyện Di Linh',68),(680,'Huyện Bảo Lâm',68),(681,'Huyện Đạ Huoai',68),(682,'Huyện Đạ Tẻh',68),(683,'Huyện Cát Tiên',68),(688,'Thị xã Phước Long',70),(689,'Thị xã Đồng Xoài',70),(690,'Thị xã Bình Long',70),(691,'Huyện Bù Gia Mập',70),(692,'Huyện Lộc Ninh',70),(693,'Huyện Bù Đốp',70),(694,'Huyện Hớn Quản',70),(695,'Huyện Đồng Phú',70),(696,'Huyện Bù Đăng',70),(697,'Huyện Chơn Thành',70),(698,'Huyện Phú Riềng',70),(703,'Thành phố Tây Ninh',72),(705,'Huyện Tân Biên',72),(706,'Huyện Tân Châu',72),(707,'Huyện Dương Minh Châu',72),(708,'Huyện Châu Thành',72),(709,'Huyện Hòa Thành',72),(710,'Huyện Gò Dầu',72),(711,'Huyện Bến Cầu',72),(712,'Huyện Trảng Bàng',72),(718,'Thành phố Thủ Dầu Một',74),(719,'Huyện Bàu Bàng',74),(720,'Huyện Dầu Tiếng',74),(721,'Thị xã Bến Cát',74),(722,'Huyện Phú Giáo',74),(723,'Thị xã Tân Uyên',74),(724,'Thị xã Dĩ An',74),(725,'Thị xã Thuận An',74),(726,'Huyện Bắc Tân Uyên',74),(731,'Thành phố Biên Hòa',75),(732,'Thị xã Long Khánh',75),(734,'Huyện Tân Phú',75),(735,'Huyện Vĩnh Cửu',75),(736,'Huyện Định Quán',75),(737,'Huyện Trảng Bom',75),(738,'Huyện Thống Nhất',75),(739,'Huyện Cẩm Mỹ',75),(740,'Huyện Long Thành',75),(741,'Huyện Xuân Lộc',75),(742,'Huyện Nhơn Trạch',75),(747,'Thành phố Vũng Tàu',77),(748,'Thành phố Bà Rịa',77),(750,'Huyện Châu Đức',77),(751,'Huyện Xuyên Mộc',77),(752,'Huyện Long Điền',77),(753,'Huyện Đất Đỏ',77),(754,'Huyện Tân Thành',77),(755,'Huyện Côn Đảo',77),(760,'Quận 1',79),(761,'Quận 12',79),(762,'Quận Thủ Đức',79),(763,'Quận 9',79),(764,'Quận Gò Vấp',79),(765,'Quận Bình Thạnh',79),(766,'Quận Tân Bình',79),(767,'Quận Tân Phú',79),(768,'Quận Phú Nhuận',79),(769,'Quận 2',79),(770,'Quận 3',79),(771,'Quận 10',79),(772,'Quận 11',79),(773,'Quận 4',79),(774,'Quận 5',79),(775,'Quận 6',79),(776,'Quận 8',79),(777,'Quận Bình Tân',79),(778,'Quận 7',79),(783,'Huyện Củ Chi',79),(784,'Huyện Hóc Môn',79),(785,'Huyện Bình Chánh',79),(786,'Huyện Nhà Bè',79),(787,'Huyện Cần Giờ',79),(794,'Thành phố Tân An',80),(795,'Thị xã Kiến Tường',80),(796,'Huyện Tân Hưng',80),(797,'Huyện Vĩnh Hưng',80),(798,'Huyện Mộc Hóa',80),(799,'Huyện Tân Thạnh',80),(800,'Huyện Thạnh Hóa',80),(801,'Huyện Đức Huệ',80),(802,'Huyện Đức Hòa',80),(803,'Huyện Bến Lức',80),(804,'Huyện Thủ Thừa',80),(805,'Huyện Tân Trụ',80),(806,'Huyện Cần Đước',80),(807,'Huyện Cần Giuộc',80),(808,'Huyện Châu Thành',80),(815,'Thành phố Mỹ Tho',82),(816,'Thị xã Gò Công',82),(817,'Thị xã Cai Lậy',82),(818,'Huyện Tân Phước',82),(819,'Huyện Cái Bè',82),(820,'Huyện Cai Lậy',82),(821,'Huyện Châu Thành',82),(822,'Huyện Chợ Gạo',82),(823,'Huyện Gò Công Tây',82),(824,'Huyện Gò Công Đông',82),(825,'Huyện Tân Phú Đông',82),(829,'Thành phố Bến Tre',83),(831,'Huyện Châu Thành',83),(832,'Huyện Chợ Lách',83),(833,'Huyện Mỏ Cày Nam',83),(834,'Huyện Giồng Trôm',83),(835,'Huyện Bình Đại',83),(836,'Huyện Ba Tri',83),(837,'Huyện Thạnh Phú',83),(838,'Huyện Mỏ Cày Bắc',83),(842,'Thành phố Trà Vinh',84),(844,'Huyện Càng Long',84),(845,'Huyện Cầu Kè',84),(846,'Huyện Tiểu Cần',84),(847,'Huyện Châu Thành',84),(848,'Huyện Cầu Ngang',84),(849,'Huyện Trà Cú',84),(850,'Huyện Duyên Hải',84),(851,'Thị xã Duyên Hải',84),(855,'Thành phố Vĩnh Long',86),(857,'Huyện Long Hồ',86),(858,'Huyện Mang Thít',86),(859,'Huyện  Vũng Liêm',86),(860,'Huyện Tam Bình',86),(861,'Thị xã Bình Minh',86),(862,'Huyện Trà Ôn',86),(863,'Huyện Bình Tân',86),(866,'Thành phố Cao Lãnh',87),(867,'Thành phố Sa Đéc',87),(868,'Thị xã Hồng Ngự',87),(869,'Huyện Tân Hồng',87),(870,'Huyện Hồng Ngự',87),(871,'Huyện Tam Nông',87),(872,'Huyện Tháp Mười',87),(873,'Huyện Cao Lãnh',87),(874,'Huyện Thanh Bình',87),(875,'Huyện Lấp Vò',87),(876,'Huyện Lai Vung',87),(877,'Huyện Châu Thành',87),(883,'Thành phố Long Xuyên',89),(884,'Thành phố Châu Đốc',89),(886,'Huyện An Phú',89),(887,'Thị xã Tân Châu',89),(888,'Huyện Phú Tân',89),(889,'Huyện Châu Phú',89),(890,'Huyện Tịnh Biên',89),(891,'Huyện Tri Tôn',89),(892,'Huyện Châu Thành',89),(893,'Huyện Chợ Mới',89),(894,'Huyện Thoại Sơn',89),(899,'Thành phố Rạch Giá',91),(900,'Thị xã Hà Tiên',91),(902,'Huyện Kiên Lương',91),(903,'Huyện Hòn Đất',91),(904,'Huyện Tân Hiệp',91),(905,'Huyện Châu Thành',91),(906,'Huyện Giồng Riềng',91),(907,'Huyện Gò Quao',91),(908,'Huyện An Biên',91),(909,'Huyện An Minh',91),(910,'Huyện Vĩnh Thuận',91),(911,'Huyện Phú Quốc',91),(912,'Huyện Kiên Hải',91),(913,'Huyện U Minh Thượng',91),(914,'Huyện Giang Thành',91),(916,'Quận Ninh Kiều',92),(917,'Quận Ô Môn',92),(918,'Quận Bình Thuỷ',92),(919,'Quận Cái Răng',92),(923,'Quận Thốt Nốt',92),(924,'Huyện Vĩnh Thạnh',92),(925,'Huyện Cờ Đỏ',92),(926,'Huyện Phong Điền',92),(927,'Huyện Thới Lai',92),(930,'Thành phố Vị Thanh',93),(931,'Thị xã Ngã Bảy',93),(932,'Huyện Châu Thành A',93),(933,'Huyện Châu Thành',93),(934,'Huyện Phụng Hiệp',93),(935,'Huyện Vị Thuỷ',93),(936,'Huyện Long Mỹ',93),(937,'Thị xã Long Mỹ',93),(941,'Thành phố Sóc Trăng',94),(942,'Huyện Châu Thành',94),(943,'Huyện Kế Sách',94),(944,'Huyện Mỹ Tú',94),(945,'Huyện Cù Lao Dung',94),(946,'Huyện Long Phú',94),(947,'Huyện Mỹ Xuyên',94),(948,'Thị xã Ngã Năm',94),(949,'Huyện Thạnh Trị',94),(950,'Thị xã Vĩnh Châu',94),(951,'Huyện Trần Đề',94),(954,'Thành phố Bạc Liêu',95),(956,'Huyện Hồng Dân',95),(957,'Huyện Phước Long',95),(958,'Huyện Vĩnh Lợi',95),(959,'Thị xã Giá Rai',95),(960,'Huyện Đông Hải',95),(961,'Huyện Hoà Bình',95),(964,'Thành phố Cà Mau',96),(966,'Huyện U Minh',96),(967,'Huyện Thới Bình',96),(968,'Huyện Trần Văn Thời',96),(969,'Huyện Cái Nước',96),(970,'Huyện Đầm Dơi',96),(971,'Huyện Năm Căn',96),(972,'Huyện Phú Tân',96),(973,'Huyện Ngọc Hiển',96);
/*!40000 ALTER TABLE `district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `pro_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nubhm59otq1cj36brot3kce6i` (`pro_id`),
  CONSTRAINT `FK_nubhm59otq1cj36brot3kce6i` FOREIGN KEY (`pro_id`) REFERENCES `product` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'/resources/image/Books/The Iliad.jpg',1),(2,'/resources/image/Books/The Iliad1.jpg',1),(3,'/resources/image/Books/The Iliad2.jpg',1),(4,'/resources/image/Books/Resurrection.jpg',3),(5,'/resources/image/Books/Resurrection1.jpg',3),(6,'/resources/image/Books/Resurrection2.jpg',3),(7,'/resources/image/Books/Catch-22.jpg',4),(8,'/resources/image/Books/Catch-22-1.jpg',4),(9,'/resources/image/Books/Catch-22-2.jpg',4),(10,'/resources/image/Books/Catch-22-2.jpg',4),(11,'/resources/image/Books/OneHundredYearsOfSolitude.jpg',5),(12,'/resources/image/Books/OneHundredYearsOfSolitude1.jpg',5),(13,'/resources/image/Books/OneHundredYearsOfSolitude2.jpg',5),(14,'/resources/image/Books/Tender is the Night.jpg',6),(15,'/resources/image/Books/Tender is the Night1.jpg',6),(16,'/resources/image/Books/Tender is the Night1.jpg',6),(17,'/resources/image/Books/Wuthering Heights.jpg',7),(18,'/resources/image/Books/Wuthering Heights1.jpg',7),(19,'/resources/image/Books/Wuthering Heights1.jpg',7),(20,'/resources/image/Books/Pride and Prejudice.jpg',8),(21,'/resources/image/Books/Pride and Prejudice1.jpg',8),(22,'/resources/image/Books/Pride and Prejudice2.jpg',8),(23,'/resources/image/Books/A Brief History Of Time.jpg',9),(24,'/resources/image/Books/A Brief History Of Time1.jpg',9),(25,'/resources/image/Books/A Brief History Of Time2.jpg',9),(26,'/resources/image/Books/Our Mutual Friend.jpg',10),(27,'/resources/image/Books/Our Mutual Friend1.jpg',10),(28,'/resources/image/Books/Our Mutual Friend2.jpg',10),(29,'/resources/image/Books/Breakfast at Tiffany\'s.jpg',11),(30,'/resources/image/Books/Breakfast at Tiffany\'s1.jpg',11),(31,'/resources/image/Books/Breakfast at Tiffany\'s2.jpg',11),(32,'/resources/image/Books/The Wonderful Story.jpg',12),(33,'/resources/image/Books/The Wonderful Story1.jpg',12),(34,'/resources/image/Books/The Wonderful Story2.jpg',12),(35,'/resources/image/Books/Madame Bovary.jpg',13),(36,'/resources/image/Books/Madame Bovary1.jpg',13),(37,'/resources/image/Books/Madame Bovary2.jpg',13),(38,'/resources/image/Books/The Picture of Dorian Gray.jpg',14),(39,'/resources/image/Books/The Picture of Dorian Gray1.jpg',14),(40,'/resources/image/Books/The Picture of Dorian Gray2.jpg',14),(41,'/resources/image/Phone/googlePixel3.jpg',15),(42,'/resources/image/Phone/googlePixel3-1.jpg',15),(43,'/resources/image/Phone/googlePixel3-2.jpg',15),(44,'/resources/image/Phone/OnePlus6T.jpg',16),(45,'/resources/image/Phone/OnePlus6T-1.jpg',16),(46,'/resources/image/Phone/OnePlus6T-2.jpg',16),(47,'/resources/image/Phone/Resurrection.jpg',17),(48,'/resources/image/Phone/SamsungS10.jpg',17),(49,'/resources/image/Phone/SamsungS10-1.jpg',17),(50,'/resources/image/Phone/SamsungS10-2.jpg',17),(51,'/resources/image/Phone/huaweiMate20pro.jpg',18),(52,'/resources/image/Phone/huaweiMate20pro1.jpg',18),(53,'/resources/image/Phone/huaweiMate20pro2.jpg',18),(54,'/resources/image/Phone/iphoneXS.jpg',19),(55,'/resources/image/Phone/iphoneXS1.jpg',19),(56,'/resources/image/Phone/iphoneXS2.jpg',19),(57,'/resources/image/Phone/SamsungS10.jpg',20),(58,'/resources/image/Phone/SamsungS10-1.jpg',20),(59,'/resources/image/Phone/SamsungS10-2.jpg',20),(60,'/resources/image/Phone/iphoneXSMAX.jpg',21),(61,'/resources/image/Phone/iphoneXSMAX1.jpg',21),(62,'/resources/image/Phone/iphoneXSMAX2.jpg',21),(63,'/resources/image/Phone/SamsungS10E.jpg',22),(64,'/resources/image/Phone/SamsungS10E-1.jpg',22),(65,'/resources/image/Phone/SamsungS10E-2.jpg',22),(66,'/resources/image/Phone/OnePlus6T.jpg',23),(67,'/resources/image/Phone/OnePlus6T-1.jpg',23),(68,'/resources/image/Phone/OnePlus6T-2.jpg',23),(69,'/resources/image/Phone/samsungNote9.jpg',24),(70,'/resources/image/Phone/samsungNote9-1.jpg',24),(71,'/resources/image/Phone/samsungNote9-2.jpg',24),(72,'/resources/image/Phone/iphoneXR.jpg',25),(73,'/resources/image/Phone/iphoneXR-1.jpg',25),(74,'/resources/image/Phone/iphoneXR-2.jpg',25),(75,'/resources/image/Phone/huaweiMate20pro.jpg',26),(76,'/resources/image/Phone/huaweiMate20pro1.jpg',26),(77,'/resources/image/Phone/huaweiMate20pro2.jpg',26),(78,'/resources/image/Phone/SamsungS10.jpg',27),(79,'/resources/image/Phone/SamsungS10-1.jpg',27),(80,'/resources/image/Phone/SamsungS10-2.jpg',27),(81,'/resources/image/Phone/iphoneXS.jpg',28),(82,'/resources/image/Phone/iphoneXS1.jpg',28),(83,'/resources/image/Phone/iphoneXS2.jpg',28),(84,'/resources/image/Phone/googlePixel3XL.jpg',29),(85,'/resources/image/Phone/googlePixel3XL-1.jpg',29),(86,'/resources/image/Phone/googlePixel3XL-2.jpg',29),(87,'/resources/image/Phone/samsungS9plus.jpg',30),(88,'/resources/image/Phone/samsungS9plus1.jpg',30),(89,'/resources/image/Phone/samsungS9plus2.jpg',30),(90,'/resources/image/Phone/iphoneX.jpg',31),(91,'/resources/image/Phone/iphoneX1.jpg',31),(92,'/resources/image/Phone/iphoneX2.jpg',31),(93,'/resources/image/Phone/googlePixel2.jpg',32),(94,'/resources/image/Phone/googlePixel2-1.jpg',32),(95,'/resources/image/Phone/googlePixel2-2.jpg',32),(96,'/resources/image/Phone/LGG7ThinQ.jpg',33),(97,'/resources/image/Phone/LGG7ThinQ1.jpg',33),(98,'/resources/image/Phone/LGG7ThinQ.jpg',33),(99,'/resources/image/Clothes/BoxyRollT.jpg',34),(100,'/resources/image/Clothes/BoxyRollT1.jpg',34),(101,'/resources/image/Clothes/BoxyRollT2.jpg',34),(102,'/resources/image/Clothes/SisterHoodCat.jpg',35),(103,'/resources/image/Clothes/SisterHoodCat1.jpg',35),(104,'/resources/image/Clothes/SisterHoodCat2.jpg',35),(105,'/resources/image/Clothes/HappilyEverAfterTShirt.jpg',36),(106,'/resources/image/Clothes/HappilyEverAfterTShirt1.jpg',36),(107,'/resources/image/Clothes/HappilyEverAfterTShirt2.jpg',36),(108,'/resources/image/Clothes/PicotTrimTshirt.jpg',37),(109,'/resources/image/Clothes/PicotTrimTshirt1.jpg',37),(110,'/resources/image/Clothes/PicotTrimTshirt2.jpg',37),(111,'/resources/image/Clothes/SheerRibbed.jpg',38),(112,'/resources/image/Clothes/SheerRibbed1.jpg',38),(113,'/resources/image/Clothes/SheerRibbed2.jpg',38),(114,'/resources/image/Clothes/AdorableCrossword.jpg',39),(115,'/resources/image/Clothes/AdorableCrossword1.jpg',39),(116,'/resources/image/Clothes/AdorableCrossword2.jpg',39),(117,'/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg',40),(118,'/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg',40),(119,'/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg',40),(120,'/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg',41),(121,'/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg',41),(122,'/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg',41),(123,'/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg',42),(124,'/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg',42),(125,'/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg',42),(126,'/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg',43),(127,'/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg',43),(128,'/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg',43),(129,'/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg',44),(130,'/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg',44),(131,'/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg',44),(132,'/resources/image/Home/texiana.jpg',45),(133,'/resources/image/Home/texiana1.jpg',45),(134,'/resources/image/Home/texiana2.jpg',45),(135,'/resources/image/Home/westneysofa.jpg',46),(136,'/resources/image/Home/westneysofa1.jpg',46),(137,'/resources/image/Home/westneysofa2.jpg',46),(138,'/resources/image/Home/everlatincol.jpg',47),(139,'/resources/image/Home/everlatincol1.jpg',47),(140,'/resources/image/Home/everlatincol2.jpg',47),(141,'/resources/image/Home/plushercoll.jpg',48),(142,'/resources/image/Home/plushercoll1.jpg',48),(143,'/resources/image/Home/plushercoll2.jpg',48),(144,'/resources/image/Home/fitzpatrickcol.jpg',49),(145,'/resources/image/Home/fitzpatrickcol1.jpg',49),(146,'/resources/image/Home/fitzpatrickcol2.jpg',49),(147,'/resources/image/Home/maddoxcoll.jpg',50),(148,'/resources/image/Home/maddoxcoll1.jpg',50),(149,'/resources/image/Home/maddoxcoll2.jpg',50),(150,'/resources/image/Home/terrillcoll.jpg',51),(151,'/resources/image/Home/terrillcoll1.jpg',51),(152,'/resources/image/Home/terrillcoll2.jpg',51),(153,'/resources/image/Home/patrick.jpg',52),(154,'/resources/image/Home/patrick1.jpg',52),(155,'/resources/image/Home/patrick2.jpg',52),(156,'/resources/image/Home/noblecoll.jpg',53),(157,'/resources/image/Home/noblecoll1.jpg',53),(158,'/resources/image/Home/noblecoll2.jpg',53),(159,'/resources/image/Home/tryp.jpg',54),(160,'/resources/image/Home/tryp1.jpg',54),(161,'/resources/image/Home/tryp2.jpg',54),(162,'/resources/image/Home/seger.jpg',55),(163,'/resources/image/Home/seger1.jpg',55),(164,'/resources/image/Home/seger2.jpg',55),(165,'/resources/image/Home/plusher.jpg',56),(166,'/resources/image/Home/plusher1.jpg',56),(167,'/resources/image/Home/plusher2.jpg',56),(168,'/resources/image/Home/maddox.jpg',57),(169,'/resources/image/Home/maddox1.jpg',57),(170,'/resources/image/Home/maddox2.jpg',57),(171,'/resources/image/Home/terill.jpg',58),(172,'/resources/image/Home/terill11.jpg',58),(173,'/resources/image/Home/terill12.jpg',58),(174,'/resources/image/Home/rake.jpg',59),(175,'/resources/image/Home/rake1.jpg',59),(176,'/resources/image/Home/rake2.jpg',59),(190,'/resources/image/Home/b.jpg',59),(199,'/resources/image/Phone/b.jpg',82);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `question` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_falj0akuqldcueq4k5pnu3nn8` (`userId`),
  CONSTRAINT `FK_falj0akuqldcueq4k5pnu3nn8` FOREIGN KEY (`userId`) REFERENCES `account_detail` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (3,'Hỏi Cái Gì HẢ','nlthanhititiu17038@gmail.com','Test','123','read',3),(5,'asdadasd','nlthanhititiu17038@gmail.com','Test','qwwqw','read',3),(6,'adasdad','nlthanhititiu17038@gmail.com','Test','SDASDdasdasd','read',3),(9,'','lamthanh1451999@gmail.com','Nguyen Lam Thanh','Chào Cậu!!!','unread',3);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `buytime` int(11) DEFAULT NULL,
  `pro_content` varchar(255) DEFAULT NULL,
  `pro_cost` int(11) DEFAULT NULL,
  `pro_name` varchar(255) DEFAULT NULL,
  `pro_quantity` int(11) DEFAULT NULL,
  `pro_star` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  KEY `FK_2c6uuvrr1big5igkjok4tha2m` (`cate_id`),
  CONSTRAINT `FK_2c6uuvrr1big5igkjok4tha2m` FOREIGN KEY (`cate_id`) REFERENCES `category` (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,3,'One of the greatest epics in Western literature, THE ILIAD recounts the story of the Trojan wars',15000,'The Iliad',150,5,1,'activated'),(2,2,'Alonso Quixano, a retired country gentleman in his fifties, lives in an unnamed section of La Mancha with his niece and a housekeeper. He has become obsessed with books of chivalry, and believes th... ',15000,'Don Quixote',150,5,1,'blocked'),(3,3,'Resurrection (1899) is the last of Tolstoy\'s major novels. It tells the story of a nobleman\'s attempt to redeem the suffering his youthful philandering inflicted on a peasant girl who ends up a prisoner in Siberia.',15000,'Resurrection ',150,5,1,'blocked'),(4,2,'Set in the closing months of World War II in an American bomber squadron off the coast of Italy ',15000,'Catch-22',150,5,1,'blocked'),(5,2,'Equally tragic, joyful and comical, Gabriel Garcia Marquez\'s masterpiece of magical realism, One Hundred Years of Solitude is a seamless blend of fantasy and reality, translated from the Spanish by Gregory Rabassa in Penguin Modern Classics.',15000,'One Hundred Years of Solitude',150,5,1,'blocked'),(6,2,'Between the First World War and the Wall Street Crash, the French Riviera was the stylish place for wealthy Americans to visit. Among the most fashionable are the Divers, Dick and Nicole who hold court at their villa.',15000,'Tender is the Night',100,5,1,'activated'),(7,2,'Emily Bronte\'s only novel, a work of tremendous and far-reaching influence, the Penguin Classics edition of Wuthering Heights is the definitive edition of the text, edited with an introduction by Pauline Nestor.',15000,'Wuthering Heights',100,5,1,'activated'),(8,1,'These are just some of the questions considered in the internationally acclaimed masterpiece by the world renowned physicist - generally considered to have been one of the world\'s greatest thinkers',15000,'Pride and Prejudice',100,5,1,'activated'),(9,5,'\'No sooner had he made it clear to himself and his friends that she had hardly a good feature in her face, than he began to find it was rendered uncommonly intelligent by the beautiful expression of her dark eyes ...\'',15000,'A Brief History Of Time : From Big Bang To Black Holes',100,5,1,'activated'),(10,1,'Following his father\'s death John Harmon returns to London to claim his inheritance, but he finds he is eligible only if he marries Bella Wilfur. ',15000,'Our Mutual Friend',100,5,1,'activated'),(11,1,'Immortalised by Audrey Hepburn\'s sparkling performance in the 1961 film of the same name, `Breakfast at Tiffany\'s` is Truman Capote\'s timeless portrait of tragicomic cultural icon Holly Golightly.',15000,'Breakfast at Tiffany\'s',100,5,1,'activated'),(12,2,'In the Wonderful Story of Henry Sugar, seven tales of the bizarre and unexpected are told by the grand master of the short story, Roald Dahl.',15000,'The Wonderful Story of Henry Sugar and Six More',100,5,1,'activated'),(13,2,'\'Would this misery go on forever? Was there no escape? And yet she was every bit as good as all those other women who led happy lives!\'',15000,'Madame Bovary : Provincial Manners',100,5,1,'activated'),(14,1,'Part of Penguin\'s beautiful hardback Clothbound Classics series, designed by the award-winning Coralie Bickford-Smith, these delectable and collectible editions are bound in high-quality colourful, tactile cloth with foil stamped into the design. ',15000,'The Picture of Dorian Gray',99,5,1,'activated'),(15,5,'The Pixel 3 and Pixel 3 XL are the best phones you can buy right now, provided you don\'t want an iPhone. The only differences between both models are the larger screen (and notch) and battery on the XL.',799,'Google Pixel 3',100,5,2,'activated'),(16,3,'Although it lacks a headphone jack, wireless charging and waterproofing, the OnePlus 6T is still one of the best phones you can buy right now.',629,'OnePlus 6T',101,5,2,'activated'),(17,1,'The Galaxy S10 Plus is the best Galaxy phone ever with outstanding cameras, build, display and performance. The Exynos version is let down by its merely acceptable battery life.',15000,'Samsung Galaxy S10 Plus',100,5,2,'activated'),(18,14,'It’s a mark of how good Huawei phones have become that there are two in our Top 10. The Mate 20 Pro has a large vibrant OLED screen, particularly great battery life and awesome triple cameras with a slender notch.',15000,'Huawei Mate 20 Pro',95,5,2,'activated'),(19,1,'With prices starting from £499, the Honor View 20\'s closest comparison is the OnePlus 6T, but there\'s a lot more on offer here than a 6T imitation.',15000,'iPhone XS',100,5,2,'activated'),(20,1,'The Samsung Galaxy S10 Plus is the best phone you can get for Android right now, if you\'re just going by specs. Naturally, as the first flagship of 2019.',20000,'Samsung Galaxy S10 Plus',100,5,2,'activated'),(21,1,'The iPhone XS Max is Apple\'s new big iPhone with an expansive 6.5-inch display that can\'t be missed if you\'re looking for the best phone running iOS 12',20000,'iPhone XS Max',100,5,2,'activated'),(22,2,'The Samsung S10e is the littlest (and least pricey) sibling of the S10 line in both size and features',20000,'Samsung Galaxy S10e',100,5,2,'activated'),(23,1,'Following its trend in recent years, OnePlus has released its iterative update in the OnePlus 6T',20000,'OnePlus 6T',100,5,2,'activated'),(24,1,'The Samsung Galaxy Note 9 is the best phone you can buy today if you\'re not one to shy away from its $1,000 starting price for the 128GB version.',20000,'Samsung Galaxy Note 9',100,5,2,'activated'),(25,1,'Apple’s iPhone XR was a little bit late to launch after the iPhone XS and XS Max that launched a bit earlier. But thanks to its lower price point, it makes for a more affordable option than the XS models',20000,'iPhone XR',100,5,2,'activated'),(26,1,'The Huawei Mate 20 Pro is the best phone from the Chinese firm to date, offering up a heady mix of design, power and performance with a few party pieces thrown in too.',20000,'Huawei Mate 20 Pro',100,5,2,'activated'),(27,1,'The bigger S10 Plus is by far Samsung\'s best phone, but the standard S10 backs almost all the same top-end features into a more compact form factor and slightly lower price tag',20000,'Samsung Galaxy S10',100,5,2,'activated'),(28,3,'iPhone XS is a minor, but important upgrade over last year\'s completely redesign iPhone. It\'s noticeably faster and has an improved dual-lens camera to make it a better choice, if you\'re willing to pay the same launch price',2000,'iPhone XS',100,5,2,'activated'),(29,1,'The Google Pixel 3 XL brings higher end internals and a notched screen to the latest iteration of Google’s larger phone.',2000,'Google Pixel 3 XL',100,5,2,'activated'),(30,1,'The Samsung Galaxy S9 Plus is the second best phone you can buy today, and meant for anyone who won\'t use the stylus.',2000,'Samsung Galaxy S9 Plus',100,5,2,'activated'),(31,1,'The iPhone X has been surpassed by the iPhone XS, but not too much has changed since the former launched in late 2017',2000,'iPhone X',100,5,2,'activated'),(32,2,'Google Pixel 2 is the best phone if you\'re looking for a pure Android experience with a big screen, incredible camera and stereo speakers',2000,' Google Pixel 2',100,5,2,'activated'),(33,1,'The LG G7 ThinQ is an impressive little phone from the brand (irritating name aside), bringing with it a strong package and a decent price in many regions.',2000,'LG G7 ThinQ',100,5,2,'activated'),(34,1,'We\'re taking inspiration by menswear this season for a pared-back and cool look',2000,'Boxy Roll T-Shirt',100,5,3,'activated'),(35,7,'Add some cute feline style with this short sleeve t-shirt with cat motif and \'sisterhood\' slogan',2000,'Sisterhood Cat T-Shirt',99,5,3,'activated'),(36,3,'Every girl loves a fairy-tale ending and a slogan tee. This white short sleeve t-shirt with roll back sleeve',2000,'Happily Ever After T-Shirt',100,5,3,'activated'),(37,2,'This short sleeve ribbed t-shirt with picot trim at neck and sleeves is just so classic',2000,'Picot Trim T-Shirt',97,5,3,'activated'),(38,2,'This sheer ribbed t-shirt is perfect for spring summer.Pair with white denim for the perfect colour combination! 58% Viscose, 42% Polyamide. Machine wash.',2000,'Sheer Ribbed T-Shirt',100,5,3,'activated'),(39,1,'This short sleeve t-shirt with adorable crossword style motif is a great addition to any weekend wardrobe',2000,'Adorable Crossword T-Shirt',100,5,3,'activated'),(40,1,'Everlane is known for being the master of basics, and their iconic t-shirts are a must-have in every man\'s dresser',799,'Best Overall T-shirt: EVERLANE Cotton Crew',100,5,3,'activated'),(41,1,'The classic henley style is rendered in a comfortable waffle-knit fabric that makes this affordable Uniqlo style a winning option for guys that ',629,'Best Henley: UNIQLO Waffle-Knit Top',100,5,3,'activated'),(42,1,'James Perse\'s premium t-shirts are super luxurious and will last you a long time. This cotton cashmere jersey tee comes in an easy, relaxed fit and has an ultra-soft soft feel. ',15000,'Best Luxury T-shirt: JAMES PERSE Jersey Tee',100,5,3,'activated'),(43,1,'Not only does this tee wick away moisture and keep you cool and dry during your workouts',15000,'Best Performance T-shirt: HANES Cool Dri T-Shirt (2 Pack)',100,5,3,'activated'),(44,1,'Sometimes you just want an everyday tee that you can lounge around in at home but that also looks good when you want to step out and run errands.',15000,'Best Casual T-shirt: URBAN OUTFITTERS Curved Hem Tee',100,5,3,'activated'),(45,1,'he Texiana glider is an ideal piece that combines a rustic style with that down home comfort feel.',15000,'TEXIANA ',100,5,4,'activated'),(46,13,'Be dazzled by our diverse mission group offering! You can choose the standard sofa, loveseat and chair or a matching chair and a half style.',15000,'WESTNEY SOFA',88,5,4,'activated'),(47,2,'With plush, overstuffed cushions and a biscuit back, the Everlasting dual reclining sofa is the definition of comfort.',15000,'EVERLASTING COL',100,5,4,'activated'),(48,2,'The cozy Plusher is highly regarded as one of the most comfortable pieces of furniture in the Best Home Furnishings line, if not the world.',15000,'PLUSHER COLL.',91,5,4,'activated'),(49,2,'Sometimes you just want an everyday tee that you can lounge around in at home but that also looks good when you want to step out and run errands.',15000,'FITZPATRICK COL ',100,5,4,'activated'),(50,1,'Never worry about sacrificing form for function with this power reclining sofa. With numerous cover options – like fabric and supple leather choices – the Maddox is the answer to your search for the perfect couch.',15000,'MADDOX COLL.',100,5,4,'blocked'),(51,1,'Take a day for yourself and kick back and relax in the luxurious Terrill reclining sofa. With customizable comfort levels.',15000,'TERRILL COLL.',100,5,4,'activated'),(52,2,'Refined and chivalrous, the Patrick will put your comfort before everything else.  This high leg recliner combines the best of both worlds',15000,'PATRICK',100,5,4,'activated'),(53,2,'The Tuscan recliner is rustic and relaxing. This mission style recliner offers the solid, handcrafted look of the mission design philosophy.',15000,'TUSCAN.',99,5,4,'blocked'),(54,1,'Style extends from the straight lines of the square back to the rounded, extra cushioned arms that are highlighted with contrasting cording.',15000,'TRYP',100,5,4,'blocked'),(55,1,'Comfort can be seen before its felt with this ultra pillow arm and pillow back recliner. Not only comfortable, but built with the Best-Max technology.',15000,'SEGER',100,5,4,'blocked'),(56,1,'The Plusher is highly regarded as one of the most comfortable recliners in the Best Home Furnishings line; if not the world.',15000,'PLUSHER',100,5,4,'activated'),(57,1,'This is the perfect chair for grabbing some freshly buttered popcorn, kicking back and watching a movie with the whole family on a Friday night.',15000,'MADDOX.',100,5,4,'activated'),(58,1,'A fresh take on this Best Home Furnishings recliner doesn’t sacrifice style for comfort.',15000,'TERRILL',100,5,4,'activated'),(59,1,'One of the largest recliners in the industry, this Beast® recliner features extra support, extra durability and extra comfort.',15000,'RAKE',100,5,4,'activated'),(82,0,'12212',1212,'1212',1212,0,3,'activated');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_answer`
--

DROP TABLE IF EXISTS `question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a_content` varchar(255) NOT NULL,
  `q_content` varchar(255) NOT NULL,
  `pro_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7tm3k6c9rna9actdf2o8urb8a` (`pro_id`),
  CONSTRAINT `FK_7tm3k6c9rna9actdf2o8urb8a` FOREIGN KEY (`pro_id`) REFERENCES `product` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_answer`
--

LOCK TABLES `question_answer` WRITE;
/*!40000 ALTER TABLE `question_answer` DISABLE KEYS */;
INSERT INTO `question_answer` VALUES (1,'Pin online liên tục được hơn 8 giờ bạn ạ.','Máy pin có trâu không?',1),(2,'Bảo hành trọn đời hư bỏ anh ạ.','Bảo hành trong bao lâu?',1),(3,'Máy chơi được cả Aspha 8 bạn nha.','Máy có chơi được PUBG không vậy?',1),(4,'Có bạn nhé.','Máy có 4G?',1),(5,'Pin online liên tục được hơn 8 giờ bạn ạ.','Máy pin có trâu không?',1),(6,'Bảo hành trọn đời hư bỏ anh ạ.','Bảo hành trong bao lâu?',1),(7,'Máy chơi được cả Aspha 8 bạn nha.','Máy có chơi được PUBG không vậy?',1),(8,'Có bạn nhé.','Máy có 4G?',1),(9,'','M�y n�y online li�n t?c trong bao l�u v???',22),(10,'','Test add question.',52),(11,'','TTTTT',18);
/*!40000 ALTER TABLE `question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_product`
--

DROP TABLE IF EXISTS `review_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `review_product` (
  `reviewID` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pro_star` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pro_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`reviewID`),
  KEY `FK_6gwxs9s933yg256pvrnsp2r1c` (`user_id`),
  KEY `FK_11g82jqcdhuogtrmm1g4o9hay` (`pro_id`),
  CONSTRAINT `FK_11g82jqcdhuogtrmm1g4o9hay` FOREIGN KEY (`pro_id`) REFERENCES `product` (`pro_id`),
  CONSTRAINT `FK_6gwxs9s933yg256pvrnsp2r1c` FOREIGN KEY (`user_id`) REFERENCES `account_detail` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_product`
--

LOCK TABLES `review_product` WRITE;
/*!40000 ALTER TABLE `review_product` DISABLE KEYS */;
INSERT INTO `review_product` VALUES (2,'my review','jsalkj@klvd.kvvk.vk;lv.vkdv;','thach',3,3,15),(3,'Test','nlthanhititiu17038@gmail.com','Nguyen Lam Thanh',2,3,18);
/*!40000 ALTER TABLE `review_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippingaddress`
--

DROP TABLE IF EXISTS `shippingaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `shippingaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mtldw9h8cu34hqawfa1hq89sr` (`userId`),
  CONSTRAINT `FK_mtldw9h8cu34hqawfa1hq89sr` FOREIGN KEY (`userId`) REFERENCES `account_detail` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippingaddress`
--

LOCK TABLES `shippingaddress` WRITE;
/*!40000 ALTER TABLE `shippingaddress` DISABLE KEYS */;
INSERT INTO `shippingaddress` VALUES (2,'Đồng Tháp','Châu Thành','Nguyen Lam Thanh',3);
/*!40000 ALTER TABLE `shippingaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viewed`
--

DROP TABLE IF EXISTS `viewed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `viewed` (
  `user_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viewed`
--

LOCK TABLES `viewed` WRITE;
/*!40000 ALTER TABLE `viewed` DISABLE KEYS */;
INSERT INTO `viewed` VALUES (2,16),(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),(3,10),(3,11),(3,12),(3,13),(3,14),(3,15),(3,16),(3,18),(3,22),(3,35),(3,36),(3,39),(3,45),(3,48),(3,52),(6,4),(17,52),(27,4),(27,18);
/*!40000 ALTER TABLE `viewed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'web1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-11 18:26:03
