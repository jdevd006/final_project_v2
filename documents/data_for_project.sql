﻿insert into category
values(null, 'Books');
insert into category
values(null, 'Smartphone');
insert into category
values(null, 'Fashion');
insert into category
values(null, 'Home');

insert into product
values(null,1, "One of the greatest epics in Western literature, THE ILIAD recounts the story of the Trojan wars", 15000, "The Iliad", 10, 5, 1);
insert into product
values(null,1, "Alonso Quixano, a retired country gentleman in his fifties, lives in an unnamed section of La Mancha with his niece and a housekeeper. He has become obsessed with books of chivalry, and believes th... ", 15000, "Don Quixote", 10, 5, 1);
insert into product
values(null,1, "Resurrection (1899) is the last of Tolstoy's major novels. It tells the story of a nobleman's attempt to redeem the suffering his youthful philandering inflicted on a peasant girl who ends up a prisoner in Siberia.", 15000, "Resurrection ", 10, 5, 1);
insert into product
values(null,1, "Set in the closing months of World War II in an American bomber squadron off the coast of Italy ", 15000, "Catch-22", 10, 5, 1);
insert into product
values(null,1, "Equally tragic, joyful and comical, Gabriel Garcia Marquez's masterpiece of magical realism, One Hundred Years of Solitude is a seamless blend of fantasy and reality, translated from the Spanish by Gregory Rabassa in Penguin Modern Classics.", 15000, "One Hundred Years of Solitude", 10, 5, 1);
insert into product
values(null,1, "Between the First World War and the Wall Street Crash, the French Riviera was the stylish place for wealthy Americans to visit. Among the most fashionable are the Divers, Dick and Nicole who hold court at their villa.", 15000, "Tender is the Night", 10, 5, 1);
insert into product
values(null,1, "Emily Bronte's only novel, a work of tremendous and far-reaching influence, the Penguin Classics edition of Wuthering Heights is the definitive edition of the text, edited with an introduction by Pauline Nestor.", 15000, "Wuthering Heights", 10, 5, 1);
insert into product
values(null,1, "These are just some of the questions considered in the internationally acclaimed masterpiece by the world renowned physicist - generally considered to have been one of the world's greatest thinkers", 15000, "Pride and Prejudice", 10, 5, 1);
insert into product
values(null,1, "'No sooner had he made it clear to himself and his friends that she had hardly a good feature in her face, than he began to find it was rendered uncommonly intelligent by the beautiful expression of her dark eyes ...'", 15000, "A Brief History Of Time : From Big Bang To Black Holes", 10, 5, 1);
insert into product
values(null,1, "Following his father's death John Harmon returns to London to claim his inheritance, but he finds he is eligible only if he marries Bella Wilfur. ", 15000, "Our Mutual Friend", 10, 5, 1);
insert into product
values(null,1, "Immortalised by Audrey Hepburn's sparkling performance in the 1961 film of the same name, `Breakfast at Tiffany's` is Truman Capote's timeless portrait of tragicomic cultural icon Holly Golightly.", 15000, "Breakfast at Tiffany's", 10, 5, 1);
insert into product
values(null,1, "In the Wonderful Story of Henry Sugar, seven tales of the bizarre and unexpected are told by the grand master of the short story, Roald Dahl.", 15000, "The Wonderful Story of Henry Sugar and Six More", 10, 5, 1);
insert into product
values(null,1, "'Would this misery go on forever? Was there no escape? And yet she was every bit as good as all those other women who led happy lives!'", 15000, "Madame Bovary : Provincial Manners", 10, 5, 1);
insert into product
values(null,1, "Part of Penguin's beautiful hardback Clothbound Classics series, designed by the award-winning Coralie Bickford-Smith, these delectable and collectible editions are bound in high-quality colourful, tactile cloth with foil stamped into the design. ", 15000, "The Picture of Dorian Gray", 10, 5, 1);


insert into product
values(null,1, "The Pixel 3 and Pixel 3 XL are the best phones you can buy right now, provided you don't want an iPhone. The only differences between both models are the larger screen (and notch) and battery on the XL.", 799, "Google Pixel 3", 10, 5, 2);
insert into product
values(null,1, "Although it lacks a headphone jack, wireless charging and waterproofing, the OnePlus 6T is still one of the best phones you can buy right now.", 629, "OnePlus 6T", 10, 5, 2);
insert into product
values(null,1, "The Galaxy S10 Plus is the best Galaxy phone ever with outstanding cameras, build, display and performance. The Exynos version is let down by its merely acceptable battery life.", 15000, "Samsung Galaxy S10 Plus", 10, 5, 2);
insert into product
values(null,1, "It’s a mark of how good Huawei phones have become that there are two in our Top 10. The Mate 20 Pro has a large vibrant OLED screen, particularly great battery life and awesome triple cameras with a slender notch.", 15000, "Huawei Mate 20 Pro", 10, 5, 2);
insert into product
values(null,1, "With prices starting from £499, the Honor View 20's closest comparison is the OnePlus 6T, but there's a lot more on offer here than a 6T imitation.", 15000, "iPhone XS", 10, 5, 2);
insert into product
values(null,1, "The Samsung Galaxy S10 Plus is the best phone you can get for Android right now, if you're just going by specs. Naturally, as the first flagship of 2019.", 20000, "Samsung Galaxy S10 Plus", 10, 5, 2);
insert into product
values(null,1, "The iPhone XS Max is Apple's new big iPhone with an expansive 6.5-inch display that can't be missed if you're looking for the best phone running iOS 12", 20000, "iPhone XS Max", 10, 5, 2);
insert into product
values(null,1, "The Samsung S10e is the littlest (and least pricey) sibling of the S10 line in both size and features", 20000, "Samsung Galaxy S10e", 10, 5, 2);
insert into product
values(null,1, "Following its trend in recent years, OnePlus has released its iterative update in the OnePlus 6T", 20000, "OnePlus 6T", 10, 5, 2);
insert into product
values(null,1, "The Samsung Galaxy Note 9 is the best phone you can buy today if you're not one to shy away from its $1,000 starting price for the 128GB version.", 20000, "Samsung Galaxy Note 9", 10, 5, 2);
insert into product
values(null,1, "Apple’s iPhone XR was a little bit late to launch after the iPhone XS and XS Max that launched a bit earlier. But thanks to its lower price point, it makes for a more affordable option than the XS models", 20000, "iPhone XR", 10, 5, 2);
insert into product
values(null,1, "The Huawei Mate 20 Pro is the best phone from the Chinese firm to date, offering up a heady mix of design, power and performance with a few party pieces thrown in too.", 20000, "Huawei Mate 20 Pro", 10, 5, 2);
insert into product
values(null,1, "The bigger S10 Plus is by far Samsung's best phone, but the standard S10 backs almost all the same top-end features into a more compact form factor and slightly lower price tag", 20000, "Samsung Galaxy S10", 10, 5, 2);
insert into product
values(null,1, "iPhone XS is a minor, but important upgrade over last year's completely redesign iPhone. It's noticeably faster and has an improved dual-lens camera to make it a better choice, if you're willing to pay the same launch price", 2000, "iPhone XS", 10, 5, 2);
insert into product
values(null,1, "The Google Pixel 3 XL brings higher end internals and a notched screen to the latest iteration of Google’s larger phone.", 2000, "Google Pixel 3 XL", 10, 5, 2);
insert into product
values(null,1, "The Samsung Galaxy S9 Plus is the second best phone you can buy today, and meant for anyone who won't use the stylus.", 2000, "Samsung Galaxy S9 Plus", 10, 5, 2);
insert into product
values(null,1, "The iPhone X has been surpassed by the iPhone XS, but not too much has changed since the former launched in late 2017", 2000, "iPhone X", 10, 5, 2);
insert into product
values(null,1, "Google Pixel 2 is the best phone if you're looking for a pure Android experience with a big screen, incredible camera and stereo speakers", 2000, " Google Pixel 2", 10, 5, 2);
insert into product
values(null,1, "The LG G7 ThinQ is an impressive little phone from the brand (irritating name aside), bringing with it a strong package and a decent price in many regions.", 2000, "LG G7 ThinQ", 10, 5, 2);

insert into product
values(null, 1,"We're taking inspiration by menswear this season for a pared-back and cool look", 2000, "Boxy Roll T-Shirt", 10, 5, 3);
insert into product
values(null,1, "Add some cute feline style with this short sleeve t-shirt with cat motif and 'sisterhood' slogan", 2000, "Sisterhood Cat T-Shirt", 10, 5, 3);
insert into product
values(null,1, "Every girl loves a fairy-tale ending and a slogan tee. This white short sleeve t-shirt with roll back sleeve", 2000, "Happily Ever After T-Shirt", 10, 5, 3);
insert into product
values(null,1, "This short sleeve ribbed t-shirt with picot trim at neck and sleeves is just so classic", 2000, "Picot Trim T-Shirt", 10, 5, 3);
insert into product
values(null,1, "This sheer ribbed t-shirt is perfect for spring summer.Pair with white denim for the perfect colour combination! 58% Viscose, 42% Polyamide. Machine wash.", 2000, "Sheer Ribbed T-Shirt", 10, 5, 3);
insert into product
values(null,1, "This short sleeve t-shirt with adorable crossword style motif is a great addition to any weekend wardrobe", 2000, "Adorable Crossword T-Shirt", 10, 5, 3);
insert into product
values(null,1, "Everlane is known for being the master of basics, and their iconic t-shirts are a must-have in every man's dresser", 799, "Best Overall T-shirt: EVERLANE Cotton Crew", 10, 5, 3);
insert into product
values(null,1, "The classic henley style is rendered in a comfortable waffle-knit fabric that makes this affordable Uniqlo style a winning option for guys that ", 629, "Best Henley: UNIQLO Waffle-Knit Top", 10, 5, 3);
insert into product
values(null,1, "James Perse's premium t-shirts are super luxurious and will last you a long time. This cotton cashmere jersey tee comes in an easy, relaxed fit and has an ultra-soft soft feel. ", 15000, "Best Luxury T-shirt: JAMES PERSE Jersey Tee", 10, 5, 3);
insert into product
values(null,1, "Not only does this tee wick away moisture and keep you cool and dry during your workouts", 15000, "Best Performance T-shirt: HANES Cool Dri T-Shirt (2 Pack)", 10, 5, 3);
insert into product
values(null,1, "Sometimes you just want an everyday tee that you can lounge around in at home but that also looks good when you want to step out and run errands.", 15000, "Best Casual T-shirt: URBAN OUTFITTERS Curved Hem Tee", 10, 5, 3);

insert into product
values(null,1, "he Texiana glider is an ideal piece that combines a rustic style with that down home comfort feel.", 15000, "TEXIANA ", 10, 5, 4);
insert into product
values(null,1, "Be dazzled by our diverse mission group offering! You can choose the standard sofa, loveseat and chair or a matching chair and a half style.", 15000, "WESTNEY SOFA", 10, 5, 4);
insert into product
values(null,1, "With plush, overstuffed cushions and a biscuit back, the Everlasting dual reclining sofa is the definition of comfort.", 15000, "EVERLASTING COL", 10, 5, 4);
insert into product
values(null,1, "The cozy Plusher is highly regarded as one of the most comfortable pieces of furniture in the Best Home Furnishings line, if not the world.", 15000, "PLUSHER COLL.", 10, 5, 4);
insert into product
values(null,1, "Sometimes you just want an everyday tee that you can lounge around in at home but that also looks good when you want to step out and run errands.", 15000, "FITZPATRICK COL ", 10, 5, 4);
insert into product
values(null,1, "Never worry about sacrificing form for function with this power reclining sofa. With numerous cover options – like fabric and supple leather choices – the Maddox is the answer to your search for the perfect couch.", 15000, "MADDOX COLL.", 10, 5, 4);
insert into product
values(null,1, "Take a day for yourself and kick back and relax in the luxurious Terrill reclining sofa. With customizable comfort levels.", 15000, "TERRILL COLL.", 10, 5, 4);
insert into product
values(null,1, "Refined and chivalrous, the Patrick will put your comfort before everything else.  This high leg recliner combines the best of both worlds", 15000, "PATRICK", 10, 5, 4);
insert into product
values(null,1, "The Tuscan recliner is rustic and relaxing. This mission style recliner offers the solid, handcrafted look of the mission design philosophy.", 15000, "TUSCAN.", 10, 5, 4);
insert into product
values(null,1, "Style extends from the straight lines of the square back to the rounded, extra cushioned arms that are highlighted with contrasting cording.", 15000, "TRYP", 10, 5, 4);
insert into product
values(null,1, "Comfort can be seen before its felt with this ultra pillow arm and pillow back recliner. Not only comfortable, but built with the Best-Max technology.", 15000, "SEGER", 10, 5, 4);
insert into product
values(null,1, "The Plusher is highly regarded as one of the most comfortable recliners in the Best Home Furnishings line; if not the world.", 15000, "PLUSHER", 10, 5, 4);
insert into product
values(null,1, "This is the perfect chair for grabbing some freshly buttered popcorn, kicking back and watching a movie with the whole family on a Friday night.", 15000, "MADDOX.", 10, 5, 4);
insert into product
values(null,1, "A fresh take on this Best Home Furnishings recliner doesn’t sacrifice style for comfort.", 15000, "TERRILL", 10, 5, 4);
insert into product
values(null,1, "One of the largest recliners in the industry, this Beast® recliner features extra support, extra durability and extra comfort.", 15000, "RAKE", 10, 5, 4);

	
/*delete from question_answer;
delete from review_product;
delete from viewed;
delete from cart;
delete from image;
delete from product;*/


insert into image
 values(null, "/resources/image/Books/The Iliad.jpg", 1);
insert into image
 values(null, "/resources/image/Books/The Iliad1.jpg", 1);
insert into image
 values(null, "/resources/image/Books/The Iliad2.jpg", 1);
insert into image
 values(null, "/resources/image/Books/Resurrection.jpg", 3);
insert into image
 values(null, "/resources/image/Books/Resurrection1.jpg", 3);
insert into image
 values(null, "/resources/image/Books/Resurrection2.jpg", 3);
insert into image
 values(null, "/resources/image/Books/Catch-22.jpg", 4);
insert into image
 values(null, "/resources/image/Books/Catch-22-1.jpg", 4);
insert into image
 values(null, "/resources/image/Books/Catch-22-2.jpg", 4);
insert into image
 values(null, "/resources/image/Books/Catch-22-2.jpg", 4);
insert into image
 values(null, "/resources/image/Books/OneHundredYearsOfSolitude.jpg", 5);
insert into image
 values(null, "/resources/image/Books/OneHundredYearsOfSolitude1.jpg", 5);
insert into image
 values(null, "/resources/image/Books/OneHundredYearsOfSolitude2.jpg", 5);
insert into image
 values(null, "/resources/image/Books/Tender is the Night.jpg", 6);
insert into image
 values(null, "/resources/image/Books/Tender is the Night1.jpg", 6);
insert into image
 values(null, "/resources/image/Books/Tender is the Night1.jpg", 6);
insert into image
 values(null, "/resources/image/Books/Wuthering Heights.jpg", 7);
insert into image
 values(null, "/resources/image/Books/Wuthering Heights1.jpg", 7);
insert into image
 values(null, "/resources/image/Books/Wuthering Heights1.jpg", 7);
insert into image
 values(null, "/resources/image/Books/Pride and Prejudice.jpg", 8);
insert into image
 values(null, "/resources/image/Books/Pride and Prejudice1.jpg", 8);
insert into image
 values(null, "/resources/image/Books/Pride and Prejudice2.jpg", 8);
insert into image
 values(null, "/resources/image/Books/A Brief History Of Time.jpg", 9);
insert into image
 values(null, "/resources/image/Books/A Brief History Of Time1.jpg", 9);
insert into image
 values(null, "/resources/image/Books/A Brief History Of Time2.jpg", 9);
insert into image
 values(null, "/resources/image/Books/Our Mutual Friend.jpg", 10);
insert into image
 values(null, "/resources/image/Books/Our Mutual Friend1.jpg", 10);
insert into image
 values(null, "/resources/image/Books/Our Mutual Friend2.jpg", 10);
insert into image
 values(null, "/resources/image/Books/Breakfast at Tiffany's.jpg", 11);
insert into image
 values(null, "/resources/image/Books/Breakfast at Tiffany's1.jpg", 11);
insert into image
 values(null, "/resources/image/Books/Breakfast at Tiffany's2.jpg", 11);
insert into image
 values(null, "/resources/image/Books/The Wonderful Story.jpg", 12);
insert into image
 values(null, "/resources/image/Books/The Wonderful Story1.jpg", 12);
insert into image
 values(null, "/resources/image/Books/The Wonderful Story2.jpg", 12);
insert into image
 values(null, "/resources/image/Books/Madame Bovary.jpg", 13);
insert into image
 values(null, "/resources/image/Books/Madame Bovary1.jpg", 13);
insert into image
 values(null, "/resources/image/Books/Madame Bovary2.jpg", 13);
insert into image
 values(null, "/resources/image/Books/The Picture of Dorian Gray.jpg", 14);
insert into image
 values(null, "/resources/image/Books/The Picture of Dorian Gray1.jpg", 14);
insert into image
 values(null, "/resources/image/Books/The Picture of Dorian Gray2.jpg", 14);

insert into image
 values(null, "/resources/image/Phone/googlePixel3.jpg", 15);
insert into image
 values(null, "/resources/image/Phone/googlePixel3-1.jpg", 15);
insert into image
 values(null, "/resources/image/Phone/googlePixel3-2.jpg", 15);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T.jpg", 16);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T-1.jpg", 16);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T-2.jpg", 16);
insert into image
 values(null, "/resources/image/Phone/Resurrection.jpg", 17);
insert into image
 values(null, "/resources/image/Phone/SamsungS10.jpg", 17);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-1.jpg", 17);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-2.jpg", 17);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro.jpg", 18);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro1.jpg", 18);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro2.jpg", 18);
insert into image
 values(null, "/resources/image/Phone/iphoneXS.jpg", 19);
insert into image
 values(null, "/resources/image/Phone/iphoneXS1.jpg", 19);
insert into image
 values(null, "/resources/image/Phone/iphoneXS2.jpg", 19);
insert into image
 values(null, "/resources/image/Phone/SamsungS10.jpg", 20);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-1.jpg", 20);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-2.jpg", 20);
insert into image
 values(null, "/resources/image/Phone/iphoneXSMAX.jpg", 21);
insert into image
 values(null, "/resources/image/Phone/iphoneXSMAX1.jpg", 21);
insert into image
 values(null, "/resources/image/Phone/iphoneXSMAX2.jpg", 21);
insert into image
 values(null, "/resources/image/Phone/SamsungS10E.jpg", 22);
insert into image
 values(null, "/resources/image/Phone/SamsungS10E-1.jpg", 22);
insert into image
 values(null, "/resources/image/Phone/SamsungS10E-2.jpg", 22);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T.jpg", 23);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T-1.jpg", 23);
insert into image
 values(null, "/resources/image/Phone/OnePlus6T-2.jpg", 23);
insert into image
 values(null, "/resources/image/Phone/samsungNote9.jpg", 24);
insert into image
 values(null, "/resources/image/Phone/samsungNote9-1.jpg", 24);
insert into image
 values(null, "/resources/image/Phone/samsungNote9-2.jpg", 24);
insert into image
 values(null, "/resources/image/Phone/iphoneXR.jpg", 25);
insert into image
 values(null, "/resources/image/Phone/iphoneXR-1.jpg", 25);
insert into image
 values(null, "/resources/image/Phone/iphoneXR-2.jpg", 25);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro.jpg", 26);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro1.jpg", 26);
insert into image
 values(null, "/resources/image/Phone/huaweiMate20pro2.jpg", 26);
insert into image
 values(null, "/resources/image/Phone/SamsungS10.jpg", 27);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-1.jpg", 27);
insert into image
 values(null, "/resources/image/Phone/SamsungS10-2.jpg", 27);
insert into image
 values(null, "/resources/image/Phone/iphoneXS.jpg", 28);
insert into image
 values(null, "/resources/image/Phone/iphoneXS1.jpg", 28);
insert into image
 values(null, "/resources/image/Phone/iphoneXS2.jpg", 28);
insert into image
 values(null, "/resources/image/Phone/googlePixel3XL.jpg", 29);
insert into image
 values(null, "/resources/image/Phone/googlePixel3XL-1.jpg", 29);
insert into image
 values(null, "/resources/image/Phone/googlePixel3XL-2.jpg", 29);
insert into image
 values(null, "/resources/image/Phone/samsungS9plus.jpg", 30);
insert into image
 values(null, "/resources/image/Phone/samsungS9plus1.jpg", 30);
insert into image
 values(null, "/resources/image/Phone/samsungS9plus2.jpg", 30);
insert into image
 values(null, "/resources/image/Phone/iphoneX.jpg", 31);
insert into image
 values(null, "/resources/image/Phone/iphoneX1.jpg", 31);
insert into image
 values(null, "/resources/image/Phone/iphoneX2.jpg", 31);
insert into image
 values(null, "/resources/image/Phone/googlePixel2.jpg", 32);
insert into image
 values(null, "/resources/image/Phone/googlePixel2-1.jpg", 32);
insert into image
 values(null, "/resources/image/Phone/googlePixel2-2.jpg", 32);
insert into image
 values(null, "/resources/image/Phone/LGG7ThinQ.jpg", 33);
insert into image
 values(null, "/resources/image/Phone/LGG7ThinQ1.jpg", 33);
insert into image
 values(null, "/resources/image/Phone/LGG7ThinQ.jpg", 33);

insert into image
 values(null, "/resources/image/Clothes/BoxyRollT.jpg", 34);
insert into image
 values(null, "/resources/image/Clothes/BoxyRollT1.jpg", 34);
insert into image
 values(null, "/resources/image/Clothes/BoxyRollT2.jpg", 34);
insert into image
 values(null, "/resources/image/Clothes/SisterHoodCat.jpg", 35);
insert into image
 values(null, "/resources/image/Clothes/SisterHoodCat1.jpg", 35);
insert into image
 values(null, "/resources/image/Clothes/SisterHoodCat2.jpg", 35);
insert into image
 values(null, "/resources/image/Clothes/HappilyEverAfterTShirt.jpg", 36);
insert into image
 values(null, "/resources/image/Clothes/HappilyEverAfterTShirt1.jpg", 36);
insert into image
 values(null, "/resources/image/Clothes/HappilyEverAfterTShirt2.jpg", 36);
insert into image
 values(null, "/resources/image/Clothes/PicotTrimTshirt.jpg", 37);
insert into image
 values(null, "/resources/image/Clothes/PicotTrimTshirt1.jpg", 37);
insert into image
 values(null, "/resources/image/Clothes/PicotTrimTshirt2.jpg", 37);
insert into image
 values(null, "/resources/image/Clothes/SheerRibbed.jpg", 38);
insert into image
 values(null, "/resources/image/Clothes/SheerRibbed1.jpg", 38);
insert into image
 values(null, "/resources/image/Clothes/SheerRibbed2.jpg", 38);
insert into image
 values(null, "/resources/image/Clothes/AdorableCrossword.jpg", 39);
insert into image
 values(null, "/resources/image/Clothes/AdorableCrossword1.jpg", 39);
insert into image
 values(null, "/resources/image/Clothes/AdorableCrossword2.jpg", 39);
insert into image
 values(null, "/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg", 40);
insert into image
 values(null, "/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg", 40);
insert into image
 values(null, "/resources/image/Clothes/Best Overall T-shirt EVERLANE Cotton Crew.jpg", 40);
insert into image
 values(null, "/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg", 41);
insert into image
 values(null, "/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg", 41);
insert into image
 values(null, "/resources/image/Clothes/Best Henley UNIQLO Waffle-Knit Top.jpg", 41);
insert into image
 values(null, "/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg", 42);
insert into image
 values(null, "/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg", 42);
insert into image
 values(null, "/resources/image/Clothes/Best Luxury T-shirt JAMES PERSE Jersey Tee.jpg", 42);
insert into image
 values(null, "/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg", 43);
insert into image
 values(null, "/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg", 43);
insert into image
 values(null, "/resources/image/Clothes/Best Performance T-shirt HANES Cool Dri T-Shirt (2 Pack).jpg", 43);
insert into image
 values(null, "/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg", 44);
insert into image
 values(null, "/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg", 44);
insert into image
 values(null, "/resources/image/Clothes/Best Casual T-shirt URBAN OUTFITTERS Curved Hem Tee.jpg", 44);

insert into image
 values(null, "/resources/image/Home/texiana.jpg", 45);
insert into image
 values(null, "/resources/image/Home/texiana1.jpg", 45);
insert into image
 values(null, "/resources/image/Home/texiana2.jpg", 45);
insert into image
 values(null, "/resources/image/Home/westneysofa.jpg", 46);
insert into image
 values(null, "/resources/image/Home/westneysofa1.jpg", 46);
insert into image
 values(null, "/resources/image/Home/westneysofa2.jpg", 46);
insert into image
 values(null, "/resources/image/Home/everlatincol.jpg", 47);
insert into image
 values(null, "/resources/image/Home/everlatincol1.jpg", 47);
insert into image
 values(null, "/resources/image/Home/everlatincol2.jpg", 47);
insert into image
 values(null, "/resources/image/Home/plushercoll.jpg", 48);
insert into image
 values(null, "/resources/image/Home/plushercoll1.jpg", 48);
insert into image
 values(null, "/resources/image/Home/plushercoll2.jpg", 48);
insert into image
 values(null, "/resources/image/Home/fitzpatrickcol.jpg", 49);
insert into image
 values(null, "/resources/image/Home/fitzpatrickcol1.jpg", 49);
insert into image
 values(null, "/resources/image/Home/fitzpatrickcol2.jpg", 49);
insert into image
 values(null, "/resources/image/Home/maddoxcoll.jpg", 50);
insert into image
 values(null, "/resources/image/Home/maddoxcoll1.jpg", 50);
insert into image
 values(null, "/resources/image/Home/maddoxcoll2.jpg", 50);
insert into image
 values(null, "/resources/image/Home/terrillcoll.jpg", 51);
insert into image
 values(null, "/resources/image/Home/terrillcoll1.jpg", 51);
insert into image
 values(null, "/resources/image/Home/terrillcoll2.jpg", 51);
insert into image
 values(null, "/resources/image/Home/patrick.jpg", 52);
insert into image
 values(null, "/resources/image/Home/patrick1.jpg", 52);
insert into image
 values(null, "/resources/image/Home/patrick2.jpg", 52);
insert into image
 values(null, "/resources/image/Home/noblecoll.jpg", 53);
insert into image
 values(null, "/resources/image/Home/noblecoll1.jpg", 53);
insert into image
 values(null, "/resources/image/Home/noblecoll2.jpg", 53);
insert into image
 values(null, "/resources/image/Home/tryp.jpg", 54);
insert into image
 values(null, "/resources/image/Home/tryp1.jpg", 54);
insert into image
 values(null, "/resources/image/Home/tryp2.jpg", 54);
insert into image
 values(null, "/resources/image/Home/seger.jpg", 55);
insert into image
 values(null, "/resources/image/Home/seger1.jpg", 5);
insert into image
 values(null, "/resources/image/Home/seger2.jpg", 5);
insert into image
 values(null, "/resources/image/Home/plusher.jpg", 56);
insert into image
 values(null, "/resources/image/Home/plusher1.jpg", 56);
insert into image
 values(null, "/resources/image/Home/plusher2.jpg", 56);
insert into image
 values(null, "/resources/image/Home/maddox.jpg", 57);
insert into image
 values(null, "/resources/image/Home/maddox1.jpg", 57);
insert into image
 values(null, "/resources/image/Home/maddox2.jpg", 57);
insert into image
 values(null, "/resources/image/Home/terill.jpg", 58);
insert into image
 values(null, "/resources/image/Home/terill11.jpg", 58);
insert into image
 values(null, "/resources/image/Home/terill12.jpg", 58);
insert into image
 values(null, "/resources/image/Home/rake.jpg", 59);
insert into image
 values(null, "/resources/image/Home/rake1.jpg", 59);
insert into image
 values(null, "/resources/image/Home/rake2.jpg", 59);


INSERT INTO `city` VALUES (1, 'Thành phố Hà Nội');
INSERT INTO `city` VALUES (2, 'Tỉnh Hà Giang');
INSERT INTO `city` VALUES (4, 'Tỉnh Cao Bằng');
INSERT INTO `city` VALUES (6, 'Tỉnh Bắc Kạn');
INSERT INTO `city` VALUES (8, 'Tỉnh Tuyên Quang');
INSERT INTO `city` VALUES (10, 'Tỉnh Lào Cai');
INSERT INTO `city` VALUES (11, 'Tỉnh Điện Biên');
INSERT INTO `city` VALUES (12, 'Tỉnh Lai Châu');
INSERT INTO `city` VALUES (14, 'Tỉnh Sơn La');
INSERT INTO `city` VALUES (15, 'Tỉnh Yên Bái');
INSERT INTO `city` VALUES (17, 'Tỉnh Hoà Bình');
INSERT INTO `city` VALUES (19, 'Tỉnh Thái Nguyên');
INSERT INTO `city` VALUES (20, 'Tỉnh Lạng Sơn');
INSERT INTO `city` VALUES (22, 'Tỉnh Quảng Ninh');
INSERT INTO `city` VALUES (24, 'Tỉnh Bắc Giang');
INSERT INTO `city` VALUES (25, 'Tỉnh Phú Thọ');
INSERT INTO `city` VALUES (26, 'Tỉnh Vĩnh Phúc');
INSERT INTO `city` VALUES (27, 'Tỉnh Bắc Ninh');
INSERT INTO `city` VALUES (30, 'Tỉnh Hải Dương');
INSERT INTO `city` VALUES (31, 'Thành phố Hải Phòng');
INSERT INTO `city` VALUES (33, 'Tỉnh Hưng Yên');
INSERT INTO `city` VALUES (34, 'Tỉnh Thái Bình');
INSERT INTO `city` VALUES (35, 'Tỉnh Hà Nam');
INSERT INTO `city` VALUES (36, 'Tỉnh Nam Định');
INSERT INTO `city` VALUES (37, 'Tỉnh Ninh Bình');
INSERT INTO `city` VALUES (38, 'Tỉnh Thanh Hóa');
INSERT INTO `city` VALUES (40, 'Tỉnh Nghệ An');
INSERT INTO `city` VALUES (42, 'Tỉnh Hà Tĩnh');
INSERT INTO `city` VALUES (44, 'Tỉnh Quảng Bình');
INSERT INTO `city` VALUES (45, 'Tỉnh Quảng Trị');
INSERT INTO `city` VALUES (46, 'Tỉnh Thừa Thiên Huế');
INSERT INTO `city` VALUES (48, 'Thành phố Đà Nẵng');
INSERT INTO `city` VALUES (49, 'Tỉnh Quảng Nam');
INSERT INTO `city` VALUES (51, 'Tỉnh Quảng Ngãi');
INSERT INTO `city` VALUES (52, 'Tỉnh Bình Định');
INSERT INTO `city` VALUES (54, 'Tỉnh Phú Yên');
INSERT INTO `city` VALUES (56, 'Tỉnh Khánh Hòa');
INSERT INTO `city` VALUES (58, 'Tỉnh Ninh Thuận');
INSERT INTO `city` VALUES (60, 'Tỉnh Bình Thuận');
INSERT INTO `city` VALUES (62, 'Tỉnh Kon Tum');
INSERT INTO `city` VALUES (64, 'Tỉnh Gia Lai');
INSERT INTO `city` VALUES (66, 'Tỉnh Đắk Lắk');
INSERT INTO `city` VALUES (67, 'Tỉnh Đắk Nông');
INSERT INTO `city` VALUES (68, 'Tỉnh Lâm Đồng');
INSERT INTO `city` VALUES (70, 'Tỉnh Bình Phước');
INSERT INTO `city` VALUES (72, 'Tỉnh Tây Ninh');
INSERT INTO `city` VALUES (74, 'Tỉnh Bình Dương');
INSERT INTO `city` VALUES (75, 'Tỉnh Đồng Nai');
INSERT INTO `city` VALUES (77, 'Tỉnh Bà Rịa - Vũng Tàu');
INSERT INTO `city` VALUES (79, 'Thành phố Hồ Chí Minh');
INSERT INTO `city` VALUES (80, 'Tỉnh Long An');
INSERT INTO `city` VALUES (82, 'Tỉnh Tiền Giang');
INSERT INTO `city` VALUES (83, 'Tỉnh Bến Tre');
INSERT INTO `city` VALUES (84, 'Tỉnh Trà Vinh');
INSERT INTO `city` VALUES (86, 'Tỉnh Vĩnh Long');
INSERT INTO `city` VALUES (87, 'Tỉnh Đồng Tháp');
INSERT INTO `city` VALUES (89, 'Tỉnh An Giang');
INSERT INTO `city` VALUES (91, 'Tỉnh Kiên Giang');
INSERT INTO `city` VALUES (92, 'Thành phố Cần Thơ');
INSERT INTO `city` VALUES (93, 'Tỉnh Hậu Giang');
INSERT INTO `city` VALUES (94, 'Tỉnh Sóc Trăng');
INSERT INTO `city` VALUES (95, 'Tỉnh Bạc Liêu');
INSERT INTO `city` VALUES (96, 'Tỉnh Cà Mau');

-- ----------------------------
-- Table structure for district
-- ----------------------------
-- ----------------------------
-- Records of district
-- ----------------------------
INSERT INTO `district` VALUES (1, 'Quận Ba Đình', 1);
INSERT INTO `district` VALUES (2, 'Quận Hoàn Kiếm', 1);
INSERT INTO `district` VALUES (3, 'Quận Tây Hồ', 1);
INSERT INTO `district` VALUES (4, 'Quận Long Biên', 1);
INSERT INTO `district` VALUES (5, 'Quận Cầu Giấy', 1);
INSERT INTO `district` VALUES (6, 'Quận Đống Đa', 1);
INSERT INTO `district` VALUES (7, 'Quận Hai Bà Trưng', 1);
INSERT INTO `district` VALUES (8, 'Quận Hoàng Mai', 1);
INSERT INTO `district` VALUES (9, 'Quận Thanh Xuân', 1);
INSERT INTO `district` VALUES (16, 'Huyện Sóc Sơn', 1);
INSERT INTO `district` VALUES (17, 'Huyện Đông Anh', 1);
INSERT INTO `district` VALUES (18, 'Huyện Gia Lâm', 1);
INSERT INTO `district` VALUES (19, 'Quận Nam Từ Liêm', 1);
INSERT INTO `district` VALUES (20, 'Huyện Thanh Trì', 1);
INSERT INTO `district` VALUES (21, 'Quận Bắc Từ Liêm', 1);
INSERT INTO `district` VALUES (24, 'Thành phố Hà Giang', 2);
INSERT INTO `district` VALUES (26, 'Huyện Đồng Văn', 2);
INSERT INTO `district` VALUES (27, 'Huyện Mèo Vạc', 2);
INSERT INTO `district` VALUES (28, 'Huyện Yên Minh', 2);
INSERT INTO `district` VALUES (29, 'Huyện Quản Bạ', 2);
INSERT INTO `district` VALUES (30, 'Huyện Vị Xuyên', 2);
INSERT INTO `district` VALUES (31, 'Huyện Bắc Mê', 2);
INSERT INTO `district` VALUES (32, 'Huyện Hoàng Su Phì', 2);
INSERT INTO `district` VALUES (33, 'Huyện Xín Mần', 2);
INSERT INTO `district` VALUES (34, 'Huyện Bắc Quang', 2);
INSERT INTO `district` VALUES (35, 'Huyện Quang Bình', 2);
INSERT INTO `district` VALUES (40, 'Thành phố Cao Bằng', 4);
INSERT INTO `district` VALUES (42, 'Huyện Bảo Lâm', 4);
INSERT INTO `district` VALUES (43, 'Huyện Bảo Lạc', 4);
INSERT INTO `district` VALUES (44, 'Huyện Thông Nông', 4);
INSERT INTO `district` VALUES (45, 'Huyện Hà Quảng', 4);
INSERT INTO `district` VALUES (46, 'Huyện Trà Lĩnh', 4);
INSERT INTO `district` VALUES (47, 'Huyện Trùng Khánh', 4);
INSERT INTO `district` VALUES (48, 'Huyện Hạ Lang', 4);
INSERT INTO `district` VALUES (49, 'Huyện Quảng Uyên', 4);
INSERT INTO `district` VALUES (50, 'Huyện Phục Hoà', 4);
INSERT INTO `district` VALUES (51, 'Huyện Hoà An', 4);
INSERT INTO `district` VALUES (52, 'Huyện Nguyên Bình', 4);
INSERT INTO `district` VALUES (53, 'Huyện Thạch An', 4);
INSERT INTO `district` VALUES (58, 'Thành Phố Bắc Kạn', 6);
INSERT INTO `district` VALUES (60, 'Huyện Pác Nặm', 6);
INSERT INTO `district` VALUES (61, 'Huyện Ba Bể', 6);
INSERT INTO `district` VALUES (62, 'Huyện Ngân Sơn', 6);
INSERT INTO `district` VALUES (63, 'Huyện Bạch Thông', 6);
INSERT INTO `district` VALUES (64, 'Huyện Chợ Đồn', 6);
INSERT INTO `district` VALUES (65, 'Huyện Chợ Mới', 6);
INSERT INTO `district` VALUES (66, 'Huyện Na Rì', 6);
INSERT INTO `district` VALUES (70, 'Thành phố Tuyên Quang', 8);
INSERT INTO `district` VALUES (71, 'Huyện Lâm Bình', 8);
INSERT INTO `district` VALUES (72, 'Huyện Nà Hang', 8);
INSERT INTO `district` VALUES (73, 'Huyện Chiêm Hóa', 8);
INSERT INTO `district` VALUES (74, 'Huyện Hàm Yên', 8);
INSERT INTO `district` VALUES (75, 'Huyện Yên Sơn', 8);
INSERT INTO `district` VALUES (76, 'Huyện Sơn Dương', 8);
INSERT INTO `district` VALUES (80, 'Thành phố Lào Cai', 10);
INSERT INTO `district` VALUES (82, 'Huyện Bát Xát', 10);
INSERT INTO `district` VALUES (83, 'Huyện Mường Khương', 10);
INSERT INTO `district` VALUES (84, 'Huyện Si Ma Cai', 10);
INSERT INTO `district` VALUES (85, 'Huyện Bắc Hà', 10);
INSERT INTO `district` VALUES (86, 'Huyện Bảo Thắng', 10);
INSERT INTO `district` VALUES (87, 'Huyện Bảo Yên', 10);
INSERT INTO `district` VALUES (88, 'Huyện Sa Pa', 10);
INSERT INTO `district` VALUES (89, 'Huyện Văn Bàn', 10);
INSERT INTO `district` VALUES (94, 'Thành phố Điện Biên Phủ', 11);
INSERT INTO `district` VALUES (95, 'Thị Xã Mường Lay', 11);
INSERT INTO `district` VALUES (96, 'Huyện Mường Nhé', 11);
INSERT INTO `district` VALUES (97, 'Huyện Mường Chà', 11);
INSERT INTO `district` VALUES (98, 'Huyện Tủa Chùa', 11);
INSERT INTO `district` VALUES (99, 'Huyện Tuần Giáo', 11);
INSERT INTO `district` VALUES (100, 'Huyện Điện Biên', 11);
INSERT INTO `district` VALUES (101, 'Huyện Điện Biên Đông', 11);
INSERT INTO `district` VALUES (102, 'Huyện Mường Ảng', 11);
INSERT INTO `district` VALUES (103, 'Huyện Nậm Pồ', 11);
INSERT INTO `district` VALUES (105, 'Thành phố Lai Châu', 12);
INSERT INTO `district` VALUES (106, 'Huyện Tam Đường', 12);
INSERT INTO `district` VALUES (107, 'Huyện Mường Tè', 12);
INSERT INTO `district` VALUES (108, 'Huyện Sìn Hồ', 12);
INSERT INTO `district` VALUES (109, 'Huyện Phong Thổ', 12);
INSERT INTO `district` VALUES (110, 'Huyện Than Uyên', 12);
INSERT INTO `district` VALUES (111, 'Huyện Tân Uyên', 12);
INSERT INTO `district` VALUES (112, 'Huyện Nậm Nhùn', 12);
INSERT INTO `district` VALUES (116, 'Thành phố Sơn La', 14);
INSERT INTO `district` VALUES (118, 'Huyện Quỳnh Nhai', 14);
INSERT INTO `district` VALUES (119, 'Huyện Thuận Châu', 14);
INSERT INTO `district` VALUES (120, 'Huyện Mường La', 14);
INSERT INTO `district` VALUES (121, 'Huyện Bắc Yên', 14);
INSERT INTO `district` VALUES (122, 'Huyện Phù Yên', 14);
INSERT INTO `district` VALUES (123, 'Huyện Mộc Châu', 14);
INSERT INTO `district` VALUES (124, 'Huyện Yên Châu', 14);
INSERT INTO `district` VALUES (125, 'Huyện Mai Sơn', 14);
INSERT INTO `district` VALUES (126, 'Huyện Sông Mã', 14);
INSERT INTO `district` VALUES (127, 'Huyện Sốp Cộp', 14);
INSERT INTO `district` VALUES (128, 'Huyện Vân Hồ', 14);
INSERT INTO `district` VALUES (132, 'Thành phố Yên Bái', 15);
INSERT INTO `district` VALUES (133, 'Thị xã Nghĩa Lộ', 15);
INSERT INTO `district` VALUES (135, 'Huyện Lục Yên', 15);
INSERT INTO `district` VALUES (136, 'Huyện Văn Yên', 15);
INSERT INTO `district` VALUES (137, 'Huyện Mù Căng Chải', 15);
INSERT INTO `district` VALUES (138, 'Huyện Trấn Yên', 15);
INSERT INTO `district` VALUES (139, 'Huyện Trạm Tấu', 15);
INSERT INTO `district` VALUES (140, 'Huyện Văn Chấn', 15);
INSERT INTO `district` VALUES (141, 'Huyện Yên Bình', 15);
INSERT INTO `district` VALUES (148, 'Thành phố Hòa Bình', 17);
INSERT INTO `district` VALUES (150, 'Huyện Đà Bắc', 17);
INSERT INTO `district` VALUES (151, 'Huyện Kỳ Sơn', 17);
INSERT INTO `district` VALUES (152, 'Huyện Lương Sơn', 17);
INSERT INTO `district` VALUES (153, 'Huyện Kim Bôi', 17);
INSERT INTO `district` VALUES (154, 'Huyện Cao Phong', 17);
INSERT INTO `district` VALUES (155, 'Huyện Tân Lạc', 17);
INSERT INTO `district` VALUES (156, 'Huyện Mai Châu', 17);
INSERT INTO `district` VALUES (157, 'Huyện Lạc Sơn', 17);
INSERT INTO `district` VALUES (158, 'Huyện Yên Thủy', 17);
INSERT INTO `district` VALUES (159, 'Huyện Lạc Thủy', 17);
INSERT INTO `district` VALUES (164, 'Thành phố Thái Nguyên', 19);
INSERT INTO `district` VALUES (165, 'Thành phố Sông Công', 19);
INSERT INTO `district` VALUES (167, 'Huyện Định Hóa', 19);
INSERT INTO `district` VALUES (168, 'Huyện Phú Lương', 19);
INSERT INTO `district` VALUES (169, 'Huyện Đồng Hỷ', 19);
INSERT INTO `district` VALUES (170, 'Huyện Võ Nhai', 19);
INSERT INTO `district` VALUES (171, 'Huyện Đại Từ', 19);
INSERT INTO `district` VALUES (172, 'Thị xã Phổ Yên', 19);
INSERT INTO `district` VALUES (173, 'Huyện Phú Bình', 19);
INSERT INTO `district` VALUES (178, 'Thành phố Lạng Sơn', 20);
INSERT INTO `district` VALUES (180, 'Huyện Tràng Định', 20);
INSERT INTO `district` VALUES (181, 'Huyện Bình Gia', 20);
INSERT INTO `district` VALUES (182, 'Huyện Văn Lãng', 20);
INSERT INTO `district` VALUES (183, 'Huyện Cao Lộc', 20);
INSERT INTO `district` VALUES (184, 'Huyện Văn Quan', 20);
INSERT INTO `district` VALUES (185, 'Huyện Bắc Sơn', 20);
INSERT INTO `district` VALUES (186, 'Huyện Hữu Lũng', 20);
INSERT INTO `district` VALUES (187, 'Huyện Chi Lăng', 20);
INSERT INTO `district` VALUES (188, 'Huyện Lộc Bình', 20);
INSERT INTO `district` VALUES (189, 'Huyện Đình Lập', 20);
INSERT INTO `district` VALUES (193, 'Thành phố Hạ Long', 22);
INSERT INTO `district` VALUES (194, 'Thành phố Móng Cái', 22);
INSERT INTO `district` VALUES (195, 'Thành phố Cẩm Phả', 22);
INSERT INTO `district` VALUES (196, 'Thành phố Uông Bí', 22);
INSERT INTO `district` VALUES (198, 'Huyện Bình Liêu', 22);
INSERT INTO `district` VALUES (199, 'Huyện Tiên Yên', 22);
INSERT INTO `district` VALUES (200, 'Huyện Đầm Hà', 22);
INSERT INTO `district` VALUES (201, 'Huyện Hải Hà', 22);
INSERT INTO `district` VALUES (202, 'Huyện Ba Chẽ', 22);
INSERT INTO `district` VALUES (203, 'Huyện Vân Đồn', 22);
INSERT INTO `district` VALUES (204, 'Huyện Hoành Bồ', 22);
INSERT INTO `district` VALUES (205, 'Thị xã Đông Triều', 22);
INSERT INTO `district` VALUES (206, 'Thị xã Quảng Yên', 22);
INSERT INTO `district` VALUES (207, 'Huyện Cô Tô', 22);
INSERT INTO `district` VALUES (213, 'Thành phố Bắc Giang', 24);
INSERT INTO `district` VALUES (215, 'Huyện Yên Thế', 24);
INSERT INTO `district` VALUES (216, 'Huyện Tân Yên', 24);
INSERT INTO `district` VALUES (217, 'Huyện Lạng Giang', 24);
INSERT INTO `district` VALUES (218, 'Huyện Lục Nam', 24);
INSERT INTO `district` VALUES (219, 'Huyện Lục Ngạn', 24);
INSERT INTO `district` VALUES (220, 'Huyện Sơn Động', 24);
INSERT INTO `district` VALUES (221, 'Huyện Yên Dũng', 24);
INSERT INTO `district` VALUES (222, 'Huyện Việt Yên', 24);
INSERT INTO `district` VALUES (223, 'Huyện Hiệp Hòa', 24);
INSERT INTO `district` VALUES (227, 'Thành phố Việt Trì', 25);
INSERT INTO `district` VALUES (228, 'Thị xã Phú Thọ', 25);
INSERT INTO `district` VALUES (230, 'Huyện Đoan Hùng', 25);
INSERT INTO `district` VALUES (231, 'Huyện Hạ Hoà', 25);
INSERT INTO `district` VALUES (232, 'Huyện Thanh Ba', 25);
INSERT INTO `district` VALUES (233, 'Huyện Phù Ninh', 25);
INSERT INTO `district` VALUES (234, 'Huyện Yên Lập', 25);
INSERT INTO `district` VALUES (235, 'Huyện Cẩm Khê', 25);
INSERT INTO `district` VALUES (236, 'Huyện Tam Nông', 25);
INSERT INTO `district` VALUES (237, 'Huyện Lâm Thao', 25);
INSERT INTO `district` VALUES (238, 'Huyện Thanh Sơn', 25);
INSERT INTO `district` VALUES (239, 'Huyện Thanh Thuỷ', 25);
INSERT INTO `district` VALUES (240, 'Huyện Tân Sơn', 25);
INSERT INTO `district` VALUES (243, 'Thành phố Vĩnh Yên', 26);
INSERT INTO `district` VALUES (244, 'Thị xã Phúc Yên', 26);
INSERT INTO `district` VALUES (246, 'Huyện Lập Thạch', 26);
INSERT INTO `district` VALUES (247, 'Huyện Tam Dương', 26);
INSERT INTO `district` VALUES (248, 'Huyện Tam Đảo', 26);
INSERT INTO `district` VALUES (249, 'Huyện Bình Xuyên', 26);
INSERT INTO `district` VALUES (250, 'Huyện Mê Linh', 1);
INSERT INTO `district` VALUES (251, 'Huyện Yên Lạc', 26);
INSERT INTO `district` VALUES (252, 'Huyện Vĩnh Tường', 26);
INSERT INTO `district` VALUES (253, 'Huyện Sông Lô', 26);
INSERT INTO `district` VALUES (256, 'Thành phố Bắc Ninh', 27);
INSERT INTO `district` VALUES (258, 'Huyện Yên Phong', 27);
INSERT INTO `district` VALUES (259, 'Huyện Quế Võ', 27);
INSERT INTO `district` VALUES (260, 'Huyện Tiên Du', 27);
INSERT INTO `district` VALUES (261, 'Thị xã Từ Sơn', 27);
INSERT INTO `district` VALUES (262, 'Huyện Thuận Thành', 27);
INSERT INTO `district` VALUES (263, 'Huyện Gia Bình', 27);
INSERT INTO `district` VALUES (264, 'Huyện Lương Tài', 27);
INSERT INTO `district` VALUES (268, 'Quận Hà Đông', 1);
INSERT INTO `district` VALUES (269, 'Thị xã Sơn Tây', 1);
INSERT INTO `district` VALUES (271, 'Huyện Ba Vì', 1);
INSERT INTO `district` VALUES (272, 'Huyện Phúc Thọ', 1);
INSERT INTO `district` VALUES (273, 'Huyện Đan Phượng', 1);
INSERT INTO `district` VALUES (274, 'Huyện Hoài Đức', 1);
INSERT INTO `district` VALUES (275, 'Huyện Quốc Oai', 1);
INSERT INTO `district` VALUES (276, 'Huyện Thạch Thất', 1);
INSERT INTO `district` VALUES (277, 'Huyện Chương Mỹ', 1);
INSERT INTO `district` VALUES (278, 'Huyện Thanh Oai', 1);
INSERT INTO `district` VALUES (279, 'Huyện Thường Tín', 1);
INSERT INTO `district` VALUES (280, 'Huyện Phú Xuyên', 1);
INSERT INTO `district` VALUES (281, 'Huyện Ứng Hòa', 1);
INSERT INTO `district` VALUES (282, 'Huyện Mỹ Đức', 1);
INSERT INTO `district` VALUES (288, 'Thành phố Hải Dương', 30);
INSERT INTO `district` VALUES (290, 'Thị xã Chí Linh', 30);
INSERT INTO `district` VALUES (291, 'Huyện Nam Sách', 30);
INSERT INTO `district` VALUES (292, 'Huyện Kinh Môn', 30);
INSERT INTO `district` VALUES (293, 'Huyện Kim Thành', 30);
INSERT INTO `district` VALUES (294, 'Huyện Thanh Hà', 30);
INSERT INTO `district` VALUES (295, 'Huyện Cẩm Giàng', 30);
INSERT INTO `district` VALUES (296, 'Huyện Bình Giang', 30);
INSERT INTO `district` VALUES (297, 'Huyện Gia Lộc', 30);
INSERT INTO `district` VALUES (298, 'Huyện Tứ Kỳ', 30);
INSERT INTO `district` VALUES (299, 'Huyện Ninh Giang', 30);
INSERT INTO `district` VALUES (300, 'Huyện Thanh Miện', 30);
INSERT INTO `district` VALUES (303, 'Quận Hồng Bàng', 31);
INSERT INTO `district` VALUES (304, 'Quận Ngô Quyền', 31);
INSERT INTO `district` VALUES (305, 'Quận Lê Chân', 31);
INSERT INTO `district` VALUES (306, 'Quận Hải An', 31);
INSERT INTO `district` VALUES (307, 'Quận Kiến An', 31);
INSERT INTO `district` VALUES (308, 'Quận Đồ Sơn', 31);
INSERT INTO `district` VALUES (309, 'Quận Dương Kinh', 31);
INSERT INTO `district` VALUES (311, 'Huyện Thuỷ Nguyên', 31);
INSERT INTO `district` VALUES (312, 'Huyện An Dương', 31);
INSERT INTO `district` VALUES (313, 'Huyện An Lão', 31);
INSERT INTO `district` VALUES (314, 'Huyện Kiến Thuỵ', 31);
INSERT INTO `district` VALUES (315, 'Huyện Tiên Lãng', 31);
INSERT INTO `district` VALUES (316, 'Huyện Vĩnh Bảo', 31);
INSERT INTO `district` VALUES (317, 'Huyện Cát Hải', 31);
INSERT INTO `district` VALUES (318, 'Huyện Bạch Long Vĩ', 31);
INSERT INTO `district` VALUES (323, 'Thành phố Hưng Yên', 33);
INSERT INTO `district` VALUES (325, 'Huyện Văn Lâm', 33);
INSERT INTO `district` VALUES (326, 'Huyện Văn Giang', 33);
INSERT INTO `district` VALUES (327, 'Huyện Yên Mỹ', 33);
INSERT INTO `district` VALUES (328, 'Huyện Mỹ Hào', 33);
INSERT INTO `district` VALUES (329, 'Huyện Ân Thi', 33);
INSERT INTO `district` VALUES (330, 'Huyện Khoái Châu', 33);
INSERT INTO `district` VALUES (331, 'Huyện Kim Động', 33);
INSERT INTO `district` VALUES (332, 'Huyện Tiên Lữ', 33);
INSERT INTO `district` VALUES (333, 'Huyện Phù Cừ', 33);
INSERT INTO `district` VALUES (336, 'Thành phố Thái Bình', 34);
INSERT INTO `district` VALUES (338, 'Huyện Quỳnh Phụ', 34);
INSERT INTO `district` VALUES (339, 'Huyện Hưng Hà', 34);
INSERT INTO `district` VALUES (340, 'Huyện Đông Hưng', 34);
INSERT INTO `district` VALUES (341, 'Huyện Thái Thụy', 34);
INSERT INTO `district` VALUES (342, 'Huyện Tiền Hải', 34);
INSERT INTO `district` VALUES (343, 'Huyện Kiến Xương', 34);
INSERT INTO `district` VALUES (344, 'Huyện Vũ Thư', 34);
INSERT INTO `district` VALUES (347, 'Thành phố Phủ Lý', 35);
INSERT INTO `district` VALUES (349, 'Huyện Duy Tiên', 35);
INSERT INTO `district` VALUES (350, 'Huyện Kim Bảng', 35);
INSERT INTO `district` VALUES (351, 'Huyện Thanh Liêm', 35);
INSERT INTO `district` VALUES (352, 'Huyện Bình Lục', 35);
INSERT INTO `district` VALUES (353, 'Huyện Lý Nhân', 35);
INSERT INTO `district` VALUES (356, 'Thành phố Nam Định', 36);
INSERT INTO `district` VALUES (358, 'Huyện Mỹ Lộc', 36);
INSERT INTO `district` VALUES (359, 'Huyện Vụ Bản', 36);
INSERT INTO `district` VALUES (360, 'Huyện Ý Yên', 36);
INSERT INTO `district` VALUES (361, 'Huyện Nghĩa Hưng', 36);
INSERT INTO `district` VALUES (362, 'Huyện Nam Trực', 36);
INSERT INTO `district` VALUES (363, 'Huyện Trực Ninh', 36);
INSERT INTO `district` VALUES (364, 'Huyện Xuân Trường', 36);
INSERT INTO `district` VALUES (365, 'Huyện Giao Thủy', 36);
INSERT INTO `district` VALUES (366, 'Huyện Hải Hậu', 36);
INSERT INTO `district` VALUES (369, 'Thành phố Ninh Bình', 37);
INSERT INTO `district` VALUES (370, 'Thành phố Tam Điệp', 37);
INSERT INTO `district` VALUES (372, 'Huyện Nho Quan', 37);
INSERT INTO `district` VALUES (373, 'Huyện Gia Viễn', 37);
INSERT INTO `district` VALUES (374, 'Huyện Hoa Lư', 37);
INSERT INTO `district` VALUES (375, 'Huyện Yên Khánh', 37);
INSERT INTO `district` VALUES (376, 'Huyện Kim Sơn', 37);
INSERT INTO `district` VALUES (377, 'Huyện Yên Mô', 37);
INSERT INTO `district` VALUES (380, 'Thành phố Thanh Hóa', 38);
INSERT INTO `district` VALUES (381, 'Thị xã Bỉm Sơn', 38);
INSERT INTO `district` VALUES (382, 'Thị xã Sầm Sơn', 38);
INSERT INTO `district` VALUES (384, 'Huyện Mường Lát', 38);
INSERT INTO `district` VALUES (385, 'Huyện Quan Hóa', 38);
INSERT INTO `district` VALUES (386, 'Huyện Bá Thước', 38);
INSERT INTO `district` VALUES (387, 'Huyện Quan Sơn', 38);
INSERT INTO `district` VALUES (388, 'Huyện Lang Chánh', 38);
INSERT INTO `district` VALUES (389, 'Huyện Ngọc Lặc', 38);
INSERT INTO `district` VALUES (390, 'Huyện Cẩm Thủy', 38);
INSERT INTO `district` VALUES (391, 'Huyện Thạch Thành', 38);
INSERT INTO `district` VALUES (392, 'Huyện Hà Trung', 38);
INSERT INTO `district` VALUES (393, 'Huyện Vĩnh Lộc', 38);
INSERT INTO `district` VALUES (394, 'Huyện Yên Định', 38);
INSERT INTO `district` VALUES (395, 'Huyện Thọ Xuân', 38);
INSERT INTO `district` VALUES (396, 'Huyện Thường Xuân', 38);
INSERT INTO `district` VALUES (397, 'Huyện Triệu Sơn', 38);
INSERT INTO `district` VALUES (398, 'Huyện Thiệu Hóa', 38);
INSERT INTO `district` VALUES (399, 'Huyện Hoằng Hóa', 38);
INSERT INTO `district` VALUES (400, 'Huyện Hậu Lộc', 38);
INSERT INTO `district` VALUES (401, 'Huyện Nga Sơn', 38);
INSERT INTO `district` VALUES (402, 'Huyện Như Xuân', 38);
INSERT INTO `district` VALUES (403, 'Huyện Như Thanh', 38);
INSERT INTO `district` VALUES (404, 'Huyện Nông Cống', 38);
INSERT INTO `district` VALUES (405, 'Huyện Đông Sơn', 38);
INSERT INTO `district` VALUES (406, 'Huyện Quảng Xương', 38);
INSERT INTO `district` VALUES (407, 'Huyện Tĩnh Gia', 38);
INSERT INTO `district` VALUES (412, 'Thành phố Vinh', 40);
INSERT INTO `district` VALUES (413, 'Thị xã Cửa Lò', 40);
INSERT INTO `district` VALUES (414, 'Thị xã Thái Hoà', 40);
INSERT INTO `district` VALUES (415, 'Huyện Quế Phong', 40);
INSERT INTO `district` VALUES (416, 'Huyện Quỳ Châu', 40);
INSERT INTO `district` VALUES (417, 'Huyện Kỳ Sơn', 40);
INSERT INTO `district` VALUES (418, 'Huyện Tương Dương', 40);
INSERT INTO `district` VALUES (419, 'Huyện Nghĩa Đàn', 40);
INSERT INTO `district` VALUES (420, 'Huyện Quỳ Hợp', 40);
INSERT INTO `district` VALUES (421, 'Huyện Quỳnh Lưu', 40);
INSERT INTO `district` VALUES (422, 'Huyện Con Cuông', 40);
INSERT INTO `district` VALUES (423, 'Huyện Tân Kỳ', 40);
INSERT INTO `district` VALUES (424, 'Huyện Anh Sơn', 40);
INSERT INTO `district` VALUES (425, 'Huyện Diễn Châu', 40);
INSERT INTO `district` VALUES (426, 'Huyện Yên Thành', 40);
INSERT INTO `district` VALUES (427, 'Huyện Đô Lương', 40);
INSERT INTO `district` VALUES (428, 'Huyện Thanh Chương', 40);
INSERT INTO `district` VALUES (429, 'Huyện Nghi Lộc', 40);
INSERT INTO `district` VALUES (430, 'Huyện Nam Đàn', 40);
INSERT INTO `district` VALUES (431, 'Huyện Hưng Nguyên', 40);
INSERT INTO `district` VALUES (432, 'Thị xã Hoàng Mai', 40);
INSERT INTO `district` VALUES (436, 'Thành phố Hà Tĩnh', 42);
INSERT INTO `district` VALUES (437, 'Thị xã Hồng Lĩnh', 42);
INSERT INTO `district` VALUES (439, 'Huyện Hương Sơn', 42);
INSERT INTO `district` VALUES (440, 'Huyện Đức Thọ', 42);
INSERT INTO `district` VALUES (441, 'Huyện Vũ Quang', 42);
INSERT INTO `district` VALUES (442, 'Huyện Nghi Xuân', 42);
INSERT INTO `district` VALUES (443, 'Huyện Can Lộc', 42);
INSERT INTO `district` VALUES (444, 'Huyện Hương Khê', 42);
INSERT INTO `district` VALUES (445, 'Huyện Thạch Hà', 42);
INSERT INTO `district` VALUES (446, 'Huyện Cẩm Xuyên', 42);
INSERT INTO `district` VALUES (447, 'Huyện Kỳ Anh', 42);
INSERT INTO `district` VALUES (448, 'Huyện Lộc Hà', 42);
INSERT INTO `district` VALUES (449, 'Thị xã Kỳ Anh', 42);
INSERT INTO `district` VALUES (450, 'Thành Phố Đồng Hới', 44);
INSERT INTO `district` VALUES (452, 'Huyện Minh Hóa', 44);
INSERT INTO `district` VALUES (453, 'Huyện Tuyên Hóa', 44);
INSERT INTO `district` VALUES (454, 'Huyện Quảng Trạch', 44);
INSERT INTO `district` VALUES (455, 'Huyện Bố Trạch', 44);
INSERT INTO `district` VALUES (456, 'Huyện Quảng Ninh', 44);
INSERT INTO `district` VALUES (457, 'Huyện Lệ Thủy', 44);
INSERT INTO `district` VALUES (458, 'Thị xã Ba Đồn', 44);
INSERT INTO `district` VALUES (461, 'Thành phố Đông Hà', 45);
INSERT INTO `district` VALUES (462, 'Thị xã Quảng Trị', 45);
INSERT INTO `district` VALUES (464, 'Huyện Vĩnh Linh', 45);
INSERT INTO `district` VALUES (465, 'Huyện Hướng Hóa', 45);
INSERT INTO `district` VALUES (466, 'Huyện Gio Linh', 45);
INSERT INTO `district` VALUES (467, 'Huyện Đa Krông', 45);
INSERT INTO `district` VALUES (468, 'Huyện Cam Lộ', 45);
INSERT INTO `district` VALUES (469, 'Huyện Triệu Phong', 45);
INSERT INTO `district` VALUES (470, 'Huyện Hải Lăng', 45);
INSERT INTO `district` VALUES (471, 'Huyện Cồn Cỏ', 45);
INSERT INTO `district` VALUES (474, 'Thành phố Huế', 46);
INSERT INTO `district` VALUES (476, 'Huyện Phong Điền', 46);
INSERT INTO `district` VALUES (477, 'Huyện Quảng Điền', 46);
INSERT INTO `district` VALUES (478, 'Huyện Phú Vang', 46);
INSERT INTO `district` VALUES (479, 'Thị xã Hương Thủy', 46);
INSERT INTO `district` VALUES (480, 'Thị xã Hương Trà', 46);
INSERT INTO `district` VALUES (481, 'Huyện A Lưới', 46);
INSERT INTO `district` VALUES (482, 'Huyện Phú Lộc', 46);
INSERT INTO `district` VALUES (483, 'Huyện Nam Đông', 46);
INSERT INTO `district` VALUES (490, 'Quận Liên Chiểu', 48);
INSERT INTO `district` VALUES (491, 'Quận Thanh Khê', 48);
INSERT INTO `district` VALUES (492, 'Quận Hải Châu', 48);
INSERT INTO `district` VALUES (493, 'Quận Sơn Trà', 48);
INSERT INTO `district` VALUES (494, 'Quận Ngũ Hành Sơn', 48);
INSERT INTO `district` VALUES (495, 'Quận Cẩm Lệ', 48);
INSERT INTO `district` VALUES (497, 'Huyện Hòa Vang', 48);
INSERT INTO `district` VALUES (498, 'Huyện Hoàng Sa', 48);
INSERT INTO `district` VALUES (502, 'Thành phố Tam Kỳ', 49);
INSERT INTO `district` VALUES (503, 'Thành phố Hội An', 49);
INSERT INTO `district` VALUES (504, 'Huyện Tây Giang', 49);
INSERT INTO `district` VALUES (505, 'Huyện Đông Giang', 49);
INSERT INTO `district` VALUES (506, 'Huyện Đại Lộc', 49);
INSERT INTO `district` VALUES (507, 'Thị xã Điện Bàn', 49);
INSERT INTO `district` VALUES (508, 'Huyện Duy Xuyên', 49);
INSERT INTO `district` VALUES (509, 'Huyện Quế Sơn', 49);
INSERT INTO `district` VALUES (510, 'Huyện Nam Giang', 49);
INSERT INTO `district` VALUES (511, 'Huyện Phước Sơn', 49);
INSERT INTO `district` VALUES (512, 'Huyện Hiệp Đức', 49);
INSERT INTO `district` VALUES (513, 'Huyện Thăng Bình', 49);
INSERT INTO `district` VALUES (514, 'Huyện Tiên Phước', 49);
INSERT INTO `district` VALUES (515, 'Huyện Bắc Trà My', 49);
INSERT INTO `district` VALUES (516, 'Huyện Nam Trà My', 49);
INSERT INTO `district` VALUES (517, 'Huyện Núi Thành', 49);
INSERT INTO `district` VALUES (518, 'Huyện Phú Ninh', 49);
INSERT INTO `district` VALUES (519, 'Huyện Nông Sơn', 49);
INSERT INTO `district` VALUES (522, 'Thành phố Quảng Ngãi', 51);
INSERT INTO `district` VALUES (524, 'Huyện Bình Sơn', 51);
INSERT INTO `district` VALUES (525, 'Huyện Trà Bồng', 51);
INSERT INTO `district` VALUES (526, 'Huyện Tây Trà', 51);
INSERT INTO `district` VALUES (527, 'Huyện Sơn Tịnh', 51);
INSERT INTO `district` VALUES (528, 'Huyện Tư Nghĩa', 51);
INSERT INTO `district` VALUES (529, 'Huyện Sơn Hà', 51);
INSERT INTO `district` VALUES (530, 'Huyện Sơn Tây', 51);
INSERT INTO `district` VALUES (531, 'Huyện Minh Long', 51);
INSERT INTO `district` VALUES (532, 'Huyện Nghĩa Hành', 51);
INSERT INTO `district` VALUES (533, 'Huyện Mộ Đức', 51);
INSERT INTO `district` VALUES (534, 'Huyện Đức Phổ', 51);
INSERT INTO `district` VALUES (535, 'Huyện Ba Tơ', 51);
INSERT INTO `district` VALUES (536, 'Huyện Lý Sơn', 51);
INSERT INTO `district` VALUES (540, 'Thành phố Qui Nhơn', 52);
INSERT INTO `district` VALUES (542, 'Huyện An Lão', 52);
INSERT INTO `district` VALUES (543, 'Huyện Hoài Nhơn', 52);
INSERT INTO `district` VALUES (544, 'Huyện Hoài Ân', 52);
INSERT INTO `district` VALUES (545, 'Huyện Phù Mỹ', 52);
INSERT INTO `district` VALUES (546, 'Huyện Vĩnh Thạnh', 52);
INSERT INTO `district` VALUES (547, 'Huyện Tây Sơn', 52);
INSERT INTO `district` VALUES (548, 'Huyện Phù Cát', 52);
INSERT INTO `district` VALUES (549, 'Thị xã An Nhơn', 52);
INSERT INTO `district` VALUES (550, 'Huyện Tuy Phước', 52);
INSERT INTO `district` VALUES (551, 'Huyện Vân Canh', 52);
INSERT INTO `district` VALUES (555, 'Thành phố Tuy Hoà', 54);
INSERT INTO `district` VALUES (557, 'Thị xã Sông Cầu', 54);
INSERT INTO `district` VALUES (558, 'Huyện Đồng Xuân', 54);
INSERT INTO `district` VALUES (559, 'Huyện Tuy An', 54);
INSERT INTO `district` VALUES (560, 'Huyện Sơn Hòa', 54);
INSERT INTO `district` VALUES (561, 'Huyện Sông Hinh', 54);
INSERT INTO `district` VALUES (562, 'Huyện Tây Hoà', 54);
INSERT INTO `district` VALUES (563, 'Huyện Phú Hoà', 54);
INSERT INTO `district` VALUES (564, 'Huyện Đông Hòa', 54);
INSERT INTO `district` VALUES (568, 'Thành phố Nha Trang', 56);
INSERT INTO `district` VALUES (569, 'Thành phố Cam Ranh', 56);
INSERT INTO `district` VALUES (570, 'Huyện Cam Lâm', 56);
INSERT INTO `district` VALUES (571, 'Huyện Vạn Ninh', 56);
INSERT INTO `district` VALUES (572, 'Thị xã Ninh Hòa', 56);
INSERT INTO `district` VALUES (573, 'Huyện Khánh Vĩnh', 56);
INSERT INTO `district` VALUES (574, 'Huyện Diên Khánh', 56);
INSERT INTO `district` VALUES (575, 'Huyện Khánh Sơn', 56);
INSERT INTO `district` VALUES (576, 'Huyện Trường Sa', 56);
INSERT INTO `district` VALUES (582, 'Thành phố Phan Rang-Tháp Chàm', 58);
INSERT INTO `district` VALUES (584, 'Huyện Bác Ái', 58);
INSERT INTO `district` VALUES (585, 'Huyện Ninh Sơn', 58);
INSERT INTO `district` VALUES (586, 'Huyện Ninh Hải', 58);
INSERT INTO `district` VALUES (587, 'Huyện Ninh Phước', 58);
INSERT INTO `district` VALUES (588, 'Huyện Thuận Bắc', 58);
INSERT INTO `district` VALUES (589, 'Huyện Thuận Nam', 58);
INSERT INTO `district` VALUES (593, 'Thành phố Phan Thiết', 60);
INSERT INTO `district` VALUES (594, 'Thị xã La Gi', 60);
INSERT INTO `district` VALUES (595, 'Huyện Tuy Phong', 60);
INSERT INTO `district` VALUES (596, 'Huyện Bắc Bình', 60);
INSERT INTO `district` VALUES (597, 'Huyện Hàm Thuận Bắc', 60);
INSERT INTO `district` VALUES (598, 'Huyện Hàm Thuận Nam', 60);
INSERT INTO `district` VALUES (599, 'Huyện Tánh Linh', 60);
INSERT INTO `district` VALUES (600, 'Huyện Đức Linh', 60);
INSERT INTO `district` VALUES (601, 'Huyện Hàm Tân', 60);
INSERT INTO `district` VALUES (602, 'Huyện Phú Quí', 60);
INSERT INTO `district` VALUES (608, 'Thành phố Kon Tum', 62);
INSERT INTO `district` VALUES (610, 'Huyện Đắk Glei', 62);
INSERT INTO `district` VALUES (611, 'Huyện Ngọc Hồi', 62);
INSERT INTO `district` VALUES (612, 'Huyện Đắk Tô', 62);
INSERT INTO `district` VALUES (613, 'Huyện Kon Plông', 62);
INSERT INTO `district` VALUES (614, 'Huyện Kon Rẫy', 62);
INSERT INTO `district` VALUES (615, 'Huyện Đắk Hà', 62);
INSERT INTO `district` VALUES (616, 'Huyện Sa Thầy', 62);
INSERT INTO `district` VALUES (617, 'Huyện Tu Mơ Rông', 62);
INSERT INTO `district` VALUES (618, 'Huyện Ia H\' Drai', 62);
INSERT INTO `district` VALUES (622, 'Thành phố Pleiku', 64);
INSERT INTO `district` VALUES (623, 'Thị xã An Khê', 64);
INSERT INTO `district` VALUES (624, 'Thị xã Ayun Pa', 64);
INSERT INTO `district` VALUES (625, 'Huyện KBang', 64);
INSERT INTO `district` VALUES (626, 'Huyện Đăk Đoa', 64);
INSERT INTO `district` VALUES (627, 'Huyện Chư Păh', 64);
INSERT INTO `district` VALUES (628, 'Huyện Ia Grai', 64);
INSERT INTO `district` VALUES (629, 'Huyện Mang Yang', 64);
INSERT INTO `district` VALUES (630, 'Huyện Kông Chro', 64);
INSERT INTO `district` VALUES (631, 'Huyện Đức Cơ', 64);
INSERT INTO `district` VALUES (632, 'Huyện Chư Prông', 64);
INSERT INTO `district` VALUES (633, 'Huyện Chư Sê', 64);
INSERT INTO `district` VALUES (634, 'Huyện Đăk Pơ', 64);
INSERT INTO `district` VALUES (635, 'Huyện Ia Pa', 64);
INSERT INTO `district` VALUES (637, 'Huyện Krông Pa', 64);
INSERT INTO `district` VALUES (638, 'Huyện Phú Thiện', 64);
INSERT INTO `district` VALUES (639, 'Huyện Chư Pưh', 64);
INSERT INTO `district` VALUES (643, 'Thành phố Buôn Ma Thuột', 66);
INSERT INTO `district` VALUES (644, 'Thị Xã Buôn Hồ', 66);
INSERT INTO `district` VALUES (645, 'Huyện Ea H\'leo', 66);
INSERT INTO `district` VALUES (646, 'Huyện Ea Súp', 66);
INSERT INTO `district` VALUES (647, 'Huyện Buôn Đôn', 66);
INSERT INTO `district` VALUES (648, 'Huyện Cư M\'gar', 66);
INSERT INTO `district` VALUES (649, 'Huyện Krông Búk', 66);
INSERT INTO `district` VALUES (650, 'Huyện Krông Năng', 66);
INSERT INTO `district` VALUES (651, 'Huyện Ea Kar', 66);
INSERT INTO `district` VALUES (652, 'Huyện M\'Đrắk', 66);
INSERT INTO `district` VALUES (653, 'Huyện Krông Bông', 66);
INSERT INTO `district` VALUES (654, 'Huyện Krông Pắc', 66);
INSERT INTO `district` VALUES (655, 'Huyện Krông A Na', 66);
INSERT INTO `district` VALUES (656, 'Huyện Lắk', 66);
INSERT INTO `district` VALUES (657, 'Huyện Cư Kuin', 66);
INSERT INTO `district` VALUES (660, 'Thị xã Gia Nghĩa', 67);
INSERT INTO `district` VALUES (661, 'Huyện Đăk Glong', 67);
INSERT INTO `district` VALUES (662, 'Huyện Cư Jút', 67);
INSERT INTO `district` VALUES (663, 'Huyện Đắk Mil', 67);
INSERT INTO `district` VALUES (664, 'Huyện Krông Nô', 67);
INSERT INTO `district` VALUES (665, 'Huyện Đắk Song', 67);
INSERT INTO `district` VALUES (666, 'Huyện Đắk R\'Lấp', 67);
INSERT INTO `district` VALUES (667, 'Huyện Tuy Đức', 67);
INSERT INTO `district` VALUES (672, 'Thành phố Đà Lạt', 68);
INSERT INTO `district` VALUES (673, 'Thành phố Bảo Lộc', 68);
INSERT INTO `district` VALUES (674, 'Huyện Đam Rông', 68);
INSERT INTO `district` VALUES (675, 'Huyện Lạc Dương', 68);
INSERT INTO `district` VALUES (676, 'Huyện Lâm Hà', 68);
INSERT INTO `district` VALUES (677, 'Huyện Đơn Dương', 68);
INSERT INTO `district` VALUES (678, 'Huyện Đức Trọng', 68);
INSERT INTO `district` VALUES (679, 'Huyện Di Linh', 68);
INSERT INTO `district` VALUES (680, 'Huyện Bảo Lâm', 68);
INSERT INTO `district` VALUES (681, 'Huyện Đạ Huoai', 68);
INSERT INTO `district` VALUES (682, 'Huyện Đạ Tẻh', 68);
INSERT INTO `district` VALUES (683, 'Huyện Cát Tiên', 68);
INSERT INTO `district` VALUES (688, 'Thị xã Phước Long', 70);
INSERT INTO `district` VALUES (689, 'Thị xã Đồng Xoài', 70);
INSERT INTO `district` VALUES (690, 'Thị xã Bình Long', 70);
INSERT INTO `district` VALUES (691, 'Huyện Bù Gia Mập', 70);
INSERT INTO `district` VALUES (692, 'Huyện Lộc Ninh', 70);
INSERT INTO `district` VALUES (693, 'Huyện Bù Đốp', 70);
INSERT INTO `district` VALUES (694, 'Huyện Hớn Quản', 70);
INSERT INTO `district` VALUES (695, 'Huyện Đồng Phú', 70);
INSERT INTO `district` VALUES (696, 'Huyện Bù Đăng', 70);
INSERT INTO `district` VALUES (697, 'Huyện Chơn Thành', 70);
INSERT INTO `district` VALUES (698, 'Huyện Phú Riềng', 70);
INSERT INTO `district` VALUES (703, 'Thành phố Tây Ninh', 72);
INSERT INTO `district` VALUES (705, 'Huyện Tân Biên', 72);
INSERT INTO `district` VALUES (706, 'Huyện Tân Châu', 72);
INSERT INTO `district` VALUES (707, 'Huyện Dương Minh Châu', 72);
INSERT INTO `district` VALUES (708, 'Huyện Châu Thành', 72);
INSERT INTO `district` VALUES (709, 'Huyện Hòa Thành', 72);
INSERT INTO `district` VALUES (710, 'Huyện Gò Dầu', 72);
INSERT INTO `district` VALUES (711, 'Huyện Bến Cầu', 72);
INSERT INTO `district` VALUES (712, 'Huyện Trảng Bàng', 72);
INSERT INTO `district` VALUES (718, 'Thành phố Thủ Dầu Một', 74);
INSERT INTO `district` VALUES (719, 'Huyện Bàu Bàng', 74);
INSERT INTO `district` VALUES (720, 'Huyện Dầu Tiếng', 74);
INSERT INTO `district` VALUES (721, 'Thị xã Bến Cát', 74);
INSERT INTO `district` VALUES (722, 'Huyện Phú Giáo', 74);
INSERT INTO `district` VALUES (723, 'Thị xã Tân Uyên', 74);
INSERT INTO `district` VALUES (724, 'Thị xã Dĩ An', 74);
INSERT INTO `district` VALUES (725, 'Thị xã Thuận An', 74);
INSERT INTO `district` VALUES (726, 'Huyện Bắc Tân Uyên', 74);
INSERT INTO `district` VALUES (731, 'Thành phố Biên Hòa', 75);
INSERT INTO `district` VALUES (732, 'Thị xã Long Khánh', 75);
INSERT INTO `district` VALUES (734, 'Huyện Tân Phú', 75);
INSERT INTO `district` VALUES (735, 'Huyện Vĩnh Cửu', 75);
INSERT INTO `district` VALUES (736, 'Huyện Định Quán', 75);
INSERT INTO `district` VALUES (737, 'Huyện Trảng Bom', 75);
INSERT INTO `district` VALUES (738, 'Huyện Thống Nhất', 75);
INSERT INTO `district` VALUES (739, 'Huyện Cẩm Mỹ', 75);
INSERT INTO `district` VALUES (740, 'Huyện Long Thành', 75);
INSERT INTO `district` VALUES (741, 'Huyện Xuân Lộc', 75);
INSERT INTO `district` VALUES (742, 'Huyện Nhơn Trạch', 75);
INSERT INTO `district` VALUES (747, 'Thành phố Vũng Tàu', 77);
INSERT INTO `district` VALUES (748, 'Thành phố Bà Rịa', 77);
INSERT INTO `district` VALUES (750, 'Huyện Châu Đức', 77);
INSERT INTO `district` VALUES (751, 'Huyện Xuyên Mộc', 77);
INSERT INTO `district` VALUES (752, 'Huyện Long Điền', 77);
INSERT INTO `district` VALUES (753, 'Huyện Đất Đỏ', 77);
INSERT INTO `district` VALUES (754, 'Huyện Tân Thành', 77);
INSERT INTO `district` VALUES (755, 'Huyện Côn Đảo', 77);
INSERT INTO `district` VALUES (760, 'Quận 1', 79);
INSERT INTO `district` VALUES (761, 'Quận 12', 79);
INSERT INTO `district` VALUES (762, 'Quận Thủ Đức', 79);
INSERT INTO `district` VALUES (763, 'Quận 9', 79);
INSERT INTO `district` VALUES (764, 'Quận Gò Vấp', 79);
INSERT INTO `district` VALUES (765, 'Quận Bình Thạnh', 79);
INSERT INTO `district` VALUES (766, 'Quận Tân Bình', 79);
INSERT INTO `district` VALUES (767, 'Quận Tân Phú', 79);
INSERT INTO `district` VALUES (768, 'Quận Phú Nhuận', 79);
INSERT INTO `district` VALUES (769, 'Quận 2', 79);
INSERT INTO `district` VALUES (770, 'Quận 3', 79);
INSERT INTO `district` VALUES (771, 'Quận 10', 79);
INSERT INTO `district` VALUES (772, 'Quận 11', 79);
INSERT INTO `district` VALUES (773, 'Quận 4', 79);
INSERT INTO `district` VALUES (774, 'Quận 5', 79);
INSERT INTO `district` VALUES (775, 'Quận 6', 79);
INSERT INTO `district` VALUES (776, 'Quận 8', 79);
INSERT INTO `district` VALUES (777, 'Quận Bình Tân', 79);
INSERT INTO `district` VALUES (778, 'Quận 7', 79);
INSERT INTO `district` VALUES (783, 'Huyện Củ Chi', 79);
INSERT INTO `district` VALUES (784, 'Huyện Hóc Môn', 79);
INSERT INTO `district` VALUES (785, 'Huyện Bình Chánh', 79);
INSERT INTO `district` VALUES (786, 'Huyện Nhà Bè', 79);
INSERT INTO `district` VALUES (787, 'Huyện Cần Giờ', 79);
INSERT INTO `district` VALUES (794, 'Thành phố Tân An', 80);
INSERT INTO `district` VALUES (795, 'Thị xã Kiến Tường', 80);
INSERT INTO `district` VALUES (796, 'Huyện Tân Hưng', 80);
INSERT INTO `district` VALUES (797, 'Huyện Vĩnh Hưng', 80);
INSERT INTO `district` VALUES (798, 'Huyện Mộc Hóa', 80);
INSERT INTO `district` VALUES (799, 'Huyện Tân Thạnh', 80);
INSERT INTO `district` VALUES (800, 'Huyện Thạnh Hóa', 80);
INSERT INTO `district` VALUES (801, 'Huyện Đức Huệ', 80);
INSERT INTO `district` VALUES (802, 'Huyện Đức Hòa', 80);
INSERT INTO `district` VALUES (803, 'Huyện Bến Lức', 80);
INSERT INTO `district` VALUES (804, 'Huyện Thủ Thừa', 80);
INSERT INTO `district` VALUES (805, 'Huyện Tân Trụ', 80);
INSERT INTO `district` VALUES (806, 'Huyện Cần Đước', 80);
INSERT INTO `district` VALUES (807, 'Huyện Cần Giuộc', 80);
INSERT INTO `district` VALUES (808, 'Huyện Châu Thành', 80);
INSERT INTO `district` VALUES (815, 'Thành phố Mỹ Tho', 82);
INSERT INTO `district` VALUES (816, 'Thị xã Gò Công', 82);
INSERT INTO `district` VALUES (817, 'Thị xã Cai Lậy', 82);
INSERT INTO `district` VALUES (818, 'Huyện Tân Phước', 82);
INSERT INTO `district` VALUES (819, 'Huyện Cái Bè', 82);
INSERT INTO `district` VALUES (820, 'Huyện Cai Lậy', 82);
INSERT INTO `district` VALUES (821, 'Huyện Châu Thành', 82);
INSERT INTO `district` VALUES (822, 'Huyện Chợ Gạo', 82);
INSERT INTO `district` VALUES (823, 'Huyện Gò Công Tây', 82);
INSERT INTO `district` VALUES (824, 'Huyện Gò Công Đông', 82);
INSERT INTO `district` VALUES (825, 'Huyện Tân Phú Đông', 82);
INSERT INTO `district` VALUES (829, 'Thành phố Bến Tre', 83);
INSERT INTO `district` VALUES (831, 'Huyện Châu Thành', 83);
INSERT INTO `district` VALUES (832, 'Huyện Chợ Lách', 83);
INSERT INTO `district` VALUES (833, 'Huyện Mỏ Cày Nam', 83);
INSERT INTO `district` VALUES (834, 'Huyện Giồng Trôm', 83);
INSERT INTO `district` VALUES (835, 'Huyện Bình Đại', 83);
INSERT INTO `district` VALUES (836, 'Huyện Ba Tri', 83);
INSERT INTO `district` VALUES (837, 'Huyện Thạnh Phú', 83);
INSERT INTO `district` VALUES (838, 'Huyện Mỏ Cày Bắc', 83);
INSERT INTO `district` VALUES (842, 'Thành phố Trà Vinh', 84);
INSERT INTO `district` VALUES (844, 'Huyện Càng Long', 84);
INSERT INTO `district` VALUES (845, 'Huyện Cầu Kè', 84);
INSERT INTO `district` VALUES (846, 'Huyện Tiểu Cần', 84);
INSERT INTO `district` VALUES (847, 'Huyện Châu Thành', 84);
INSERT INTO `district` VALUES (848, 'Huyện Cầu Ngang', 84);
INSERT INTO `district` VALUES (849, 'Huyện Trà Cú', 84);
INSERT INTO `district` VALUES (850, 'Huyện Duyên Hải', 84);
INSERT INTO `district` VALUES (851, 'Thị xã Duyên Hải', 84);
INSERT INTO `district` VALUES (855, 'Thành phố Vĩnh Long', 86);
INSERT INTO `district` VALUES (857, 'Huyện Long Hồ', 86);
INSERT INTO `district` VALUES (858, 'Huyện Mang Thít', 86);
INSERT INTO `district` VALUES (859, 'Huyện  Vũng Liêm', 86);
INSERT INTO `district` VALUES (860, 'Huyện Tam Bình', 86);
INSERT INTO `district` VALUES (861, 'Thị xã Bình Minh', 86);
INSERT INTO `district` VALUES (862, 'Huyện Trà Ôn', 86);
INSERT INTO `district` VALUES (863, 'Huyện Bình Tân', 86);
INSERT INTO `district` VALUES (866, 'Thành phố Cao Lãnh', 87);
INSERT INTO `district` VALUES (867, 'Thành phố Sa Đéc', 87);
INSERT INTO `district` VALUES (868, 'Thị xã Hồng Ngự', 87);
INSERT INTO `district` VALUES (869, 'Huyện Tân Hồng', 87);
INSERT INTO `district` VALUES (870, 'Huyện Hồng Ngự', 87);
INSERT INTO `district` VALUES (871, 'Huyện Tam Nông', 87);
INSERT INTO `district` VALUES (872, 'Huyện Tháp Mười', 87);
INSERT INTO `district` VALUES (873, 'Huyện Cao Lãnh', 87);
INSERT INTO `district` VALUES (874, 'Huyện Thanh Bình', 87);
INSERT INTO `district` VALUES (875, 'Huyện Lấp Vò', 87);
INSERT INTO `district` VALUES (876, 'Huyện Lai Vung', 87);
INSERT INTO `district` VALUES (877, 'Huyện Châu Thành', 87);
INSERT INTO `district` VALUES (883, 'Thành phố Long Xuyên', 89);
INSERT INTO `district` VALUES (884, 'Thành phố Châu Đốc', 89);
INSERT INTO `district` VALUES (886, 'Huyện An Phú', 89);
INSERT INTO `district` VALUES (887, 'Thị xã Tân Châu', 89);
INSERT INTO `district` VALUES (888, 'Huyện Phú Tân', 89);
INSERT INTO `district` VALUES (889, 'Huyện Châu Phú', 89);
INSERT INTO `district` VALUES (890, 'Huyện Tịnh Biên', 89);
INSERT INTO `district` VALUES (891, 'Huyện Tri Tôn', 89);
INSERT INTO `district` VALUES (892, 'Huyện Châu Thành', 89);
INSERT INTO `district` VALUES (893, 'Huyện Chợ Mới', 89);
INSERT INTO `district` VALUES (894, 'Huyện Thoại Sơn', 89);
INSERT INTO `district` VALUES (899, 'Thành phố Rạch Giá', 91);
INSERT INTO `district` VALUES (900, 'Thị xã Hà Tiên', 91);
INSERT INTO `district` VALUES (902, 'Huyện Kiên Lương', 91);
INSERT INTO `district` VALUES (903, 'Huyện Hòn Đất', 91);
INSERT INTO `district` VALUES (904, 'Huyện Tân Hiệp', 91);
INSERT INTO `district` VALUES (905, 'Huyện Châu Thành', 91);
INSERT INTO `district` VALUES (906, 'Huyện Giồng Riềng', 91);
INSERT INTO `district` VALUES (907, 'Huyện Gò Quao', 91);
INSERT INTO `district` VALUES (908, 'Huyện An Biên', 91);
INSERT INTO `district` VALUES (909, 'Huyện An Minh', 91);
INSERT INTO `district` VALUES (910, 'Huyện Vĩnh Thuận', 91);
INSERT INTO `district` VALUES (911, 'Huyện Phú Quốc', 91);
INSERT INTO `district` VALUES (912, 'Huyện Kiên Hải', 91);
INSERT INTO `district` VALUES (913, 'Huyện U Minh Thượng', 91);
INSERT INTO `district` VALUES (914, 'Huyện Giang Thành', 91);
INSERT INTO `district` VALUES (916, 'Quận Ninh Kiều', 92);
INSERT INTO `district` VALUES (917, 'Quận Ô Môn', 92);
INSERT INTO `district` VALUES (918, 'Quận Bình Thuỷ', 92);
INSERT INTO `district` VALUES (919, 'Quận Cái Răng', 92);
INSERT INTO `district` VALUES (923, 'Quận Thốt Nốt', 92);
INSERT INTO `district` VALUES (924, 'Huyện Vĩnh Thạnh', 92);
INSERT INTO `district` VALUES (925, 'Huyện Cờ Đỏ', 92);
INSERT INTO `district` VALUES (926, 'Huyện Phong Điền', 92);
INSERT INTO `district` VALUES (927, 'Huyện Thới Lai', 92);
INSERT INTO `district` VALUES (930, 'Thành phố Vị Thanh', 93);
INSERT INTO `district` VALUES (931, 'Thị xã Ngã Bảy', 93);
INSERT INTO `district` VALUES (932, 'Huyện Châu Thành A', 93);
INSERT INTO `district` VALUES (933, 'Huyện Châu Thành', 93);
INSERT INTO `district` VALUES (934, 'Huyện Phụng Hiệp', 93);
INSERT INTO `district` VALUES (935, 'Huyện Vị Thuỷ', 93);
INSERT INTO `district` VALUES (936, 'Huyện Long Mỹ', 93);
INSERT INTO `district` VALUES (937, 'Thị xã Long Mỹ', 93);
INSERT INTO `district` VALUES (941, 'Thành phố Sóc Trăng', 94);
INSERT INTO `district` VALUES (942, 'Huyện Châu Thành', 94);
INSERT INTO `district` VALUES (943, 'Huyện Kế Sách', 94);
INSERT INTO `district` VALUES (944, 'Huyện Mỹ Tú', 94);
INSERT INTO `district` VALUES (945, 'Huyện Cù Lao Dung', 94);
INSERT INTO `district` VALUES (946, 'Huyện Long Phú', 94);
INSERT INTO `district` VALUES (947, 'Huyện Mỹ Xuyên', 94);
INSERT INTO `district` VALUES (948, 'Thị xã Ngã Năm', 94);
INSERT INTO `district` VALUES (949, 'Huyện Thạnh Trị', 94);
INSERT INTO `district` VALUES (950, 'Thị xã Vĩnh Châu', 94);
INSERT INTO `district` VALUES (951, 'Huyện Trần Đề', 94);
INSERT INTO `district` VALUES (954, 'Thành phố Bạc Liêu', 95);
INSERT INTO `district` VALUES (956, 'Huyện Hồng Dân', 95);
INSERT INTO `district` VALUES (957, 'Huyện Phước Long', 95);
INSERT INTO `district` VALUES (958, 'Huyện Vĩnh Lợi', 95);
INSERT INTO `district` VALUES (959, 'Thị xã Giá Rai', 95);
INSERT INTO `district` VALUES (960, 'Huyện Đông Hải', 95);
INSERT INTO `district` VALUES (961, 'Huyện Hoà Bình', 95);
INSERT INTO `district` VALUES (964, 'Thành phố Cà Mau', 96);
INSERT INTO `district` VALUES (966, 'Huyện U Minh', 96);
INSERT INTO `district` VALUES (967, 'Huyện Thới Bình', 96);
INSERT INTO `district` VALUES (968, 'Huyện Trần Văn Thời', 96);
INSERT INTO `district` VALUES (969, 'Huyện Cái Nước', 96);
INSERT INTO `district` VALUES (970, 'Huyện Đầm Dơi', 96);
INSERT INTO `district` VALUES (971, 'Huyện Năm Căn', 96);
INSERT INTO `district` VALUES (972, 'Huyện Phú Tân', 96);
INSERT INTO `district` VALUES (973, 'Huyện Ngọc Hiển', 96);

insert into cart()
values(1, 3, 50);
insert into cart()
values(2, 3, 50);
insert into cart()
values(3, 3, 50);
insert into cart()
values(4, 3, 50);
insert into cart()
values(5, 3, 50);

insert into cart()
values(1, 2, 50);
insert into cart()
values(2, 2, 50);
insert into cart()
values(3, 2, 50);
insert into cart()
values(4, 2, 50);
insert into cart()
values(5, 2, 50);

insert into cart()
values(1, 1, 50);
insert into cart()
values(2, 1, 50);
insert into cart()
values(3, 1, 50);
insert into cart()
values(4, 1, 50);
insert into cart()
values(5, 1, 50);

insert into cart()
values(1, 7, 50);
insert into cart()
values(2, 7, 50);
insert into cart()
values(3, 7, 50);
insert into cart()
values(4, 7, 50);
insert into cart()
values(5, 7, 50);

insert into question_answer
values(null, "Pin online liên tục được hơn 8 giờ bạn ạ.", "Máy pin có trâu không?", 1);
insert into question_answer
values(null, "Bảo hành trọn đời hư bỏ anh ạ.", "Bảo hành trong bao lâu?", 1);
insert into question_answer
values(null, "Máy chơi được cả Aspha 8 bạn nha.", "Máy có chơi được PUBG không vậy?", 1);
insert into question_answer
values(null, "Có bạn nhé.", "Máy có 4G?", 1);


insert into viewed
values(3, 1);
insert into viewed
values(3, 2);
insert into viewed
values(3, 3);
insert into viewed
values(3, 4);
insert into viewed
values(3, 5);
insert into viewed
values(3, 6);
insert into viewed
values(3, 7);
insert into viewed
values(3, 8);
insert into viewed
values(3, 9);
insert into viewed
values(3, 10);
insert into viewed
values(3, 11);
insert into viewed
values(3, 12);
insert into viewed
values(3, 13);
insert into viewed
values(3, 14);
insert into viewed
values(3, 15);
insert into viewed
values(3, 16);
insert into viewed
values(2, 16);

insert into question_answer
values(null, "Pin online liên tục được hơn 8 giờ bạn ạ.", "Máy pin có trâu không?", 1);
insert into question_answer
values(null, "Bảo hành trọn đời hư bỏ anh ạ.", "Bảo hành trong bao lâu?", 1);
insert into question_answer
values(null, "Máy chơi được cả Aspha 8 bạn nha.", "Máy có chơi được PUBG không vậy?", 1);
insert into question_answer
values(null, "Có bạn nhé.", "Máy có 4G?", 1);

insert into account_role
values(null, "user");
insert into account_role
values(null, "admin");

insert into account_detail
values(null, "123@gmail.com", null, 123, "JDEVD006", 1);
insert into account_detail
values(null, "1234@gmail.com", null, 123, "JDEVD006", 2);
