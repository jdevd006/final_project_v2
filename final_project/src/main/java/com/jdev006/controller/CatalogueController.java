package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Category_service;

@Controller
@RequestMapping(value = "/categoryController")
public class CatalogueController {
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ModelAndView categoryPage(@ModelAttribute Category category,
			@RequestParam(value = "lang", defaultValue = "en") String language) {

		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_Catalog");
		model.addObject("categories", categoryServices.getAllIdAsc());
		model.addObject("even", categoryServices.getAllIdAsc().size() / 6);
		return model;
	}

	@RequestMapping(value = "/category", method = RequestMethod.POST)
	public String addCategory(@ModelAttribute Category category, @RequestParam String btnSaveInModal) {
		System.out.println("sdsdsd : " + btnSaveInModal.split("-")[0]);
		if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {
			category.setCate_id(Integer.parseInt(btnSaveInModal.split("-")[1]));
			categoryServices.update(category);
		} else {
			category.setStatus("activated");
			categoryServices.add(category);
		}
		return "redirect:/categoryController/categories";
	}

	@RequestMapping(value = "/category/{cate_id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable() int cate_id) {
		categoryServices.delete(cate_id);
	}

	private class CateAndId {
		private Category cate;
		private int id;

		public CateAndId(Category cate, int id) {
			this.cate = cate;
			this.id = id;
		}

		public CateAndId() {
		}

		public Category getCate() {
			return cate;
		}

		public void setCate(Category cate) {
			this.cate = cate;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

	}

	/*
	 * @RequestMapping(value = "/getcategories", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public List<Category> getCategoriesByOrder(@RequestParam("by")
	 * String by) { List<Category> categories = new ArrayList<Category>(); if
	 * (by.equalsIgnoreCase("nameDsc")) categories =
	 * categoryServices.getAllAlphaDsc(); else if (by.equalsIgnoreCase("nameAsc"))
	 * categories = categoryServices.getAllAlphaAsc(); else if
	 * (by.equalsIgnoreCase("idDsc")) categories = categoryServices.getAllIdDsc();
	 * else categories = categoryServices.getAllIdAsc(); return categories; }
	 */
	
	@RequestMapping(value = "/getcategories", method = RequestMethod.GET)
	@ResponseBody
	public List<CateAndId> getCategoriesByOrder(@RequestParam("by") String by) {
		List<Category> categories = new ArrayList<Category>();
		List<CateAndId> list = new ArrayList<CatalogueController.CateAndId>();
		if (by.equalsIgnoreCase("nameDsc"))
			categories = categoryServices.getAllAlphaDsc();
		else if (by.equalsIgnoreCase("nameAsc"))
			categories = categoryServices.getAllAlphaAsc();
		else if (by.equalsIgnoreCase("idDsc"))
			categories = categoryServices.getAllIdDsc();
		else
			categories = categoryServices.getAllIdAsc();
		for (Category c : categories)
			list.add(new CateAndId(c, c.getCate_id()));
		return list;
	}

	@RequestMapping(value = "/block/{cateId}", method = RequestMethod.POST)
	@ResponseBody
	public void block(@PathVariable("cateId") int cateId) {
		Category cate = categoryServices.get(cateId);
		cate.setStatus("blocked");
		categoryServices.update(cate);
	}

	@RequestMapping(value = "/activate/{cateId}", method = RequestMethod.POST)
	@ResponseBody
	public void activate(@PathVariable("cateId") int cateId) {
		Category cate = categoryServices.get(cateId);
		cate.setStatus("activated");
		categoryServices.update(cate);
	}

	@RequestMapping(value = "/getcategory/{cateId}", method = RequestMethod.GET)
	@ResponseBody
	public Category getACategory(@PathVariable("cateId") int cateId) {
		Category cate = categoryServices.get(cateId);
		return cate;
	}
	
	@RequestMapping(value = "/getcates", method = RequestMethod.GET)
	@ResponseBody
	public List<CateAndId> getCategoriesSearching(@RequestParam("key") String key) {
		List<Category> categories = new ArrayList<Category>();
		List<CateAndId> list = new ArrayList<CatalogueController.CateAndId>();
		categories = categoryServices.getResultOfSearching(key);
		for (Category c : categories)
			list.add(new CateAndId(c, c.getCate_id()));
		return list;
	}
}
