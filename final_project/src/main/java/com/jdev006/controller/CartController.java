package com.jdev006.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.BillDetailId;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.CartId;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.City;
import com.jdev006.entitties.District;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.SendMail;
import com.jdev006.entitties.ShippingAddress;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.BillDetaiService;
import com.jdev006.service.Bill_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Category_service;
import com.jdev006.service.City_services;
import com.jdev006.service.District_services;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;
import com.jdev006.service.ShippingAddressServices;

@Controller
@RequestMapping(value = "/cartController")
public class CartController {
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private City_services cityServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private District_services districtServices;
	@Autowired
	private Bill_service billServices;
	@Autowired
	private BillDetaiService billDetailServices;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private ShippingAddressServices shippingAddressServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/cartPage", method = RequestMethod.GET)
	public String cartPage(ModelMap model, HttpSession httpSession,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		int numItem = 0;
		ArrayList<Item> listItems = new ArrayList<Item>();
		ArrayList<Image> listImage = new ArrayList<Image>();
		ArrayList<Image> listAllImage = (ArrayList<Image>) imgServices.getAll();
		ArrayList<ShippingAddress> listShippingAddress = new ArrayList<ShippingAddress>();

		if (httpSession.getAttribute("currentUser") != null) {
			Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == acc.getUser_id()) {
					numItem += cart.getNum();
					listItems.add(new Item(productServices.get(cart.getId().getpro_id()), cart.getNum()));
				}
			}
			for (Item item : listItems) {
				for (Image img : listAllImage) {
					if (item.getProduct().getPro_id() == img.getProduct().getPro_id()) {
						listImage.add(img);
						break;
					}
				}
			}
			listShippingAddress = (ArrayList<ShippingAddress>) shippingAddressServices
					.getListByUserId(acc.getUser_id());
		} else {
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == 0) {
					numItem += cart.getNum();
					listItems.add(new Item(productServices.get(cart.getId().getpro_id()), cart.getNum()));
				}
			}
			for (Item item : listItems) {
				for (Image img : listAllImage) {
					if (item.getProduct().getPro_id() == img.getProduct().getPro_id()) {
						listImage.add(img);
						break;
					}
				}
			}

		}

		model.addAttribute("listItems", listItems);
		model.addAttribute("listImage", listImage);
		model.addAttribute("cartItems", numItem);
		model.addAttribute("addresses", listShippingAddress);
		model.addAttribute("cartCategories", categoryServices.getAll());
		return "cartPage";
	}

	@RequestMapping(value = "/item", method = RequestMethod.POST)
	@ResponseBody
	public void addItem(@RequestParam("proid") int pro_id, @RequestParam("task") String task) {
		if (task.equalsIgnoreCase("descrease")) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == pro_id) {
					if (c.getNum() > 1) {
						c.setNum(c.getNum() - 1);
						cartServices.update(c);
					}
					Product product = productServices.getAProduct(pro_id);
					product.setPro_quantity(product.getPro_quantity() + 1);
					productServices.update(product);
					break;
				}
			}
		}
		if (task.equalsIgnoreCase("inscrease")) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == pro_id) {
					c.setNum(c.getNum() + 1);
					cartServices.update(c);
				}
				Product product = productServices.getAProduct(pro_id);
				product.setPro_quantity(product.getPro_quantity() - 1);
				productServices.update(product);
				break;
			}
		}
	}

	@RequestMapping(value = "/item", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteItem(@RequestParam("proid") int pro_id, HttpSession httpSession) {
		Cart cart = new Cart();
		CartId c = new CartId(pro_id, ((Account_detail) httpSession.getAttribute("currentUser")).getUser_id());
		cart.setId(c);
		cartServices.delete(cart);
	}

	@RequestMapping(value = "/cities", method = RequestMethod.GET)
	@ResponseBody
	public List<City> returnCity() {
		List<City> cities = new ArrayList<City>();
		cities = cityServices.getAll();
		cityServices.getAll2();
		return cities;
	}

	@RequestMapping(value = "/districts/{city_id}", method = RequestMethod.GET)
	@ResponseBody
	public List<District> returnDistrict(@PathVariable("city_id") int city_id) {
		List<District> districts = new ArrayList<District>();
		districts = districtServices.getListByCityId(city_id);
		System.out.println(city_id);
		return districts;
	}

	@RequestMapping(value = "/bill", method = RequestMethod.POST)
	@ResponseBody
	public String[] handlBuy(@RequestParam("data") String data, HttpSession httpSession,
			@RequestParam("more") String more) {
		// nguyen
		// lam
		// thanh,nlthanhititu17038@gmail.com,0964774964,1,1,750000,MasterCart/Visa-1.50,2.50,3.50,&more=sdssasdasd
		Account_detail account = null;
		String[] main = data.split("-");
		String[] details = main[0].split(",");
		String[] proIds = main[1].split(",");
		Bill bill = new Bill();

		Calendar c = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = c.getTime();
		if (httpSession.getAttribute("currentUser") != null) {
			account = (Account_detail) httpSession.getAttribute("currentUser");
			bill.setTotal(Integer.parseInt(details[5]));
			bill.setAccount_detail(account);
			bill.setOrder_date(format.format(date));
			c.add(Calendar.DAY_OF_MONTH, 2);
			bill.setDue_date(format.format(c.getTime()));

			try {
				Integer.parseInt(details[4]);
				bill.setAddress(cityServices.get(Integer.parseInt(details[3])).getName() + "-"
						+ districtServices.get(Integer.parseInt(details[4])).getName() + "-" + more);
			} catch (Exception e) {
				bill.setAddress(details[3] + "-" + details[4] + "-" + more);
			}

			bill.setName(details[0]);
			bill.setPhone(details[2]);
			bill.setEmail(details[1]);
			bill.setStatus("Waiting");
			bill.setPaymentMode(details[6]);
			billServices.add(bill);
		} else {
			bill.setTotal(Integer.parseInt(details[5]));
			bill.setAccount_detail(null);
			bill.setOrder_date(format.format(date));
			c.add(Calendar.DAY_OF_MONTH, 2);
			bill.setDue_date(format.format(c.getTime()));
			bill.setAddress(cityServices.get(Integer.parseInt(details[3])).getName() + "-"
					+ districtServices.get(Integer.parseInt(details[4])).getName() + "-" + more);
			bill.setName(details[0]);
			bill.setPhone(details[2]);
			bill.setEmail(details[1]);
			bill.setStatus("Waiting");
			bill.setPaymentMode(details[6]);
			billServices.add(bill);
		}
		return proIds;
	}

	@RequestMapping(value = "/billdetail", method = RequestMethod.POST)
	@ResponseBody
	public Bill handleBillDetai(@RequestParam("data") String data, HttpSession httpSession) {
		System.out.println(data);
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		BillDetail billDetail = new BillDetail();
		String[] proIds = data.split(",");
		BillDetailId billDetailId = new BillDetailId();
		billDetailId.setBill_id(billServices.getAll().get(billServices.getAll().size() - 1).getBill_id());
		Product prouduct = new Product();
		if (httpSession.getAttribute("currentUser") != null) {
			for (int i = 0; i < proIds.length; i++) {
				billDetailId.setPro_id(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				prouduct = productServices.get(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				billDetail.setNum(Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				prouduct.setPro_quantity(
						prouduct.getPro_quantity() - Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				billDetail.setTotal(prouduct.getPro_cost() * billDetail.getNum());
				prouduct.setBuyTime(
						prouduct.getBuyTime() + Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				productServices.update(prouduct);
				billServices.deteleByProId(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))),
						account.getUser_id());
				billDetail.setId(billDetailId);
				billDetailServices.add(billDetail);
			}

			Bill bill = new Bill();
			bill = billServices.getAll().get(billServices.getAll().size() - 1);
			List<BillDetail> billDetails = (List<BillDetail>) billDetailServices.getListByBillId(bill.getBill_id());

			String form = "<div id=\"warpper\"\r\n"
					+ "										style=\"display: flex; flex-direction: column;\"\r\n"
					+ "										class=\"container\">\r\n"
					+ "										<div class=\"row justify-content-md-center\">\r\n"
					+ "											<div class=\"col-lg-8\">\r\n"
					+ "												<div id=\"logo\" style=\"text-align: center;\">\r\n"
					+ "													<h1 style=\"color: blue; text-align: center;\"><a href=\"http://localhost:8080/final_project/homecontroller/homepage\" style=\"text-decoration: none;\">TATALO</a></h1>\r\n"
					+ "												</div>\r\n"
					+ "												<div id=\"inform\">\r\n"
					+ "													<h4 id=\"sendTo\">Đơn hàng đã sẵn sàng để giao đến quý khách #sendTo#</h4>\r\n"
					+ "													<pChúng tôi vừa bàn giao đơn hàng của quý khách đến đối tác vận chuyển NinjaVan. Dự kiến giao hàng vào Thứ hai, 26/11/2018 -> Thứ ba, 27/11/2018</p>\r\n"
					+ "													<p id=\"billCode\">Thông tin đơn hàng ##billCode# (Ngày 24 tháng 11 năm 2018 13:29:48 +07)"
					+ "													<br>\r\n"
					+ "												</div>\r\n"
					+ "												<hr>\r\n"
					+ "												<div id=\"userInfo-container\" style=\"margin-top: -40px;\">\r\n"
					+ "													<table id=\"userInfo-table\" style=\"\">\r\n"
					+ "														<thead>\r\n"
					+ "															<tr>\r\n"
					+ "																<th style=\"width: 400px\">Thông tin thanh toán</th>\r\n"
					+ "																<th style=\"width: 400px\">Địa chỉ giao hàng</th>\r\n"
					+ "															</tr>\r\n"
					+ "														</thead>\r\n"
					+ "														<tbody>\r\n"
					+ "															<tr>\r\n"
					+ "																<td><p id=\"cust-name-info\">#custNameInfo#</p>\r\n"
					+ "																	<p id=\"cust-email-info\">#custEmailInfo#</p>\r\n"
					+ "																	<p id=\"cust-phone-info\"></p>#custPhoneInfo#</td>\r\n"
					+ "																<td>\r\n"
					+ "																	<p id=\"cust-name-shipping\">#custNameShipping#</p>\r\n"
					+ "																	<p id=\"cust-address-shipping\">#custAddressShipping#</p>\r\n"
					+ "																	<p id=\"cust-phone-shipping\">#custPhoneShipping#</p>\r\n"
					+ "																</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"2\">\r\n"
					+ "																	<p>\r\n"
					+ "																		<strong>Phí vận chuyển: </strong>0đ (Miễn phí)\r\n"
					+ "																	</p>\r\n"
					+ "																	<p>\r\n"
					+ "																		<strong>Phương thức thanh toán: </strong>#paymentMode#"
					+ "																	</p>\r\n"
					+ "																</td>\r\n"
					+ "															</tr>\r\n"
					+ "														</tbody>\r\n"
					+ "													</table>\r\n"
					+ "												</div>\r\n"
					+ "												<p>CHI TIẾT ĐƠN HÀNG</p>\r\n"
					+ "												<hr style=\"margin-top: -18px;\">\r\n"
					+ "												<div id=\"cartSummary-container\">\r\n"
					+ "													<table id=\"cartSummary-table\">\r\n"
					+ "														<thead style=\"background-color: #02ACEA;\">\r\n"
					+ "															<tr style=\"background-color: #02acea; text-align: left;\">\r\n"
					+ "																<th style=\"width: 300px; font-size: 14px;\">SẢN PHẨM</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">ĐƠN GIÁ</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">SỐ LƯỢNG</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">GIẢM GIÁ</th>\r\n"
					+ "																<th\r\n"
					+ "																	style=\"font-size: 14px; width: 100px; padding-right: 10px; text-align: right;\">TỔNG\r\n"
					+ "																	TẠM</th>\r\n"
					+ "															</tr>\r\n"
					+ "														</thead>\r\n"
					+ "														<tbody>\r\n"
					+ "															<!-- list sp -->\r\n"
					+ "															#cartSummary#\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Tổng giá trị sản phẩm chưa giảm</td>\r\n"
					+ "																<td style=\"text-align: right;\" id=\"first-total\">#firstTotal# ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Giảm giá Phiếu Quà Tặng</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Chi phí vận chuyển</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Phí xử lý đơn hàng</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr style=\"border-color: #eeeeee; font-weight: 700;\">\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Tổng giá trị đơn hàng</td>\r\n"
					+ "																<td style=\"text-align: right;\"\r\n"
					+ "																	id=\"total-cartSummary-table\">#totalCartSummaryTable# ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "														</tbody>\r\n"
					+ "													</table>\r\n"
					+ "												</div>\r\n"
					+ "												<p style=\"margin-top: 40px;\">Nếu sản phẩm có dấu hiệu hư hỏng/ bể vỡ hoặc không đúng với thông tin trên website, bạn vui lòng liên hệ với TATALO trong vòng 48 giờ kể từ thời điểm nhận hàng để được hỗ trợ.</p>\r\n"
					+ "												<p>Bạn có thể tham khảo thêm tại trang Trung tâm hỗ trợ hoặc liên hệ với TATALO bằng cách để lại tin nhắn tại trang Liên hệ hoặc gửi thư đến đây. Hotline 1900 6035 (8-21h cả T7,CN).</p>\r\n"
					+ "												<p>\r\n"
					+ "													<strong>TATALO.vn cảm ơn quý khách</strong>\r\n"
					+ "												</p>\r\n"
					+ "											</div>\r\n"
					+ "										</div>\r\n" + "									</div>";

			form = form.replace("#sendTo#", bill.getName());
			form = form.replace("#billCode#", bill.getBill_id() + "");
			form = form.replace("#custNameInfo#", account.getUser_name());
			form = form.replace("#custEmailInfo#", account.getEmail());
			form = form.replace("#custPhoneInfo#", account.getPhone());
			form = form.replace("#custNameShipping#", bill.getName());
			form = form.replace("#custAddressShipping#", bill.getAddress());
			form = form.replace("#custPhoneShipping#", bill.getPhone());
			form = form.replace("#firstTotal#", bill.getTotal() + "");
			form = form.replace("#totalCartSummaryTable#", bill.getTotal() + "");
			form = form.replace("#paymentMode#", bill.getPaymentMode());
			String rowProduct = "";
			for (BillDetail item : billDetails) {
				rowProduct += ("<tr style=\"background-color: #eeeeee;text-align:left;\">" + "<td>"
						+ productServices.getAProduct(item.getId().getpro_id()).getPro_name() + "</td>" + "<td>"
						+ productServices.getAProduct(item.getId().getpro_id()).getPro_cost() + "đ" + "</td>" + "<td>"
						+ item.getNum() + "</td>" + "<td>" + "0,00đ" + "</td>"
						+ "<td style=\"text-align:right;padding-right:10px;\">"
						+ (productServices.getAProduct(item.getId().getpro_id()).getPro_cost() * item.getNum()) + "đ"
						+ "</td>" + "</tr>");
			}
			form = form.replace("#cartSummary#", rowProduct);
			SendMail.send(form, bill.getEmail());
		} else {
			/*
			 * for (int i = 0; i < proIds.length; i++) {
			 * billDetailId.setPro_id(Integer.parseInt(proIds[i].substring(0,
			 * proIds[0].indexOf("."))));
			 * billServices.deteleByProId(Integer.parseInt(proIds[i].substring(0,
			 * proIds[0].indexOf("."))), 0); billDetail.setId(billDetailId);
			 * billDetailServices.add(billDetail); }
			 */

			for (int i = 0; i < proIds.length; i++) {
				billDetailId.setPro_id(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				prouduct = productServices.get(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				billDetail.setNum(Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				prouduct.setPro_quantity(
						prouduct.getPro_quantity() - Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				prouduct.setBuyTime(
						prouduct.getBuyTime() + Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".") + 1)));
				productServices.update(prouduct);
				billDetail.setTotal(prouduct.getPro_cost() * billDetail.getNum());
				billDetail.setId(billDetailId);
				billDetailServices.add(billDetail);
			}

			Bill bill = new Bill();
			bill = billServices.getAll().get(billServices.getAll().size() - 1);
			List<BillDetail> billDetails = (List<BillDetail>) billDetailServices.getListByBillId(bill.getBill_id());

			String form = "<div id=\"warpper\"\r\n"
					+ "										style=\"display: flex; flex-direction: column;\"\r\n"
					+ "										class=\"container\">\r\n"
					+ "										<div class=\"row justify-content-md-center\">\r\n"
					+ "											<div class=\"col-lg-8\">\r\n"
					+ "												<div id=\"logo\" style=\"text-align: center;\">\r\n"
					+ "													<h1 style=\"color: blue; text-align: center;\"><a href=\"http://localhost:8080/final_project/homecontroller/homepage\" style=\"text-decoration: none;\">TATALO</a></h1>\r\n"
					+ "												</div>\r\n"
					+ "												<div id=\"inform\">\r\n"
					+ "													<h4 id=\"sendTo\">Đơn hàng đã sẵn sàng để giao đến quý khách #sendTo#</h4>\r\n"
					+ "													<pChúng tôi vừa bàn giao đơn hàng của quý khách đến đối tác vận chuyển NinjaVan. Dự kiến giao hàng vào Thứ hai, 26/11/2018 -> Thứ ba, 27/11/2018</p>\r\n"
					+ "													<p id=\"billCode\">Thông tin đơn hàng ##billCode# (Ngày 24 tháng 11 năm 2018 13:29:48 +07)"
					+ "													<br>\r\n"
					+ "												</div>\r\n"
					+ "												<hr>\r\n"
					+ "												<div id=\"userInfo-container\" style=\"margin-top: -40px;\">\r\n"
					+ "													<table id=\"userInfo-table\" style=\"\">\r\n"
					+ "														<thead>\r\n"
					+ "															<tr>\r\n"
					+ "																<th style=\"width: 400px\">Thông tin thanh toán</th>\r\n"
					+ "																<th style=\"width: 400px\">Địa chỉ giao hàng</th>\r\n"
					+ "															</tr>\r\n"
					+ "														</thead>\r\n"
					+ "														<tbody>\r\n"
					+ "															<tr>\r\n"
					+ "																<td><p id=\"cust-name-info\">#custNameInfo#</p>\r\n"
					+ "																	<p id=\"cust-email-info\">#custEmailInfo#</p>\r\n"
					+ "																	<p id=\"cust-phone-info\"></p>#custPhoneInfo#</td>\r\n"
					+ "																<td>\r\n"
					+ "																	<p id=\"cust-name-shipping\">#custNameShipping#</p>\r\n"
					+ "																	<p id=\"cust-address-shipping\">#custAddressShipping#</p>\r\n"
					+ "																	<p id=\"cust-phone-shipping\">#custPhoneShipping#</p>\r\n"
					+ "																</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"2\">\r\n"
					+ "																	<p>\r\n"
					+ "																		<strong>Phí vận chuyển: </strong>0đ (Miễn phí)\r\n"
					+ "																	</p>\r\n"
					+ "																	<p>\r\n"
					+ "																		<strong>Phương thức thanh toán: </strong>#paymentMode#"
					+ "																	</p>\r\n"
					+ "																</td>\r\n"
					+ "															</tr>\r\n"
					+ "														</tbody>\r\n"
					+ "													</table>\r\n"
					+ "												</div>\r\n"
					+ "												<p>CHI TIẾT ĐƠN HÀNG</p>\r\n"
					+ "												<hr style=\"margin-top: -18px;\">\r\n"
					+ "												<div id=\"cartSummary-container\">\r\n"
					+ "													<table id=\"cartSummary-table\">\r\n"
					+ "														<thead style=\"background-color: #02ACEA;\">\r\n"
					+ "															<tr style=\"background-color: #02acea; text-align: left;\">\r\n"
					+ "																<th style=\"width: 300px; font-size: 14px;\">SẢN PHẨM</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">ĐƠN GIÁ</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">SỐ LƯỢNG</th>\r\n"
					+ "																<th style=\"font-size: 14px; width: 100px;\">GIẢM GIÁ</th>\r\n"
					+ "																<th\r\n"
					+ "																	style=\"font-size: 14px; width: 100px; padding-right: 10px; text-align: right;\">TỔNG\r\n"
					+ "																	TẠM</th>\r\n"
					+ "															</tr>\r\n"
					+ "														</thead>\r\n"
					+ "														<tbody>\r\n"
					+ "															<!-- list sp -->\r\n"
					+ "															#cartSummary#\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Tổng giá trị sản phẩm chưa giảm</td>\r\n"
					+ "																<td style=\"text-align: right;\" id=\"first-total\">#firstTotal# ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Giảm giá Phiếu Quà Tặng</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Chi phí vận chuyển</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr>\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Phí xử lý đơn hàng</td>\r\n"
					+ "																<td style=\"text-align: right;\">0,00 ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "															<tr style=\"border-color: #eeeeee; font-weight: 700;\">\r\n"
					+ "																<td colspan=\"4\"\r\n"
					+ "																	style=\"text-align: right; padding-right: 18px;\">Tổng giá trị đơn hàng</td>\r\n"
					+ "																<td style=\"text-align: right;\"\r\n"
					+ "																	id=\"total-cartSummary-table\">#totalCartSummaryTable# ₫</td>\r\n"
					+ "															</tr>\r\n"
					+ "														</tbody>\r\n"
					+ "													</table>\r\n"
					+ "												</div>\r\n"
					+ "												<p style=\"margin-top: 40px;\">Nếu sản phẩm có dấu hiệu hư hỏng/ bể vỡ hoặc không đúng với thông tin trên website, bạn vui lòng liên hệ với TATALO trong vòng 48 giờ kể từ thời điểm nhận hàng để được hỗ trợ.</p>\r\n"
					+ "												<p>Bạn có thể tham khảo thêm tại trang Trung tâm hỗ trợ hoặc liên hệ với TATALO bằng cách để lại tin nhắn tại trang Liên hệ hoặc gửi thư đến đây. Hotline 1900 6035 (8-21h cả T7,CN).</p>\r\n"
					+ "												<p>\r\n"
					+ "													<strong>TATALO.vn cảm ơn quý khách</strong>\r\n"
					+ "												</p>\r\n"
					+ "											</div>\r\n"
					+ "										</div>\r\n" + "									</div>";

			form = form.replace("#sendTo#", bill.getName());
			form = form.replace("#billCode#", bill.getBill_id() + "");
			form = form.replace("#custNameInfo#", bill.getName());
			form = form.replace("#custEmailInfo#", bill.getEmail());
			form = form.replace("#custPhoneInfo#", bill.getPhone());
			form = form.replace("#custNameShipping#", bill.getName());
			form = form.replace("#custAddressShipping#", bill.getAddress());
			form = form.replace("#custPhoneShipping#", bill.getPhone());
			form = form.replace("#firstTotal#", bill.getTotal() + "");
			form = form.replace("#totalCartSummaryTable#", bill.getTotal() + "");
			form = form.replace("#paymentMode#", bill.getPaymentMode());
			String rowProduct = "";
			for (BillDetail item : billDetails) {
				rowProduct += ("<tr style=\"background-color: #eeeeee;text-align:left;\">" + "<td>"
						+ productServices.getAProduct(item.getId().getpro_id()).getPro_name() + "</td>" + "<td>"
						+ productServices.getAProduct(item.getId().getpro_id()).getPro_cost() + "đ" + "</td>" + "<td>"
						+ item.getNum() + "</td>" + "<td>" + "0,00đ" + "</td>"
						+ "<td style=\"text-align:right;padding-right:10px;\">"
						+ (productServices.getAProduct(item.getId().getpro_id()).getPro_cost() * item.getNum()) + "đ"
						+ "</td>" + "</tr>");
			}
			form = form.replace("#cartSummary#", rowProduct);
			SendMail.send(form, bill.getEmail());
		}
		Bill bill = new Bill();
		bill = billServices.getAll().get(billServices.getAll().size() - 1);
		return bill;
	}

	@RequestMapping(value = "/billdetailreturn", method = RequestMethod.GET)
	@ResponseBody
	public Account_detail getBillDetail(HttpSession httpSession) {
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		Bill bill = new Bill();
		bill = billServices.getBillByUserId(account.getUser_id());
		List<Object> listObjects = new ArrayList<Object>();
		listObjects.add(bill);
		listObjects.add(account);
		return account;
	}

	@RequestMapping(value = "/sendmail/{content}/{email}", method = RequestMethod.POST)
	public void sendMail(@PathVariable("content") String content, @PathVariable("email") String email) {

	}

	@RequestMapping(value = "/product/{proId}", method = RequestMethod.GET)
	@ResponseBody
	public Product getAProduct(@PathVariable("proId") int proId) {
		Product product = new Product();
		product = productServices.getAProduct(proId);
		return product;
	}

	private class ProductAndImage {
		private Product product;
		private Image image;
		private int num;

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

		public Image getImage() {
			return image;
		}

		public void setImage(Image image) {
			this.image = image;
		}

		public int getNum() {
			return num;
		}

		public void setNum(int num) {
			this.num = num;
		}

		public ProductAndImage(Product product, Image image, int num) {
			this.product = product;
			this.image = image;
			this.num = num;
		}

		public ProductAndImage() {
		}
	}

	@RequestMapping(value = "/cartguest", method = RequestMethod.GET)
	@ResponseBody
	public List<ProductAndImage> gerCartGuest(@RequestParam("data") String data) {
		String[] detail = data.split(",");
		List<ProductAndImage> list = new ArrayList<CartController.ProductAndImage>();
		for (int i = 0; i < detail.length; i++) {
			list.add(new ProductAndImage(productServices.getAProduct(Integer.parseInt(detail[i].split("-")[0])),
					imgServices.getByProId(Integer.parseInt(detail[i].split("-")[0])),
					Integer.parseInt(detail[i].split("-")[1])));
		}
		return list;
	}

}
