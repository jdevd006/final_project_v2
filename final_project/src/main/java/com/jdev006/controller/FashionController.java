package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value="/fashioncontroller")
public class FashionController {
	@Autowired
	private Product_service productservice;
	@Autowired
	private Image_service imgservice;
	@Autowired
	private SessionLocaleResolver localeResolver;
	@RequestMapping(value = "/fashions", method = RequestMethod.GET)
	private ModelAndView Fashion(@ModelAttribute ("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value="lang", defaultValue="en") String language) {
		
		
		switch(language) {
		case "en": 
		{
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		}
		case "vi" :
		{
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		}
		default:
			break;
	}
		
		
		ModelAndView model = new ModelAndView();
		model.setViewName("fashion");

		List<Image> listimg_fashion = new ArrayList<>();
		List<Product> listfashion = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 3) {
				listfashion.add(pro);
			}
		}
		for (Product pro : listfashion) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_fashion.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_fashion.add(null);
			}
		}
		model.addObject("listimg_fashion", listimg_fashion);
		model.addObject("listfashion", productservice.getpro(3, 0));
		model.addObject("evenTr", productservice.getpro(3, 0).size()/4);
		model.addObject("num", listfashion);
		return model;
	}
	
	
	@RequestMapping(value = "/fashion", method = RequestMethod.GET)
	@ResponseBody //annotation tra ve ajax
	public ModelAndView getFashion(@RequestParam("pag") int startpoint){
		ModelAndView model = new ModelAndView();
		model.setViewName("fashion");
		List<Product> fashions = new ArrayList<>();
		fashions = productservice.getpro(3, startpoint);
		
		List<Image> listimg_fashion = new ArrayList<>();
		for (Product pro : fashions) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_fashion.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_fashion.add(null);
			}
		}
		
		List<Product> listfashion = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 3) {
				listfashion.add(pro);
			}
		}
		model.addObject("listimg_fashion", listimg_fashion);
		model.addObject("listfashion", fashions);
		model.addObject("num", listfashion);
		model.addObject("evenTr", productservice.getpro(3, 0).size()/4);
		return model;
	}
	
	
	

}
