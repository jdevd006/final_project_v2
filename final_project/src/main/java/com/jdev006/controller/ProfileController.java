package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Address;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.Message;
import com.jdev006.entitties.ShippingAddress;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Address_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.City_services;
import com.jdev006.service.District_services;
import com.jdev006.service.Message_service;
import com.jdev006.service.ShippingAddressServices;

@Controller
@RequestMapping("/profileController")
public class ProfileController {
	@Autowired
	private ShippingAddressServices shippingAddressServices;
	@Autowired
	private Account_detail_service accountDetailServices;
	@Autowired
	private Message_service messageServices;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private City_services cityServices;
	@Autowired
	private District_services districtServices;
	@Autowired
	private Address_service addressServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profilePage(HttpSession httpSession,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("userPage");
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		List<ShippingAddress> addresses = new ArrayList<ShippingAddress>();
		List<Message> messages = new ArrayList<Message>();
		int numItem = 0;
		if (account != null) {
			addresses = shippingAddressServices.getListByUserId(account.getUser_id());

			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == ((Account_detail) httpSession.getAttribute("currentUser"))
						.getUser_id())
					numItem += cart.getNum();
			}

			messages = messageServices.getMessagesByUserId(account.getUser_id());
		}
		model.addObject("addresses", addresses);
		model.addObject("messages", messages);
		model.addObject("cartItems", numItem);
		model.addObject("pws", TripleDES.Decrypt(account.getPassword(), "12345"));
		return model;
	}

	@RequestMapping(value = "/address/{addressId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteAddress(@PathVariable("addressId") int addressId) {
		shippingAddressServices.delete(shippingAddressServices.get(addressId));
	}

	@RequestMapping(value = "/address", method = RequestMethod.POST)
	@ResponseBody
	public void updateAdd(@RequestParam("task") String task, @RequestParam("data") String data,
			HttpSession httpSession) {
		String[] details = data.split(",");
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		if (task.equalsIgnoreCase("update")) {
			ShippingAddress shippingAddress = new ShippingAddress();
			shippingAddress = shippingAddressServices.get(Integer.parseInt(details[0]));
			shippingAddress.setName(details[1]);
			shippingAddress.setCity(details[2]);
			shippingAddress.setDistrict(details[3]);
			shippingAddressServices.update(shippingAddress);
		} else {
			ShippingAddress shippingAddress = new ShippingAddress();
			shippingAddress.setName(details[0]);
			shippingAddress.setCity(details[1]);
			shippingAddress.setDistrict(details[2]);
			shippingAddress.setAccount_detail(account);
			shippingAddressServices.add(shippingAddress);
		}
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	@ResponseBody
	public void updateProfile(@RequestParam("data") String data, HttpSession httpSession) {
		String[] details = data.split(",");
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		Address address = new Address();
		account.setUser_name(details[0]);
		account.setEmail(details[1]);
		account.setPhone(details[2]);
		account.setPassword(TripleDES.Encrypt(details[3], "12345"));
		if (account.getAddress() == null) {
			if (Integer.parseInt(details[4]) != -1 && Integer.parseInt(details[5]) != -1) {
				address.setCity(cityServices.get(Integer.parseInt(details[4])).getName());
				address.setDistrict(districtServices.get(Integer.parseInt(details[5])).getName());
				address.setStreet(details[6]);
				addressServices.add(address);
				address = addressServices.getAll().get(addressServices.getAll().size() - 1);
			}
		} else {
			if (Integer.parseInt(details[4]) != -1 && Integer.parseInt(details[5]) != -1) {
				address.setCity(cityServices.get(Integer.parseInt(details[4])).getName());
				address.setDistrict(districtServices.get(Integer.parseInt(details[5])).getName());
				address.setStreet(details[6]);
				address.setId(account.getAddress().getId());
			}
		}
		account.setAddress(address);
		address.setAccount_detail(account);
		accountDetailServices.update(account);
	}

	@RequestMapping(value = "/messages", method = RequestMethod.GET)
	@ResponseBody
	public List<Message> getMessagesAnswered(HttpSession httpSession) {
		List<Message> messages = new ArrayList<Message>();
		messages = messageServices.getAll();
		List<Message> ms = new ArrayList<Message>();
		Account_detail account = new Account_detail();
		if (httpSession.getAttribute("currentUser") != null) {
			account = (Account_detail) httpSession.getAttribute("currentUser");
			for (Message m : messages) {
				if (m.getAccount_detail() != null)
					if (!m.getAnswer().equalsIgnoreCase("")
							&& m.getAccount_detail().getUser_id() == account.getUser_id())
						ms.add(m);
			}
		}
		return ms;
	}
}
