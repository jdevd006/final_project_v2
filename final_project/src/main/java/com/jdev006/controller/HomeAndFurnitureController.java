package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value="/homeandfurniturecontroller")
public class HomeAndFurnitureController {
	@Autowired
	private Product_service productservice;
	@Autowired
	private Image_service imgservice;
	
	@Autowired
	private SessionLocaleResolver localeResolver;
	@RequestMapping(value = "/hafs", method = RequestMethod.GET)
	private ModelAndView HomeandFurniture(@ModelAttribute ("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value="lang", defaultValue="en") String language) {
		switch(language) {
		case "en": 
		{
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		}
		case "vi" :
		{
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		}
		default:
			break;
	}
		
		
		ModelAndView model = new ModelAndView();
		model.setViewName("homeandfurniture");

		List<Image> listimg_haf = new ArrayList<>();
		List<Product> listhaf = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 4) {
				listhaf.add(pro);
			}
		}
		for (Product pro : listhaf) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_haf.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_haf.add(null);
			}
		}
		model.addObject("listimg_haf", listimg_haf);
		model.addObject("listhaf", productservice.getpro(4, 0));
		model.addObject("evenTr", productservice.getpro(4, 0).size()/4);
		model.addObject("num", listhaf);
		return model;
	}
	
	
	@RequestMapping(value = "/haf", method = RequestMethod.GET)
	
	public ModelAndView getFashion(@RequestParam("pag") int startpoint){
		ModelAndView model = new ModelAndView();
		model.setViewName("homeandfurniture");
		List<Product> hafs = new ArrayList<>();
		hafs = productservice.getpro(4, startpoint);
		
		List<Image> listimg_haf = new ArrayList<>();
		for (Product pro : hafs) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_haf.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_haf.add(null);
			}
		}
		
		List<Product> listhaf = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 4) {
				listhaf.add(pro);
			}
		}
		model.addObject("listimg_haf", listimg_haf);
		model.addObject("listhaf", hafs);
		model.addObject("num", listhaf);
		model.addObject("evenTr", productservice.getpro(4, 0).size()/4);
		return model;
	}
	
	
}
