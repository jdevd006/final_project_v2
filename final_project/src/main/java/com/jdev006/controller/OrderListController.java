package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.service.BillDetaiService;
import com.jdev006.service.Bill_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Category_service;
import com.jdev006.service.Image_service;
import com.jdev006.service.Message_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/orderListController")
public class OrderListController {
	@Autowired
	private BillDetaiService billdetailServices;
	@Autowired
	private Bill_service billServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/orderList", method = RequestMethod.GET)
	public ModelAndView returnOrderList(HttpSession httpSession,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		int numItem = 0;
		List<Bill> bills = null;
		if ((Account_detail) httpSession.getAttribute("currentUser") != null)
			bills = billServices
					.getListBillByUserId(((Account_detail) httpSession.getAttribute("currentUser")).getUser_id());
		List<BillDetail> billDetails = new ArrayList<BillDetail>();
		List<Image> imgs = new ArrayList<Image>();
		List<Item> items = new ArrayList<Item>();
		Item item = new Item();
		Product product = new Product();
		if (bills != null)
			if (bills.size() >= 1) {
				for (Bill bill : bills) {
					billDetails.addAll(billdetailServices.getListByBillId(bill.getBill_id()));
				}
				for (BillDetail billDetail : billDetails) {
					if (imgServices.getByProId(billDetail.getId().getpro_id()) != null)
						imgs.add(imgServices.getByProId(billDetail.getId().getpro_id()));
				}
				for (BillDetail billDetail : billDetails) {
					product = new Product();
					item = new Item();
					product = productServices.get(billDetail.getId().getpro_id());
					item.setProduct(product);
					item.setNum(billDetail.getNum());
					items.add(item);
				}
			}
		if (httpSession.getAttribute("currentUser") != null) {
			Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == acc.getUser_id()) {
					numItem += cart.getNum();
				}
			}
		}
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("orderListPage");
		modelAndView.addObject("bills", bills);
		modelAndView.addObject("billDetails", billDetails);
		modelAndView.addObject("imgs", imgs);
		modelAndView.addObject("items", items);
		modelAndView.addObject("cartItems", numItem);
		modelAndView.addObject("orderCategories", categoryServices.getAll());
		return modelAndView;
	}

	@RequestMapping(value = "/order/{billId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deteleOrder(@PathVariable("billId") int billId) {
		Bill billCanceled = billServices.get(billId);
		List<BillDetail> billDetails = billdetailServices.getListByBillId(billId);
		Product product = new Product();
		for (BillDetail billdetail : billDetails) {
			product = productServices.getAProduct(billdetail.getId().getpro_id());
			product.setPro_quantity(product.getPro_quantity() + billdetail.getNum());
			productServices.update(product);
			billdetailServices.delete(billdetail);
		}
		billServices.delete(billCanceled);
	}
	
	@RequestMapping(value = "/order/{billId}", method = RequestMethod.POST)
	@ResponseBody
	public void cancelOrder(@PathVariable("billId") int billId) {
		Bill billCanceled = billServices.get(billId);
		billCanceled.setStatus("Canceled");
		billServices.update(billCanceled);
	}
}
