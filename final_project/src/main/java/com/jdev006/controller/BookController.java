package com.jdev006.controller;

import java.awt.image.ImageFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/bookcontroller")
public class BookController {
	@Autowired
	private Product_service productservice;
	@Autowired
	private Image_service imgservice;
	@Autowired
	private SessionLocaleResolver localeResolver;
	@RequestMapping(value = "/books", method = RequestMethod.GET)
	private ModelAndView Book(@ModelAttribute ("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value="lang", defaultValue="en") String language) {
		
		switch(language) {
		case "en": 
		{
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		}
		case "vi" :
		{
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		}
		default:
			break;
	}
		
		ModelAndView model = new ModelAndView();
		model.setViewName("book");

		List<Image> listimg_book = new ArrayList<>();
		List<Product> listbook = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 1) {
				listbook.add(pro);
			}
		}
		for (Product pro : listbook) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_book.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_book.add(null);
			}
		}
		model.addObject("listimg_book", listimg_book);
		model.addObject("listbook", productservice.getpro(1, 0));
		model.addObject("evenTr", productservice.getpro(1, 0).size()/4);
		model.addObject("num", listbook);
		return model;
	}
	
	
	@RequestMapping(value = "/book", method = RequestMethod.GET)
	@ResponseBody //annotation tra ve ajax
	public ModelAndView getBooks(@RequestParam("pag") int startpoint){
		ModelAndView model = new ModelAndView();
		model.setViewName("book");
		List<Product> books = new ArrayList<>();
		books = productservice.getpro(1, startpoint);
		
		List<Image> listimg_book = new ArrayList<>();
		for (Product pro : books) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_book.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_book.add(null);
			}
		}
		
		List<Product> listbook = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 1) {
				listbook.add(pro);
			}
		}
		model.addObject("listimg_book", listimg_book);
		model.addObject("listbook", books);
		model.addObject("num", listbook);
		model.addObject("evenTr", productservice.getpro(1, 0).size()/4);
		return model;
	}
	
	
	

}
