package com.jdev006.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.RevenueCategory;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.BillDetaiService;
import com.jdev006.service.Bill_service;
import com.jdev006.service.Category_service;
import com.jdev006.service.Message_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/dashBoardController")
public class DashBoardController {
	@Autowired
	private Bill_service billServices;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private BillDetaiService billDetailServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private Account_detail_service accountDetailServices;
	@Autowired
	private Message_service messageServices;
	@Autowired
	private SessionLocaleResolver localeResolver; 

	@RequestMapping(value = "/dashBoard", method = RequestMethod.GET)
	public ModelAndView dashPage(@RequestParam(value="lang", defaultValue="en") String language)  
	{
		{
			switch (language) {
			case "en":
				localeResolver.setDefaultLocale(Locale.ENGLISH);
				break;
			case "vi":
				localeResolver.setDefaultLocale(new Locale("vi", "VN"));
				break;
			default:
				break;
			}
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_DashBoard");
		model.addObject("year", billServices.getRevenueOfYear(2019));
		model.addObject("users", accountDetailServices.getAll().size());
		model.addObject("message", messageServices.getMessagesUnreadForAdmin().size());
		model.addObject("order", billServices.getBillWaiting().size());
		return model;
		}
	}

	@RequestMapping(value = "/revenue", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getLongList() {
		Calendar cal = Calendar.getInstance();
		return billServices.getRevenueOfYear(cal.get(Calendar.YEAR));
	}

	@RequestMapping(value = "/revenue/{year}", method = RequestMethod.GET)
	@ResponseBody
	public List<Long> getLongList2(@PathVariable("year") int year) {
		return billServices.getRevenueOfYear(year);
	}

	@RequestMapping(value = "/categories")
	@ResponseBody
	public List<Category> getAllCategory() {
		List<Category> categories = new ArrayList<Category>();
		if (categoryServices.getAll().size() >= 1)
			categories = categoryServices.getAll();
		return categories;
	}

	@RequestMapping(value = "/revenuecategories")
	@ResponseBody
	public RevenueCategory[] revennueCategories() {
		List<Category> categories = new ArrayList<Category>();
		if (categoryServices.getAll().size() >= 1)
			categories = categoryServices.getAll();
		RevenueCategory[] revenue = new RevenueCategory[categories.size()];
		RevenueCategory t;
		for (int i = 0; i < revenue.length; i++) {
			t = new RevenueCategory();
			t.setName(categories.get(i).getCate_name());
			revenue[i] = t;
		}
		for (BillDetail billDetail : billDetailServices.getAll()) {
			for (Category cate : categories) {
				if (productServices.getAProduct(billDetail.getId().getpro_id()).getCategory().getCate_id() == cate
						.getCate_id()) {
					t = revenue[categories.indexOf(cate)];
					if (t == null)
						t = new RevenueCategory();
					t.setName(cate.getCate_name());
					t.setRevenue(t.getRevenue() + (long) (billDetail.getNum()
							* productServices.getAProduct(billDetail.getId().getpro_id()).getPro_cost()));
					revenue[categories.indexOf(cate)] = t;
				}
			}
		}
		return revenue;
	}

	@RequestMapping(value = "/revenuecategory", method = RequestMethod.GET)
	@ResponseBody
	public RevenueCategory[] revennueCateByDuration(@RequestParam("data") String data) {
		List<Category> categories = new ArrayList<Category>();
		List<Item> products = new ArrayList<>();
		String[] words = data.split(",");
		if (categoryServices.getAll().size() >= 1)
			categories = categoryServices.getAll();
		if (billServices.getListByDuration(words[0], words[1], words[2], words[3], words[4]).size() >= 1)
			products = billServices.getListByDuration(words[0], words[1], words[2], words[3], words[4]);
		RevenueCategory[] revenue = new RevenueCategory[categories.size()];
		RevenueCategory t;
		
		for (int i = 0; i < revenue.length; i++) {
			t = new RevenueCategory();
			t.setName(categories.get(i).getCate_name());
			revenue[i] = t;
		}
		for (Item product : products) {
			for (Category cate : categories) {
				if (product.getProduct().getCategory().getCate_id() == cate.getCate_id()) {
					revenue[categories.indexOf(cate)].setRevenue(revenue[categories.indexOf(cate)].getRevenue() + (long) (product.getNum() *
					 product.getProduct().getPro_cost()));
				}
			}
		}
		return revenue;
	}

}
