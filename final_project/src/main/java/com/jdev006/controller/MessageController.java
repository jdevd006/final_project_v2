package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Message;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.SendMail;
import com.jdev006.service.Message_service;

@Controller
@RequestMapping("/messageController")
public class MessageController {
	@Autowired
	private Message_service messageServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/messages", method = RequestMethod.GET)
	public ModelAndView messagePage(@ModelAttribute("message") Message message,
			@ModelAttribute("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		{
			switch (language) {
			case "en":
				localeResolver.setDefaultLocale(Locale.ENGLISH);
				break;
			case "vi":
				localeResolver.setDefaultLocale(new Locale("vi", "VN"));
				break;
			default:
				break;
			}
			ModelAndView model = new ModelAndView();
			model.setViewName("admin_Message");
			List<Message> messages = new ArrayList<Message>();
			messages.addAll(messageServices.getMessagesUnreadForAdmin());
			messages.addAll(messageServices.getMessagesReadForAdmin());

			model.addObject("messages", messages);
			model.addObject("even", messages.size()/6);
			return model;
		}

	}

	@RequestMapping(value = "/message", method = RequestMethod.GET)
	@ResponseBody
	public Message getAMessage(@RequestParam("messageId") int messageId) {
		Message message = new Message();
		message = messageServices.get(messageId);
		message.setStatus("read");
		messageServices.update(message);
		return message;
	}

	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public String answer(@ModelAttribute Message message, @RequestParam String btnSaveInModal) {
		Message messageOrigin = new Message();
		if (((Account_detail) messageServices.get(Integer.parseInt(btnSaveInModal)).getAccount_detail() == null)) {
			messageOrigin = messageServices.get(Integer.parseInt(btnSaveInModal));
			messageOrigin.setAnswer(message.getAnswer());
			message.setStatus("read");
		} else {
			messageOrigin = messageServices.get(Integer.parseInt(btnSaveInModal));
			messageOrigin.setAnswer(message.getAnswer());
			message.setStatus("read");
		}
		SendMail.send("<h3 class='text text-primary'>TATALO</h3><br>First, thanks for your feedback/message : <br>Your question : " + messageOrigin.getQuestion() + "<br>Our answer : " + messageOrigin.getAnswer(), messageOrigin.getEmail());
		messageServices.update(messageOrigin);
		return "redirect:/messageController/messages";
	}

	@RequestMapping(value = "/message/{messageId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@PathVariable("messageId") int messageId) {
		messageServices.delete(messageId);
	}

	private class MessageAndAccount {
		private Message message;
		private String account;

		public MessageAndAccount(Message message, String account) {
			this.message = message;
			this.account = account;
		}

		public MessageAndAccount() {
		}

		public Message getMessage() {
			return message;
		}

		public void setMessage(Message message) {
			this.message = message;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

	}

	@RequestMapping(value = "/getmessages", method = RequestMethod.GET)
	@ResponseBody
	public List<MessageAndAccount> getMessagesByOrder(@RequestParam("by") String by) {
		List<MessageAndAccount> messages = new ArrayList<MessageAndAccount>();
		List<Message> list = new ArrayList<Message>();
		MessageAndAccount mAccount = new MessageAndAccount();
		if (by.equalsIgnoreCase("read")) {
			list.addAll(messageServices.getMessagesReadForAdmin());
			list.addAll(messageServices.getMessagesUnreadForAdmin());
			for (Message m : list) {
				if (m.getAccount_detail() == null)
					messages.add(new MessageAndAccount(m, "GUEST"));
				else 
					messages.add(new MessageAndAccount(m, m.getAccount_detail().getUser_id()+""));
			}
		} else {
			list.addAll(messageServices.getMessagesUnreadForAdmin());
			list.addAll(messageServices.getMessagesReadForAdmin());
			for (Message m : list) {
				if (m.getAccount_detail() == null)
					messages.add(new MessageAndAccount(m, "GUEST"));
				else 
					messages.add(new MessageAndAccount(m, m.getAccount_detail().getUser_id()+""));
			}
		}
		return messages;
	}
	
	@RequestMapping(value = "/getmesses", method = RequestMethod.GET)
	@ResponseBody
	public List<MessageAndAccount> getMessagesSearching(@RequestParam("key") String key) {
		List<MessageAndAccount> messages = new ArrayList<MessageAndAccount>();
		List<Message> list = new ArrayList<Message>();
		MessageAndAccount mAccount = new MessageAndAccount();
		list = messageServices.getResultOfSearching(key);
		for (Message m : list) {
			if (m.getAccount_detail() == null)
				messages.add(new MessageAndAccount(m, "GUEST"));
			else 
				messages.add(new MessageAndAccount(m, m.getAccount_detail().getUser_id()+""));
		}
		return messages;
	}
}
