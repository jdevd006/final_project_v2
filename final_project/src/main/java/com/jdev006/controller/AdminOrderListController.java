package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.Product;
import com.jdev006.service.BillDetaiService;
import com.jdev006.service.Bill_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/adminOrderListController")
public class AdminOrderListController {
	@Autowired
	private Bill_service billServices;
	@Autowired
	private BillDetaiService billDetailServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView orderListPage(@ModelAttribute("order") Bill order,
			@RequestParam(value = "lang", defaultValue = "en") String language) {

		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_OrderList");
		List<Bill> orders = new ArrayList<Bill>();
		orders.addAll(billServices.getBillWaiting());
		orders.addAll(billServices.getBillProcessing());
		orders.addAll(billServices.getBillFinish());
		orders.addAll(billServices.getBillCanceled());
		model.addObject("orders", orders);
		model.addObject("even", orders.size()/6);
		return model;
	}

	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public String update(@ModelAttribute Bill order, @RequestParam String btnSaveInModal) {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_OrderList");
		Bill orderOrigin = new Bill();
		orderOrigin = billServices.get(Integer.parseInt(btnSaveInModal));
		orderOrigin.setName(order.getName());
		orderOrigin.setAddress(order.getAddress());
		orderOrigin.setTotal(order.getTotal());
		orderOrigin.setStatus(order.getStatus());
		billServices.update(orderOrigin);
		return "redirect:/adminOrderListController/orders";
	}

	@RequestMapping(value = "/order/{orderId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@PathVariable("orderId") int orderId) {
		billServices.delete(orderId);
	}

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	@ResponseBody
	public Bill getOrder(@RequestParam("orderId") int oderId) {
		Bill order = new Bill();
		order = billServices.get(oderId);
		return order;
	}

	@RequestMapping(value = "/getorders", method = RequestMethod.GET)
	@ResponseBody
	public List<Bill> getListOrder(@RequestParam("by") String by) {
		List<Bill> orders = new ArrayList<Bill>();
		System.out.println(by);
		switch (by) {
		case "finish":
			orders.addAll(billServices.getBillFinish());
			orders.addAll(billServices.getBillWaiting());
			orders.addAll(billServices.getBillProcessing());
			orders.addAll(billServices.getBillCanceled());
			break;
		case "processing":
			orders.addAll(billServices.getBillProcessing());
			orders.addAll(billServices.getBillFinish());
			orders.addAll(billServices.getBillWaiting());
			orders.addAll(billServices.getBillCanceled());
			break;
		case "waiting":
			orders.addAll(billServices.getBillWaiting());
			orders.addAll(billServices.getBillFinish());
			orders.addAll(billServices.getBillProcessing());
			orders.addAll(billServices.getBillCanceled());
			break;
		case "canceled":
			orders.addAll(billServices.getBillCanceled());
			orders.addAll(billServices.getBillFinish());
			orders.addAll(billServices.getBillWaiting());
			orders.addAll(billServices.getBillProcessing());
			break;
		case "totalAsc":
			orders = billServices.getBillTotalAsc();
			break;
		case "totalDsc":
			orders = billServices.getBillTotalDsc();
			break;
		case "idAsc":
			orders = billServices.getBillIdAsc();
			break;
		case "idDsc":
			orders = billServices.getBillIdDsc();
			break;
		default:
			break;
		}
		return orders;
	}

	@RequestMapping(value = "/cancel/{billId}", method = RequestMethod.POST)
	@ResponseBody
	public void cancel(@PathVariable("billId") int billId) {
		Bill bill = billServices.get(billId);
		bill.setStatus("Canceled");
		billServices.update(bill);
	}

	private class BillDetailAndProduct {
		private BillDetail detail;
		private Product product;

		public BillDetailAndProduct(BillDetail detail, Product product) {
			this.detail = detail;
			this.product = product;
		}

		public BillDetailAndProduct() {
		}

		public BillDetail getDetail() {
			return detail;
		}

		public void setDetail(BillDetail detail) {
			this.detail = detail;
		}

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

	}

	/*
	 * @RequestMapping(value = "/more/{billId}", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public List<BillDetail> getBillDetail(@PathVariable("billId")
	 * int billId) { List<BillDetail> billDetails = new ArrayList<BillDetail>();
	 * billDetails = billDetailServices.getListByBillId(billId); return billDetails;
	 * }
	 */
	
	@RequestMapping(value = "/more/{billId}", method = RequestMethod.GET)
	@ResponseBody
	public List<BillDetailAndProduct> getBillDetail(@PathVariable("billId") int billId) {
		List<BillDetailAndProduct> list = new ArrayList<AdminOrderListController.BillDetailAndProduct>();
		List<BillDetail> billDetails = new ArrayList<BillDetail>();
		billDetails = billDetailServices.getListByBillId(billId);
		for (BillDetail b : billDetails) {
			list.add(new BillDetailAndProduct(b, productServices.getAProduct(b.getId().getpro_id())));
		}
		return list;
	}

	@RequestMapping(value = "/product/{proId}", method = RequestMethod.GET)
	@ResponseBody
	public Product getProduct(@PathVariable("proId") int proId) {
		Product product = new Product();
		product = productServices.getAProduct(proId);
		return product;
	}
	
	@RequestMapping(value = "/getbills", method = RequestMethod.GET)
	@ResponseBody
	public List<Bill> getListSearching(@RequestParam("key") String key) {
		List<Bill> orders = new ArrayList<Bill>();
		orders.addAll(billServices.getResultNameOfSearching(key));
		orders.addAll(billServices.getResultDateOfSearching(key));
		return orders;
	}
}
