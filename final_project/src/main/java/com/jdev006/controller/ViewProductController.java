package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.CartId;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.Question_Answer;
import com.jdev006.entitties.Review_Product;
import com.jdev006.entitties.Viewed;
import com.jdev006.entitties.ViewedId;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Category_service;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;
import com.jdev006.service.Question_Answer_service;
import com.jdev006.service.Review_Product_service;
import com.jdev006.service.Viewed_Services;

@Controller
@RequestMapping(value = "/viewProductController")
public class ViewProductController {
	@Autowired
	private Product_service productServices;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Question_Answer_service qaServices;
	@Autowired
	private Viewed_Services viewedServices;
	@Autowired
	private Review_Product_service reviewServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	private ModelAndView test(@RequestParam("proid") int pro_id, HttpSession httpSession,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		int numItem = 0;
		ArrayList<Product> listViewed = new ArrayList<Product>();
		ArrayList<Question_Answer> listAQ = new ArrayList<Question_Answer>();
		ArrayList<Image> listImage = new ArrayList<Image>();
		List<Image> imgViewed = new ArrayList<Image>();
		List<Image> imgRelate = new ArrayList<Image>();
		int count = 0;
		if (httpSession.getAttribute("currentUser") != null) {
			Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");

			if (!viewedServices.isExistProduct(pro_id, acc.getUser_id())) {
				Viewed viewed = new Viewed();
				ViewedId viewedId = new ViewedId();
				viewedId.setAccount_detail(acc.getUser_id());
				viewedId.setProduct(pro_id);
				viewed.setId(viewedId);
				viewedServices.add(viewed);
			}

			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == acc.getUser_id())
					numItem += cart.getNum();
			}

			for (Viewed viewed : viewedServices.getAll()) {
				if (viewed.getId().getAccount_detail() == acc.getUser_id()) {
					listViewed.add(productServices.get(viewed.getId().getProduct()));
					imgViewed.add(imgServices.getByProId(productServices.get(viewed.getId().getProduct()).getPro_id()));
				}
			}

		}

		for (Question_Answer qa : qaServices.getAll()) {
			if (qa.getProduct().getPro_id() == pro_id)
				listAQ.add(qa);
		}

		count = 0;
		for (Image img : imgServices.getAll()) {
			if (count == 4)
				break;
			if (img.getProduct().getPro_id() == pro_id) {
				listImage.add(img);
			}

		}

		for (Product product : productServices
				.getListByCateId(productServices.getAProduct(pro_id).getCategory().getCate_id())) {
			imgRelate.add(imgServices.getByProId(product.getPro_id()));
		}
		imgRelate.remove(imgServices.getByProId(productServices.getAProduct(pro_id).getPro_id()));

		model.setViewName("view");
		model.addObject("cartItems", numItem);
		model.addObject("listQA", listAQ);

		model.addObject("listViewed", listViewed);
		model.addObject("imgViewed", imgViewed);
		model.addObject("even", listViewed.size() / 5);

		model.addObject("listReview", reviewServices.getReviewsByProId(pro_id));
		model.addObject("evenReview", reviewServices.getReviewsByProId(pro_id).size() / 6);

		model.addObject("listQa", qaServices.getQABProId(pro_id));
		model.addObject("evenQa", qaServices.getQABProId(pro_id).size() / 6);

		model.addObject("listImage", listImage);
		model.addObject("product", productServices.get(pro_id));

		model.addObject("viewCategories", categoryServices.getAll());

		model.addObject("category", productServices.getAProduct(pro_id).getCategory().getCate_name());

		model.addObject("listRelate",
				productServices.getListByCateId(productServices.getAProduct(pro_id).getCategory().getCate_id()));
		model.addObject("evenRelate",
				productServices.getListByCateId(productServices.getAProduct(pro_id).getCategory().getCate_id()).size()
						/ 5);
		model.addObject("imgRelate", imgRelate);
		for (Image i : imgRelate) {
			if (i != null)
				System.out.println(i.getImg());
		}

		model.addObject("quantity", productServices.getAProduct(pro_id).getPro_quantity());
		model.addObject("pro", productServices.get(pro_id));
		return model;
	}

	@RequestMapping(value = "/cartPage", method = RequestMethod.GET)
	public String cart(ModelMap model) {
		return "redirect:/cartController/cartPage";
	}

	@RequestMapping(value = "/addCart/{proId}/{quantity}", method = RequestMethod.POST)
	@ResponseBody
	public void addToCart(@PathVariable("proId") int id, HttpSession httpSession,
			@PathVariable("quantity") int quantity) {
		Cart cart = null;
		CartId cartId = new CartId();
		Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
		System.out.println(quantity);
		System.out.println("pro : " + id + "     id : " + acc.getUser_id());
		if (acc != null) {

			cart = cartServices.getCartByProIdUserId(id, acc.getUser_id());
			if (cart == null) {
				System.out.println("Nullll");
				cartId = new CartId(id, acc.getUser_id());
				cart = new Cart();
				cart.setId(cartId);
				cart.setNum(quantity);
				cartServices.add(cart);
			} else {
				cart.setNum(cart.getNum() + quantity);
				cartServices.update(cart);
			}
		}
	}

	@RequestMapping(value = "/review", method = RequestMethod.POST)
	@ResponseBody
	public void review(@RequestParam("star") int star, @RequestParam("content") String content,
			@RequestParam("proId") int pro_id, @RequestParam("email") String email, @RequestParam("name") String name,
			HttpSession httpSession) {
		Review_Product review = new Review_Product();
		if ((Account_detail) httpSession.getAttribute("currentUser") != null) {
			review.setName(name);
			review.setEmail(email);
			review.setPro_star(star);
			review.setContent(content);
			review.setProduct(productServices.getAProduct(pro_id));
			review.setAccount_detail((Account_detail) httpSession.getAttribute("currentUser"));
		} else {
			review.setName(name);
			review.setEmail(email);
			review.setPro_star(star);
			review.setContent(content);
			review.setProduct(productServices.getAProduct(pro_id));
			review.setAccount_detail(null);
		}
		reviewServices.add(review);
	}

	@RequestMapping(value = "/question", method = RequestMethod.POST)
	@ResponseBody
	public void quesion(@RequestParam("proId") int pro_id, @RequestParam("qContent") String q_content) {
		Question_Answer qa = new Question_Answer();
		qa.setProduct(productServices.get(pro_id));
		qa.setQ_content(q_content);
		qa.setA_content("");
		qaServices.add(qa);
	}

	@RequestMapping(value = "/movetologin", method = RequestMethod.GET)
	public String login() {
		return "redirect:/loginRegisterController/login?lang=en";
	}

	private class ProductAndImage {
		private Product product;
		private String img;

		public ProductAndImage(Product product, String img) {
			this.product = product;
			this.img = img;
		}

		public ProductAndImage() {
		}

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

		public String getImg() {
			return img;
		}

		public void setImg(String img) {
			this.img = img;
		}
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	public List<ProductAndImage> getSearchResult(@RequestParam("key") String key) {
		List<Product> products = new ArrayList<Product>();
		List<ProductAndImage> proAndImages = new ArrayList<ViewProductController.ProductAndImage>();
		key = key.trim();
		products = productServices.getResultOfSearching(key);
		for (Product pro : products) {
			if (imgServices.getByProId(pro.getPro_id()) != null)
				proAndImages.add(new ProductAndImage(pro, imgServices.getByProId(pro.getPro_id()).getImg()));
			else {
				proAndImages.add(new ProductAndImage(pro, "asas"));
			}
		}
		return proAndImages;
	}
}
