package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value="/techcontroller")
public class TechController {
	@Autowired
	private Product_service productservice;
	@Autowired
	private Image_service imgservice;
	@Autowired
	private SessionLocaleResolver localeResolver;
	@RequestMapping(value = "/techs", method = RequestMethod.GET)
	private ModelAndView Tech(@ModelAttribute ("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value="lang", defaultValue="en") String language) {
		switch(language) {
			case "en":{
				localeResolver.setDefaultLocale(Locale.ENGLISH);
				break;
			}
			case "vi":{
				localeResolver.setDefaultLocale(new Locale("vi","VN"));
				break;
			}
			default:
				break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("tech");

		List<Image> listimg_tech = new ArrayList<>();
		List<Product> listtech = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 2) {
				listtech.add(pro);
			}
		}
		for (Product pro : listtech) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_tech.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_tech.add(null);
			}
		}
		model.addObject("listimg_tech", listimg_tech);
		model.addObject("listtech", productservice.getpro(2, 0));
		model.addObject("evenTr", productservice.getpro(2, 0).size()/4);
		model.addObject("num", listtech);
		return model;
	}
	
	
	@RequestMapping(value = "/tech", method = RequestMethod.GET)
	
	public ModelAndView getFashion(@RequestParam("pag") int startpoint){
		ModelAndView model = new ModelAndView();
		model.setViewName("tech");
		List<Product> techs = new ArrayList<>();
		techs = productservice.getpro(2, startpoint);
		
		List<Image> listimg_tech = new ArrayList<>();
		for (Product pro : techs) {
			int flag = 0;
			for (Image img : imgservice.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listimg_tech.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listimg_tech.add(null);
			}
		}
		
		List<Product> listtech = new ArrayList<>();

		for (Product pro : productservice.getAll()) {
			if (pro.getCategory().getCate_id() == 2) {
				listtech.add(pro);
			}
		}
		model.addObject("listimg_tech", listimg_tech);
		model.addObject("listtech", techs);
		model.addObject("num", listtech);
		model.addObject("evenTr", productservice.getpro(2, 0).size()/4);
		return model;
	}
	
	
}
