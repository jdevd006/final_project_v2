package com.jdev006.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Category_service;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;

@Controller
@RequestMapping(value = "/homecontroller")
public class HomepageController {
	@Autowired
	private Product_service productService;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	public class BestAndImage {
		private Product pro;
		private String img;

		public BestAndImage(Product pro, String img) {
			this.pro = pro;
			this.img = img;
		}

		public BestAndImage() {
		}

		public Product getPro() {
			return pro;
		}

		public void setPro(Product pro) {
			this.pro = pro;
		}

		public String getImg() {
			return img;
		}

		public void setImg(String img) {
			this.img = img;
		}

	}

	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	private ModelAndView homepagetest(HttpSession httpSession,
			@ModelAttribute("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value = "lang", defaultValue = "en") String language) {

		switch (language) {
		case "en": {
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		}
		case "vi": {
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		}
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("homePage");

		List<Product> listbook = new ArrayList<>();
		List<Product> listtech = new ArrayList<>();
		List<Product> listfashion = new ArrayList<>();
		List<Product> listhaf = new ArrayList<>();

		List<Image> listImg_book = new ArrayList<>();
		List<Image> listImg_tech = new ArrayList<>();
		List<Image> listImg_fashion = new ArrayList<>();
		List<Image> listImg_haf = new ArrayList<>();

		List<Category> categories = new ArrayList<Category>();

		categories = categoryServices.getAll();

		for (Product pro : productService.getAll()) {
			if (pro.getCategory().getCate_id() == 1) {
				listbook.add(pro);
			}
			if (pro.getCategory().getCate_id() == 2) {
				listtech.add(pro);
			}
			if (pro.getCategory().getCate_id() == 3) {
				listfashion.add(pro);
			}
			if (pro.getCategory().getCate_id() == 4) {
				listhaf.add(pro);
			}
		}
		for (Product pro : listbook) {
			int flag = 0;
			for (Image img : imgServices.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listImg_book.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listImg_book.add(null);
			}
		}

		///////////// TECH
		for (Product pro : listtech) {
			int flag = 0;
			for (Image img : imgServices.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listImg_tech.add(img);
					flag = 1;
					break;
				}

			}
			if (flag == 0) {
				listImg_tech.add(null);
			}
		}

		/////////////// FASHION
		for (Product pro : listfashion) {
			int flag = 0;
			for (Image img : imgServices.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listImg_fashion.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listImg_fashion.add(null);
			}

		}
		//////////////////// FASHION
		for (Product pro : listhaf) {
			int flag = 0;
			for (Image img : imgServices.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listImg_haf.add(img);
					flag = 1;
					break;
				}
			}
			if (flag == 0) {
				listImg_haf.add(null);
			}
		}

		List<BestAndImage> bests = new ArrayList<>();
		List<Product> products = productService.getFirstTenSeller();
		for (Product pro : products)
			bests.add(new BestAndImage(pro, imgServices.getByProId(pro.getPro_id()).getImg()));

		model.addObject("bests", bests);
		model.addObject("evenBest", bests.size() / 5);

		model.addObject("listimg_book", listImg_book);
		model.addObject("listbook", listbook);

		model.addObject("listimg_tech", listImg_tech);
		model.addObject("listtech", listtech);

		model.addObject("listfashion", listfashion);
		model.addObject("listimg_fashion", listImg_fashion);

		model.addObject("listhaf", listhaf);
		model.addObject("listimg_haf", listImg_haf);

		model.addObject("homeCategories", categories);

		return model;
	}

}
