package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.Address;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Account_role_service;
import com.jdev006.service.City_services;
import com.jdev006.service.District_services;

@Controller
@RequestMapping(value = "/userController")
public class UserControlller {
	@Autowired
	private Account_detail_service accountDetailServices;
	@Autowired
	private Account_role_service accountRoleServices;
	@Autowired
	private SessionLocaleResolver localeResolver;
	@Autowired
	private City_services cityServices;
	@Autowired
	private District_services districtServices;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView accounts(@ModelAttribute("user") Account_detail user,
			@ModelAttribute("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value = "lang", defaultValue = "en") String language) {

		{
			switch (language) {
			case "en":
				localeResolver.setDefaultLocale(Locale.ENGLISH);
				break;
			case "vi":
				localeResolver.setDefaultLocale(new Locale("vi", "VN"));
				break;
			default:
				break;
			}
			ModelAndView model = new ModelAndView();
			model.setViewName("admin_Account");
			model.addObject("users", accountDetailServices.getAllAlphaAsc());
			model.addObject("even", accountDetailServices.getAllAlphaAsc().size() / 6);
			return model;
		}

	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String addCategory(@ModelAttribute Account_detail user, @RequestParam String btnSaveInModal, ModelMap model) {
		Account_role role = new Account_role();
		List<Account_detail> l = new ArrayList<Account_detail>();
		Account_detail account = new Account_detail();
		Address address = new Address();
		System.out.println(user.getAddress() + "sdsdsd");

		if (Integer.parseInt(user.getAddress().getCity()) != -1 && Integer.parseInt(user.getAddress().getDistrict()) != -1) {
			if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {
				user.setUser_id(Integer.parseInt(btnSaveInModal.split("-")[1]));
				user.setPassword(TripleDES.Encrypt(user.getPassword(), "12345"));
				address = new Address();
				address.setCity(cityServices.get(Integer.parseInt(user.getAddress().getCity())).getName());
				address.setDistrict(districtServices.get(Integer.parseInt(user.getAddress().getDistrict())).getName());
				address.setStreet(user.getAddress().getStreet());
				user.setAddress(address);
				address.setAccount_detail(user);
				role = accountRoleServices.get(accountDetailServices.get(user.getUser_id()).getAccount_role().getId());
				role.setRole(user.getAccount_role().getRole());
				accountRoleServices.update(role);
				user.getAccount_role().setId(role.getId());
				user.setStatus(user.getStatus());
				accountDetailServices.update(user);
			} else {
				user.setPassword(TripleDES.Encrypt(user.getPassword(), "12345"));
				user.setStatus("activated");
				address = new Address();
				address.setCity(cityServices.get(Integer.parseInt(user.getAddress().getCity())).getName());
				address.setDistrict(districtServices.get(Integer.parseInt(user.getAddress().getDistrict())).getName());
				address.setStreet(user.getAddress().getStreet());
				user.setAddress(address);
				address.setAccount_detail(user);
				role.setRole(user.getAccount_role().getRole());
				user.setAccount_role(role);
				l.add(user);
				role.setAccount_detail(l);
				accountDetailServices.add(user);
			}
		}else {
		}
		return "redirect:/userController/users";
	}

	@RequestMapping(value = "/user/{userId}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable() int userId) {
		accountRoleServices.delete(accountDetailServices.get(userId).getAccount_role().getId());
		accountDetailServices.delete(userId);
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ResponseBody
	public List<Object> getAProduct(@RequestParam("userId") int userId) {
		List<Object> list = new ArrayList<Object>();
		Account_detail account = new Account_detail();
		Account_role role = new Account_role();
		account = accountDetailServices.get(userId);
		account.setPassword(TripleDES.Decrypt(account.getPassword(), "12345"));
		role = account.getAccount_role();
		list.add(account);
		list.add(role);
		list.add(account.getAddress());
		return list;
	}

	private class AccountAndRole {
		private Account_detail account;
		private Account_role role;

		public AccountAndRole(Account_detail account, Account_role role) {
			this.account = account;
			this.role = role;
		}

		public AccountAndRole() {
			super();
		}

		public Account_detail getAccount() {
			return account;
		}

		public void setAccount(Account_detail account) {
			this.account = account;
		}

		public Account_role getRole() {
			return role;
		}

		public void setRole(Account_role role) {
			this.role = role;
		}

	}

	@RequestMapping(value = "/getusers", method = RequestMethod.GET)
	@ResponseBody
	public List<AccountAndRole> getUsersByOrder(@RequestParam("by") String by) {
		List<AccountAndRole> users = new ArrayList<AccountAndRole>();
		List<Account_detail> l = new ArrayList<Account_detail>();
		AccountAndRole accAnRole = new AccountAndRole();
		if (by.equalsIgnoreCase("nameDsc")) {
			l = accountDetailServices.getAllAlphaDsc();
		} else if (by.equalsIgnoreCase("nameAsc")) {
			l = accountDetailServices.getAllAlphaAsc();
		} else if (by.equalsIgnoreCase("idDsc")) {
			l = accountDetailServices.getAllIdDsc();
		} else {
			l = accountDetailServices.getAllIdAsc();
		}

		// Decrypt password
		for (AccountAndRole account : users) {
			account.getAccount().setPassword(TripleDES.Decrypt(account.getAccount().getPassword(), "12345"));
		}
		//

		for (Account_detail acc : l) {
			users.add(new AccountAndRole(acc, accountRoleServices.get(acc.getAccount_role().getId())));
		}

		/*
		 * //Get role follow the order of users for (Account_detail account : users) {
		 * roles.add(accountRoleServices.getByUserId(account.getUser_id())); } //
		 */ return users;
	}

	@RequestMapping(value = "/role/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public Account_role getRole(@PathVariable("roleId") int roleId) {
		Account_role role = new Account_role();
		role = accountRoleServices.get(roleId);
		return role;
	}

	@RequestMapping(value = "/block/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public void block(@PathVariable("userId") int userId) {
		Account_detail account = accountDetailServices.get(userId);
		account.setStatus("blocked");
		accountDetailServices.update(account);
	}

	@RequestMapping(value = "/activate/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public void activate(@PathVariable("userId") int userId) {
		Account_detail account = accountDetailServices.get(userId);
		account.setStatus("activated");
		accountDetailServices.update(account);
	}

	@RequestMapping(value = "/getaccs", method = RequestMethod.GET)
	@ResponseBody
	public List<AccountAndRole> getUsersSearching(@RequestParam("key") String key) {
		List<AccountAndRole> users = new ArrayList<AccountAndRole>();
		List<Account_detail> l = new ArrayList<Account_detail>();
		l = accountDetailServices.getResultOfSearching(key);
		// Decrypt password
		for (AccountAndRole account : users) {
			account.getAccount().setPassword(TripleDES.Decrypt(account.getAccount().getPassword(), "12345"));
		}
		//

		for (Account_detail acc : l) {
			users.add(new AccountAndRole(acc, accountRoleServices.get(acc.getAccount_role().getId())));
		}
		return users;
	}
}
