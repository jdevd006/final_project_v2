package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.Address;
import com.jdev006.entitties.RegrexClass;
import com.jdev006.entitties.SendMail;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Account_role_service;

@Controller
@RequestMapping(value = "/loginRegisterController")
@SessionAttributes({ "currentUser", "role" })
public class Login_RegisterController {
	@Autowired
	private Account_detail_service account_detaiService;
	@Autowired
	private Account_role_service account_roleService;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView motoLoginPage(HttpServletRequest request,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}

		ModelAndView model = new ModelAndView();
		model.setViewName("regist_form");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("email") String email, @RequestParam("password") String password,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Account_detail account = null;
		if (RegrexClass.isValidMail(email)) {
			if (!RegrexClass.isEmpty(password)) {
				account = account_detaiService.getByEmail(email);
				if (account != null) {
					if (password.equalsIgnoreCase(TripleDES.Decrypt(account.getPassword(), "12345"))) {

						if (account.getAccount_role().getRole().equalsIgnoreCase("admin")) {
							if (account.getStatus().equalsIgnoreCase("activated")) {
								model.addObject("currentUser", account);
								model.setViewName("redirect:/dashBoardController/dashBoard");
							} else {
								model.setViewName("regist_form");
								model.addObject("isblocked",
										"Your account blocked, please contact us to unclock your account or create a new account!!!");
							}
						} else {
							if (account.getStatus().equalsIgnoreCase("activated")) {
								model.addObject("currentUser", account);
								model.setViewName("redirect:/homecontroller/homepage");
							} else {
								model.setViewName("regist_form");
								model.addObject("isblocked",
										"Your account blocked, please contact us to unclock your account or create a new account!!!");
							}
						}
					} else {
						model.setViewName("regist_form");
						model.addObject("incorrectPass",
								"Password is incorrect, please correct your password if you do not want your account to be blocked!!!");
					}
				} else {
					model.setViewName("regist_form");
					model.addObject("doesNotExist", "Email does not exist, please make sure your mail is correct!!!");
				}
			} else {
				model.setViewName("regist_form");
				model.addObject("emptyPass",
						"Password must not contain any space character, please try another one!!!");
			}
		} else {
			model.setViewName("regist_form");
			model.addObject("invalidMail", "Invalid mail, please fill with an valid email!!!");
		}
		return model;
	}

	/*
	 * @RequestMapping(value = "/loginauthor", method = RequestMethod.POST) public
	 * ModelAndView loginAutho(@RequestParam("email") String
	 * email, @RequestParam("password") String password, HttpServletRequest request)
	 * { ModelAndView model = new ModelAndView(); Account_detail account =
	 * account_detaiService.getByEmail(email); model.addObject("currentUser",
	 * account); if (account.getAccount_role().getRole().equalsIgnoreCase("admin"))
	 * { if (account.getStatus().equalsIgnoreCase("activated")) {
	 * model.setViewName("redirect:/dashBoardController/dashBoard"); } else {
	 * model.setViewName("regist_form"); model.addObject("isblocked",
	 * "Your account blocked, please contact us to unclock your account or create a new account!!!"
	 * ); } } else { if (account.getStatus().equalsIgnoreCase("activated")) {
	 * model.setViewName("redirect:/homecontroller/homepage"); } else {
	 * model.setViewName("regist_form"); model.addObject("isblocked",
	 * "Your account blocked, please contact us to unclock your account or create a new account!!!"
	 * ); } } return model; }
	 */

	private class Test {
		private String str;

		public String getStr() {
			return str;
		}

		public void setStr(String str) {
			this.str = str;
		}

		public Test(String str) {
			this.str = str;
		}

		public Test() {
		}
	}

	@RequestMapping(value = "/loginauthor", method = RequestMethod.GET)
	@ResponseBody
	public Test loginAutho(@RequestParam("email") String email, @RequestParam("password") String password,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Account_detail account = account_detaiService.getByEmail(email);
		Test test = new Test("");
		if (account.getAccount_role().getRole().equalsIgnoreCase("admin")) {
			if (account.getStatus().equalsIgnoreCase("activated")) {
				model.addObject("currentUser", account);
				model.setViewName("redirect:/dashBoardController/dashBoard");
			} else {
				model.setViewName("regist_form");
				model.addObject("isblocked",
						"Your account blocked, please contact us to unclock your account or create a new account!!!");
				return new Test("Your account blocked, please contact us to unclock your account or create a new account!!!");
			}
		} else {
			if (account.getStatus().equalsIgnoreCase("activated")) {
				model.addObject("currentUser", account);
				model.setViewName("redirect:/homecontroller/homepage");
			} else {
				model.setViewName("regist_form");
				model.addObject("isblocked",
						"Your account blocked, please contact us to unclock your account or create a new account!!!");
				return new Test("Your account blocked, please contact us to unclock your account or create a new account!!!");
			}
		}
		return test;
	}
	
	@RequestMapping(value = "/gohome", method = RequestMethod.GET)
	public ModelAndView test(@RequestParam("mail") String email) {
		ModelAndView model = new ModelAndView();
		model.addObject("currentUser", account_detaiService.getByEmail(email));
		model.setViewName("redirect:/homecontroller/homepage");
		return model;
	}

	@RequestMapping(value = "/returnstate", method = RequestMethod.GET)
	public ModelAndView returnUserState(@RequestParam("data") String data) {
		ModelAndView model = new ModelAndView();
		model.setViewName("regist_form");
		model.addObject("isblocked", data);
		return model;
	}

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ResponseBody
	public void register(@RequestParam("email") String email, @RequestParam("psw") String password,
			@RequestParam("username") String username, @RequestParam("phone") String phone) {
		Account_detail account = new Account_detail();
		Account_role accoutRole = new Account_role();
		List<Account_detail> l = new ArrayList<Account_detail>();
		Address address = new Address();
		account.setEmail(email);
		account.setUser_name(username);
		account.setPhone(phone);
		account.setPassword(TripleDES.Encrypt(password, "12345"));
		
		address = new Address();
		address.setCity("");
		address.setDistrict("");
		address.setStreet("");
		account.setAddress(address);
		address.setAccount_detail(account);
		
		// set default role : user
		accoutRole.setRole("user");
		account.setAccount_role(accoutRole);
		l.add(account);
		accoutRole.setAccount_detail(l);
		//
		
		account.setStatus("activated");
		account_detaiService.add(account);
	}
	
	@RequestMapping(value = "/registfb", method = RequestMethod.POST)
	@ResponseBody
	public void registerWhenLoginFb(@RequestParam("email") String email, @RequestParam("psw") String password,
			@RequestParam("username") String username, @RequestParam("phone") String phone) {
		ModelAndView model = new ModelAndView();
		Account_detail account = new Account_detail();
		Account_role accoutRole = new Account_role();
		List<Account_detail> l = new ArrayList<Account_detail>();
		Address address = new Address();
		account.setEmail(email);
		account.setUser_name(username);
		account.setPhone(phone);
		account.setPassword(TripleDES.Encrypt(password, "12345"));

		address = new Address();
		address.setCity("");
		address.setDistrict("");
		address.setStreet("");
		account.setAddress(address);
		address.setAccount_detail(account);
		
		// set default role : user
		accoutRole.setRole("user");
		account.setAccount_role(accoutRole);
		l.add(account);
		accoutRole.setAccount_detail(l);
		//

		account.setStatus("activated");
		model.addObject("currentUser", account);
		SendMail.send("TATLO\nThis is your account : \n" + email + "\nPassword : The first 9 letters in your email ("
				+ email.substring(0, 10) + ")", email);
		account_detaiService.add(account);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(SessionStatus sessionStatus) {
		ModelAndView model = new ModelAndView();
		sessionStatus.setComplete();
		model.setViewName("redirect:/loginRegisterController/login");
		return model;
	}

	@RequestMapping(value = "/getuser", method = RequestMethod.GET)
	@ResponseBody
	public Account_detail getUser(@RequestParam("email") String email) {
		Account_detail user = null;
		user = account_detaiService.getByEmail(email);
		System.out.println(user);
		return user;
	}
	
	@RequestMapping(value = "/sendpasscode", method = RequestMethod.GET)
	@ResponseBody
	public Test sendPassCode(@RequestParam("email") String email) {
		Random rd = new Random();
		String str = "TATALO\nPasscode : ";
		String passcode = (100000 + rd.nextInt(900000))+"";
		SendMail.send(str + passcode, email);
		return new Test(passcode);
	}
	
	@RequestMapping(value = "/changepass", method = RequestMethod.POST)
	@ResponseBody
	public void changePass(@RequestParam("email") String email, @RequestParam("pass") String pass) {
		Account_detail user = account_detaiService.getByEmail(email);
		user.setPassword(TripleDES.Encrypt(pass, "12345"));
		account_detaiService.update(user);
	}
}
