package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.Message;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Category_service;
import com.jdev006.service.Message_service;

@Controller
@RequestMapping(value = "/contactcontroller")

public class contactPageController {
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private SessionLocaleResolver localeResolver;
	
	@Autowired
	private Cart_services cartServices;
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	private ModelAndView contactpage( 
			@ModelAttribute ("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value="lang", defaultValue="en") String language,HttpSession httpSession) {
		switch(language) {
		case "en":{
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		}
		case "vi":{
			localeResolver.setDefaultLocale( new Locale("vi","VN"));
			break;
		}
		default:
			break;
		
		}
		
		ModelAndView model = new ModelAndView();
		model.setViewName("contactPage");
		model.addObject("contactCategories", categoryServices.getAll());
		int numItem = 0;
		if (httpSession.getAttribute("currentUser") != null) {

			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == ((Account_detail)httpSession.getAttribute("currentUser")).getUser_id())
					numItem += cart.getNum();
			}
		}
		model.addObject("cartItems", numItem);
		return model;


	}

	@Autowired
	private Message_service messageService;
	
	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	public String addMessage(@RequestParam String comment, @RequestParam String name, @RequestParam String email,
			HttpSession httpSession) {
		
		
		
		Account_detail account = new Account_detail();
		List<Message> l = new ArrayList<Message>();
		Message message = new Message();
		if (httpSession.getAttribute("currentUser") != null) {
			account = (Account_detail) httpSession.getAttribute("currentUser");
			message.setQuestion(comment);
			message.setEmail(email);
			message.setName(name);
			message.setAnswer("");
			message.setStatus("unread");
			message.setAccount_detail(account);
			l.add(message);
			account.setMessage(l);
		} else {
			message.setQuestion(comment);
			message.setEmail(email);
			message.setName(name);
			message.setAnswer("");
			message.setStatus("unread");
		}

		messageService.add(message);

		return "redirect:/contactcontroller/contact";

	}
}
