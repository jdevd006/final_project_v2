package com.jdev006.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.MyUploadForm;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Category_service;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/productController")
@SessionAttributes("categories")
public class ManageProductController {
	@Autowired
	private Product_service productService;
	@Autowired
	private Category_service categoryServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private SessionLocaleResolver localeResolver;

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ModelAndView getAll(@ModelAttribute Product product,
			@ModelAttribute("myUploadForm") MyUploadForm myUploadForm,
			@RequestParam(value = "lang", defaultValue = "en") String language) {
		switch (language) {
		case "en":
			localeResolver.setDefaultLocale(Locale.ENGLISH);
			break;
		case "vi":
			localeResolver.setDefaultLocale(new Locale("vi", "VN"));
			break;
		default:
			break;
		}
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_Product");
		model.addObject("products", productService.getAllIdAsc());
		model.addObject("categories", categoryServices.getAll());
		model.addObject("even", productService.getAllIdAsc().size()/6);
		return model;
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public String addProduct(@ModelAttribute Product product, @RequestParam String btnSaveInModal,
			HttpServletRequest request, Model model) {
		ModelAndView model1 = new ModelAndView();
		model1.setViewName("admin_Product");
		if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {
			product.setPro_id(Integer.parseInt(btnSaveInModal.split("-")[1]));
			product.setStatus(product.getStatus());
			productService.update(product);
		} else {
			Product pro = product;
			product.setPro_star(0);
			product.setStatus("activated");
			productService.add(product);
			pro.upload.setDescription(productService.getNewestProductId()+"");
			this.doUpload(request, model, pro.upload);
			
		}
		 return "redirect:/productController/products"; 
	}

	@RequestMapping(value = "/product/{pro_id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable(value = "pro_id") int pro_id) {
		imgServices.deleteByProId(pro_id);
		productService.deteleByProId(pro_id);
	}

	@RequestMapping(value = "/aproduct", method = RequestMethod.GET)
	@ResponseBody
	public List<Object> getAProduct(@RequestParam("proId") int proId) {
		Product product = new Product();
		Category cate = new Category();
		product = productService.getAProduct(proId);
		List<Object> list = new ArrayList<Object>();
		System.out.println(product.getCategory().getCate_name());
		list.add(product);
		list.add(categoryServices.get(product.getCategory().getCate_id()));
		return list;
	}

	// Handl file upload
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		/* System.out.println("Target=" + target); */

		if (target.getClass() == MyUploadForm.class) {

			dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		}
	}

	private void doUpload(HttpServletRequest request, Model model, //
			MyUploadForm myUploadForm) {
		String description = myUploadForm.getDescription();
		Product product = new Product();
		Category category = new Category();
		Image img = new Image();
		product = productService.getAProduct(Integer.parseInt(description));
		category = categoryServices.get(product.getCategory().getCate_id());
		System.out.println(category.getCate_name());

		String uploadRootPath = request.getServletContext().getRealPath("/resources/image");
		if (category.getCate_name().equalsIgnoreCase("Books")) {
			uploadRootPath = request.getServletContext().getRealPath("/resources/image/Books");
		} else if (category.getCate_name().equalsIgnoreCase("Smartphone")) {
			uploadRootPath = request.getServletContext().getRealPath("/resources/image/Smartphone");
		} else if (category.getCate_name().equalsIgnoreCase("Fashion")) {
			uploadRootPath = request.getServletContext().getRealPath("/resources/image/Fashion");
		} else if (category.getCate_name().equalsIgnoreCase("Home")) {
			uploadRootPath = request.getServletContext().getRealPath("/resources/image/Home");
		}

		System.out.println(uploadRootPath);

		File uploadRootDir = new File(uploadRootPath);
		if (!uploadRootDir.exists()) {
			uploadRootDir.mkdirs();
		}
		CommonsMultipartFile[] fileDatas = myUploadForm.getFileDatas();
		List<File> uploadedFiles = new ArrayList<File>();
		for (CommonsMultipartFile fileData : fileDatas) {
			String name = fileData.getOriginalFilename();
			System.out.println("Client File Name = " + name);

			if (name != null && name.length() > 0) {
				img = new Image();
				if (category.getCate_name().equalsIgnoreCase("Books")) {
					List<Image> imgs = new ArrayList<Image>();
					img.setImg("/resources/image/Books/" + fileData.getOriginalFilename());
					img.setId(Integer.parseInt(description));
					img.setProduct(product);
					imgs.add(img);
					product.setImg(imgs);
					imgServices.add(img);
				} else if (category.getCate_name().equalsIgnoreCase("Smartphone")) {
					List<Image> imgs = new ArrayList<Image>();
					img.setImg("/resources/image/Phone/" + fileData.getOriginalFilename());
					img.setId(Integer.parseInt(description));
					img.setProduct(product);
					imgs.add(img);
					product.setImg(imgs);
					imgServices.add(img);
				} else if (category.getCate_name().equalsIgnoreCase("Fashion")) {
					List<Image> imgs = new ArrayList<Image>();
					img.setImg("/resources/image/Clothes/" + fileData.getOriginalFilename());
					img.setId(Integer.parseInt(description));
					img.setProduct(product);
					imgs.add(img);
					product.setImg(imgs);
					imgServices.add(img);
				} else if (category.getCate_name().equalsIgnoreCase("Home")) {
					List<Image> imgs = new ArrayList<Image>();
					img.setImg("/resources/image/Home/" + fileData.getOriginalFilename());
					img.setId(Integer.parseInt(description));
					img.setProduct(product);
					imgs.add(img);
					product.setImg(imgs);
					imgServices.add(img);
				}
				try {
					// Tạo file tại Server.
					File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

					// Luồng ghi dữ liệu vào file trên Server.
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(fileData.getBytes());
					stream.close();
					//
					uploadedFiles.add(serverFile);
					System.out.println("Write file: " + serverFile);
				} catch (Exception e) {
					System.out.println("Error Write file: " + name);
				}
			}
		}

		/* return "redirect:/productController/products"; */

	}
	//

	@RequestMapping(value = "/getproducts", method = RequestMethod.GET)
	@ResponseBody
	public List<Product> getUsersByOrder(@RequestParam("by") String by) {
		List<Account_detail> list = new ArrayList<Account_detail>();
		List<Product> products = new ArrayList<Product>();
		if (by.equalsIgnoreCase("nameDsc"))
			products = productService.getAllAlphaDsc();
		else if (by.equalsIgnoreCase("nameAsc"))
			products = productService.getAllAlphaAsc();
		else if (by.equalsIgnoreCase("idDsc"))
			products = productService.getAllIdDsc();
		else
			products = productService.getAllIdAsc();
		return products;
	}
	
	@RequestMapping(value = "/getpros", method = RequestMethod.GET)
	@ResponseBody
	public List<Product> getProductsSearching(@RequestParam("key") String key) {
		List<Product> products = new ArrayList<Product>();
		products = productService.getResultOfSearching(key);
		return products;
	}

	@RequestMapping(value = "/block/{proId}", method = RequestMethod.POST)
	@ResponseBody
	public void block(@PathVariable("proId") int proId) {
		Product product = productService.getAProduct(proId);
		product.setStatus("blocked");
		productService.update(product);
	}

	@RequestMapping(value = "/activate/{proId}", method = RequestMethod.POST)
	@ResponseBody
	public void activate(@PathVariable("proId") int proId) {
		Product product = productService.getAProduct(proId);
		product.setStatus("activated");
		productService.update(product);
	}
}
