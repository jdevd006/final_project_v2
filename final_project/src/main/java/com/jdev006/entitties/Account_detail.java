package com.jdev006.entitties;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account_detail", catalog = "web1")
public class Account_detail {
	private int user_id;
	private String user_name;
	private String password;
	private String email;
	private String phone;
	private String img;
	private String status;

	private Account_role account_role;
	private Address address;
	private List<Bill> bills = new ArrayList<Bill>();
	/* private Message message; */
	private List<Message> message = new ArrayList<Message>();
	private List<Review_Product> reviews = new ArrayList<>();
	private List<ShippingAddress> addresses = new ArrayList<ShippingAddress>();

	public Account_detail() {
	}

	public Account_detail(int user_id, String user_name, String password, String email, String phone, String img,
			Account_role role) {
		this.user_id = user_id;
		this.user_name = user_name;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.img = img;
		this.account_role = role;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Column(name = "user_name", nullable = false)
	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "email", nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone", nullable = false)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JsonIgnore
	@JoinColumn(name = "roleId", insertable = false, updatable = false)
	public Account_role getAccount_role() {
		return account_role;
	}

	public void setAccount_role(Account_role account_role) {
		this.account_role = account_role;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JsonIgnore
	@JoinColumn(name = "addressId")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@OneToMany(mappedBy = "account_detail", cascade = CascadeType.ALL)
	@JsonIgnore
	public List<Bill> getBills() {
		return bills;
	}

	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}

	@OneToMany(mappedBy = "account_detail", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	public List<Review_Product> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review_Product> reviews) {
		this.reviews = reviews;
	}
	
	@OneToMany
	@JsonIgnore
    @JoinColumn(name = "userId")
	public List<Message> getMessage() {
		return message;
	}

	public void setMessage(List<Message> message) {
		this.message = message;
	}

	@OneToMany(mappedBy = "account_detail", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JsonIgnore
	public List<ShippingAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<ShippingAddress> addresses) {
		this.addresses = addresses;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
