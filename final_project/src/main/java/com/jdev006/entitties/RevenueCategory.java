package com.jdev006.entitties;

public class RevenueCategory {
	private String name;
	private long revenue;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getRevenue() {
		return revenue;
	}

	public void setRevenue(long revenue) {
		this.revenue = revenue;
	}

	public RevenueCategory(String name, long revenue) {
		super();
		this.name = name;
		this.revenue = revenue;
	}

	public RevenueCategory() {
		super();
		revenue = 0;
	}

}
