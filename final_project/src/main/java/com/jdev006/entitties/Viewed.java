package com.jdev006.entitties;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "viewed", catalog = "web1")
public class Viewed {
	@EmbeddedId
	private ViewedId id;

	public Viewed() {
	}

	public Viewed(ViewedId id) {
		this.id = id;
	}

	public ViewedId getId() {
		return id;
	}

	public void setId(ViewedId id) {
		this.id = id;
	}

}
