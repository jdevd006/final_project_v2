package com.jdev006.entitties;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "review_product", catalog = "web1")
public class Review_Product {
	private int id;
	private String content;
	private int pro_star;
	private String name;
	private String email;

	private Account_detail account_detail;
	private Product product;
	
	public Review_Product() {
	}

	public Review_Product(int user_id, int pro_id, String content) {
		this.content = content;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reviewID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "content", nullable = false)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "pro_id")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user_id")
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}
	
	@Column(name = "pro_star")
	public int getPro_star() {
		return pro_star;
	}

	public void setPro_star(int pro_star) {
		this.pro_star = pro_star;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
