package com.jdev006.entitties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "shippingaddress")
public class ShippingAddress {
	private int id;
	private String city;
	private String district;
	private String name;
	private Account_detail account_detail;

	public ShippingAddress() {

	}

	public ShippingAddress(int id, String city, String district, Account_detail account_detail) {
		this.id = id;
		this.city = city;
		this.district = district;
		this.account_detail = account_detail;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "district")
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JsonIgnore
	@JoinColumn(name = "userId")
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
