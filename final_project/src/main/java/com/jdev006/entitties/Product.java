package com.jdev006.entitties;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product", catalog = "web1")
public class Product {
	private int pro_id;
	private String pro_name;
	private int pro_cost;
	private String pro_content;
	private int pro_quantity;
	private int pro_star;
	private int buyTime;
	private String status;
	private Category category;
	private List<Image> img = new ArrayList<>();
	private Cart cart;
	public MyUploadForm upload = new MyUploadForm();
	/* private List<Bill_detail> bill_detail = new ArrayList<Bill_detail>(); */
	/*private List<Bill> bills = new ArrayList<Bill>();*/
	private List<Question_Answer> q_a = new ArrayList<Question_Answer>();
	private List<Review_Product> reviews = new ArrayList<>();

	public Product() {
	}

	@Id
	@Column(name = "pro_id", unique = true, nullable = false, insertable = true, updatable = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getPro_id() {
		return pro_id;
	}

	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}

	@Column(name = "pro_name", nullable = true)
	public String getPro_name() {
		return pro_name;
	}

	public void setPro_name(String pro_name) {
		this.pro_name = pro_name;
	}

	@Column(name = "pro_cost", nullable = true)
	public int getPro_cost() {
		return pro_cost;
	}

	public void setPro_cost(int pro_cost) {
		this.pro_cost = pro_cost;
	}

	@Column(name = "pro_content", nullable = true)
	public String getPro_content() {
		return pro_content;
	}

	public void setPro_content(String pro_content) {
		this.pro_content = pro_content;
	}

	@Column(name = "pro_quantity", nullable = true)
	public int getPro_quantity() {
		return pro_quantity;
	}

	public void setPro_quantity(int pro_quantity) {
		this.pro_quantity = pro_quantity;
	}

	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "cate_id")
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JsonIgnore
	public List<Image> getImg() {
		return img;
	}

	public void setImg(List<Image> img) {
		this.img = img;
	}

	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<Question_Answer> getQ_a() {
		return q_a;
	}

	public void setQ_a(List<Question_Answer> q_a) {
		this.q_a = q_a;
	}

	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<Review_Product> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review_Product> reviews) {
		this.reviews = reviews;
	}
	
	@Column(name = "pro_star")
	public int getPro_star() {
		return pro_star;
	}

	public void setPro_star(int pro_star) {
		this.pro_star = pro_star;
	}

	@Column(name = "buytime")
	public int getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(int buyTime) {
		this.buyTime = buyTime;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public MyUploadForm getUpload() {
		return upload;
	}

	public void setUpload(MyUploadForm upload) {
		this.upload = upload;
	}
	
	
	
}
