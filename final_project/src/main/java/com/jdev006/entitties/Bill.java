package com.jdev006.entitties;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bill", catalog = "web1")
public class Bill {
	private int bill_id;
	private int total;
	private String order_date;
	private String due_date;
	private String address;
	private String name;
	private String phone;
	private String email;
	private String status;
	private String paymentMode;
	private Account_detail account_detail;

	public Bill() {
	}

	public Bill(int bill_id, int total, String order_date, Account_detail account_detail) {
		this.bill_id = bill_id;
		this.total = total;
		this.order_date = order_date;
		this.account_detail = account_detail;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getBill_id() {
		return bill_id;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	@Column(name = "total", nullable = false)
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Column(name = "order_date", nullable = false)
	public String getOrder_date() {
		return order_date;
	}

	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user_id")
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}

	@Column(name = "due_date")
	public String getDue_date() {
		return due_date;
	}

	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "staus")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "paymentMode")
	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	
}