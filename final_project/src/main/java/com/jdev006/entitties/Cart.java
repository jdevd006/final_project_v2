package com.jdev006.entitties;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "cart", catalog = "web1")
public class Cart {
	private int num;
	
	@EmbeddedId
	private CartId id;
	
	public Cart(int num) {
		this.num = num;
	}

	public Cart() {
	}
	
	@Column(name = "num", nullable = false)
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public CartId getId() {
		return id;
	}

	public void setId(CartId id) {
		this.id = id;
	}
	
}
