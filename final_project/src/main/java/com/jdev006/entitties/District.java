package com.jdev006.entitties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district", catalog = "web1")
public class District {
	private int id;
	private String name;
	
	private City city;

	public District() {
	}

	public District(int id, String name, City city) {
		this.id = id;
		this.name = name;
		this.city = city;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "city_id", nullable = false, referencedColumnName="id")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
		
}
