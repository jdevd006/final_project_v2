package com.jdev006.entitties;

public class Item {
	private Product product;
	private int num;

	public Item() {
	}

	public Item(Product product, int num) {
		this.product = product;
		this.num = num;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
