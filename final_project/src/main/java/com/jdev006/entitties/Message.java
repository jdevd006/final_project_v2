package com.jdev006.entitties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "message")
public class Message {
	private int id;
	private String question;
	private String answer;
	private String email;
	private String name;
	private String status;
	
	private Account_detail account_detail;

	public Message() {
	}

	public Message(int id, String question, String answer) {
		this.id = id;
		this.question = question;
		this.answer = answer;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "question", nullable = false)
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(name = "answer", nullable = false)
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/*
	 * @OneToOne
	 * 
	 * @JsonIgnore
	 * 
	 * @JoinColumn(name = "user_id") public Account_detail getAccount_detail() {
	 * return account_detail; }
	 * 
	 * public void setAccount_detail(Account_detail account_detail) {
	 * this.account_detail = account_detail; }
	 */
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "userId", insertable = false, updatable = false)
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
