package com.jdev006.entitties;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CartId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "pro_id")
	private int pro_id;
	@Column(name = "user_id")
	private int user_id;

	public CartId() {
	}

	public CartId(int pro_id, int user_id) {
		this.pro_id = pro_id;
		this.user_id = user_id;
	}

	public int getpro_id() {
		return pro_id;
	}

	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof CartId))
			return false;
		CartId that = (CartId) o;
		return Objects.equals(getpro_id(), that.getpro_id()) && Objects.equals(getUser_id(), that.getUser_id());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getpro_id(), getUser_id());
	}
}
