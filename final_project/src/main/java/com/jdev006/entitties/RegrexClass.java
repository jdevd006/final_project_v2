package com.jdev006.entitties;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegrexClass {
	private static String mail;

	private static String phone;
	
	private static String empty;
	
	public static boolean isValidMail(String email) {
		mail = "^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,5}(\\.[a-z]{2,3}){1,2}$";
		Pattern pt = Pattern.compile(mail);
		Matcher mc = pt.matcher(email);
		if (mc.matches()) return true;
		return false;
	}

	public static boolean isValidPhone(String phonenumber) {
		phone = "^03|09|08[0-9]{8}$";
		Pattern pt = Pattern.compile(phone);
		Matcher mc = pt.matcher(phonenumber);
		if (mc.matches()) return true;
		return false;
	}
	
	public static boolean isEmpty(String str) {
		empty = "\\s+";
		Pattern pt = Pattern.compile(empty);
		Matcher mc = pt.matcher(str);
		if (mc.find()) return true;
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println(RegrexClass.isValidMail("nlthanhititiu17038@gmail.com      "));
		System.out.println(RegrexClass.isValidPhone("0898489101"));
		System.out.println(RegrexClass.isEmpty("123   123"));
	}
}
