package com.jdev006.entitties;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account_role", catalog = "web1")
public class Account_role {
	private String role;
	private int id;

	/*
	 * @OneToOne(mappedBy = "account_role", cascade = CascadeType.ALL) private
	 * Account_detail account_detail;
	 */

	private List<Account_detail> account_detail = new ArrayList<Account_detail>();

	public Account_role() {
	}

	public Account_role(Account_detail account_detail, String role) {
		/* this.account_detail = account_detail; */
		this.role = role;
	}

	@Column(name = "role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/*
	 * @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	 * 
	 * @JsonIgnore
	 * 
	 * @JoinColumn(name = "userId", referencedColumnName = "user_id") public
	 * Account_detail getAccount_detail() { return account_detail; }
	 * 
	 * public void setAccount_detail(Account_detail account_detail) {
	 * this.account_detail = account_detail; }
	 */

	/* @OneToMany(mappedBy = "account_role", cascade = CascadeType.ALL) */
	@OneToMany
	@JsonIgnore
    @JoinColumn(name = "roleId")
	public List<Account_detail> getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(List<Account_detail> account_detail) {
		this.account_detail = account_detail;
	}

}
