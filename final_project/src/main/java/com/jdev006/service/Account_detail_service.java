package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Product;

@Transactional
@Service
public class Account_detail_service {
	@Autowired
	DAO<Account_detail> Account_detail_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Account_detail> getAll() {
		return Account_detail_DAO.getAll();
	}

	public Account_detail get(int id) {
		return Account_detail_DAO.get(id);
	}

	public Account_detail add(Account_detail t) {
		return Account_detail_DAO.add(t);
	}

	public Boolean update(Account_detail t) {
		return Account_detail_DAO.update(t);
	}

	public Boolean delete(Account_detail t) {
		return Account_detail_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Account_detail_DAO.delete(id);
	}
	
	public static void main(String[] args) {
		Account_detail_service a = new Account_detail_service();
		System.out.println(a.getByEmail("lamthanh1451999@gmail.com"));
	}
	
	public Account_detail getByEmail(String email) {
		Account_detail account = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail where email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email", email);
			if (query.list().size() >= 1)
				account = (Account_detail) query.list().get(0);
		return account;
	}
	
	public List<Account_detail> getAllAlphaAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail order by user_name asc";
		Query query = session.createQuery(hql);
		List<Account_detail> listAcc = query.list();
		return listAcc;
	}
	
	public List<Account_detail> getAllAlphaDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail order by user_name desc";
		Query query = session.createQuery(hql);
		List<Account_detail> listAcc = query.list();
		return listAcc;
	} 
	
	public List<Account_detail> getAllIdAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail order by user_id asc";
		Query query = session.createQuery(hql);
		List<Account_detail> listAcc = query.list();
		return listAcc;
	}
	
	public List<Account_detail> getAllIdDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail order by user_id desc";
		Query query = session.createQuery(hql);
		List<Account_detail> listAcc = query.list();
		return listAcc;
	} 
	
	public List<Account_detail> getResultOfSearching(String keyWord) {
		List<Account_detail> products = new ArrayList<Account_detail>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail where user_name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
}
