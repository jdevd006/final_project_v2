package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Category;

@Transactional
@Service
public class Category_service {
	@Autowired
	DAO<Category> Category_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	public List<Category> getAll(){
		return Category_DAO.getAll();
	}
	
	public Category get(int id){
		return Category_DAO.get(id);
	}
	
	public Category add(Category t){
		return Category_DAO.add(t);
	}
	
	public Boolean update(Category t){
		return Category_DAO.update(t);
	}
	
	public Boolean delete(Category t){
		return Category_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Category_DAO.delete(id);
	}
	
	public List<Category> getAllAlphaAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category order by cate_name asc";
		Query query = session.createQuery(hql);
		List<Category> listCate = query.list();
		return listCate;
	}
	
	public List<Category> getAllAlphaDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category order by cate_name desc";
		Query query = session.createQuery(hql);
		List<Category> listCate = query.list();
		return listCate;
	} 
	
	public List<Category> getAllIdAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category order by cate_id asc";
		Query query = session.createQuery(hql);
		List<Category> listCate = query.list();
		return listCate;
	}
	
	public List<Category> getAllIdDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category order by cate_id desc";
		Query query = session.createQuery(hql);
		List<Category> listCate = query.list();
		return listCate;
	} 
	
	public List<Category> getResultOfSearching(String keyWord) {
		List<Category> products = new ArrayList<Category>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category where cate_name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
}
