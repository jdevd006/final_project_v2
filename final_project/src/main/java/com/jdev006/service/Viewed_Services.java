package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Viewed;

@Transactional
@Service
public class Viewed_Services {
	@Autowired
	private DAO<Viewed> Viewed_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Viewed> getAll() {
		return Viewed_DAO.getAll();
	}

	public Viewed get(int id) {
		return Viewed_DAO.get(id);
	}

	public Viewed add(Viewed t) {
		return Viewed_DAO.add(t);
	}

	public Boolean update(Viewed t) {
		return Viewed_DAO.update(t);
	}

	public Boolean delete(Viewed t) {
		return Viewed_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Viewed_DAO.delete(id);
	}
	
	public boolean isExistProduct(int proId, int userId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Viewed where pro_id = :proId and user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			return true;
		return false;
	}
	
	public void deleteByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Viewed where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
}
