package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Question_Answer;

@Transactional
@Service
public class Question_Answer_service {
	@Autowired
	DAO<Question_Answer> Question_Answer_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Question_Answer> getAll() {
		return Question_Answer_DAO.getAll();
	}

	public Question_Answer get(int id) {
		return Question_Answer_DAO.get(id);
	}

	public Question_Answer add(Question_Answer t) {
		return Question_Answer_DAO.add(t);
	}

	public Boolean update(Question_Answer t) {
		return Question_Answer_DAO.update(t);
	}

	public Boolean delete(Question_Answer t) {
		return Question_Answer_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Question_Answer_DAO.delete(id);
	}

	public List<Question_Answer> getAllById(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Question_Answer where pro_id = " + id;
		return session.createQuery(hql).list();
	}
	
	public List<Question_Answer> getQABProId(int proId){
		List<Question_Answer> qas = new ArrayList<Question_Answer>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Question_Answer where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		if (query.list().size() >= 1)
			qas = query.list();
		return qas;
	}
	
	public void deleteByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Question_Answer where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
}
