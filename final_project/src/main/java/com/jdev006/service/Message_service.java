package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.Message;

@Transactional
@Service
public class Message_service {
	@Autowired
	DAO<Message> Message_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Message> getAll(){
		return Message_DAO.getAll();
	}
	
	public Message get(int id){
		return Message_DAO.get(id);
	}
	
	public Message add(Message t){
		return Message_DAO.add(t);
	}
	
	public Boolean update(Message t){
		return Message_DAO.update(t);
	}
	
	public Boolean delete(Message t){
		return Message_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Message_DAO.delete(id);
	}
	
	public List<Message> getMessageByUserId(int userId){
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = new ArrayList<Message>();
		String hql = "from Message where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			messages = query.list();
		return messages;
	}
	
	public List<Message> getMessagesUnreadForAdmin(){
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = new ArrayList<Message>();
		String hql = "from Message where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "unread");
		if (query.list().size() >= 1)
			messages = query.list();
		return messages;
	}
	
	public List<Message> getMessagesReadForAdmin(){
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = new ArrayList<Message>();
		String hql = "from Message where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "read");
		if (query.list().size() >= 1)
			messages = query.list();
		return messages;
	}
	
	public List<Message> getMessagesAnswerd(){
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = new ArrayList<Message>();
		String hql = "from Message where answer = :ans";
		Query query = session.createQuery(hql);
		query.setParameter("ans", "");
		if (query.list().size() >= 1)
			messages = query.list();
		return messages;
	}
	
	public List<Message> getResultOfSearching(String keyWord) {
		List<Message> products = new ArrayList<Message>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Message where name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
	
	public List<Message> getMessagesByUserId(int userId){
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = new ArrayList<Message>();
		String hql = "from Message where userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			messages = query.list();
		return messages;
	}
	
}
