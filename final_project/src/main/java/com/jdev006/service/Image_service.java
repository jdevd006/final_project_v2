package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Image;

@Transactional
@Service
public class Image_service {
	@Autowired
	DAO<Image> Image_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Image> getAll(){
		return Image_DAO.getAll();
	}
	
	public Image get(int id){
		return Image_DAO.get(id);
	}
	
	public Image add(Image t){
		return Image_DAO.add(t);
	}
	
	public Boolean update(Image t){
		return Image_DAO.update(t);
	}
	
	public Boolean delete(Image t){
		return Image_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Image_DAO.delete(id);
	}
	
	public Image getByProId(int proId) {
		Image img = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Image where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		if (query.list().size() >= 1)
			img = (Image)query.list().get(0);
		return img;
	}
	
	public void deleteByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Image where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
}
