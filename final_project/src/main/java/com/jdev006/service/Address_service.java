package com.jdev006.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Address;

@Transactional
@Service
public class Address_service {
	@Autowired
	DAO<Address> Address_DAO;
	
	public List<Address> getAll(){
		return Address_DAO.getAll();
	}
	
	public Address get(int id){
		return Address_DAO.get(id);
	}
	
	public Address add(Address t){
		return Address_DAO.add(t);
	}
	
	public Boolean update(Address t){
		return Address_DAO.update(t);
	}
	
	public Boolean delete(Address t){
		return Address_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Address_DAO.delete(id);
	}
}
