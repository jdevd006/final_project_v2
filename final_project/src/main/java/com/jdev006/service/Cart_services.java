package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Cart;

@Transactional
@Service
public class Cart_services {
	@Autowired
	private DAO<Cart> Cart_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Cart> getAll(){
		return Cart_DAO.getAll();
	}
	
	public Cart get(int id){
		return Cart_DAO.get(id);
	}
	
	public Cart add(Cart t){
		return Cart_DAO.add(t);
	}
	
	public Boolean update(Cart t){
		return Cart_DAO.update(t);
	}
	
	public Boolean delete(Cart t){
		return Cart_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Cart_DAO.delete(id);
	}
	
	public void deleteByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Cart where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
	
	public Cart getCartByProIdUserId(int proId, int userId) {
		Cart cart = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Cart where pro_id = :proId and user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			cart = (Cart) query.list().get(query.list().size()-1);
		return cart;
	}
}
