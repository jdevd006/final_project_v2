package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.ShippingAddress;

@Transactional
@Service
public class ShippingAddressServices {
	@Autowired
	DAO<ShippingAddress> shippingAddressDao;
	@Autowired
	private SessionFactory sessionFactory;

	public List<ShippingAddress> getAll() {
		return shippingAddressDao.getAll();
	}

	public ShippingAddress get(int id) {
		return shippingAddressDao.get(id);
	}

	public ShippingAddress add(ShippingAddress t) {
		return shippingAddressDao.add(t);
	}

	public Boolean update(ShippingAddress t) {
		return shippingAddressDao.update(t);
	}

	public Boolean delete(ShippingAddress t) {
		return shippingAddressDao.delete(t);
	}

	public Boolean delete(int id) {
		return shippingAddressDao.delete(id);
	}
	
	public List<ShippingAddress> getListByUserId(int userId){
		List<ShippingAddress> shippingAddresses = new ArrayList<ShippingAddress>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from ShippingAddress where userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() >= 1)
			shippingAddresses = query.list();
		return shippingAddresses;
	}
}
