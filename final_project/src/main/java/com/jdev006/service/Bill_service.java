package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.Product;

@Transactional
@Service
public class Bill_service {
	@Autowired
	DAO<Bill> Bill_DAO;
	@Autowired
	private BillDetaiService billDetailServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private SessionFactory sessionFactory;
	public List<Bill> getAll(){
		return Bill_DAO.getAll();
	}
	
	public Bill get(int id){
		return Bill_DAO.get(id);
	}
	
	public Bill add(Bill t){
		return Bill_DAO.add(t);
	}
	
	public Boolean update(Bill t){
		return Bill_DAO.update(t);
	}
	
	public Boolean delete(Bill t){
		return Bill_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Bill_DAO.delete(id);
	}
	
	public Bill getBillByUserId(int userId) {
		Bill bill = new Bill();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() == 0) return null;
		return (Bill) query.list().get(query.list().size()-1);
	}
	
	public List<Bill> getListBillByUserId(int userId) {
		Bill bill = new Bill();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() == 0) return null;
		return query.list();
	}
	
	public void deteleByProId(int proId, int userId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete Cart where pro_id = :proId and user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.setParameter("userId", userId);
		query.executeUpdate();
	}
	
	public List<Bill> getBillUnPaid(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "Unpaid");
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillProcessing(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "Processing");
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillFinish(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "Finish");
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillWaiting(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "Waiting");
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillCanceled(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", "Canceled");
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillTotalAsc(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill order by total asc";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillTotalDsc(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill order by total desc";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillIdAsc(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill order by bill_id asc";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Bill> getBillIdDsc(){
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill order by bill_id desc";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			bills = query.list();
		return bills;
	}
	
	public List<Long> getRevenueOfYear(int year) {
		double temp = 0;
		List<Long> revenue = new ArrayList<Long>();
		List<Bill> bills = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where order_date between :start and :end";
		Query query = session.createQuery(hql);
		query.setParameter("start", year+"-01-01");
		query.setParameter("end", year+"-12-31");
		
		for (int i = 0 ; i < 12; i++) {
			hql = "select sum(total) from Bill where order_date between :start and :end";
			query = session.createQuery(hql);
			if (i < 9) {
				query.setParameter("start", year+"-0"+(i+1)+"-01");
				query.setParameter("end", year+"-0"+(i+1)+"-28");
			}else {
				query.setParameter("start", year+"-"+(i+1)+"-01");
				query.setParameter("end", year+"-"+(i+1)+"-28");
			}
			if (query.list().size() >= 1)
				revenue.add((Long) query.list().get(0));
			else
				revenue.add((long) 0);
		}
		for (Long d : revenue)
			System.out.println(d);
		return revenue;
	}
	
	public List<Item> getListByDuration(String startDay, String endDay, String startMonth, String endMonth, String year){
		List<Item> products = new ArrayList<Item>();
		List<Bill> bills = new ArrayList<Bill>();
		List<BillDetail>  billDetails = new ArrayList<BillDetail>();
		Item item;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where order_date between :start and :end";
		Query query = session.createQuery(hql);
		query.setParameter("start", year+"-"+startMonth+"-"+startDay);
		query.setParameter("end", year+"-"+endMonth+"-"+endDay);
		
		if (query.list().size() >= 1) {
			bills = query.list();
			for (Bill bill : bills) {
				billDetails = billDetailServices.getListByBillId(bill.getBill_id());
				for (BillDetail billDetail : billDetails) {
					item = new Item();
					item.setProduct(productServices.getAProduct(billDetail.getId().getpro_id()));
					item.setNum(billDetail.getNum());
					products.add(item);
				}
			}
		}
		return products;
	}
	
	public List<Bill> getResultNameOfSearching(String keyWord) {
		List<Bill> products = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
	
	public List<Bill> getResultDateOfSearching(String keyWord) {
		List<Bill> products = new ArrayList<Bill>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where order_date like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
}
