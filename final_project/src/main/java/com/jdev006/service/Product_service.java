package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Product;

@Transactional
@Service
public class Product_service {
	@Autowired
	DAO<Product> Product_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Product> getAll() {
		return Product_DAO.getAll();
	}

	public Product get(int id) {
		return Product_DAO.get(id);
	}

	public Product add(Product t) {
		return Product_DAO.add(t);
	}

	public Boolean update(Product t) {
		return Product_DAO.update(t);
	}

	public Boolean delete(Product t) {
		return Product_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Product_DAO.delete(id);
	}

	public void deteleByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}

	public Product getAProduct(int proId) {
		Product product = new Product();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		product = (Product) query.list().get(query.list().size() - 1);
		return product;
	}

	public List<Product> getAllAlphaAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product order by pro_name asc";
		Query query = session.createQuery(hql);
		List<Product> listPro = query.list();
		return listPro;
	}

	public List<Product> getAllAlphaDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product order by pro_name desc";
		Query query = session.createQuery(hql);
		List<Product> listPro = query.list();
		return listPro;
	}

	public List<Product> getAllIdAsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product order by pro_id asc";
		Query query = session.createQuery(hql);
		List<Product> listPro = query.list();
		return listPro;
	}

	public List<Product> getAllIdDsc() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product order by pro_id desc";
		Query query = session.createQuery(hql);
		List<Product> listPro = query.list();
		return listPro;
	}
	
	public List<Product> getpro(int cateId, int start){
		Session session = sessionFactory.getCurrentSession();
		List<Product> products = new ArrayList<>();
		String hql = "from Product where cate_id = :cateId";
		Query query = session.createQuery(hql);
		query.setParameter("cateId", cateId);
		query.setFirstResult(start);
		query.setMaxResults(12);
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}

	public List<Product> getListByCateId(int cateId){
		List<Product> products = new ArrayList<Product>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product where cate_id = :cateId";
		Query query = session.createQuery(hql);
		query.setParameter("cateId", cateId);
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
	public List<Product> getResultOfSearching(String keyWord) {
		List<Product> products = new ArrayList<Product>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product where pro_name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyWord+"%");
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
	
	public int getNewestProductId() {
		int proId = 0;
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(p.pro_id) from Product p";
		Query query = session.createQuery(hql);
		proId = (int) query.list().get(0);
		return proId;
	}
	
	public List<Product> getFirstTenSeller(){
		List<Product> products = new ArrayList<Product>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product order by buytime desc";
		Query query = session.createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(10);
		if (query.list().size() >= 1)
			products = query.list();
		return products;
	}
}
