package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Question_Answer;
import com.jdev006.entitties.Review_Product;

@Transactional
@Service
public class Review_Product_service {
	@Autowired
	DAO<Review_Product> Review_Product_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Review_Product> getAll(){
		return Review_Product_DAO.getAll();
	}
	
	public Review_Product get(int id){
		return Review_Product_DAO.get(id);
	}
	
	public Review_Product add(Review_Product t){
		return Review_Product_DAO.add(t);
	}
	
	public Boolean update(Review_Product t){
		return Review_Product_DAO.update(t);
	}
	
	public Boolean delete(Review_Product t){
		return Review_Product_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Review_Product_DAO.delete(id);
	}
	
	public List<Review_Product> getReviewsByProId(int proId){
		List<Review_Product> reviews = new ArrayList<Review_Product>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Review_Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		if (query.list().size() >= 1)
			reviews = query.list();
		return reviews;
		
	}
	
	public void deleteByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Review_Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
}
