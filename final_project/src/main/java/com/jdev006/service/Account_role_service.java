package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_role;

@Transactional
@Service
public class Account_role_service {
	@Autowired
	DAO<Account_role> Account_role_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Account_role> getAll() {
		return Account_role_DAO.getAll();
	}

	public Account_role get(int id) {
		return Account_role_DAO.get(id);
	}

	public Account_role add(Account_role t) {
		return Account_role_DAO.add(t);
	}

	public Boolean update(Account_role t) {
		return Account_role_DAO.update(t);
	}

	public Boolean delete(Account_role t) {
		return Account_role_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Account_role_DAO.delete(id);
	}

	public Account_role getByUserId(int userId) {
		Account_role accountRole = new Account_role();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_role where userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		accountRole = (Account_role) query.list().get(query.list().size()-1);
		return accountRole;
	}
	
	public void updateByUserId(int userId, String role) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "update Account_role set role = :role where userId = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("role", role);
		query.setParameter("userId", userId);
		query.executeUpdate();
	}
	
	public void deleteByUserId(int userId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete Account_role where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		query.executeUpdate();
	}
}
