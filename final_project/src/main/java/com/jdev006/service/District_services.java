package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.City;
import com.jdev006.entitties.District;

@Transactional
@Service
public class District_services {
	@Autowired
	DAO<District> District_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<District> getAll(){
		return District_DAO.getAll();
	}
	
	public District get(int id){
		return District_DAO.get(id);
	}
	
	public District add(District t){
		return District_DAO.add(t);
	}
	
	public Boolean update(District t){
		return District_DAO.update(t);
	}
	
	public Boolean delete(District t){
		return District_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return District_DAO.delete(id);
	}
	
	public List<District> getAll2(){
		Session session = sessionFactory.getCurrentSession();
		List<District> list = new ArrayList<District>();
		String hql = "from District";
		Query query = session.createQuery(hql);
		list = query.list();
		System.out.println(list);
		return list;
	}
	
	public List<District> getListByCityId(int cityId){
		Session session = sessionFactory.getCurrentSession();
		List<District> list = new ArrayList<District>();
		String hql = "from District where city_id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", cityId);
		list = query.list();
		return list;
	}
}
