package com.jdev006.config;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.Address;
import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.BillDetailId;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.Category;
import com.jdev006.entitties.City;
import com.jdev006.entitties.District;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.Question_Answer;
import com.jdev006.entitties.Review_Product;
import com.jdev006.entitties.Viewed;

@EnableTransactionManagement
@Configuration
public class DatabaseConfig extends WebMvcConfigurerAdapter {
	@Bean
	public LocalSessionFactoryBean sessionFactory(BasicDataSource dataSource) {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		sessionFactory.setPackagesToScan(new String[] { "com.jdev006.entitties" });
		sessionFactory.setAnnotatedClasses(Viewed.class,Account_detail.class, Account_role.class, Bill.class, Address.class, Product.class, Category.class, Question_Answer.class, Review_Product.class, Image.class, City.class, District.class, Cart.class, BillDetail.class);
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.format_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}

	@Bean(name = "dataSource")
	public BasicDataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/web1");
		dataSource.setUsername("root");
		dataSource.setPassword("nlthanh");
		dataSource.setInitialSize(10);
		return dataSource;
	}

	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}
