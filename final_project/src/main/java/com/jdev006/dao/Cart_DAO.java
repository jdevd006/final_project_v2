package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Cart;
import com.jdev006.entitties.CartId;

@Repository
public class Cart_DAO extends DAO<Cart>{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Cart> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Cart").list();
	}

	@Override
	public Cart get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		CartId c = new CartId();
		c.setPro_id(id);
		return (Cart) session.get(Cart.class, c);
	}

	@Override
	public Cart add(Cart cart) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(cart);
		return cart;
	}

	@Override
	public Boolean update(Cart cart) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(cart);
			return Boolean.TRUE;
		} catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Cart cart) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != cart)
		{
			try {
				cart = (Cart) session.merge(cart);
				session.delete(cart);
				return Boolean.TRUE;
			}
			catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Cart cart = (Cart) session.load(Cart.class, id);
		if(null != cart)
		{
			session.delete(cart);
		}
		return Boolean.FALSE;
	}
}
