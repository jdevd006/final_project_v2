package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Account_detail;

@Repository
public class Account_detailDAO extends DAO<Account_detail> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Account_detail> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Account_detail").list();
	}

	@Override
	public Account_detail get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Account_detail) session.get(Account_detail.class, id);
	}

	@Override
	public Account_detail add(Account_detail account_detail) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(account_detail);
		return account_detail;
	}

	@Override
	public Boolean update(Account_detail account_detail) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(account_detail);
			return Boolean.TRUE;
		}catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Account_detail account_detail) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != account_detail) {
			try {
				session.delete(account_detail);
				return Boolean.TRUE;
			} catch(Exception e){
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Account_detail account_detail = (Account_detail) session.load(Account_detail.class, id);
		if(null != account_detail)
		{
			session.delete(account_detail);
			return Boolean.TRUE;
		}
				return Boolean.FALSE;
	}

}
