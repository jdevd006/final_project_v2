package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.ShippingAddress;

@Repository
public class ShippingAddressDAO extends DAO<ShippingAddress> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<ShippingAddress> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from ShippingAddress").list();
	}

	@Override
	public ShippingAddress get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (ShippingAddress) session.get(ShippingAddress.class, id);
	}

	@Override
	public ShippingAddress add(ShippingAddress shippingAddress) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(shippingAddress);
		return shippingAddress;
	}

	@Override
	public Boolean update(ShippingAddress shippingAddress) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(shippingAddress);
			return Boolean.TRUE;
		}catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(ShippingAddress shippingAddress) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != shippingAddress) {
			try {
				session.delete(shippingAddress);
				return Boolean.TRUE;
			} catch(Exception e){
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ShippingAddress shippingAddress = (ShippingAddress) session.load(ShippingAddress.class, id);
		if(null != shippingAddress)
		{
			session.delete(shippingAddress);
			return Boolean.TRUE;
		}
				return Boolean.FALSE;
	}

}
