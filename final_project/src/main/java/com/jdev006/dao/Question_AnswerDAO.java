package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Question_Answer;
@Repository
public class Question_AnswerDAO extends DAO<Question_Answer> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Question_Answer> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Question_Answer").list();
	}

	@Override
	public Question_Answer get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Question_Answer) session.get(Question_Answer.class, id);
	}

	@Override
	public Question_Answer add(Question_Answer question_answer) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(question_answer);
		return question_answer;
	}

	@Override
	public Boolean update(Question_Answer question_answer) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(question_answer);
			return Boolean.TRUE;
		}catch(Exception e)
		{
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Question_Answer question_answer) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != question_answer)
		{
			try {
				session.update(question_answer);
				return Boolean.TRUE;
			}catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Question_Answer question_answer = (Question_Answer) session.load(Question_Answer.class, id);
		if(null != question_answer)
		{
			session.delete(question_answer);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
