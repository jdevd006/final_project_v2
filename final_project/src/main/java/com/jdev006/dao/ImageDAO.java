package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Image;
@Repository
public class ImageDAO extends DAO<Image> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Image> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Image").list();
	}

	@Override
	public Image get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Image) session.get(Image.class, id);
	}

	@Override
	public Image add(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(image);
		return image;
	}

	@Override
	public Boolean update(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(image);
			return Boolean.TRUE;
		}
		catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Image image) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != image)
		{
			try {
				session.update(image);
				return Boolean.TRUE;
			}
			catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Image image = (Image) session.load(Image.class, id);
		if(null != image)
		{
			session.delete(image);
		}
		return Boolean.FALSE;
	}

}
