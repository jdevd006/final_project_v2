package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Message;
@Repository
public class MessageDAO extends DAO<Message> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Message> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Message").list();
	}

	@Override
	public Message get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Message) session.get(Message.class, id);
	}

	@Override
	public Message add(Message message) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(message);
		return message;
	}

	@Override
	public Boolean update(Message message) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(message);
			return Boolean.TRUE;
		}catch(Exception e){
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Message message) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != message)
		{
			try {
				session.update(message);
				return Boolean.TRUE;
			}catch(Exception e){
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Message message = (Message) session.load(Message.class, id);
		if(null != message)
		{
				session.delete(message);
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
