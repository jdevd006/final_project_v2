package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Address;
@Repository
public class AddressDAO extends DAO<Address>{
	@Autowired 
	private SessionFactory sessionFactory;

	@Override
	public List<Address> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Address").list();
	}

	@Override
	public Address get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return  (Address) session.get(Address.class, id);
	}

	@Override
	public Address add(Address address) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(address);
		return address;
	}

	@Override
	public Boolean update(Address address) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(address);
			return Boolean.TRUE;
		} catch(Exception e)
		{
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Address address) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != address) {
			try {
				session.delete(address);
				return Boolean.TRUE;
			} catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Address address = (Address) session.load(Address.class, id);
		if(null != address)
		{
			session.delete(address);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
