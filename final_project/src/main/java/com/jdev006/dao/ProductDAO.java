package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Product;
@Repository
public class ProductDAO extends DAO<Product> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Product> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Product").list();
	}

	@Override
	public Product get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Product) session.get(Product.class, id);
	}

	@Override
	public Product add(Product product) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(product);
		return product;
	}

	@Override
	public Boolean update(Product product) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(product);
			return Boolean.TRUE;
		}
		catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Product product) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != product)
		{
			try {
				session.update(product);
				return Boolean.TRUE;
			}
			catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Product product = (Product) session.load(Product.class, id);
		if(null != product)
		{
				session.update(product);
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
