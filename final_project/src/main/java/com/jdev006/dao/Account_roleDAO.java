package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Account_role;
@Repository
public class Account_roleDAO extends DAO<Account_role>{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Account_role> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Account_role").list();
	}

	@Override
	public Account_role get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Account_role) session.get(Account_role.class, id);
	}

	@Override
	public Account_role add(Account_role account_role) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(account_role);
		return account_role;
	}

	@Override
	public Boolean update(Account_role account_role) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(account_role);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;	
		}
	}

	@Override
	public Boolean delete(Account_role account_role) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != account_role) {
			try {
				session.delete(account_role);
				return Boolean.TRUE;
			} catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Account_role account_role = (Account_role) session.load(Account_role.class, id);
		if(null !=account_role)
		{
			session.delete(account_role);
			return Boolean.TRUE;
		}
				return Boolean.FALSE;
	}
	
}
