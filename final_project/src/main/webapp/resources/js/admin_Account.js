var noUser = 0;

function setUser(t) {
	if (t == undefined)
		noUser = 1;
}

function getSelectValue() {
	return document.getElementById("followSelect").value;
}

function getRadioValue() {
	if (document.getElementById("ascRd").checked == true)
		return "asc";
	return "dsc";
}

function getUsersByOrder(order) {
	$.ajax({
		url : "./getusers?by=" + order,
		type : "GET",
		data : '',
		contentType : "application/json",
		success : function(data) {
			resetUserTable(data);
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function selectClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function resetUserTable(listObject) {
	var c = 0;
	var max;
	if (listObject.length % 6 == 0) {
		max = Math.floor(listObject.length / 6);
	} else {
		max = Math.floor(listObject.length / 6) + 1;
	}
	for (var k = 0; k < max; k++) {
		var usersTable = document.getElementById("users-table" + k);
		var tbodyTag = usersTable.getElementsByTagName("tbody");
		var trTag = tbodyTag[0].getElementsByTagName("tr");
		var tdTag;
		var btnTag;
		// Delete old data
		for (var i = trTag.length; i >= 1; i--)
			usersTable.deleteRow(i);

		// Set table
		for (var i = 0; i < 6; i++) {
			if (c <= listObject.length - 1) {
				trTag = document.createElement("tr");
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].account.user_id;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].account.user_name;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].account.email;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				if (listObject[c].account.address == null)
					tdTag.innerHTML = "";
				else
					tdTag.innerHTML = listObject[c].account.address.city + "-"
							+ listObject[c].account.address.district + "-"
							+ listObject[c].account.address.street;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].account.phone;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].role.role;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].account.status;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Update";
				btnTag.className = "btn btn-primary";
				btnTag.setAttribute("data-toggle", "modal");
				btnTag.setAttribute("data-target", "#myModal");
				var t = function(btn, id) {
					return function() {
						update(btn, id);
					}
				}
				btnTag.onclick = t(btnTag, listObject[c].account.user_id);
				tdTag.appendChild(btnTag);
				if (listObject[c].account.status == "activated") {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Block";
					btnTag.className = "btn btn-warning";
					var t = function(userId, order, btn, orderTable) {
						return function() {
							block(userId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].account.user_id, i + 1,
							btnTag, k);
					tdTag.appendChild(btnTag);
				} else {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Activate";
					btnTag.className = "btn btn-warning";
					var t = function(userId, order, btn, orderTable) {
						return function() {
							activate(userId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].account.user_id, i + 1,
							btnTag, k);
					tdTag.appendChild(btnTag);
				}
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Delete";
				btnTag.className = "btn btn-danger";
				btnTag.value = c;
				t = function(btn, id, orderTable) {
					return function() {
						deleterUser(btn, id, orderTable);
					}
				}
				btnTag.onclick = t(btnTag, listObject[c].account.user_id, k);
				tdTag.appendChild(btnTag);
				trTag.appendChild(tdTag);
				tbodyTag[0].appendChild(trTag);
				c++;
			}
		}
	}
}

function radioClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function cleanAddModal() {
	document.getElementById("btnSaveInModal").value = "";

	document.getElementById("txtUsername").value = "";
	document.getElementById("txtUseremail").value = "";
	document.getElementById("txtUserphone").value = "";
	document.getElementById("txtUserpassword").value = "";
	document.getElementById("adminRd").checked = false;
	document.getElementById("userRd").checked = false;
}

function getCities() {
	$.ajax({
		url : "../cartController/cities",
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("city-select-tag");
			var option;
			var txt;
			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				option.id = "c" + data[i].name;
				var aFunction = function(op){
					return function (){
						document.getElementById("txtCity").value = op.innerHTML;
						getDistricts(op);
					}
				}
				option.onclick = aFunction(option);
				selectTag.appendChild(option);
			}
		}
	});
}

function getDistricts(option) {
	$.ajax({
		url : "../cartController/districts/" + option.value,
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("district-select-tag");
			var option = selectTag.getElementsByTagName("option");
			var txt;

			// Delete all district after selecting another city
			for (var i = option.length - 1; i > 0; i--) {
				selectTag.remove(1)
			}

			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				option.id = "d" + data[i].name;
				var aFunction = function(op){
					return function (){
						document.getElementById("txtDistrict").value = op.innerHTML;
					}
				}
				option.onclick = aFunction(option);
				selectTag.appendChild(option);
			}
		}
	});
}


// Search function
var first = 0;
$(document)
		.ready(
				function() {
					$("#searchName").keydown(
							function() {
								if ($("#searchName").val().split(/\s+/)
										.join("").length == 0) {
									$("#users").show();
									$("#searchResult").hide();
								} else {
									$("#users").hide();
									$("#searchResult").show();
								}
							});
					$("#searchName")
							.keyup(
									function() {
										if ($("#searchName").val().split(/\s+/)
												.join("").length == 0) {
											$("#users").show();
											$("#searchResult").hide();
										} else {
											$("#users").hide();
											$("#searchResult").show();
											$
													.ajax({
														url : "./getaccs?key="
																+ $(
																		"#searchName")
																		.val()
																		.trim(),
														type : "GET",
														data : '',
														contentType : "application/json",
														success : function(
																listObject) {
															var usersTable = document
																	.getElementById("userSearchTable");
															var tbodyTag = usersTable
																	.getElementsByTagName("tbody");
															var trTag = tbodyTag[0]
																	.getElementsByTagName("tr");
															var tdTag;
															var btnTag;
															// Delete old data
															var k = 0;
															for (var i = trTag.length; i >= 1; i--)
																usersTable
																		.deleteRow(i);

															// Set table
															for (var i = 0; i < listObject.length; i++) {
																trTag = document
																		.createElement("tr");
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].account.user_id;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].account.user_name;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].account.email;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																if (listObject[i].account.address == null)
																	tdTag.innerHTML = "";
																else
																	tdTag.innerHTML = listObject[i].account.address.city
																			+ "-"
																			+ listObject[i].account.address.district
																			+ "-"
																			+ listObject[i].account.address.street;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].account.phone;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].role.role;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].account.status;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Update";
																btnTag.className = "btn btn-primary";
																btnTag
																		.setAttribute(
																				"data-toggle",
																				"modal");
																btnTag
																		.setAttribute(
																				"data-target",
																				"#myModal");
																var t = function(
																		btn, id) {
																	return function() {
																		update(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		listObject[i].account.user_id);
																tdTag
																		.appendChild(btnTag);
																if (listObject[i].status == "activated") {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Block";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			userId,
																			order,
																			btn,
																			orderTable) {
																		return function() {
																			blockForSearch(
																					userId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].account.user_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																} else {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Activate";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			userId,
																			order,
																			btn,
																			orderTable) {
																		return function() {
																			activateForSearch(
																					userId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].account.user_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																}
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Delete";
																btnTag.className = "btn btn-danger";
																btnTag.value = i;
																t = function(
																		btn,
																		id,
																		orderTable) {
																	return function() {
																		deleteForSearch(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		listObject[i].account.user_id);
																tdTag
																		.appendChild(btnTag);
																trTag
																		.appendChild(tdTag);
																tbodyTag[0]
																		.appendChild(trTag);
															}
														}
													});
										}
									});
				});

function deleteForSearch(button, id) {
	$.ajax({
		url : "./user/" + id,
		type : "DELETE",
		data : "",
		contentType : "application/json",
		success : function() {
			var table = document.getElementById("userSearchTable");
			table.deleteRow(button.value);
			//
			var tr = table.getElementsByTagName("tr");
			for (var i = 1; i < tr.length; i++) {
				tr[i].getElementsByTagName("button")[1].value = i;
			}
		}
	});
}

function blockForSearch(userId, order, button) {
	$.ajax({
		url : "./block/" + userId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[6].innerHTML = "blocked";
			button.innerHTML = "Activate";
			var func = function(userId1, order1, button1) {
				return function() {
					activateForSearch(userId1, order1, button1);
				}
			}
			button.onclick = func(userId, order, button);
		}
	});
}

function activateForSearch(userId, order, button) {
	$.ajax({
		url : "./activate/" + userId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[6].innerHTML = "activated";
			button.innerHTML = "Block";
			var func = function(userId1, order1, button1) {
				return function() {
					blockForSearch(userId1, order1, button1);
				}
			}
			button.onclick = func(userId, order, button);
		}
	});
}
