//function deleteItem(pro_id, order) {
//	var url = "../cartController/item?proid=" + pro_id;
//	$.ajax({
//		url : url,
//		type : 'POST',
//		data : '',
//		contentType : 'application/json',
//		success : function(data) {
//			alert(1);
//		},
//		error : function(request, msg, error) {
//		}
//	});
//}
function changeWhenDescrease(pro_id, order) {
	var url = "../cartController/item?proid=" + pro_id + "&task=" + "descrease";
	$
			.ajax({
				url : url,
				type : 'POST',
				data : '',
				contentType : 'application/json',
				success : function(data) {
					var table = document.getElementById("cart-table");
					var tr = table.getElementsByTagName("tr");
					if (parseInt(tr[order].getElementsByTagName("input")[0].value) == 1)
						console.log();
					else {
						tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
								.getElementsByTagName("input")[0].value) - 1;
						if (parseInt(document
								.getElementById("num-item-in-cart").innerHTML) != 0)
							document.getElementById("num-item-in-cart").innerHTML = parseInt(document
									.getElementById("num-item-in-cart").innerHTML) - 1;
						document.getElementById("item-quantity-in-cart").innerHTML = document
								.getElementById("item-quantity-in-cart").innerHTML
								.replace(
										document
												.getElementById("item-quantity-in-cart").innerHTML
												.match(/[0-9]+/),
										document
												.getElementById("num-item-in-cart").innerHTML);
					}
					initializeCheckOutContainer();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
}

function getContextRootPath() {
	var pathName = location.pathname.substring(0);
	var token = pathName.split("/");
	var rootContextPath = "/" + token[0] + "/";
	return pathName;
}

function changeWhenInscrease(pro_id, order) {
	var url = "/item?proid=" + pro_id + "&task=" + "inscrease";
	/* console.log(getContextRootPath().replace("/cartPage", url)); */
	$
			.ajax({
				url : "/final_project/cartController/item",
				method : "POST",
				data : {
					proid : pro_id,
					task : "inscrease"
				},
				success : function(data) {
					var table = document.getElementById("cart-table");
					var tr = table.getElementsByTagName("tr");
					// Inscrease price
					/*
					 * tr[order].getElementsByTagName("p")[0].innerHTML =
					 * parseInt(tr[order]
					 * .getElementsByTagName("p")[0].innerHTML) + Math
					 * .floor(parseInt(tr[order]
					 * .getElementsByTagName("p")[0].innerHTML) /
					 * parseInt(tr[order]
					 * .getElementsByTagName("input")[0].value));
					 */
					// Incease quantity
					tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
							.getElementsByTagName("input")[0].value) + 1;
					// Inscrease in cart button
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML) + 1;
					document.getElementById("item-quantity-in-cart").innerHTML = document
							.getElementById("item-quantity-in-cart").innerHTML
							.replace(
									document
											.getElementById("item-quantity-in-cart").innerHTML
											.match(/[0-9]+/),
									document.getElementById("num-item-in-cart").innerHTML);

					/*
					 * // Reset Total Price var total = 0; for (var i = 0; i <
					 * tr.length; i++) { total +=
					 * parseInt(tr[i].getElementsByTagName("input")[0].value)
					 * parseInt(tr[i].getElementsByTagName("p")[0].innerHTML); }
					 * document.getElementById("total-price").innerHTML = total;
					 */
					initializeCheckOutContainer();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});

}

function implementDeleteItem(pro_id, order, button) {
	document.getElementById("agreeRemoveButton").value = "../cartController/item?proid="
			+ pro_id + "&task=delete" + "|" + button.value;
	document.getElementById("open-alert-remove-button").click();
}

function agreeRemove(yes_button) {
	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	var deleteButton;
	$
			.ajax({
				url : yes_button.value.split("|")[0],
				type : 'DELETE',
				data : '',
				contentType : 'application/json',
				success : function() {
					// Decrease in cart button
					/* console.log(yes_button.value.split("|")[1]); */
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML)
							- parseInt(tr[yes_button.value.split("|")[1]]
									.getElementsByTagName("input")[0].value);

					// Remove this item from table
					table.deleteRow(yes_button.value.split("|")[1]);
					table = document.getElementById("cart-table");
					tr = table.getElementsByTagName("tr")
					for (var i = 0; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[2].value = i;
					}

					initializeCheckOutContainer();
				}
			});

}

function initializeCheckOutContainer() {
	var total = 0;
	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	for (var i = 0; i < tr.length; i++)
		total += parseInt(tr[i].getElementsByTagName("p")[0].innerHTML)
				* parseInt(tr[i].getElementsByTagName("input")[0].value);
	if (total > 0) {
		document.getElementById("check-out-button").style.visibility = "visible";
		document.getElementById("total-container").style.visibility = "visible";
		document.getElementById("keep-shopping-button").style.visibility = "hidden";
		document.getElementById("empty-cart-inform").style.visibility = "hidden";
		document.getElementById("total-price").innerHTML = total;

	} else {
		document.getElementById("check-out-button").style.visibility = "hidden";
		document.getElementById("total-container").style.visibility = "hidden";
		document.getElementById("total-price").style.visibility = "hidden";
		document.getElementById("keep-shopping-button").style.visibility = "visible";
		document.getElementById("empty-cart-inform").style.visibility = "visible";
	}
}

function getCities() {
	$.ajax({
		url : "./cities",
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("city-select-tag");
			var option;
			var txt;
			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				option.onclick = function() {
					getDistricts(this);
				}
				selectTag.appendChild(option);
			}
		}
	});
}

function getDistricts(option) {
	/* console.log(option.value); */
	$.ajax({
		url : "./districts/" + option.value,
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("district-select-tag");
			var option = selectTag.getElementsByTagName("option");
			var txt;

			// Delete all district after selecting another city
			for (var i = option.length - 1; i > 0; i--) {
				selectTag.remove(1)
			}

			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				selectTag.appendChild(option);
			}
		}
	});
}

// Cart summary
function getInfoCartForCheckOut() {
	var mainCart = document.getElementById("cart-table");
	var cartCheckOut = document.getElementById("info-cart-table");
	var trTag;
	var tdTag;
	var aTag;
	var inputTag;
	var p;
	var listProduct = "";
	var listNum = "";

	// Xoa het
	trTag = cartCheckOut.getElementsByTagName("tr");
	for (var i = trTag.length - 1; i >= 0; i--) {
		cartCheckOut.deleteRow(i);
	}
	//

	// Chen tieu de
	trTag = document.createElement("tr");
	tdTag = document.createElement("td");
	tdTag.colSpan = 2;
	p = document.createElement("p");
	p.innerHTML = "CART SUMMARY";
	p.style.fontSize = "18px";
	p.style.cssFloat = "left";
	var hr = document.createElement("hr");
	tdTag.appendChild(p);
	tdTag.style.height = "30px";
	tdTag.style.padding = "0 15px";
	tdTag.style.display = "flex";
	trTag.appendChild(tdTag);
	trTag.style.borderBottom = "1px solid #e4e4e4";

	cartCheckOut.appendChild(trTag);
	//

	//
	var tr = mainCart.getElementsByTagName("tr");
	/* console.log(tr.length); */
	for (var i = 0; i < tr.length; i++) {
		trTag = document.createElement("tr");
		tdTag = document.createElement("td");
		tdTag.innerHTML = tr[i].getElementsByTagName("a")[0].innerHTML + " x "
				+ tr[i].getElementsByTagName("input")[0].value;
		tdTag.style.cssFloat = "left";
		tdTag.style.padding = "0 15px";
		trTag.appendChild(tdTag);
		/*tdTag = document.createElement("td");
		tdTag.innerHTML = tr[i].getElementsByTagName("p")[0].innerHTML
				+ "<span>&#8363</span>";
		tdTag.style.padding = "0 15px";
		trTag.appendChild(tdTag);*/
		tdTag = document.createElement("td");
		tdTag.innerHTML = parseInt(tr[i].getElementsByTagName("p")[0].innerHTML)
				* parseInt(tr[i].getElementsByTagName("input")[0].value)
				+ "<span>&#8363</span>";
		tdTag.style.textAlign = "right";
		trTag.appendChild(tdTag);
		trTag.style.borderBottom = "1px solid #e4e4e4";
		trTag.style.height = "30px";
		cartCheckOut.appendChild(trTag);
		// ../viewProductController/product?proid=3, lay ra list proid cho viec
		// xuat danh sach khi thanh toan va xoa ra khoi cart
		listProduct += tr[i].getElementsByTagName("a")[0].href.split("?")[1]
				.split("=")[1]
				+ "." + tr[i].getElementsByTagName("input")[0].value + ",";
	}// total-price
	trTag = document.createElement("tr");
	tdTag = document.createElement("td");
	tdTag.innerHTML = "Total : ";
	tdTag.style.fontSize = "20px";
	tdTag.style.fontWeight = "bold";
	tdTag.style.cssFloat = "left";
	tdTag.style.padding = "0 15px";
	trTag.appendChild(tdTag);
	tdTag = document.createElement("td");
	tdTag.style.fontSize = "19px";
	tdTag.innerHTML = document.getElementById("total-price").innerHTML
			+ "<span>&#8363</span>";
	tdTag.style.padding = "0 15px";
	trTag.appendChild(tdTag);
	cartCheckOut.appendChild(trTag);

	document.getElementById("buy-button").value = listProduct;
	/* console.log(document.getElementById("buy-button").value); */
}

function buy(buy_button, user) {
	var count = 0;
	var fname = document.getElementById("txtFName").value.trim();
	var lname = document.getElementById("txtLName").value.trim();
	var email = document.getElementById("txtEmail").value.trim();
	var phone = document.getElementById("txtPhone").value.trim();
	var detail;
	var city;
	var district;
	var paymentMode;
	var empty = /\s+/;
	var rgMail = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,5}(\.[a-z]{2,3}){1,2}$/;
	var rgPhone = /^03|09|08[0-9]{8}$/;

	if (document.getElementById("cash").checked) {
		paymentMode = document.getElementById("cash").value;
	} else if (document.getElementById("atm").checked) {
		paymentMode = document.getElementById("atm").value;
	} else {
		paymentMode = document.getElementById("master").value;
	}

	if (user == undefined) {
		city = document.getElementById("city-select-tag").value;
		district = document.getElementById("district-select-tag").value;
		detail = document.getElementById("txtAddressDetail").value.trim();

		detail = detail.split(empty).join(" ");
		if (detail.length == 0) {
			document.getElementById("more").style.visibility = "visible";
		} else {
			count++;
			document.getElementById("more").style.visibility = "hidden";
		}

		if (city != -1) {
			count++;
			document.getElementById("city").style.visibility = "hidden";
		} else {
			document.getElementById("city").style.visibility = "visible";
		}

		if (district != -1) {
			count++;
			document.getElementById("district").style.visibility = "hidden";
		} else {
			document.getElementById("district").style.visibility = "visible";
		}
	} else {
		if (document.getElementById("address").checked) {
			if (document.getElementById("available-address-select-tag").value != -1) {
				count += 3;
				document.getElementById("available").style.visibility = "hidden";

				city = document.getElementById("available-address-select-tag").value
						.split("-")[0];
				district = document
						.getElementById("available-address-select-tag").value
						.split("-")[1];
				detail = "";
			} else {
				document.getElementById("available").style.visibility = "visible";
			}
		} else {
			city = document.getElementById("city-select-tag").value;
			district = document.getElementById("district-select-tag").value;
			detail = document.getElementById("txtAddressDetail").value.trim();

			detail = detail.split(empty).join(" ");
			if (detail.length == 0) {
				document.getElementById("more").style.visibility = "visible";
			} else {
				count++;
				document.getElementById("more").style.visibility = "hidden";
			}

			if (city != -1) {
				count++;
				document.getElementById("city").style.visibility = "hidden";
			} else {
				document.getElementById("city").style.visibility = "visible";
			}

			if (district != -1) {
				count++;
				document.getElementById("district").style.visibility = "hidden";
			} else {
				document.getElementById("district").style.visibility = "visible";
			}
		}
	}

	fname = fname.split(empty).join(" ");
	if (fname.length == 0) {
		document.getElementById("fname").style.visibility = "visible";
	} else {
		count++;
		document.getElementById("fname").style.visibility = "hidden";
	}

	lname = lname.split(empty).join(" ");
	if (lname.length == 0) {
		document.getElementById("lname").style.visibility = "visible";
	} else {
		count++;
		document.getElementById("lname").style.visibility = "hidden";
	}

	email = email.split(empty).join(" ");
	if (email.length == 0) {
		document.getElementById("email").style.visibility = "visible";
	} else {
		if (email.match(rgMail) != null) {
			count++;
			document.getElementById("email").style.visibility = "hidden";
		} else {
			document.getElementById("email").style.visibility = "visible";
		}
	}

	phone = phone.split(empty).join(" ");
	if (phone.length == 0) {
		document.getElementById("phone").style.visibility = "visible";
	} else {
		if (phone.match(rgPhone) != null) {
			count++;
			document.getElementById("phone").style.visibility = "hidden";
		} else {
			document.getElementById("phone").style.visibility = "visible";
		}
	}

	console.log(count);
	if (count == 7) {
		fname = fname.trim() + " " + lname.trim();
		var data = fname + "," + email.trim() + "," + phone.trim() + "," + city
				+ "," + district + ","
				+ document.getElementById("total-price").innerHTML + ","
				+ paymentMode + "-" + buy_button.value;
		console.log(data);

		$.ajax({
			url : "./bill?data=" + data + "&more=" + detail,
			type : "POST",
			data : "",
			contentType : 'application/json',
			success : function(data) {
				console.log(data.join(","));
				writeBillDetail(data.join(","));
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
		});

	}
}

function writeBillDetail(data) {
	var listProduct = data.split("-");
	var earchProduct = data.split("-")[0].split(",");
	/* console.log(earchProduct); */
	$
			.ajax({
				url : "./billdetail?data=" + data,
				type : "POST",
				data : "",
				contentType : 'application/json',
				success : function(data) {
					document.getElementById("form-checkout").style.display = "none";
					document.getElementById("buy-button").style.display = "none";
					document.getElementById("form-notify").style.display = "flex";
					document.getElementById("close-button").style.display = "block";
					document.cookie = "guest=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
}

function clickAddress(cbTag) {
	if (cbTag.checked) {
		document.getElementById("more").style.visibility = "hidden";
		document.getElementById("city").style.visibility = "hidden";
		document.getElementById("district").style.visibility = "hidden";

		document.getElementById("available-address-container").style.display = "block";
		document.getElementById("city-select-tag").style.display = "none";
		document.getElementById("district-select-tag").style.display = "none";
		document.getElementById("txtAddressDetail").style.display = "none";
	} else {
		document.getElementById("available-address-container").style.display = "none";
		document.getElementById("city-select-tag").style.display = "block";
		document.getElementById("district-select-tag").style.display = "block";
		document.getElementById("txtAddressDetail").style.display = "block";
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function showProductOfGuest(t) {
	if (t == undefined) {
		if (getCookie("guest") != "") {
			var num = 0;
			var total = 0;
			for (var i = 0; i < getCookie("guest").split(",").length; i++) {
				num += parseInt(getCookie("guest").split(",")[i].split("-")[1]);
			}

			document.getElementById("num-item-in-cart-guest").innerHTML = num;
			document.getElementById("item-quantity-in-cart").innerHTML = num
					+ " "
					+ document.getElementById("item-quantity-in-cart").innerHTML;
			document.getElementById("check-out-button").style.visibility = "visible";
			document.getElementById("total-container").style.visibility = "visible";
			document.getElementById("keep-shopping-button").style.visibility = "hidden";
			document.getElementById("empty-cart-inform").style.visibility = "hidden";
			document.getElementById("total-price").style.visibility = "visible";

			$
					.ajax({
						url : "./cartguest?data=" + getCookie("guest"),
						type : 'GET',
						data : '',
						contentType : "application/json",
						success : function(data) {
							for (var i = 0; i < data.length; i++) {
								total += (data[i].num * data[i].product.pro_cost);
							}

							var cartTable = document
									.getElementById("cart-table");
							for (var i = cartTable.getElementsByTagName("tr").length - 1; i >= 0; i--)
								cartTable.deleteRow(i);
							var tr;
							var td;
							var div1;
							var div2;
							var img;
							var p;
							var span;
							var button;
							var aFunction;
							var input;
							for (var i = 0; i < data.length; i++) {
								tr = document.createElement("tr");
								tr.style.borderBottom = "1px solid #efefef";
								tr.style.borderTop = "1px solid #efefef";
								tr.style.height = "100px";
								td = document.createElement("td");
								img = document.createElement("img");
								img.style.width = "50px";
								img.style.height = "50px";
								img.src = "/final_project/" + data[i].image.img;
								td.appendChild(img);
								tr.appendChild(td);
								td = document.createElement("td");
								td.style.display = "flex";
								td.style.flexDirection = "column";
								td.style.justifyContent = "space-around";
								td.style.height = "100px";
								div1 = document.createElement("div");
								div1.style.display = "flex";
								div1.style.flexDirection = "column";
								div1.style.justifyContent = "space-around";
								div1.style.height = "100px";
								div2 = document.createElement("div");
								div2.style.marginTop = "15px";
								a = document.createElement("a");
								a.className = "product-title";
								a.href = "../viewProductController/product?proid="
										+ data[i].product.pro_id;
								a.style.textTransform = "upercase";
								a.style.textDecoration = "none";
								a.style.fontWeight = "700";
								a.innerHTML = data[i].product.pro_name;
								div2.appendChild(a);
								p = document.createElement("p");
								p.className = "product-cost";
								p.innerHTML = data[i].product.pro_cost
										+ "&#8363";
								div2.appendChild(p);
								div1.appendChild(div2);
								td.appendChild(div1);
								tr.appendChild(td);
								td = document.createElement("td");
								div1 = document.createElement("div");
								div1.style.display = "inline-flex";
								button = document.createElement("button");
								button.className = "btn btn-outline-secondary";
								button.value = i;
								aFunction = function(proId, order, btn) {
									return function() {
										descreaseItemCartGuest(proId, order,
												btn);
									}
								};
								button.onclick = aFunction(
										data[i].product.pro_id, i, this);
								button.innerHTML = "<i class='fas fa-minus'></i>";
								div1.appendChild(button);
								input = document.createElement("input");
								input.value = data[i].num;
								input.style.width = "39.833px";
								input.style.height = "37.833px";
								input.style.backgroundColor = "#f4f4f4";
								input.style.textAlign = "center";
								input.style.borderRadius = ".2rem";
								input.style.border = "1px solid #6c757d";
								div1.appendChild(input);
								button = document.createElement("button");
								button.className = "btn btn-outline-secondary";
								button.value = i;
								aFunction = function(proId, order, btn) {
									return function() {
										inscreaseItemCartGuest(proId, order,
												btn);
									}
								};
								button.onclick = aFunction(
										data[i].product.pro_id, i, this);
								button.innerHTML = "<i class='fas fa-plus'></i>";
								div1.appendChild(button);
								td.appendChild(div1);
								tr.appendChild(td);
								td = document.createElement("td");
								button = document.createElement("button");
								button.className = "remove-item";
								button.value = i;
								aFunction = function(proId, order, btn) {
									return function() {
										document.getElementById(
												"open-alert-remove-button")
												.click();
										var func = function(proId, order, btn) {
											return function() {
												deleteItemCartGuest(proId,
														order, btn);
											}
										}
										document
												.getElementById("agreeRemoveItemGuest").onclick = func(
												proId, order, btn);
									}
								};
								button.onclick = aFunction(
										data[i].product.pro_id, i, this);
								button.innerHTML = "<i class='far fa-trash-alt remove-item' style='cursor: pointer;'></i>";
								button.style.border = "none";
								button.style.backgroundColor = "#f4f4f4";
								td.appendChild(button);
								tr.appendChild(td);
								cartTable.appendChild(tr);
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
			document.getElementById("total-price").innerHTML = total;
		} else {
			var cartTable = document.getElementById("cart-table");
			for (var i = cartTable.getElementsByTagName("tr").length - 1; i >= 0; i--)
				cartTable.deleteRow(i);
			document.getElementById("num-item-in-cart-guest").innerHTML = 0;
			document.getElementById("check-out-button").style.visibility = "hidden";
			document.getElementById("total-container").style.visibility = "hidden";
			document.getElementById("keep-shopping-button").style.visibility = "visible";
			document.getElementById("empty-cart-inform").style.visibility = "visible";
			document.getElementById("total-price").style.visibility = "hidden";
			document.getElementById("item-quantity-in-cart").innerHTML = 0
					+ " "
					+ document.getElementById("item-quantity-in-cart").innerHTML;
		}
	}
}

function deleteItemCartGuest(proId, order, button) {
	setCookie("guest", proId, 1);
	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	document.getElementById("num-item-in-cart-guest").innerHTML = parseInt(document
			.getElementById("num-item-in-cart-guest").innerHTML)
			- parseInt(tr[order].getElementsByTagName("input")[0].value);

	// Remove this item from table
	table.deleteRow(order);
	table = document.getElementById("cart-table");
	tr = table.getElementsByTagName("tr")
	for (var i = 0; i < tr.length; i++) {
		tr[i].getElementsByTagName("button")[2].value = i;
	}

	initializeCheckOutContainer();
}

function inscreaseItemCartGuest(proId, order, button) {
	var d = new Date();
	d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	var regrexPro = new RegExp(proId + "-[0-9]+");
	var regrexNum = new RegExp("-[0-9]+");

	var result = getCookie("guest").match(regrexPro);
	/* console.log(result); */
	result = getCookie("guest").match(regrexPro)[0];
	var result2 = result.match(regrexNum)[0];
	var pro = getCookie("guest");
	var i = pro.replace(result, result.replace(result2, "-"
			+ (parseInt(result.split("-")[1]) + 1)));
	document.cookie = "guest" + "=" + i + ";" + expires + ";path=/";

	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	/*
	 * // Inscrease price tr[order].getElementsByTagName("p")[0].innerHTML =
	 * parseInt(tr[order] .getElementsByTagName("p")[0].innerHTML) + Math
	 * .floor(parseInt(tr[order].getElementsByTagName("p")[0].innerHTML) /
	 * parseInt(tr[order].getElementsByTagName("input")[0].value));
	 */
	// Incease quantity
	tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
			.getElementsByTagName("input")[0].value) + 1;
	// Inscrease in cart button
	document.getElementById("num-item-in-cart-guest").innerHTML = parseInt(document
			.getElementById("num-item-in-cart-guest").innerHTML) + 1;
	document.getElementById("item-quantity-in-cart").innerHTML = document
			.getElementById("item-quantity-in-cart").innerHTML.replace(document
			.getElementById("item-quantity-in-cart").innerHTML.match(/[0-9]+/),
			document.getElementById("num-item-in-cart-guest").innerHTML);
	initializeCheckOutContainer();
}

function descreaseItemCartGuest(proId, order, button) {
	var d = new Date();
	d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	var regrexPro = new RegExp(proId + "-[0-9]+");
	var regrexNum = new RegExp("-[0-9]+");
	var i;

	var result = getCookie("guest").match(regrexPro);
	/* console.log(result); */
	result = getCookie("guest").match(regrexPro)[0];
	var result2 = result.match(regrexNum)[0];
	var pro = getCookie("guest");
	if (parseInt(result.split("-")[1]) > 1) {
		i = pro.replace(result, result.replace(result2, "-"
				+ (parseInt(result.split("-")[1]) - 1)));
		document.getElementById("num-item-in-cart-guest").innerHTML = parseInt(document
				.getElementById("num-item-in-cart-guest").innerHTML) - 1;
		document.getElementById("item-quantity-in-cart").innerHTML = document
				.getElementById("item-quantity-in-cart").innerHTML.replace(
				document.getElementById("item-quantity-in-cart").innerHTML
						.match(/[0-9]+/), document
						.getElementById("num-item-in-cart-guest").innerHTML);
	} else
		i = pro;
	document.cookie = "guest" + "=" + i + ";" + expires + ";path=/";

	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");

	if (parseInt(tr[order].getElementsByTagName("input")[0].value) > 1) {
		tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
				.getElementsByTagName("input")[0].value) - 1;
	}

	initializeCheckOutContainer();
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	var regrexPro = new RegExp(cvalue + "-[0-9]+");
	var regrexNum = new RegExp("-[0-9]+");
	var newStr = "";

	var list = getCookie(cname).split(",");
	for (var i = 0; i < list.length; i++) {
		if (list[i].match(regrexPro) == null)
			newStr += (list[i] + ",")
	}
	if (newStr.substring(newStr.length - 1, newStr.length) == ",")
		newStr = newStr.substring(0, newStr.length - 1);
	document.cookie = cname + "=" + newStr + ";" + expires + ";path=/";

}
