function productRating(num) {
	var stars = document.getElementById("rating-table-content");
	var star = stars.getElementsByTagName("td");
	for (var i = 0; i < num; i++) {
		star[i].innerHTML = "<i class='fas fa-star'></i>";
		star[i].style.color = "yellow";
	}
}

function addCart(id, isNullUser) {
	if (parseInt(document.getElementById("txtQuantity").value) > parseInt(document
			.getElementById("quantity").innerHTML)) {
		$(document).ready(function() {
			$('.toast').toast('show');
		});
	} else {
		if (isNullUser == 0) {
			console.log(parseInt(document
										.getElementById("txtQuantity").value));
			$
					.ajax({
						url : "./addCart/"
								+ id
								+ "/"
								+ parseInt(document
										.getElementById("txtQuantity").value),
						type : "POST",
						data : '',
						contentType : "application/json",
						success : function() {
							document.getElementById("num-item-in-cart").innerHTML = parseInt(document
									.getElementById("num-item-in-cart").innerHTML)
									+ parseInt(document
											.getElementById("txtQuantity").value);
							document.getElementById("quantity").innerHTML = parseInt(document
									.getElementById("quantity").innerHTML)
									- parseInt(document
											.getElementById("txtQuantity").value);
							if (parseInt(document.getElementById("quantity").innerHTML) <= 0) {
								document.getElementById("add-cart-button").innerHTML = "OUT OF STCOK";
								document.getElementById("add-cart-button").disabled = true;
								document.getElementById("add-cart-button").style.backgroundColor = "#ff1a1a";
								document.getElementById("add-cart-button").style.color = "white";
								document.getElementById("add-cart-button").style.fontWeight = "500";
								document.getElementById("add-cart-button").style.border = "1px solid #ff1a1a";
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
		} else {
			setCookie("guest", id, 1);
			document.getElementById("num-item-in-cart-guest").innerHTML = parseInt(document
					.getElementById("num-item-in-cart-guest").innerHTML)
					+ parseInt(document.getElementById("txtQuantity").value);
			document.getElementById("quantity").innerHTML = parseInt(document
					.getElementById("quantity").innerHTML)
					- parseInt(document.getElementById("txtQuantity").value);
			if (parseInt(document.getElementById("quantity").innerHTML) <= 0) {
				document.getElementById("add-cart-button").innerHTML = "OUT OF STCOK";
				document.getElementById("add-cart-button").disabled = true;
				document.getElementById("add-cart-button").style.backgroundColor = "#ff1a1a";
				document.getElementById("add-cart-button").style.color = "white";
				document.getElementById("add-cart-button").style.fontWeight = "500";
				document.getElementById("add-cart-button").style.border = "1px solid #ff1a1a";
			}
		}
	}
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	var regrexPro = new RegExp(cvalue + "-[0-9]+");
	var regrexNum = new RegExp("-[0-9]+");
	if (getCookie("guest") == "") {
		document.cookie = cname + "=" + cvalue + "-"
				+ parseInt(document.getElementById("txtQuantity").value) + ";"
				+ expires + ";path=/";
	} else {
		var result = getCookie("guest").match(regrexPro);
		if (result != null) {
			console.log(result);
			result = getCookie("guest").match(regrexPro)[0];
			var result2 = result.match(regrexNum)[0];
			var pro = getCookie("guest");
			var i = pro.replace(result, result.replace(result2, "-"
					+ (parseInt(result.split("-")[1]) + parseInt(document
							.getElementById("txtQuantity").value))));
			console.log(i);
			document.cookie = cname + "=" + i + ";" + expires + ";path=/";
		} else {
			document.cookie = cname + "=" + getCookie("guest") + "," + cvalue
					+ "-"
					+ parseInt(document.getElementById("txtQuantity").value)
					+ ";" + expires + ";path=/";
		}
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function inscrease(inscreaseBtn) {
	document.getElementById("txtQuantity").value = parseInt(document
			.getElementById("txtQuantity").value) + 1;
}

function descrease(descreaseBtn) {
	if (parseInt(document.getElementById("txtQuantity").value) > 1)
		document.getElementById("txtQuantity").value = parseInt(document
				.getElementById("txtQuantity").value) - 1;
}

function clickImage(imgTag) {
	switch (imgTag.id) {
	case "img-sm-1":
		document.getElementById("img-lg").src = "/final_project"
				+ imgTag.src.split("final_project")[1];
		imgTag.style.border = "3px solid darkblue";
		document.getElementById("img-sm-2").style.border = "none";
		document.getElementById("img-sm-3").style.border = "none";
		break;
	case "img-sm-2":
		document.getElementById("img-lg").src = "/final_project"
				+ imgTag.src.split("final_project")[1];
		imgTag.style.border = "3px solid darkblue";
		document.getElementById("img-sm-1").style.border = "none";
		document.getElementById("img-sm-3").style.border = "none";
		break;
	case "img-sm-3":
		document.getElementById("img-lg").src = "/final_project"
				+ imgTag.src.split("final_project")[1];
		imgTag.style.border = "3px solid darkblue";
		document.getElementById("img-sm-1").style.border = "none";
		document.getElementById("img-sm-2").style.border = "none";
		break;
	default:
		break;
	}
}

function over(tdTag) {
	var ratingReviewTable = document.getElementById("rating-table-review");
	var tds = ratingReviewTable.getElementsByTagName("td");
	for (var i = 0; i <= tdTag.cellIndex; i++) {
		tds[i].innerHTML = "<i class='fas fa-star'></i>";
		tds[i].style.color = "yellow";
	}
}

function leave(tdTag) {
	var ratedPos = document.getElementById("remove-rating-button").value;
	var ratingReviewTable = document.getElementById("rating-table-review");
	var tds = ratingReviewTable.getElementsByTagName("td");
	if (ratedPos != "") {
		for (var i = 0; i < tds.length; i++) {
			if (i > parseInt(ratedPos)) {
				tds[i].innerHTML = "<i class='far fa-star'></i>";
				tds[i].style.color = "";
			}
		}
	} else {
		for (var i = 0; i < tds.length; i++) {
			tds[i].innerHTML = "<i class='far fa-star'></i>";
			tds[i].style.color = "";
		}
	}
}

function rate(tdTag) {
	document.getElementById("remove-rating-button").style.visibility = "visible";
	document.getElementById("remove-rating-button").value = tdTag.cellIndex;
	var ratingReviewTable = document.getElementById("rating-table-review");
	var tds = ratingReviewTable.getElementsByTagName("td");
	for (var i = 0; i < tds.length; i++) {
		if (i <= tdTag.cellIndex) {
			tds[i].innerHTML = "<i class='fas fa-star'></i>";
			tds[i].style.color = "yellow";
		} else {
			tds[i].innerHTML = "<i class='far fa-star'></i>";
			tds[i].style.color = "";
		}
	}
}

function removeRate(btnTag) {
	document.getElementById("remove-rating-button").style.visibility = "hidden";
	document.getElementById("remove-rating-button").value = "";
	var ratingReviewTable = document.getElementById("rating-table-review");
	var tds = ratingReviewTable.getElementsByTagName("td");
	for (var i = 0; i < tds.length; i++) {
		tds[i].innerHTML = "<i class='far fa-star'></i>";
		tds[i].style.color = "";
	}
}

function sendReview(proId) {
	var content = document.getElementById("review-content").value;
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var star
	if (document.getElementById("remove-rating-button").value == "")
		star = 0;
	else
		star = parseInt(document.getElementById("remove-rating-button").value) + 1;

	$
			.ajax({
				url : "./review?proId=" + proId + "&content=" + content
						+ "&star=" + star + "&email=" + email + "&name=" + name,
				type : "POST",
				data : '',
				contentType : "application/json",
				success : function(data) {
					document.getElementById("review-content").value = "";

					document.getElementById("name").value = "";
					document.getElementById("name").readOnly = false;

					document.getElementById("email").value = "";
					document.getElementById("email").readOnly = false;

					document.getElementById("emailCheckBox").checked = false;
					document.getElementById("nameCheckBox").checked = false;

					document.getElementById("remove-rating-button").value = "";
					document.getElementById("remove-rating-button").style.visibility = "hidden";

					var ratingReviewTable = document
							.getElementById("rating-table-review");
					var tds = ratingReviewTable.getElementsByTagName("td");
					for (var i = 0; i < tds.length; i++) {
						tds[i].innerHTML = "<i class='far fa-star'></i>";
						tds[i].style.color = "";
					}
					location.reload();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
}

function sendQuestion(proId) {
	var questionContent = document.getElementById("question-content").value;
	$
			.ajax({
				url : "./question?proId=" + proId + "&qContent="
						+ questionContent,
				type : "POST",
				data : '',
				contentType : "application/json",
				success : function(data) {
					document.getElementById("question-content").value = "";
					alert("Successed, this page will be reloaded to show your question!!!");
					location.reload();
				}
			});
}

function clickNameCheckBox(cbTag) {
	if (cbTag.checked) {
		document.getElementById("name").value = cbTag.value;
		document.getElementById("name").readOnly = true;
	} else {
		document.getElementById("name").value = "";
		document.getElementById("name").readOnly = false;
	}
}

function clickEmailCheckBox(cbTag) {
	if (cbTag.checked) {
		document.getElementById("email").value = cbTag.value;
		document.getElementById("email").readOnly = true;
	} else {
		document.getElementById("email").value = "";
		document.getElementById("email").readOnly = false;
	}
}

/*function setNumItemCartGuest() {
	if (getCookie("guest") == "")
		document.getElementById("num-item-in-cart-guest").innerHTML = 0;
	else {
		var num = 0;
		for (var i = 0; i < getCookie("guest").split(",").length; i++) {
			num += parseInt(getCookie("guest").split(",")[i].split("-")[1]);
		}
		document.getElementById("num-item-in-cart-guest").innerHTML = num;
	}
}*/