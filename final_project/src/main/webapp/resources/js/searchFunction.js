var noUser = 0;

function setUser(t) {
	if (t == undefined)
		noUser = 1;
}

var first = 0;
$(document)
		.ready(
				function() {
					/* alert(noUser); */
					$("#searchBox").keydown(function() {
						if (first != 0)
							if ($("#searchBox").val() == "") {
								$("#contentContainer").show();
								$("#searchResult").hide();
							} else {
								$("#contentContainer").hide();
								$("#searchResult").show();
							}
						first++;
					});
					$("#searchBox")
							.keyup(
									function() {
										if (first != 0)
											if ($("#searchBox").val() == "") {
												$("#contentContainer").show();
												$("#searchResult").hide();
											} else {
												$("#contentContainer").hide();
												$("#searchResult").show();
												$
														.ajax({
															url : '../viewProductController/search?key='
																	+ $(
																			"#searchBox")
																			.val(),
															type : 'GET',
															data : '',
															contentType : "application/json",
															success : function(
																	data) {
																/* console.log(data); */
																var count = 0;
																var resultTable = document
																		.getElementById("searchResult-table");
																var numTr;
																var tr;
																var td;
																var a;
																var img;
																var button;
																for (var i = resultTable
																		.getElementsByTagName("tr").length - 1; i >= 0; i--)
																	resultTable
																			.deleteRow(i);

																if (data.length % 5 == 0) {
																	numTr = Math
																			.floor(data.length / 5);
																} else {
																	numTr = Math
																			.floor(data.length / 5) + 1;
																}
																for (var i = 0; i < numTr; i++) {
																	tr = document
																			.createElement("tr");
																	tr.style.marginLeft = "auto";
																	tr.style.marginRight = "auto";
																	tr.style.display = "flex";
																	tr.flexDirection = "row";
																	tr.style.justifyContent = "space-around";
																	for (var j = count; j < count + 5; j++) {
																		if (j <= data.length - 1) {
																			td = document
																					.createElement("td");
																			var div1 = document
																					.createElement("div");
																			var cardDiv = document
																					.createElement("div");
																			cardDiv.className = "card";
																			cardDiv.style.width = "200px";
																			cardDiv.style.height = "390px";
																			img = document
																					.createElement("img");
																			img.style.height = "200px";
																			img.className = "card-img-top";
																			img.src = "/final_project"
																					+ data[j].img;
																			cardDiv
																					.appendChild(img);
																			var cardBodyDiv = document
																					.createElement("div");
																			cardBodyDiv.className = "card-body";
																			cardBodyDiv.style.display = "flex";
																			cardBodyDiv.style.flexDirection = "column";
																			cardBodyDiv.style.justifyContent = "space-between";
																			a = document
																					.createElement("a");
																			a.className = "card-title";
																			a.style.textAlign = "center";
																			a.style.textDecoration = "none";
																			a.href = "../viewProductController/product?proid="
																					+ data[j].product.pro_id;
																			a.innerHTML = data[j].product.pro_name;
																			cardBodyDiv
																					.appendChild(a);
																			var p = document
																					.createElement("p");
																			p.className = "card-text";
																			p.style.textAlign = "center";
																			p.innerHTML = data[j].product.pro_cost
																					+ "&#8363";
																			cardBodyDiv
																					.appendChild(p);
																			var div = document
																					.createElement("div");
																			div.style.display = "flex";
																			div.style.flexDirection = "row";
																			div.style.justifyContent = "space-between";
																			a = document
																					.createElement("a");
																			a.className = "btn btn-primary";
																			a.style.fontSize = "12px";
																			a.style.width = "86px";
																			a.innerHTML = "Details";
																			a.href = "../viewProductController/product?proid="
																					+ data[j].product.pro_id;
																			div
																					.appendChild(a);
																			button = document
																					.createElement("button");
																			button.className = "btn btn-success";
																			button.style.fontSize = "12px";
																			button.style.width = "86px";
																			button.style.backgroundColor = "#1fc0a0";
																			button.style.border = "1px solid #1fc0a0";
																			var aFunction = function(
																					proId,
																					isNullUer) {
																				return function() {
																					addCartSearch(
																							proId,
																							isNullUer);
																				}
																			}
																			if (noUser == 0)
																				button.onclick = aFunction(
																						data[j].product.pro_id,
																						0);
																			else
																				button.onclick = aFunction(
																						data[j].product.pro_id,
																						1);
																			if (data[j].product.pro_quantity <= 0) {
																				button.disabled = true;
																				button.innerHTML = "Out of stock";
																			} else
																				button.innerHTML = "Add to cart";
																			div
																					.appendChild(button);
																			cardBodyDiv
																					.appendChild(div);
																			cardDiv
																					.appendChild(cardBodyDiv);
																			div1
																					.appendChild(cardDiv);
																			td
																					.appendChild(div1);
																			tr
																					.appendChild(td);
																		}
																	}
																	count += 5;
																	resultTable
																			.appendChild(tr);
																}
															}
														});
											}
										first++;
									});
				});

function addCartSearch(id, isNullUser) {

	if (isNullUser == 0) {
		$
				.ajax({
					url : "../viewProductController/addCart/" + id + "/" + 1,
					type : "POST",
					data : '',
					contentType : "application/json",
					success : function() {
						document.getElementById("num-item-in-cart").innerHTML = parseInt(document
								.getElementById("num-item-in-cart").innerHTML) + 1;
						/*
						 * document.getElementById("quantity").innerHTML =
						 * parseInt(document
						 * .getElementById("quantity").innerHTML) -
						 * parseInt(document
						 * .getElementById("txtQuantity").value); if
						 * (parseInt(document.getElementById("quantity").innerHTML) <=
						 * 0) {
						 * document.getElementById("add-cart-button").innerHTML =
						 * "OUT OF STCOK";
						 * document.getElementById("add-cart-button").disabled =
						 * true;
						 * document.getElementById("add-cart-button").style.backgroundColor =
						 * "#ff1a1a";
						 * document.getElementById("add-cart-button").style.color =
						 * "white";
						 * document.getElementById("add-cart-button").style.fontWeight =
						 * "500";
						 * document.getElementById("add-cart-button").style.border =
						 * "1px solid #ff1a1a"; }
						 */
					},
					async : false,
					error : function(e) {
						console.log(e);
					}
				});
	} else {
		setCookieForAddingCart("guest", id, 1);
		document.getElementById("num-item-in-cart-guest").innerHTML = parseInt(document
				.getElementById("num-item-in-cart-guest").innerHTML) + 1;
	}
}

function setCookieForAddingCart(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	var regrexPro = new RegExp(cvalue + "-[0-9]+");
	var regrexNum = new RegExp("-[0-9]+");
	if (getCookie("guest") == "") {
		document.cookie = cname + "=" + cvalue + "-" + 1 + ";" + expires
				+ ";path=/";
	} else {
		var result = getCookie("guest").match(regrexPro);
		if (result != null) {
			console.log(result);
			result = getCookie("guest").match(regrexPro)[0];
			var result2 = result.match(regrexNum)[0];
			var pro = getCookie("guest");
			var i = pro.replace(result, result.replace(result2, "-"
					+ (parseInt(result.split("-")[1]) + 1)));
			console.log(i);
			document.cookie = cname + "=" + i + ";" + expires + ";path=/";
		} else {
			document.cookie = cname + "=" + getCookie("guest") + "," + cvalue
					+ "-" + 1 + ";" + expires + ";path=/";
		}
	}
}

// General function

function setNumItemCartGuest() {
	if (noUser == 1) {
		if (getCookie("guest") == "") {
			document.getElementById("num-item-in-cart-guest").innerHTML = 0;
		} else {
			var num = 0;
			for (var i = 0; i < getCookie("guest").split(",").length; i++) {
				num += parseInt(getCookie("guest").split(",")[i].split("-")[1]);
			}
			document.getElementById("num-item-in-cart-guest").innerHTML = num;
		}
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
