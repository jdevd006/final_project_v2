function getSelectValue() {
	return document.getElementById("followSelect").value;
}

/*
 * function getRadioValue() { if (document.getElementById("ascRd").checked ==
 * true) return "asc"; return "dsc"; }
 */

function getMessagesByOrder(order) {
	$.ajax({
		url : "./getmessages?by=" + order,
		type : "GET",
		data : '',
		contentType : "application/json",
		success : function(data) {
			resetMessagesTable(data);
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function selectClick() {
	if (getSelectValue() == "unread")
		getMessagesByOrder("unread");
	else
		getMessagesByOrder("read");
}

function resetMessagesTable(orders) {
	var c = 0;
	var max;
	if (orders.length % 6 == 0) {
		max = Math.floor(orders.length / 6);
	} else {
		max = Math.floor(orders.length / 6) + 1;
	}
	for (var k = 0; k < max; k++) {
		var ordersTable = document.getElementById("messages-table" + k);
		var tbodyTag = ordersTable.getElementsByTagName("tbody");
		var trTag = tbodyTag[0].getElementsByTagName("tr");
		var tdTag;
		var btnTag;
		// Delete old data
		for (var i = trTag.length; i >= 1; i--)
			ordersTable.deleteRow(i);
		//

		// set
		for (var i = 0; i < 6; i++) {
			if (c <= orders.length - 1) {
				trTag = document.createElement("tr");
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].message.id;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].account;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].message.name;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].message.email;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].message.question;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].message.answer;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				if (orders[i].message.status == "read") {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Answer";
					btnTag.className = "btn btn-success";
					btnTag.setAttribute("data-toggle", "modal");
					btnTag.setAttribute("data-target", "#myModal");
					var t = function(btn, id, orderTable) {
						return function() {
							answer(btn, id, orderTable);
						}
					}
					btnTag.onclick = t(btnTag, orders[c].message.id, k);
					tdTag.appendChild(btnTag);
				} else {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Answer<span class='text text-warning'>New</span>";
					btnTag.className = "btn btn-success";
					btnTag.setAttribute("data-toggle", "modal");
					btnTag.setAttribute("data-target", "#myModal");
					var t = function(btn, id, orderTable) {
						return function() {
							answer(btn, id, orderTable);
						}
					}
					btnTag.onclick = t(btnTag, orders[c].message.id, k);
					tdTag.appendChild(btnTag);
				}
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Delete";
				btnTag.className = "btn btn-danger";
				btnTag.value = i;
				t = function(btn, id, orderTable) {
					return function() {
						deleteMessage(btn, id, orderTable);
					}
				}
				btnTag.onclick = t(btnTag, orders[c].message.id, k);
				tdTag.appendChild(btnTag);
				trTag.appendChild(tdTag);
				tbodyTag[0].appendChild(trTag);
				c++;
			}
		}
	}
	//
}

function radioClick() {
	if (getSelectValue() == "total") {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("totalAsc");
		} else {
			getOrdersByOrder("totalDsc");
		}
	}
	if (getSelectValue() == "id") {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("idAsc");
		} else {
			getOrdersByOrder("idDsc");
		}
	}
}

// Search function
var first = 0;
$(document)
		.ready(
				function() {
					$("#searchName").keydown(
							function() {
								if ($("#searchName").val().split(/\s+/)
										.join("").length == 0) {
									$("#messages").show();
									$("#searchResult").hide();
								} else {
									$("#messages").hide();
									$("#searchResult").show();
								}
							});
					$("#searchName")
							.keyup(
									function() {
										if ($("#searchName").val().split(/\s+/)
												.join("").length == 0) {
											$("#messages").show();
											$("#searchResult").hide();
										} else {
											$("#messages").hide();
											$("#searchResult").show();
											$
													.ajax({
														url : "./getmesses?key="
																+ $(
																		"#searchName")
																		.val()
																		.trim(),
														type : "GET",
														data : '',
														contentType : "application/json",
														success : function(
																orders) {
															var usersTable = document
																	.getElementById("userSearchTable");
															var tbodyTag = usersTable
																	.getElementsByTagName("tbody");
															var trTag = tbodyTag[0]
																	.getElementsByTagName("tr");
															var tdTag;
															var btnTag;
															// Delete old data
															var k = 0;
															for (var i = trTag.length; i >= 1; i--)
																usersTable
																		.deleteRow(i);

															// Set table
															for (var i = 0; i < orders.length; i++) {
																trTag = document
																		.createElement("tr");
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].message.id;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].account;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].message.name;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].message.email;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].message.question;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].message.answer;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																if (orders[i].message.status == "read") {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Answer";
																	btnTag.className = "btn btn-success";
																	btnTag
																			.setAttribute(
																					"data-toggle",
																					"modal");
																	btnTag
																			.setAttribute(
																					"data-target",
																					"#myModal");
																	var t = function(
																			btn,
																			id) {
																		return function() {
																			answer(
																					btn,
																					id);
																		}
																	}
																	btnTag.onclick = t(
																			btnTag,
																			orders[i].message.id);
																	tdTag
																			.appendChild(btnTag);
																} else {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Answer<span class='text text-warning'>New</span>";
																	btnTag.className = "btn btn-success";
																	btnTag
																			.setAttribute(
																					"data-toggle",
																					"modal");
																	btnTag
																			.setAttribute(
																					"data-target",
																					"#myModal");
																	var t = function(
																			btn,
																			id) {
																		return function() {
																			answer(
																					btn,
																					id);
																		}
																	}
																	btnTag.onclick = t(
																			btnTag,
																			orders[i].message.id);
																	tdTag
																			.appendChild(btnTag);
																}
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Delete";
																btnTag.className = "btn btn-danger";
																btnTag.value = i+1;
																t = function(
																		btn,
																		id,
																		orderTable) {
																	return function() {
																		deleteMessageForSearch(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		orders[i].message.id);
																tdTag
																		.appendChild(btnTag);
																trTag
																		.appendChild(tdTag);
																tbodyTag[0]
																		.appendChild(trTag);
															}
														}
													});
										}
									});
				});

function deleteMessageForSearch(button, messageId) {
	$.ajax({
		url : "./message/" + messageId,
		type : "DELETE",
		data : "",
		contentType : "application/json",
		success : function() {
			var table = document.getElementById("userSearchTable");
			table.deleteRow(button.value);
			//
			var tr = table.getElementsByTagName("tr");
			for (var i = 1; i < tr.length; i++) {
				tr[i].getElementsByTagName("button")[1].value = i;
			}
		}
	});
}
