var myChart;
var pieChart;
var barchart;
function setFirst() {
	var d = new Date();
	var n = d.getFullYear();
	$.ajax({
		url : "./revenue",
		type : 'GET',
		date : '',
		contentType : "application/json",
		success : function(data) {
			var t = 50;
			var y = 2019;
			var ctx = document.getElementById('myChart');
			if (myChart != null) {
				myChart.destroy();
			}
			myChart = new Chart(ctx, {
				type : 'bar',
				data : {
					labels : [ 'January', 'February', 'March', 'April', 'May',
							'June', 'July', 'August', 'September', 'October',
							'November', 'December' ],
					datasets : [ {
						label : 'Revenue in ' + y,
						data : [ data[0] == null ? 0 : data[0],
								data[1] == null ? 0 : data[1],
								data[2] == null ? 0 : data[2],
								data[3] == null ? 0 : data[3],
								data[4] == null ? 0 : data[4],
								data[5] == null ? 0 : data[5],
								data[6] == null ? 0 : data[6],
								data[7] == null ? 0 : data[7],
								data[8] == null ? 0 : data[8],
								data[9] == null ? 0 : data[9],
								data[10] == null ? 0 : data[10],
								data[11] == null ? 0 : data[11] ],
						backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)' ],
						borderColor : [ 'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)' ],
						borderWidth : 1
					} ]
				},
				options : {
					scales : {
						yAxes : [ {
							ticks : {
								beginAtZero : true
							}
						} ]
					}
				}
			});
		}
	});
}

function revenue(option) {
	$.ajax({
		url : "./revenue/" + option.value,
		type : 'GET',
		date : '',
		contentType : "application/json",
		success : function(data) {
			var ctx = document.getElementById('myChart');

			if (myChart != null) {
				myChart.destroy();
			}
			myChart = new Chart(ctx, {
				type : 'bar',
				data : {
					labels : [ 'January', 'February', 'March', 'April', 'May',
							'June', 'July', 'August', 'September', 'October',
							'November', 'December' ],
					datasets : [ {
						label : 'Revenue in ' + option.value,
						data : [ data[0] == null ? 0 : data[0],
								data[1] == null ? 0 : data[1],
								data[2] == null ? 0 : data[2],
								data[3] == null ? 0 : data[3],
								data[4] == null ? 0 : data[4],
								data[5] == null ? 0 : data[5],
								data[6] == null ? 0 : data[6],
								data[7] == null ? 0 : data[7],
								data[8] == null ? 0 : data[8],
								data[9] == null ? 0 : data[9],
								data[10] == null ? 0 : data[10],
								data[11] == null ? 0 : data[11] ],
						backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)' ],
						borderColor : [ 'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)' ],
						borderWidth : 1
					} ]
				},
				options : {
					scales : {
						yAxes : [ {
							ticks : {
								beginAtZero : true
							}
						} ]
					}
				}
			});
		}
	});
}

function setYearBarChart() {
	var d = new Date();
	var n = d.getFullYear();
	var select = document.getElementById("bar-chart");
	var option = select.getElementsByTagName("option");
	for (var i = 0; i < 5; i++) {
		console.log(d.getFullYear() - i);
		option[i].value = d.getFullYear() - i;
		option[i].innerHTML = d.getFullYear() - i;
	}
}

function setPieChartFisrt() {
	var d = new Date();
	var n = d.getFullYear();
	var lables = [];
	var datas = [];

	$.ajax({
		url : "./revenuecategories",
		type : "GET",
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var total = 0;
			for (var i = 0; i < data.length; i++) {
				lables[i] = data[i].name;
			}
			for (var i = 0; i < data.length; i++) {
				datas[i] = data[i].revenue;
				total += datas[i];
			}
			console.log(datas);
			var data1 = {
				labels : lables,
				datasets : [ {
					label : "Categories",
					data : datas,
					backgroundColor : [ "#DEB887", "#A9A9A9", "#DC143C",
							"#F4A460", "#2E8B57" ],
					borderColor : [ "#CDA776", "#989898", "#CB252B", "#E39371",
							"#1D7A46" ],
					borderWidth : [ 1, 1, 1, 1, 1 ]
				} ]
			};
			var options = {
				responsive : true,
				title : {
					display : true,
					position : "top",
					text : "Categories : " + total,
					fontSize : 18,
					fontColor : "#111"
				},
				legend : {
					display : true,
					position : "bottom",
					labels : {
						fontColor : "#333",
						fontSize : 16
					}
				}
			};
			if (pieChart != null) {
				pieChart.destroy();
			}
			pieChart = new Chart(document.getElementById("pie-chart"), {
				type : "pie",
				data : data1,
				options : options
			});

			if (barchart != null) {
				barchart.destroy();
			}
			barchart = new Chart(document.getElementById('barChart'), {
				type : 'bar',
				data : {
					labels : lables,
					datasets : [ {
						label : 'Revenue of categories in ' + "1/1/2019 - 31/1/2019 : " + total,
						data : datas,
						backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)' ],
						borderColor : [ 'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)' ],
						borderWidth : 1
					} ]
				},
				options : {
					scales : {
						yAxes : [ {
							ticks : {
								beginAtZero : true
							}
						} ]
					}
				}
			});

		},

	})
}

function generateFirstMonth() {
	var daySelectTag = document.getElementById("day");
	var tdaySelectTag = document.getElementById("tday");
	var optionTag = daySelectTag.getElementsByTagName("option");
	// Remove old options
	for (var i = optionTag.length - 1; i >= 0; i--) {
		daySelectTag.remove(i);
		optionTag.remove(i);
	}

	var currentYear = new Date().getFullYear();
	var aFunction;
	for (var i = 1; i <= new Date(
			parseInt(document.getElementById("year").value), 1, 0).getDate(); i++) {
		optionTag = document.createElement("option");
		optionTag.value = i;
		if (i <= 9)
			optionTag.innerHTML = "0" + i;
		else
			optionTag.innerHTML = i;
		aFunction = function() {
			return function() {
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction(this);
		daySelectTag.appendChild(optionTag);
	}

	for (var i = 1; i <= new Date(
			parseInt(document.getElementById("year").value), 1, 0).getDate(); i++) {
		optionTag = document.createElement("option");
		optionTag.value = i;
		if (i <= 9)
			optionTag.innerHTML = "0" + i;
		else
			optionTag.innerHTML = i;
		aFunction = function() {
			return function() {
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction(this);
		tdaySelectTag.appendChild(optionTag);
	}
}

function generateDays(selectTag) {
	var daySelectTag = document.getElementById("day");
	var optionTag = daySelectTag.getElementsByTagName("option");
	// Remove old options
	for (var i = optionTag.length - 1; i >= 0; i--) {
		daySelectTag.remove(i);
	}

	var currentYear = new Date().getFullYear();
	var aFunction;
	for (var i = 1; i <= new Date(
			parseInt(document.getElementById("year").value),
			parseInt(selectTag.value), 0).getDate(); i++) {
		optionTag = document.createElement("option");
		optionTag.value = i;
		optionTag.innerHTML = i;
		aFunction = function() {
			return function() {
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction();
		daySelectTag.appendChild(optionTag);
	}
}

function generateTDays(selectTag) {
	var tdaySelectTag = document.getElementById("tday");
	var optionTag = tdaySelectTag.getElementsByTagName("option");
	// Remove old options
	for (var i = optionTag.length - 1; i >= 0; i--) {
		tdaySelectTag.remove(i);
	}

	var currentYear = new Date().getFullYear();
	var aFunction;
	for (var i = 1; i <= new Date(
			parseInt(document.getElementById("year").value), parseInt(document
					.getElementById("tmonth").value), 0).getDate(); i++) {
		optionTag = document.createElement("option");
		optionTag.value = i;
		optionTag.innerHTML = i;
		aFunction = function() {
			return function() {
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction();
		tdaySelectTag.appendChild(optionTag);
	}
}

function generateToMonth(select) {
	var tmonthSelect = document.getElementById("tmonth");

	//
	for (var i = tmonthSelect.getElementsByTagName("option").length - 1; i >= 0; i--) {
		tmonthSelect.remove(i);
	}
	//

	var fmonthSelect = document.getElementById("month");
	var optionTag;
	var aFunction;

	for (var i = fmonthSelect.selectedIndex; i < fmonthSelect
			.getElementsByTagName("option").length; i++) {
		optionTag = document.createElement("option");
		optionTag.value = fmonthSelect.getElementsByTagName("option")[i].value;
		optionTag.innerHTML = fmonthSelect.getElementsByTagName("option")[i].innerHTML;
		aFunction = function() {
			return function() {
				generateTDays();
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction(this);
		tmonthSelect.appendChild(optionTag);
	}

	// generate to day
	var tdaySelectTag = document.getElementById("tday");
	var optionTag = tdaySelectTag.getElementsByTagName("option");
	// Remove old options
	for (var i = optionTag.length - 1; i >= 0; i--) {
		tdaySelectTag.remove(i);
	}

	var currentYear = new Date().getFullYear();
	var aFunction;
	for (var i = 1; i <= new Date(
			parseInt(document.getElementById("year").value),
			parseInt(fmonthSelect.value), 0).getDate(); i++) {
		optionTag = document.createElement("option");
		optionTag.value = i;
		optionTag.innerHTML = i;
		aFunction = function() {
			return function() {
				generatePieChart();
			}
		}
		optionTag.onclick = aFunction();
		tdaySelectTag.appendChild(optionTag);
	}
}

function generatePieChart() {
	if (document.getElementById("fmonth").checked) {
		var startDay = "01";
		var endDay = new Date(parseInt(document.getElementById("year").value),
				parseInt(document.getElementById("tmonth").value), 0).getDate();
		var startMonth = document.getElementById("month").value;
		var endMonth = document.getElementById("tmonth").value;
		var year = parseInt(document.getElementById("year").value);
		var str = startDay + "," + endDay + "," + startMonth + "," + endMonth
				+ "," + year;
		var str2 = startDay + "/" + startMonth + "/" + year + " - " + endDay
				+ "/" + endMonth + "/" + year;
	} else {
		var startDay = document.getElementById("day").value;
		var endDay = document.getElementById("tday").value;
		var startMonth = document.getElementById("month").value;
		var endMonth = document.getElementById("tmonth").value;
		var year = parseInt(document.getElementById("year").value);
		var str = startDay + "," + endDay + "," + startMonth + "," + endMonth
				+ "," + year;
		var str2 = startDay + "/" + startMonth + "/" + year + " - " + endDay
				+ "/" + endMonth + "/" + year;
	}
	var url = "./revenuecategory?data=" + str;
	var lables = [];
	var datas = [];

	$.ajax({
		url : url,
		type : "GET",
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var total = 0;
			for (var i = 0; i < data.length; i++) {
				lables[i] = data[i].name;
			}
			for (var i = 0; i < data.length; i++) {
				datas[i] = data[i].revenue;
				total += datas[i];
			}
			console.log(datas);
			var data1 = {
				labels : lables,
				datasets : [ {
					label : "Categories : " + total,
					data : datas,
					backgroundColor : [ "#DEB887", "#A9A9A9", "#DC143C",
							"#F4A460", "#2E8B57" ],
					borderColor : [ "#CDA776", "#989898", "#CB252B", "#E39371",
							"#1D7A46" ],
					borderWidth : [ 1, 1, 1, 1, 1 ]
				} ]
			};
			var options = {
				responsive : true,
				title : {
					display : true,
					position : "top",
					text : "Categories : " + total,
					fontSize : 18,
					fontColor : "#111"
				},
				legend : {
					display : true,
					position : "bottom",
					labels : {
						fontColor : "#333",
						fontSize : 16
					}
				}
			};
			if (pieChart != null) {
				pieChart.destroy();
			}
			pieChart = new Chart(document.getElementById("pie-chart"), {
				type : "pie",
				data : data1,
				options : options
			});

			if (barchart != null) {
				barchart.destroy();
			}
			barchart = new Chart(document.getElementById('barChart'), {
				type : 'bar',
				data : {
					labels : lables,
					datasets : [ {
						label : 'Revenue cartgories ' + str2 + " : " + total,
						data : datas,
						backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)' ],
						borderColor : [ 'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)' ],
						borderWidth : 1
					} ]
				},
				options : {
					scales : {
						yAxes : [ {
							ticks : {
								beginAtZero : true
							}
						} ]
					}
				}
			});

		},
		async : false,
		error : function(e) {
			console.log(e);
		}

	})
}

function clickFollow(radio) {
	if (radio.value == "fmonth") {
		document.getElementById("day").disabled = true;
		document.getElementById("tday").disabled = true;
	} else {
		document.getElementById("day").disabled = false;
		document.getElementById("tday").disabled = false;
	}
}
