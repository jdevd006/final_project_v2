function getSelectValue() {
	return document.getElementById("followSelect").value;
}

function getRadioValue() {
	if (document.getElementById("ascRd").checked == true)
		return "asc";
	return "dsc";
}

function getUsersByOrder(order) {
	$.ajax({
		url : "./getproducts?by=" + order,
		type : "GET",
		data : '',
		contentType : "application/json",
		success : function(data) {
			resetProductsTable(data);
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function selectClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function resetProductsTable(listObject) {
	var c = 0;
	var max;
	if (listObject.length % 6 == 0) {
		max = Math.floor(listObject.length / 6);
	} else {
		max = Math.floor(listObject.length / 6) + 1;
	}
	for (var k = 0; k < max; k++) {
		var usersTable = document.getElementById("products-table" + k);
		var tbodyTag = usersTable.getElementsByTagName("tbody");
		var trTag = tbodyTag[0].getElementsByTagName("tr");
		var tdTag;
		var btnTag;
		// Delete old data
		for (var i = trTag.length; i >= 1; i--)
			usersTable.deleteRow(i);
		//

		// Set table
		for (var i = 0; i < 6; i++) {
			if (c <= listObject.length - 1) {
				trTag = document.createElement("tr");
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_id;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_name;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_quantity;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_content;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_star;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].pro_cost;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].status;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Update";
				btnTag.className = "btn btn-primary";
				btnTag.setAttribute("data-toggle", "modal");
				btnTag.setAttribute("data-target", "#myModal");
				var t = function(btn, id) {
					return function() {
						update(btn, id);
					}
				}
				btnTag.onclick = t(btnTag, listObject[c].pro_id);
				tdTag.appendChild(btnTag);
				if (listObject[c].status == "activated") {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Block";
					btnTag.className = "btn btn-warning";
					var t = function(proId, order, btn, orderTable) {
						return function() {
							block(proId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].pro_id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				} else {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Activate";
					btnTag.className = "btn btn-warning";
					var t = function(proId, order, btn, orderTable) {
						return function() {
							activate(proId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].pro_id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				}
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Delete";
				btnTag.className = "btn btn-danger";
				btnTag.value = i + 1;
				t = function(btn, id, orderTable) {
					return function() {
						deleteProduct(btn, id, orderTable);
					}
				}
				btnTag.onclick = t(btnTag, listObject[c].pro_id, k);
				tdTag.appendChild(btnTag);
				tdTag.appendChild(btnTag);
				trTag.appendChild(tdTag);
				tbodyTag[0].appendChild(trTag);
				c++;
			}
		}
	}
	//
}

function radioClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function cleanAddModal() {
	document.getElementById("btnSaveInModal").value = "";

	document.getElementById("txtProductName").value = "";
	document.getElementById("txtProductCost").value = "";
	document.getElementById("txtProductQuantity").value = "";
	document.getElementById("txtProductContent").value = "";
	var select = document.getElementById("category-select");
	select.selectedIndex = "0";
}

var first = 0;
$(document)
		.ready(
				function() {
					$("#searchName").keydown(
							function() {
								if ($("#searchName").val().split(/\s+/)
										.join("").length == 0) {
									$("#products").show();
									$("#searchResult").hide();
								} else {
									$("#products").hide();
									$("#searchResult").show();
								}
							});
					$("#searchName")
							.keyup(
									function() {
										if ($("#searchName").val().split(/\s+/)
												.join("").length == 0) {
											$("#products").show();
											$("#searchResult").hide();
										} else {
											$("#products").hide();
											$("#searchResult").show();
											$
													.ajax({
														url : "./getpros?key="
																+ $(
																		"#searchName")
																		.val()
																		.trim(),
														type : "GET",
														data : '',
														contentType : "application/json",
														success : function(
																listObject) {
															var usersTable = document
																	.getElementById("userSearchTable");
															var tbodyTag = usersTable
																	.getElementsByTagName("tbody");
															var trTag = tbodyTag[0]
																	.getElementsByTagName("tr");
															var tdTag;
															var btnTag;
															// Delete old data
															var k = 0;
															for (var i = trTag.length; i >= 1; i--)
																usersTable
																		.deleteRow(i);

															// Set table
															for (var i = 0; i < listObject.length; i++) {
																trTag = document
																		.createElement("tr");
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_id;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_name;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_quantity;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_content;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_star;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].pro_cost;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].status;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Update";
																btnTag.className = "btn btn-primary";
																btnTag
																		.setAttribute(
																				"data-toggle",
																				"modal");
																btnTag
																		.setAttribute(
																				"data-target",
																				"#myModal");
																var t = function(
																		btn, id) {
																	return function() {
																		update(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		listObject[i].pro_id);
																tdTag
																		.appendChild(btnTag);
																if (listObject[i].status == "activated") {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Block";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			proId,
																			order,
																			btn) {
																		return function() {
																			blockForSearch(
																					proId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].pro_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																} else {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Activate";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			proId,
																			order,
																			btn) {
																		return function() {
																			activateForSearch(
																					proId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].pro_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																}
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Delete";
																btnTag.className = "btn btn-danger";
																btnTag.value = i + 1;
																t = function(
																		btn, id) {
																	return function() {
																		deleteProductForSearch(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		listObject[i].pro_id);
																tdTag
																		.appendChild(btnTag);
																tdTag
																		.appendChild(btnTag);
																trTag
																		.appendChild(tdTag);
																tbodyTag[0]
																		.appendChild(trTag);
															}
														}
													});
										}
									});
				});

function deleteProductForSearch(button, pro_id) {
	$.ajax({
		url : "./product/" + pro_id,
		type : "DELETE",
		data : "",
		contentType : "application/json",
		success : function() {
			var table = document.getElementById("userSearchTable");
			table.deleteRow(button.value);
			//
			var tr = table.getElementsByTagName("tr");
			for (var i = 1; i < tr.length; i++) {
				tr[i].getElementsByTagName("button")[1].value = i;
			}
		}
	});
}

function upload(button, proId) {
	document.getElementById("txtId").value = proId;
}

function blockForSearch(proId, order, button, orderTable) {
	$.ajax({
		url : "./block/" + proId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[6].innerHTML = "blocked";
			button.innerHTML = "Activate";
			var func = function(proId1, order1, button1) {
				return function() {
					activateForSearch(proId1, order1, button1);
				}
			}
			button.onclick = func(proId, order, button);
		}
	});
}

function activateForSearch(proId, order, button) {
	$.ajax({
		url : "./activate/" + proId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[6].innerHTML = "activated";
			button.innerHTML = "Block";
			var func = function(proId1, order1, button1) {
				return function() {
					blockForSearch(proId1, order1, button1);
				}
			}
			button.onclick = func(proId, order, button);
		}
	});
}