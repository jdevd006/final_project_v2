function updateInside(cate_name, button) {
	var cate_id = button.value;
	console.log(cate_name);
	document.getElementById("btnUpdate").value = cate_id + "," + cate_name;
	document.getElementById("txtCateName").value = cate_name;
}

function deleteCategory(button, cate_id) {
	$.ajax({
		url : "./category/" + cate_id,
		type : "DELETE",
		data : "",
		contentType : "application/json",
		success : function() {
			var table = document.getElementById("categories-table");
			table.deleteRow(button.value);
			//
			var tr = table.getElementTagName("tr");
			for (var i = 1; i < tr.length; i++) {
				tr[i].getElementsByTagName("button")[1].value = i;
			}
		}
	});
}

function getSelectValue() {
	return document.getElementById("followSelect").value;
}

function getRadioValue() {
	if (document.getElementById("ascRd").checked == true)
		return "asc";
	return "dsc";
}

function getUsersByOrder(order) {
	$.ajax({
		url : "./getcategories?by=" + order,
		type : "GET",
		data : '',
		contentType : "application/json",
		success : function(data) {
			resetCategoriesTable(data);
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function selectClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function resetCategoriesTable(listObject) {
	var c = 0;
	var max;
	if (listObject.length % 6 == 0) {
		max = Math.floor(listObject.length / 6);
	} else {
		max = Math.floor(listObject.length / 6) + 1;
	}
	for (var k = 0; k < max; k++) {
		var usersTable = document.getElementById("categories-table" + k);
		var tbodyTag = usersTable.getElementsByTagName("tbody");
		var trTag = tbodyTag[0].getElementsByTagName("tr");
		var tdTag;
		var btnTag;
		// Delete old data
		for (var i = trTag.length; i >= 1; i--)
			usersTable.deleteRow(i);
		//
		// Set table
		c = 0;
		for (var i = 0; i < 6; i++) {
			if (c <= listObject.length - 1) {
				trTag = document.createElement("tr");
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].id;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].cate.cate_name;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = listObject[c].cate.status;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Update";
				btnTag.className = "btn btn-primary";
				btnTag.setAttribute("data-toggle", "modal");
				btnTag.setAttribute("data-target", "#myModal");
				btnTag.value = listObject[c].cate.cate_name;
				var t = function(btn, id) {
					return function() {
						update(btn, id);
					}
				}

				btnTag.onclick = t(btnTag, listObject[c].id);
				tdTag.appendChild(btnTag);
				if (listObject[c].cate.status == "activated") {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Block";
					btnTag.className = "btn btn-warning";
					var t = function(cateId, order, btn, orderTable) {
						return function() {
							block(cateId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				} else {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Activate";
					btnTag.className = "btn btn-warning";
					var t = function(cateId, order, btn, orderTable) {
						return function() {
							activate(cateId, order, btn, orderTable);
						}
					}
					btnTag.onclick = t(listObject[c].id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				}
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Delete";
				btnTag.className = "btn btn-danger";
				btnTag.value = i;
				t = function(btn, id, orderTable) {
					return function() {
						deleteCategory(btn, id, orderTable);
					}
				}
				btnTag.onclick = t(btnTag, listObject[c].id, k);
				tdTag.appendChild(btnTag);
				trTag.appendChild(tdTag);
				tbodyTag[0].appendChild(trTag);
				c++;
			}
		}
	}
}

function radioClick() {
	if (getSelectValue() == "name") {
		if (getRadioValue() == "asc") {
			getUsersByOrder("nameAsc");
		} else {
			getUsersByOrder("nameDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getUsersByOrder("idAsc");
		} else {
			getUsersByOrder("idDsc");
		}
	}
}

function cleanAddModal() {
	document.getElementById("btnSaveInModal").value = "";
	document.getElementById("txtCateName").value = "";
}

// Search function
var first = 0;
$(document)
		.ready(
				function() {
					$("#searchName").keydown(
							function() {
								if ($("#searchName").val().split(/\s+/)
										.join("").length == 0) {
									$("#categories").show();
									$("#searchResult").hide();
								} else {
									$("#categories").hide();
									$("#searchResult").show();
								}
							});
					$("#searchName")
							.keyup(
									function() {
										if ($("#searchName").val().split(/\s+/)
												.join("").length == 0) {
											$("#categories").show();
											$("#searchResult").hide();
										} else {
											$("#categories").hide();
											$("#searchResult").show();
											$
													.ajax({
														url : "./getcates?key="
																+ $(
																		"#searchName")
																		.val()
																		.trim(),
														type : "GET",
														data : '',
														contentType : "application/json",
														success : function(
																listObject) {
															var usersTable = document
																	.getElementById("userSearchTable");
															var tbodyTag = usersTable
																	.getElementsByTagName("tbody");
															var trTag = tbodyTag[0]
																	.getElementsByTagName("tr");
															var tdTag;
															var btnTag;
															// Delete old data
															var k = 0;
															for (var i = trTag.length; i >= 1; i--)
																usersTable
																		.deleteRow(i);

															// Set table
															for (var i = 0; i < listObject.length; i++) {
																trTag = document
																		.createElement("tr");
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].id;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].cate.cate_name;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = listObject[i].cate.status;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Update";
																btnTag.className = "btn btn-primary";
																btnTag
																		.setAttribute(
																				"data-toggle",
																				"modal");
																btnTag
																		.setAttribute(
																				"data-target",
																				"#myModal");
																btnTag.value = listObject[i].cate.cate_name;
																var t = function(
																		btn, id) {
																	return function() {
																		update(
																				btn,
																				id);
																	}
																}

																btnTag.onclick = t(
																		btnTag,
																		listObject[i].id);
																tdTag
																		.appendChild(btnTag);
																if (listObject[i].cate.status == "activated") {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Block";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			cateId,
																			order,
																			btn) {
																		return function() {
																			blockForSearch(
																					cateId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																} else {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Activate";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			cateId,
																			order,
																			btn) {
																		return function() {
																			activateForSearch(
																					cateId,
																					order,
																					btn);
																		}
																	}
																	btnTag.onclick = t(
																			listObject[i].id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																}
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Delete";
																btnTag.className = "btn btn-danger";
																btnTag.value = i;
																t = function(
																		btn, id) {
																	return function() {
																		deleteCategoryForSearch(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		listObject[i].id);
																tdTag
																		.appendChild(btnTag);
																trTag
																		.appendChild(tdTag);
																tbodyTag[0]
																		.appendChild(trTag);
															}
														}
													});
										}
									});
				});

function deleteCategoryForSearch(button, cate_id) {
	$.ajax({
		url : "./category/" + cate_id,
		type : "DELETE",
		data : "",
		contentType : "application/json",
		success : function() {
			var table = document.getElementById("userSearchTable");
			table.deleteRow(button.value);
			//
			var tr = table.getElementsByTagName("tr");
			for (var i = 1; i < tr.length; i++) {
				tr[i].getElementsByTagName("button")[1].value = i;
			}
		}
	});
}

function blockForSearch(cateId, order, button) {
	$.ajax({
		url : "./block/" + cateId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[2].innerHTML = "blocked";
			button.innerHTML = "Activate";
			var func = function(cateId1, order1, button1) {
				return function() {
					activateForSearch(cateId1, order1, button1);
				}
			}
			button.onclick = func(cateId, order, button);
		}
	});
}

function activateForSearch(cateId, order, button) {
	$.ajax({
		url : "./activate/" + cateId,
		type : "POST",
		data : '',
		contentType : "application/json",
		success : function(data) {
			var cateTable = document.getElementById("userSearchTable");
			cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[2].innerHTML = "activated";
			button.innerHTML = "Block";
			var func = function(cateId1, order1, button1) {
				return function() {
					blockForSearch(cateId1, order1, button1);
				}
			}
			button.onclick = func(cateId, order, butto);
		}
	});
}
