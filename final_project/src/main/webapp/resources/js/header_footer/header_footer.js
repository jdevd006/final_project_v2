
$(document).ready(function(){
  $(window).resize(function(){
  	//Show and hide links in footer
  	if ($(window).width() < 650){
  		$("#footer-links").hide();
  		$("#footer-links-collapse").show();
  	}
  	else{
  		$("#footer-links").show();
  		$("#footer-links-collapse").hide();
  	}
  });

  //Search box and search button
  $("#btnSearch").mouseenter(function() {
	$("#searchBox").show();
	});
	$("#btnSearch").click(function() {
		$("#searchBox").hide();
	});
	$("#lang").click(function() {
		$("#searchBox").hide();
	});

	//Sign in and Sign in button when large screen
	$("#signIn").hover(function() {
		$(this).css("color", "blue");
		$(this).css("border-color", "blue");
	}, function() {
		$(this).css("color", "#999999");
		$(this).css("border-color", "#e6e6e6");
	});

	//Sign in and Sign in button when large screen
	$("#signInOut").hover(function() {
		$(this).css("color", "blue");
		$(this).css("border-color", "blue");
	}, function() {
		$(this).css("color", "#999999");
		$(this).css("border-color", "#e6e6e6");
	});
	
	//Cart button
	$("#btnCart").hover(function() {
		$(this).css("color", "blue");
		$(this).css("border-color", "blue");
	}, function() {
		$(this).css("color", "#999999");
		$(this).css("border-color", "#e6e6e6");
	});
});