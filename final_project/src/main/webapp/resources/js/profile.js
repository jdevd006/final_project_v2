function clickUpdateProfile() {
	var regrexMail = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,5}(\.[a-z]{2,3}){1,2}$/;
	var regrexPhone = /^03|09|08[0-9]{8}$/;
	var name = document.getElementById("txtUsername").value.trim();
	var email = document.getElementById("txtEmail").value.trim();
	var phone = document.getElementById("txtPhone").value.trim();
	var pass = document.getElementById("txtPassword").value.trim();
	var city = document.getElementById("city-select-tag").value;
	var district = document.getElementById("district-select-tag").value;
	var more = document.getElementById("txtMore").value.trim();

	if (name.split(/\s+/).join("").length == 0) {
		document.getElementById("invalidName").style.display = "block";
	} else {
		document.getElementById("invalidName").style.display = "none";
		if (email.split(/\s+/).join("").length == 0) {
			document.getElementById("emptyMail").style.display = "block";
			document.getElementById("invalidMail").style.display = "none";
		} else {
			document.getElementById("invalidMail").style.display = "none";
			document.getElementById("emptyMail").style.display = "none";
			if (email.match(regrexMail) != null) {
				document.getElementById("invalidMail").style.display = "none";
				document.getElementById("emptyMail").style.display = "none";
				if (phone.split(/\s+/).join("").length == 0) {
					document.getElementById("emptyPhone").style.display = "block";
					document.getElementById("invalidPhone").style.display = "none";
				} else {
					document.getElementById("invalidPhone").style.display = "none";
					document.getElementById("emptyPhone").style.display = "none";
					if (phone.match(regrexPhone) != null) {
						document.getElementById("invalidPhone").style.display = "none";
						document.getElementById("emptyPhone").style.display = "none";
						if (parseInt(city) != -1) {
							document.getElementById("invalidCity").style.display = "none";
							if (parseInt(district) != -1) {
								if (more.split(/\s+/).join("").length == 0) {
									document.getElementById("invalidMore").style.display = "block";
								} else {
									document.getElementById("invalidMore").style.display = "none";
									if (pass.split(/\s+/).join("").length == 0) {
										document.getElementById("invalidPass").style.display = "block";
									} else {
										document.getElementById("invalidPass").style.display = "none";
										$.ajax({
											url : "./profile?data=" + name
													+ "," + email + "," + phone
													+ "," + pass + "," + city
													+ "," + district + ","
													+ more,
											type : "POST",
											data : '',
											contentType : "application/json",
											success : function(data) {
												location.reload();
											}
										});
										console.log(1);
									}
								}
								document.getElementById("invalidDistrict").style.display = "none";
							} else {
								document.getElementById("invalidDistrict").style.display = "block";
							}
						} else {
							document.getElementById("invalidCity").style.display = "block";
						}
					} else {
						document.getElementById("invalidPhone").style.display = "block";
						document.getElementById("emptyPhone").style.display = "none";
					}
				}
			} else {
				document.getElementById("invalidMail").style.display = "block";
				document.getElementById("emptyMail").style.display = "none";
				// cart.formatmail
			}

		}
	}
}

function getCities() {
	console.log(1);
	$.ajax({
		url : "../cartController/cities",
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("city-select-tag");
			var option;
			var txt;
			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				option.onclick = function() {
					getDistricts(this);
				}
				selectTag.appendChild(option);
			}
		}
	});
}

function getDistricts(option) {
	document.getElementById("city").innerHTML = option.innerHTML;
	$
			.ajax({
				url : "../cartController/districts/" + option.value,
				type : 'GET',
				data : '',
				contentType : 'application/json',
				success : function(data) {
					var selectTag = document
							.getElementById("district-select-tag");
					var option = selectTag.getElementsByTagName("option");
					var txt;

					// Delete all district after selecting another city
					for (var i = option.length - 1; i > 0; i--) {
						selectTag.remove(1)
					}

					for (var i = 0; i < data.length; i++) {
						option = document.createElement("option");
						option.innerHTML = data[i].name;
						option.value = data[i].id;
						var t = function(op) {
							return function() {
								document.getElementById("district").innerHTML = op.innerHTML;
							}
						}
						option.onclick = t(option);
						selectTag.appendChild(option);
					}
				}
			});
}
