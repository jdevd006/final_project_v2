function getSelectValue() {
	return document.getElementById("followSelect").value;
}

function getRadioValue() {
	if (document.getElementById("ascRd").checked == true)
		return "asc";
	return "dsc";
}

function getOrdersByOrder(order) {
	$.ajax({
		url : "./getorders?by=" + order,
		type : "GET",
		data : '',
		contentType : "application/json",
		success : function(data) {
			resetOrdersTable(data);
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function selectClick() {
	console.log(getSelectValue());
	if (getSelectValue() == "finish") {
		getOrdersByOrder("finish");
	} else if (getSelectValue() == "processing") {
		getOrdersByOrder("processing");
	} else if (getSelectValue() == "waiting") {
		getOrdersByOrder("waiting");
	} else if (getSelectValue() == "canceled") {
		getOrdersByOrder("canceled");
	} else if (getSelectValue() == "total") {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("totalAsc");
		} else {
			getOrdersByOrder("totalDsc");
		}
	} else {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("idAsc");
		} else {
			getOrdersByOrder("idDsc");
		}
	}
}

function resetOrdersTable(orders) {
	var c = 0;
	var max;
	if (orders.length % 6 == 0) {
		max = Math.floor(orders.length / 6);
	} else {
		max = Math.floor(orders.length / 6) + 1;
	}
	for (var k = 0; k < max; k++) {
		var ordersTable = document.getElementById("orders-table" + k);
		var tbodyTag = ordersTable.getElementsByTagName("tbody");
		var trTag = tbodyTag[0].getElementsByTagName("tr");
		var tdTag;
		var btnTag;
		// Delete old data
		for (var i = trTag.length; i >= 1; i--)
			ordersTable.deleteRow(i);
		//

		// set
		for (var i = 0; i < 6; i++) {
			if (c <= orders.length - 1) {
				trTag = document.createElement("tr");
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].bill_id;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].name;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].address;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].total;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].order_date;
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				tdTag.innerHTML = orders[c].status;
				if (orders[c].status == "Finish")
					tdTag.style.color = "green";
				else
					tdTag.style.color = "red";
				trTag.appendChild(tdTag);
				tdTag = document.createElement("td");
				btnTag = document.createElement("button");
				btnTag.innerHTML = "Update";
				btnTag.className = "btn btn-primary";
				btnTag.setAttribute("data-toggle", "modal");
				btnTag.setAttribute("data-target", "#myModal");
				var t = function(btn, id) {
					return function() {
						update(btn, id);
					}
				}
				btnTag.onclick = t(btnTag, orders[c].bill_id);
				tdTag.appendChild(btnTag);
				if (orders[i].status == "Canceled") {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Canceled";
					btnTag.className = "btn btn-warning";
					btnTag.disabled = true;
					var t = function(billId, order, button, orderTable) {
						return function() {
							cancel(billId, order, button, orderTable);
						}
					}
					btnTag.onclick = t(orders[c].bill_id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				} else {
					btnTag = document.createElement("button");
					btnTag.innerHTML = "Cancel";
					btnTag.className = "btn btn-warning";
					var t = function(billId, order, button, orderTable) {
						return function() {
							cancel(billId, order, button, orderTable);
						}
					}
					btnTag.onclick = t(orders[c].bill_id, i + 1, btnTag, k);
					tdTag.appendChild(btnTag);
				}

				btnTag = document.createElement("button");
				btnTag.innerHTML = "More detail";
				btnTag.className = "btn btn-secondary";
				btnTag.setAttribute("data-toggle", "modal");
				btnTag.setAttribute("data-target", "#moreDetail");
				t = function(billId, order, button) {
					return function() {
						more(billId, order, button);
					}
				}
				btnTag.onclick = t(orders[c].bill_id, i + 1, btnTag);
				tdTag.appendChild(btnTag);
				trTag.appendChild(tdTag);
				tbodyTag[0].appendChild(trTag);
				c++;
			}
		}
	}
	//
}

function radioClick() {
	if (getSelectValue() == "total") {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("totalAsc");
		} else {
			getOrdersByOrder("totalDsc");
		}
	}
	if (getSelectValue() == "id") {
		if (getRadioValue() == "asc") {
			getOrdersByOrder("idAsc");
		} else {
			getOrdersByOrder("idDsc");
		}
	}
}

// Search function
var first = 0;
$(document)
		.ready(
				function() {
					$("#searchName").keydown(
							function() {
								if ($("#searchName").val().split(/\s+/)
										.join("").length == 0) {
									$("#orders").show();
									$("#searchResult").hide();
								} else {
									$("#orders").hide();
									$("#searchResult").show();
								}
							});
					$("#searchName")
							.keyup(
									function() {
										if ($("#searchName").val().split(/\s+/)
												.join("").length == 0) {
											$("#orders").show();
											$("#searchResult").hide();
										} else {
											$("#orders").hide();
											$("#searchResult").show();
											$
													.ajax({
														url : "./getbills?key="
																+ $(
																		"#searchName")
																		.val()
																		.trim(),
														type : "GET",
														data : '',
														contentType : "application/json",
														success : function(
																orders) {
															var usersTable = document
																	.getElementById("userSearchTable");
															var tbodyTag = usersTable
																	.getElementsByTagName("tbody");
															var trTag = tbodyTag[0]
																	.getElementsByTagName("tr");
															var tdTag;
															var btnTag;
															// Delete old data
															var k = 0;
															for (var i = trTag.length; i >= 1; i--)
																usersTable
																		.deleteRow(i);

															// Set table
															for (var i = 0; i < orders.length; i++) {
																trTag = document
																		.createElement("tr");
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].bill_id;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].name;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].address;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].total;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].order_date;
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																tdTag.innerHTML = orders[i].status;
																if (orders[i].status == "Finish")
																	tdTag.style.color = "green";
																else
																	tdTag.style.color = "red";
																trTag
																		.appendChild(tdTag);
																tdTag = document
																		.createElement("td");
																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "Update";
																btnTag.className = "btn btn-primary";
																btnTag
																		.setAttribute(
																				"data-toggle",
																				"modal");
																btnTag
																		.setAttribute(
																				"data-target",
																				"#myModal");
																var t = function(
																		btn, id) {
																	return function() {
																		update(
																				btn,
																				id);
																	}
																}
																btnTag.onclick = t(
																		btnTag,
																		orders[i].bill_id);
																tdTag
																		.appendChild(btnTag);
																if (orders[i].status == "Canceled") {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Canceled";
																	btnTag.className = "btn btn-warning";
																	btnTag.disabled = true;
																	var t = function(
																			billId,
																			order,
																			button) {
																		return function() {
																			cancelForSearch(
																					billId,
																					order,
																					button);
																		}
																	}
																	btnTag.onclick = t(
																			orders[i].bill_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																} else {
																	btnTag = document
																			.createElement("button");
																	btnTag.innerHTML = "Cancel";
																	btnTag.className = "btn btn-warning";
																	var t = function(
																			billId,
																			order,
																			button) {
																		return function() {
																			cancelForSearch(
																					billId,
																					order,
																					button);
																		}
																	}
																	btnTag.onclick = t(
																			orders[i].bill_id,
																			i + 1,
																			btnTag);
																	tdTag
																			.appendChild(btnTag);
																}

																btnTag = document
																		.createElement("button");
																btnTag.innerHTML = "More detail";
																btnTag.className = "btn btn-secondary";
																btnTag
																		.setAttribute(
																				"data-toggle",
																				"modal");
																btnTag
																		.setAttribute(
																				"data-target",
																				"#moreDetail");
																t = function(
																		billId,
																		order,
																		button) {
																	return function() {
																		more(
																				billId,
																				order,
																				button);
																	}
																}
																btnTag.onclick = t(
																		orders[i].bill_id,
																		i + 1,
																		btnTag);
																tdTag
																		.appendChild(btnTag);
																trTag
																		.appendChild(tdTag);
																tbodyTag[0]
																		.appendChild(trTag);
															}
														}
													});
										}
									});
				});

function cancelForSearch(billId, order, button) {
	$
			.ajax({
				url : './cancel/' + billId,
				type : 'POST',
				data : '',
				contentType : "application/json",
				success : function(data) {
					var cateTable = document
							.getElementById("userSearchTable");
					cateTable.getElementsByTagName("tr")[order]
							.getElementsByTagName("td")[5].innerHTML = "Canceled";
					cateTable.getElementsByTagName("tr")[order]
					.getElementsByTagName("td")[5].style.color = "red";
					button.innerHTML = "Canceled";
					button.disabled = true;
				}
			});
}