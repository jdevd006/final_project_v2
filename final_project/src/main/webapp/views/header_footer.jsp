<!-- cop -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- At here -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<!-- At here -->
<title>Header_Footer Interface</title>
</head>
<body>
	<!-- cop -->
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light navbar-fixed-top flex-row justify-content-between"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light navbar-fixed-top">
					<a class="navbar-brand" href="#"><i class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
																			<a class="dropdown-item" href="../techcontroller/techs">Technology</a> <a
												class="dropdown-item" href="../fashioncontroller/fashions">Fashion</a> <a
												class="dropdown-item" href="../bookcontroller/books">Book</a><a
												class="dropdown-item" href="../homeandfurniturecontroller/hafs">Home And Furniture</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
				<button class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">Sign
					in/Sign up</button>
			</div>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="content">
		<h1>Content</h1>
	</div>

	<div class="container-fluid" id="footer-top">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">INSTRUCTION</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">SERVICES</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">LINKED PAGE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">PAYMENT</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">HOTLINE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">INSTRUCTION</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">SERVICES</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">LINKED PAGES</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">Green Academy</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">Địa chỉ: 212A2 Nguyễn Trãi,
									P.Nguyễn Cư Trinh, Q.1, Tp.Hồ Chí Minh</p>
								<p style="margin-bottom: 2px;">[Email]:
									support@fashionstar.vn - [Hotline]: 1900 636 820 -[Tel]: (028)
									3925 5050 Ext 200 -[Fax]: (028) 3925 5050. Bản quyền © 2015 của
									Fashion Star. Tất cả các quyền được bảo lưu</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>