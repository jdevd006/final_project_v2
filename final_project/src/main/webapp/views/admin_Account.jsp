<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title><spring:message code="adAc.title"></spring:message></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Account.css" />">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_Account.js" />"></script>
</head>
<body>
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header style="float: left;">
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<div style="width: 200px; display: flex;">
						<div style="margin: auto 0px;">
							<a style="font-size: 14px; text-decoration: none;"
								href="./users?lang=vi"><img alt=""
								src="<c:url value="/resources/image/vn.png"/>"> VN</a> <a
								style="font-size: 14px; text-decoration: none;"
								href="./users?lang=en"><img alt=""
								src="<c:url value="/resources/image/en.png"/>"> EN</a>
						</div>
						<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
						<div class="dropdown-menu" aria-labelledby="dropdownId">
							<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
						</div>
					</div>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout"><spring:message
								code="adPro.close"></spring:message></a>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" style="display: flex;">
			<div class="wrapper d-flex justify content-between">
				<div id="side-bar">
					<div class="logo" style="text-align: center">
						<spring:message code="adPro.logo"></spring:message>
					</div>
					<ul class="list-group rounded-0">
						<li class="dashboard"><a
							href="../dashBoardController/dashBoard"><i
								class="fa fa-dashboard" style="font-size: 36px"></i> <spring:message
									code="adPro.db"></spring:message></a></li>
						<li><a href="../categoryController/categories"> <i
								class="fa fa-book mr-2"></i> <spring:message code="adPro.db1"></spring:message>
						</a></li>
						<li><a href="../productController/products"> <i
								class="fa fa-user mr-2"></i> <spring:message code="adPro.db2"></spring:message>
						</a></li>
						<li style="background-color: steelblue;"><a
							href="../userController/users"><i class="fa fa-cart-plus"></i>
								<spring:message code="adPro.db3"></spring:message></a></li>
						<li><a href="../messageController/messages"><i
								class="fa fa-tags"></i> <spring:message code="adPro.db4"></spring:message></a></li>
						<li><a href="../adminOrderListController/orders"> <i
								class="fa fa-cart-plus"></i> <spring:message code="adMe.db6"></spring:message></a></li>
					</ul>
				</div>
			</div>

			<div style="margin-top: 60px; margin-left: 1px; width: 100%;">
				<div id="wrapper-content" style="width: inherit;">
					<section class="control">
						<div class="row" style="margin-left: 1px; padding: 0 5px;">
							<div class="d-flex align-items-center ">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Username"
										id="searchName"
										value="<spring:message code ="adAc.phd.name" ></spring:message>">
									<div class="input-group-prepend">
										<span class="input-group-text" id="btnTimNV"><i
											class="fa fa-search"></i></span>
									</div>
								</div>
							</div>
							<div class="align-items-center justify-content-end"
								style="display: flex; flex-direction: row; margin: auto; width: 400px;">
								<label style="width: 100px"><spring:message
										code="adPro.sort"></spring:message></label> <select id="followSelect"
									style="width: 300px;">
									<option value="name" onclick="selectClick()"
										selected="selected"><spring:message
											code="adPro.sortName"></spring:message></option>
									<option value="id" onclick="selectClick()"><spring:message
											code="adPro.sortID"></spring:message></option>
								</select>
								<form id="myRadioBtn" class="d-flex align-items-center m-2"
									action="">
									<input class="m-2" type="radio" name="gender" value="asc"
										onclick="radioClick()" checked="checked" id="ascRd">
									<spring:message code="adPro.sortI"></spring:message>
									<br> <input class="m-2" type="radio" name="gender"
										value="dscRd" onclick="radioClick()" id="dscRd">
									<spring:message code="adPro.sortD"></spring:message>
									<br>
								</form>
							</div>
						</div>



						<button class="btn btn-primary" data-toggle="modal"
							data-target="#myModal" style="float: right;"
							onmouseenter="cleanAddModal()" onclick="getCities();">
							<spring:message code="adPro.add"></spring:message>
						</button>

						<!-- <div style="display: none;" id="test"> -->
						<h2 style="margin-left: 10px;">
							<spring:message code="adAc.addAc"></spring:message>
						</h2>
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<form:form action="./user" method="post" modelAttribute="user">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">
												<spring:message code="adAc.addAc"></spring:message>
											</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<fieldset
												style="display: flex; flex-direction: column; justify-content: space-around;">
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtUsername"><spring:message
															code="adAc.fullname"></spring:message></label>
													<form:input type="text" path="user_name"
														class="form-control" id="txtUsername"
														placeholder="Full name" style="width: 300px;" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtUseremail"><spring:message
															code="adMe.email"></spring:message></label>
													<form:input type="text" placeholder="Email"
														style="width: 300px;" path="email" id="txtUseremail"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtUserphone"><spring:message
															code="login.phone"></spring:message></label>
													<form:input type="text" placeholder="Phone"
														style="width: 300px;" path="phone" id="txtUserphone"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtUserpassword"><spring:message
															code="login.password"></spring:message></label>
													<form:input type="text" placeholder="Password"
														style="width: 300px;" path="password" id="txtUserpassword"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtStatus">Status</label>
													<form:input type="text" placeholder="Status"
														style="width: 300px;" path="status" id="txtStatus"
														class="form-control" readonly="true" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<form:select path="address.city" id="city-select-tag"
														style="width: 150px">
														<option selected value="-1"><spring:message
																code="cart.sel1"></spring:message></option>
													</form:select>
													<form:input type="text" placeholder="City"
														style="width: 300px;" path="status" id="txtCity"
														class="form-control" readonly="true" />
												</div>


												<div class="form-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<form:select path="address.district"
														id="district-select-tag" style="width: 150px">
														<option selected value="-1"><spring:message
																code="cart.sel2"></spring:message></option>
													</form:select>
													<form:input type="text" placeholder="District"
														style="width: 300px;" path="status" id="txtDistrict"
														class="form-control" readonly="true" />
												</div>

												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtStreet">
														Street</label>
													<form:input type="text" placeholder="Address detail"
														style="width: 300px;" path="address.street" id="txtStreet"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<form:label class="control-label" path="account_role.role">
														<spring:message code="adAc.role"></spring:message>
													</form:label>
													<div style="width: 300px;">
														<form:radiobutton path="account_role.role" value="admin"
															label="Admin" id="adminRd" />
														<form:radiobutton path="account_role.role" value="user"
															label="User" id="userRd" />
													</div>
												</div>
											</fieldset>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary"
												name="btnSaveInModal" value="" id="btnSaveInModal"
												onclick="test()">
												<spring:message code="adPro.save"></spring:message>
											</button>
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">
												<spring:message code="adPro.close"></spring:message>
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
						<!-- </div> -->

						<div id="users" class="carousel slide" data-ride="carousel"
							style="background: none;" data-interval="false">
							<!-- Indicators -->
							<c:set var="first" value="0"></c:set>
							<ul class="carousel-indicators carousel-indicators-numbers"
								style="margin-bottom: 0px;">
								<c:if test="${fn:length(users) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#users" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#users" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(users) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#users" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#users" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
							</ul>

							<div class="carousel-inner" style="">
								<c:set var="first" value="0"></c:set>
								<c:set var="count1" value="0"></c:set>
								<c:set var="count" value="1"></c:set>
								<c:if test="${fn:length(users) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="users-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adAc.ID"></spring:message></th>
															<th><spring:message code="adAc.name"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="login.phone"></spring:message></th>
															<th><spring:message code="adAc.role"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(users)-1}">
																<tr>
																	<td>${users[count1].user_id}</td>
																	<td>${users[count1].user_name}</td>
																	<td>${users[count1].email}</td>
																	<td>${users[count1].address.city}-${users[count1].address.district}-${users[count1].address.street}</td>
																	<td>${users[count1].phone}</td>
																	<td>${users[count1].account_role.role}</td>
																	<td>${users[count1].status}</td>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${users[count1].user_name}"
																			onclick="update(this, ${users[count1].user_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${users[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${users[count1].user_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${users[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${users[count1].user_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteUser(this, ${users[count1].user_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="users-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adAc.ID"></spring:message></th>
															<th><spring:message code="adAc.name"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="login.phone"></spring:message></th>
															<th><spring:message code="adAc.role"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(users)-1}">
																<tr>
																	<td>${users[count1].user_id}</td>
																	<td>${users[count1].user_name}</td>
																	<td>${users[count1].email}</td>
																	<td>${users[count1].address.city}-${users[count1].address.district}-${users[count1].address.street}</td>
																	<td>${users[count1].phone}</td>
																	<td>${users[count1].account_role.role}</td>
																	<td>${users[count1].status}</td>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${users[count1].user_name}"
																			onclick="update(this, ${users[count1].user_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${users[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${users[count1].user_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${users[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${users[count1].user_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteUser(this, ${users[count1].user_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(users) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="users-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adAc.ID"></spring:message></th>
															<th><spring:message code="adAc.name"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="login.phone"></spring:message></th>
															<th><spring:message code="adAc.role"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(users)-1}">
																<tr>
																	<td>${users[count1].user_id}</td>
																	<td>${users[count1].user_name}</td>
																	<td>${users[count1].email}</td>
																	<td>${users[count1].address.city}-${users[count1].address.district}-${users[count1].address.street}</td>
																	<td>${users[count1].phone}</td>
																	<td>${users[count1].account_role.role}</td>
																	<td>${users[count1].status}</td>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${users[count1].user_name}"
																			onclick="update(this, ${users[count1].user_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${users[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${users[count1].user_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${users[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${users[count1].user_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteUser(this, ${users[count1].user_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="users-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adAc.ID"></spring:message></th>
															<th><spring:message code="adAc.name"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="login.phone"></spring:message></th>
															<th><spring:message code="adAc.role"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(users)-1}">
																<tr>
																	<td>${users[count1].user_id}</td>
																	<td>${users[count1].user_name}</td>
																	<td>${users[count1].email}</td>
																	<td>${users[count1].address.city}-${users[count1].address.district}-${users[count1].address.street}</td>
																	<td>${users[count1].phone}</td>
																	<td>${users[count1].account_role.role}</td>
																	<td>${users[count1].status}</td>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${users[count1].user_name}"
																			onclick="update(this, ${users[count1].user_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${users[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${users[count1].user_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${users[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${users[count1].user_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteUser(this, ${users[count1].user_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
							</div>
						</div>
						<div id="searchResult" style="display: none;">
							<table class="table table-bordered" style="width: 100%;"
								id="userSearchTable">
								<thead>
									<tr>
										<th><spring:message code="adAc.ID"></spring:message></th>
										<th><spring:message code="adAc.name"></spring:message></th>
										<th><spring:message code="adMe.email"></spring:message></th>
										<th><spring:message code="adOr.address"></spring:message></th>
										<th><spring:message code="login.phone"></spring:message></th>
										<th><spring:message code="adAc.role"></spring:message></th>
										<th>Status</th>
										<th><spring:message code="adMe.action"></spring:message></th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		</div>
		<script>
		function deleteUser(button, userId, orderTable) {
			$.ajax({
				url : "./user/" + userId,
				type : "DELETE",
				data : "",
				contentType : "application/json",
				success : function() {
					var table = document.getElementById("users-table"+orderTable);
					table.deleteRow(button.value);
					//
					var tr = table.getElementsByTagName("tr");
					for (var i = 1; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[1].value = i;
					}
				}
			});
		}
		function update(button, user_id) {
			getCities();
			$
					.ajax({
						url : "./info?userId=" + user_id,
						type : "GET",
						data : "",
						contentType : "application/json",
						success : function(data) {
							document.getElementById("btnSaveInModal").value = "update-"
									+ user_id;

							document.getElementById("txtUsername").value = data[0].user_name;
							document.getElementById("txtUseremail").value = data[0].email;
							document.getElementById("txtUserphone").value = data[0].phone;
							document.getElementById("txtUserpassword").value = data[0].password;
							document.getElementById("txtStatus").value = data[0].status;
							if (data[1].role == "admin")
								document.getElementById("adminRd").checked = true;
							if (data[1].role == "user")
								document.getElementById("userRd").checked = true;
							
							document.getElementById("txtCity").value = data[2].city;
							document.getElementById("txtDistrict").value = data[2].district;
							document.getElementById("txtStreet").value = data[2].street;
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
		}

		function block(userId, order, button, orderTable) {
			$
					.ajax({
						url : "./block/" + userId,
						type : "POST",
						data : '',
						contentType : "application/json",
						success : function(data) {
							var cateTable = document
									.getElementById("users-table"+orderTable);
							cateTable.getElementsByTagName("tr")[order]
									.getElementsByTagName("td")[6].innerHTML = "blocked";
							button.innerHTML = "Activate";
							var func = function(userId1, order1, button1, orderTable1) {
								return function() {
									activate(userId1, order1, button1, orderTable1);
								}
							}
							button.onclick = func(userId, order, button, orderTable);
						}
					});
		}

		function activate(userId, order, button, orderTable) {
			$
					.ajax({
						url : "./activate/" + userId,
						type : "POST",
						data : '',
						contentType : "application/json",
						success : function(data) {
							var cateTable = document
									.getElementById("users-table"+orderTable);
							cateTable.getElementsByTagName("tr")[order]
									.getElementsByTagName("td")[6].innerHTML = "activated";
							button.innerHTML = "Block";
							var func = function(userId1, order1, button1, orderTable1) {
								return function() {
									block(userId1, order1, button1, orderTable1);
								}
							}
							button.onclick = func(userId, order, button, orderTable);
						}
					});
		}
		function test (){
			if (parseInt(document.getElementById("district-select-tag").value) == -1 || parseInt(document.getElementById("city-select-tag").value) == -1){
				alert("Task cannot be done because some fields missed information!!!");
			}
			}
		
		
	</script>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">
				<spring:message code="adAc.memo1"></spring:message>
			</button>
			<div style="text-align: center; font-size: 30px;">
				<strong><spring:message code="adPro.del"></spring:message></strong>
				<spring:message code="adAc.memo"></spring:message>
			</div>
		</div>
	</c:if>
	<div class="alert alert-warning alert-dismissible fade show"
		role="alert" style="display: none">
		<strong>Holy guacamole!</strong> You should check in on some of those
		fields below.
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</body>
</html>