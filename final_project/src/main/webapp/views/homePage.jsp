﻿<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">
</head>
<script type="text/javascript"
	src="<c:url value="/resources/js/homepagelistproduct.js"/>"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/homepage.css"/>">
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<body
	onload="carouselBOOK();carouselTECH();carouselFASHION();carouselHAF()">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i> <spring:message
											code="header.Home"></spring:message></a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
												code="header.Category"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="../techcontroller/techs"><spring:message
													code="header.CateTech"></spring:message></a> <a
												class="dropdown-item" href="../fashioncontroller/fashions"><spring:message
													code="header.CateFashion"></spring:message></a> <a
												class="dropdown-item" href="../bookcontroller/books"><spring:message
													code="header.CateBook"></spring:message></a><a
												class="dropdown-item"
												href="../homeandfurniturecontroller/hafs"><spring:message
													code="header.CateHAF"></spring:message></a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i> <spring:message
											code="header.Contact"></spring:message></a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link"
						href="../homecontroller/homepage?lang=vi">VN</a></li>
					<li class="nav-item"><a class="nav-link"
						href="../homecontroller/homepage?lang=en">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span><spring:message
							code="header.Login"></spring:message></span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a>
						<button class="dropdown-item" onclick="logOut()">
							<spring:message code="cart.logout"></spring:message>
						</button>
					</div>
				</div>

			</c:if>

			<%-- <button class="btn" id="btnCart" type="button" value="${user_id}"
				onclick=""
				style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
				<i class="fas fa-shopping-cart"><span class="badge badge-light"
					style="background-color: yellow; margin-left: 5px;"
					id="num-item-in-cart">0</span></i>
			</button> --%>


			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="contentContainer">
		<div id="content" style="margin-top: 100px">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 br-blue">
						<div class="row">
							<i class="fa fa-fw fa-align-justify"></i>
							<spring:message code="hpCate.Cate"></spring:message>
						</div>
						<div class="row">
							<div class="dropright">
								<a href="../techcontroller/techs" class="text-info"> <span
									class="icon-wrap"> <i class="fas fa-headphones"></i>
								</span> <span> <spring:message code="hpCate.Tech"></spring:message>
								</span>
								</a>
								<div class="dropright-content">
									<div class="row">
										<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="dropright">
								<a href="../fashioncontroller/fashions" class="text-info"> <span
									class="icon-wrap"> <i class="fas fa-mask"></i>
								</span> <span> <spring:message code="hpCate.Fashion"></spring:message>
								</span>
								</a>
								<div class="dropright-content">
									<div class="row">
										<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="dropright">
								<a href="../bookcontroller/books" class="text-info"> <span
									class="icon-wrap"> <i class="fas fa-book"></i>
								</span> <span> <spring:message code="hpCate.Book"></spring:message>
								</span>
								</a>
								<div class="dropright-content">
									<div class="row">
										<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="dropright">
								<a href="../homeandfurniturecontroller/hafs" class="text-info">
									<span class="icon-wrap"> <i class="fas fa-home"></i>
								</span> <span> <spring:message code="hpCate.Hafs"></spring:message></span>
								</a>
								<div class="dropright-content">
									<div class="row">
										<a href="#"></a> <a href="#">link1</a> <a href="#">link1</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-5">
						<div id="carousel" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ul class="carousel-indicators">
								<li data-target="#carousel" data-slide-to="0" class="active"></li>
								<li data-target="#carousel" data-slide-to="1"></li>
								<li data-target="#carousel" data-slide-to="2"></li>
							</ul>
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img id="pro_img" class="card-img-top"
										src="<c:url value="/resources/image/demo1.jpg"/>"
										alt="Carousel-image"
										style="width: 450; height: 230; position: relative; margin: auto;">
								</div>
								<div class="carousel-item">
									<img id="pro_img" class="card-img-top"
										src="<c:url value="/resources/image/demo2.jpg"/>"
										alt="Carousel-image"
										style="width: 450; height: 230; position: relative; margin: auto;">
								</div>
								<div class="carousel-item">
									<img id="pro_img" class="card-img-top"
										src="<c:url value="/resources/image/demo3.jpg"/>"
										alt="Carousel-image"
										style="width: 450; height: 230; position: relative; margin: auto;">
								</div>
							</div>
							<!-- Left and right controls -->
							<a class="carousel-control-prev" href="#carousel"
								data-slide="prev"> <span class="carousel-control-prev-icon"></span>
							</a> <a class="carousel-control-next" href="#carousel"
								data-slide="next"> <span class="carousel-control-next-icon"></span>
							</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card-img-fluid bg-dark"
							style="width: 350px; height: 275px">
							<c:if test="${currentUser== null }">
								<div class="card-header">
									<img class="rounded-circle rounded mx-auto d-block"
										src="<c:url value="/resources/image/img_avatar1.png"/>"
										alt="Card image" width="100" height="100">
								</div>

								<div class="text-center card-footer" style="margin-bottom: 0">
									<a href="../cartController/cartPage" class="btn btn-info"
										style="color: white"> <spring:message code="hp.SeeCart"></spring:message>
									</a>
								</div>

							</c:if>
							<c:if test="${currentUser != null }">
								<img class="rounded-circle rounded mx-auto d-block"
									src="<c:url value="/resources/image/img_avatar1.png"/>"
									alt="Card image" width="100" height="100">
								<div class="card-body ">
									<div class="text-center" style="margin-bottom: 0">
										<h4>${currentUser.user_name }</h4>
									</div>
									<div class="text-center">
										<a href="../profileController/profile" class="btn btn-info"
											style="color: white"><spring:message code="hp.SeeProfile"></spring:message></a>
										<a href="../cartController/cartPage" class="btn btn-info"
											style="color: white"><spring:message code="hp.SeeCart"></spring:message>
										</a>
									</div>
								</div>
							</c:if>

						</div>
						<br>
					</div>
					<div class="col-lg-3">

						<img class="rounded"
							src="<c:url value="/resources/image/demo4.png"/>" width="250"
							height="150" alt="demo4">
					</div>
					<div class="col-lg-3">
						<img class="rounded"
							src="<c:url value="/resources/image/demo5.png"/>" width="250"
							height="150" alt="demo5">
					</div>
					<div class="col-lg-3">
						<img class="rounded"
							src="<c:url value="/resources/image/demo6.png"/>" width="250"
							height="150" alt="demo6">
					</div>
					<div class="col-lg-3">
						<img class="rounded"
							src="<c:url value="/resources/image/demo7.png"/>" width="250"
							height="150" alt="demo7">
					</div>

					<!-- Mua nhieu nhat -->
					<div class="container">
						<h3 class=""
							style="color: red; width: inherit; text-align: center;">TOP
							10 Best-seller</h3>
						<div id="relate" class="carousel slide" data-ride="carousel"
							style="background: none;">

							<!-- Indicators -->
							<c:set var="first" value="0"></c:set>
							<ul class="carousel-indicators carousel-indicators-numbers"
								style="margin-bottom: 0px;">
								<c:if test="${fn:length(bests) % 5 != 0}">
									<c:forEach begin="0" end="${evenBest}">
										<c:if test="${first == 0}">
											<li data-target="#relate" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#relate" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(listRelate) % 5 == 0}">
									<c:if test="${evenBest > 0}">
										<c:forEach begin="0" end="${evenBest-1}">
											<c:if test="${first == 0}">
												<li data-target="#relate" data-slide-to="${first}"
													class="active"
													style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
											</c:if>
											<c:if test="${first != 0}">
												<li data-target="#relate" data-slide-to="${first}"
													style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
											</c:if>
											<c:set var="first" value="${first+1}"></c:set>
										</c:forEach>
									</c:if>
								</c:if>
							</ul>

							<div class="carousel-inner" style="height: 460px;">
								<c:set var="first" value="0"></c:set>
								<c:set var="count" value="0"></c:set>
								<c:if test="${fn:length(bests) % 5 != 0}">
									<c:forEach begin="0" end="${evenBest}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table
													style="margin-left: auto; margin-right: auto; width: inherit;">
													<tr style="margin-left: auto; margin-right: auto;">
														<c:forEach begin="0" end="4">
															<td><c:if test="${count <= fn:length(bests)-1}">
																	<div style="width: 218px; height: 400px;">
																		<div class="card" style="width: 218px; height: 390px;">
																			<img class="card-img-top"
																				src="<c:url value="${bests[count].img}"/>"
																				alt="Card image" style="height: 200px;">
																			<div class="card-body"
																				style="display: flex; flex-direction: column; justify-content: space-between;">
																				<a class="card-title"
																					style="text-align: center; text-decoration: none;"
																					href="../viewProductController/product?proid=${bests[count].pro.pro_id}">${fn:substring(bests[count].pro.pro_name, 0, 25)}</a>
																				<p class="" style="color: burlywood;margin-left: auto;margin-right: auto;">${fn:substring(bests[count].pro.pro_content, 0, 20)}</p>
																				<p class="card-text" style="text-align: center;">${bests[count].pro.pro_cost}
																					&#8363</p>
																				<div
																					style="display: flex; flex-direction: row; justify-content: space-around;">
																					<a
																						href="../viewProductController/product?proid=${bests[count].pro.pro_id}"
																						class="btn btn-primary"
																						style="font-size: 12px;"><spring:message
																							code="view.a7"></spring:message><span
																						class="badge badge-pill badge-danger">${bests[count].pro.buyTime}</span></a>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:if></td>
															<c:set var="count" value="${count+1}"></c:set>
														</c:forEach>
													</tr>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 1}">
											<div class="carousel-item" style="background: none;">
												<table
													style="margin-left: auto; margin-right: auto; width: inherit;">
													<tr style="margin-left: auto; margin-right: auto;">
														<c:forEach begin="0" end="4">
															<td><c:if test="${count <= fn:length(bests)-1}">
																	<div style="width: 218px; height: 400px;">
																		<div class="card" style="width: 218px; height: 390px;">
																			<img class="card-img-top"
																				src="<c:url value="${bests[count].img}"/>"
																				alt="Card image" height="200px"
																				style="height: 200px;">
																			<div class="card-body"
																				style="display: flex; flex-direction: column; justify-content: space-between;">
																				<a class="card-title" style="text-align: center;"
																					href="../viewProductController/product?proid=${bests[count].pro.pro_id}">${fn:substring(bests[count].pro.pro_name, 0, 25)}</a>
																				<p class="" style="color: burlywood;margin-left: auto;margin-right: auto;">${fn:substring(bests[count].pro.pro_content, 0, 20)}</p>
																				<p class="card-text" style="text-align: center;">${bests[count].pro.pro_cost}
																					&#8363</p>
																				<div
																					style="display: flex; flex-direction: row; justify-content: space-around;">
																					<a
																						href="../viewProductController/product?proid=${bests[count].pro.pro_id}"
																						class="btn btn-primary" style="font-size: 12px;">
																						<spring:message code="view.a7"></spring:message><span
																						class="badge badge-pill badge-danger">${bests[count].pro.buyTime}</span>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:if></td>
															<c:set var="count" value="${count+1}"></c:set>
														</c:forEach>
													</tr>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>

								<c:if test="${fn:length(bests) % 5 == 0}">
									<c:if test="${evenBest > 0}">
										<c:forEach begin="1" end="${evenBest}">
											<c:if test="${first == 0}">
												<div class="carousel-item active" style="background: none;">
													<table
														style="margin-left: auto; margin-right: auto; width: inherit;">
														<tr style="margin-left: auto; margin-right: auto;">
															<c:forEach begin="0" end="4">
																<td><c:if test="${count <= fn:length(bests)-1}">
																		<div style="width: 218px; height: 400px;">
																			<div class="card"
																				style="width: 218px; height: 390px;">
																				<img class="card-img-top"
																					src="<c:url value="${bests[count].img}"/>"
																					alt="Card image" style="height: 200px;">
																				<div class="card-body"
																					style="display: flex; flex-direction: column; justify-content: space-between;">
																					<a class="card-title"
																						style="text-align: center; text-decoration: none;"
																						href="../viewProductController/product?proid=${bests[count].pro.pro_id}">${fn:substring(bests[count].pro.pro_name, 0, 25)}</a>
																					<p class="" style="color: burlywood;margin-left: auto;margin-right: auto;">${fn:substring(bests[count].pro.pro_content, 0, 20)}</p>
																					<p class="card-text" style="text-align: center;">${bests[count].pro.pro_cost}
																						&#8363</p>
																					<div
																						style="display: flex; flex-direction: row; justify-content: space-around;">
																						<a
																							href="../viewProductController/product?proid=${bests[count].pro.pro_id}"
																							class="btn btn-primary"
																							style="font-size: 12px;"><spring:message
																								code="view.a7"></spring:message><span
																							class="badge badge-pill badge-danger">${bests[count].pro.buyTime}</span></a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</c:if></td>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</tr>
													</table>
												</div>
											</c:if>
											<c:if test="${first != 1}">
												<div class="carousel-item" style="background: none;">
													<table
														style="margin-left: auto; margin-right: auto; width: inherit;">
														<tr style="margin-left: auto; margin-right: auto;">
															<c:forEach begin="0" end="4">
																<td><c:if test="${count <= fn:length(bests)-1}">
																		<div style="width: 218px; height: 400px;">
																			<div class="card"
																				style="width: 218px; height: 390px;">
																				<img class="card-img-top"
																					src="<c:url value="${bests[count].img}"/>"
																					alt="Card image" height="200px"
																					style="height: 200px;">
																				<div class="card-body"
																					style="display: flex; flex-direction: column; justify-content: space-between;">
																					<a class="card-title" style="text-align: center;"
																						href="../viewProductController/product?proid=${bests[count].pro.pro_id}">${fn:substring(bests[count].pro.pro_name, 0, 25)}</a>
																					<p class="" style="color: burlywood;margin-left: auto;margin-right: auto;">${fn:substring(bests[count].pro.pro_content, 0, 20)}</p>
																					<p class="card-text" style="text-align: center;">${bests[count].pro.pro_cost}
																						&#8363</p>
																					<div
																						style="display: flex; flex-direction: row; justify-content: space-around;">
																						<a
																							href="../viewProductController/product?proid=${bests[count].pro.pro_id}"
																							class="btn btn-primary" style="font-size: 12px;"><spring:message
																								code="view.a7"></spring:message><span
																							class="badge badge-pill badge-danger">${bests[count].pro.buyTime}</span></a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</c:if></td>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</tr>
													</table>
												</div>
											</c:if>
											<c:set var="first" value="${first+1}"></c:set>
										</c:forEach>
									</c:if>
								</c:if>
							</div>

							<!-- Left and right controls -->
							<a class="carousel-control-prev" href="#relate" data-slide="prev"
								style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-left: 3px;">
								<span class="carousel-control-prev-icon"></span>

							</a> <a class="carousel-control-next" href="#relate"
								data-slide="next"
								style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-right: 3px;">
								<span class="carousel-control-next-icon"></span>
							</a>
						</div>
					</div>

					<c:set value="0" var="count"></c:set>
					<div class="col-lg-12">
						<div class="text-center">
							<a href="../bookcontroller/books"><h3 class="text-info">
									<spring:message code="hpCate.Book"></spring:message>
								</h3></a>
						</div>
						<div id="carousel-book" class="carousel slide"
							data-ride="carousel">
							<!-- Indicators -->
							<ul class="carousel-indicators">
								<li data-target="#carousel-book" data-slide-to="0"
									class="active"></li>
								<li data-target="#carousel-book" data-slide-to="1"></li>
								<li data-target="#carousel-book" data-slide-to="2"></li>
							</ul>
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="row" id="carousel-Book-row1">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>

												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>

								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Book-row2">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>

									</div>
								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Book-row3">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listbook[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_book[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listbook[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listbook[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Left and right controls -->
							<a href="#carousel-book" data-slide="prev"> <span
								class="fa fa-arrow-left" style=""></span>

							</a> <a href="#carousel-book" data-slide="next"> <span
								class="fas fa-arrow-right"></span>
							</a>
						</div>
					</div>




					<!----------------------------TECH---------------------------------- -->

					<c:set value="0" var="count"></c:set>
					<div class="col-lg-12">
						<div class="text-center">
							<a href="../techcontroller/techs"><h3 class="text-info">
									<spring:message code="hpCate.Tech"></spring:message>
								</h3></a>
						</div>
						<div id="carousel-tech" class="carousel slide"
							data-ride="carousel">
							<!-- Indicators -->
							<ul class="carousel-indicators">
								<li data-target="#carousel-tech" data-slide-to="0"
									class="active"></li>
								<li data-target="#carousel-tech" data-slide-to="1"></li>
								<li data-target="#carousel-tech" data-slide-to="2"></li>
							</ul>
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="row" id="carousel-Tech-row1">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>

												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>

								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Tech-row2">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>

									</div>
								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Tech-row3">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listtech[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listtech[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>
								</div>
							</div>
							<!-- Left and right controls -->
							<a href="#carousel-tech" data-slide="prev"> <span
								class="fa fa-arrow-left"></span>

							</a> <a href="#carousel-tech" data-slide="next"> <span
								class="fas fa-arrow-right"></span>
							</a>
						</div>
					</div>




					<!-- --------------------------------------FASHION------------------------------------------ -->



					<c:set value="0" var="count"></c:set>
					<div class="col-lg-12">
						<div class="text-center">
							<a href="../fashioncontroller/fashions"><h3 class="text-info">
									<spring:message code="hpCate.Fashion"></spring:message>
								</h3></a>
						</div>
						<div id="carousel-fashion" class="carousel slide"
							data-ride="carousel">
							<!-- Indicators -->
							<ul class="carousel-indicators">
								<li data-target="#carousel-fashion" data-slide-to="0"
									class="active"></li>
								<li data-target="#carousel-fashion" data-slide-to="1"></li>
								<li data-target="#carousel-fashion" data-slide-to="2"></li>
							</ul>
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="row" id="carousel-Fashion-row1">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>

								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Fashion-row2">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>

										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>

									</div>
								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-Fashion-row3">
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listfashion[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_fashion[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listfashion[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listfashion[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>
								</div>
							</div>
							<!-- Left and right controls -->
							<a href="#carousel-fashion" data-slide="prev"> <span
								class="fa fa-arrow-left"></span>
							</a> <a href="#carousel-fashion" data-slide="next"> <span
								class="fas fa-arrow-right"></span>
							</a>
						</div>
					</div>


					<!-- --------------------------------------------HOME---------------------------------------------------- -->



					<c:set value="0" var="count"></c:set>
					<div class="col-lg-12">
						<div class="text-center">
							<a href="../homeandfurniturecontroller/hafs"><h3
									class="text-info">
									<spring:message code="hpCate.Hafs"></spring:message>
								</h3></a>
						</div>
						<div id="carousel-home" class="carousel slide"
							data-ride="carousel">
							<!-- Indicators -->
							<ul class="carousel-indicators">
								<li data-target="#carousel-home" data-slide-to="0"
									class="active"></li>
								<li data-target="#carousel-home" data-slide-to="1"></li>
								<li data-target="#carousel-home" data-slide-to="2"></li>
							</ul>
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="row" id="carousel-HAF-row1">
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>

								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-HAF-row2">
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>

										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>

										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>

									</div>
								</div>
								<div class="carousel-item">
									<div class="row" id="carousel-HAF-row3">
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
										<div class="column">
											<div class="card" style="width: 285px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a href="${listhaf[count].pro_id }"><img
														class="card-img-top"
														src="<c:url value="${listimg_haf[count].img }"/>"
														width="270" height="200" alt="Card image cap"></a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="hpPro.Name"></spring:message>
														: ${listhaf[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="hppro.Price"></spring:message>
														: ${listhaf[count].pro_cost }
													</p>
												</div>
											</div>
										</div>
										<c:set value="${count+1 }" var="count">
										</c:set>
									</div>
								</div>
							</div>
							<!-- Left and right controls -->
							<a href="#carousel-home" data-slide="prev"> <span
								class="fa fa-arrow-left"></span>

							</a> <a href="#carousel-home" data-slide="next"> <span
								class="fas fa-arrow-right"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div id="searchResult"
		style="display: none; margin-top: 100px; margin-bottom: 80px;"
		class="container">
		<table id="searchResult-table" style="width: 100%;">
			<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
		</table>
	</div>
	<div class="container-fluid" id="footer-top" style="margin-top: auto;">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p3"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shipping"></spring:message>
					</a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a>
					<a href="#"><spring:message code="footer.term"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p4"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a>
					<a href="../contactcontroller/contact"><spring:message
							code="footer.contactus"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p5"></spring:message>
					</p>
					<a href="https://www.lazada.vn">Lazada</a> <a
						href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
					<a href="https://www.amazon.com/">Amazon</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p6"></spring:message>
					</p>
					<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
						href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message
							code="footer.cash"></spring:message></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="address"></spring:message>
								</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="infomation"></spring:message>
								</p>
							</div>
						</div>
					</div>
					<div
						style="background-color: gray; color: black; text-align: center;">
						<p style="margin-top: auto; margin-bottom: auto;">
							<spring:message code="footer.compyright"></spring:message>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>