<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Dash board</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Catalog.css" />">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_Catalog.js"/>"></script>
<script src="https://code.jquery.com/jquery-3.4.0.js"
	integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo="
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_DashBoard.js" />"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body
	onload="setFirst();setYearBarChart();setPieChartFisrt();generateFirstMonth()">
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header>
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
					</div>
				</div>
			</nav>
		</header>
		<div class="wrapper d-flex justify content-between">
			<div id="side-bar">
				<div class="logo" style="text-align: center;">ADMIN PAGE</div>
				<ul class="list-group rounded-0">
					<li class="dashboard"><a
						href="../dashBoardController/dashBoard"><i
							class="fa fa-dashboard" style="font-size: 36px"></i> DASHBOARD</a></li>
					<li><a href="../categoryController/categories"><i
							class="fa fa-book mr-2"></i> Catalog management </a></li>
					<li><a href="../productController/products"><i
							class="fa fa-cart-plus"></i> Product management </a></li>
					<li><a href="../userController/users"> <i
							class="fa fa-user mr-2"></i> User management
					</a></li>
					<li><a href="../adminOrderListController/orders"> <i
							class="fa fa-cart-plus"></i> Order management
					</a></li>
					<li><a href="../messageController/messages"> <i
							class="fa fa-tags"></i> Customer message
					</a></li>
					
				</ul>
			</div>

			<div
				style="width: 100%; display: flex; flex-direction: column; justify-content: space-around;">
				<div
					style="width: 100%; display: flex; flex-direction: row; justify-content: space-around; padding: 10px 10px;">
					<div style="width: 300px; height: 100px; display: flex">
						<div
							style="width: 100px; height: 100px; background-color: #f39c12; display: flex; flex: row; justify-content: space-around; border-radius: 4px;">
							<div
								style="display: flex; flex-direction: column; justify-content: space-around;">
								<i class="fa fa-users" style="font-size: 48px; color: red"></i>
							</div>
						</div>
						<div style="padding-left: 20px; background-color: #ffffff">
							<h6>NUMBER OF MEMBER</h6>
							<h4 style="font-weight: 700">${users}</h4>
						</div>
					</div>
					<div style="width: 300px; height: 100px; display: flex">
						<div
							style="width: 100px; height: 100px; background-color: #3c8dbc; display: flex; flex: row; justify-content: space-around; border-radius: 4px;">
							<div
								style="display: flex; flex-direction: column; justify-content: space-around;">
								<i class="fa fa-envelope-o" style="font-size: 48px; color: red"></i>
							</div>
						</div>
						<div style="padding-left: 20px; background-color: #ffffff">
							<h6>
								MESSAGE <span class="text text-danger">(Unread)</span>
							</h6>
							<h4 style="font-weight: 700">
								<span class="badge badge-light">${message}</span>
							</h4>
						</div>
					</div>
					<div style="width: 300px; height: 100px; display: flex">
						<div
							style="width: 100px; height: 100px; background-color: #00a65a; display: flex; flex: row; justify-content: space-around; border-radius: 4px;">
							<div
								style="display: flex; flex-direction: column; justify-content: space-around;">
								<i class="fa fa-hourglass-start"
									style="font-size: 48px; color: red"></i></i>
							</div>
						</div>
						<div style="padding-left: 20px; background-color: #ffffff">
							<h6>
								Order <span class="text text-danger">(Waiting for
									processing)</span>
							</h6>
							<h4 style="font-weight: 700">
								<span class="badge badge-light">${order}</span>
							</h4>
						</div>
					</div>
				</div>

				<!-- charts  -->
				<div
					style="width: 100%; display: flex; flex-direction: row; justify-content: space-around; height: 600px;">
					<!-- Bar chart -->
					<div
						style="width: 400px; height: 400px; display: flex; flex-direction: column;">
						<div>
							<canvas id="myChart" width="400" height="400"></canvas>
						</div>
						<div style="margin-left: 20px;">
							<h4>REVENUE IN CLOSEST FIVE YEARS</h4>
							<select id="bar-chart" style="width: 300px" class="form-control">
								<option value="" onclick="revenue(this)"></option>
								<option value="" onclick="revenue(this)"></option>
								<option value="" onclick="revenue(this)"></option>
								<option value="" onclick="revenue(this)"></option>
								<option value="" onclick="revenue(this)"></option>
							</select>
						</div>
					</div>

					<div
						style="width: 100%; display: flex; flex-direction: row; justify-content: space-around; height: 600px;">
						<!-- Bar chart -->
						<div
							style="width: 400px; height: 400px; display: flex; flex-direction: column;">
							<div>
								<canvas id="barChart" width="400" height="400"></canvas>
							</div>
							<div style="margin-left: 20px;">
								<h4>REVENUE OF CATEGORIES IN STOCK</h4>
							</div>
						</div>
						<!-- Pie chart category -->
						<div>
							<div style="width: 400px; height: 100%">
								<canvas id="pie-chart" width="400" height="400"></canvas>
								<form
									style="width: 400px; display: flex; flex-direction: row; justify-content: space-around;">
									<label class="radio-inline"> <input type="radio"
										name="optradio" id="fmonth" onclick="clickFollow(this)"
										value="fmonth" checked="checked">Follow month
									</label> <label class="radio-inline"> <input type="radio"
										name="optradio" id="fduration" onclick="clickFollow(this)"
										value="fduration">Follow duration
									</label>
								</form>
								<div>
									<div style="margin: 0 auto; width: 100px; display: flex;">
										<label for="year">Year</label> <input value="2019" id="year"
											width="50px;">
									</div>
									<div
										style="display: flex; flex-direction: row; justify-content: center;">
										<label for="month"> From Month</label> <select id="month">
											<option value="01"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">January</option>
											<option value="02"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">February</option>
											<option value="03"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">March</option>
											<option value="04"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">April</option>
											<option value="05"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">May</option>
											<option value="06"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">June</option>
											<option value="07"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">July</option>
											<option value="08"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">August</option>
											<option value="09"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">September</option>
											<option value="10"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">October</option>
											<option value="11"
												onclick="generateDays(this);generateToMonth(this);generatePieChart()">November</option>
											<option value="12"
												onclick="generateDays(this);generateToMonth(this)">December</option>
										</select> <label for="tmonth">To Month</label> <select id="tmonth">
											<option value="01"
												onclick="generateTDays(this);generatePieChart()">January</option>
											<option value="02"
												onclick="generateTDays(this);generatePieChart()">February</option>
											<option value="03"
												onclick="generateTDays(this);generatePieChart()">March</option>
											<option value="04"
												onclick="generateTDays(this);generatePieChart()">April</option>
											<option value="05"
												onclick="generateTDays(this);generatePieChart()">May</option>
											<option value="06"
												onclick="generateTDays(this);generatePieChart()">June</option>
											<option value="07"
												onclick="generateTDays(this);generatePieChart()">July</option>
											<option value="08"
												onclick="generateTDays(this);generatePieChart()">August</option>
											<option value="09"
												onclick="generateTDays(this);generatePieChart()">September</option>
											<option value="10"
												onclick="generateTDays(this);generatePieChart()">October</option>
											<option value="11"
												onclick="generateTDays(this);generatePieChart()">November</option>
											<option value="12" onclick="generateTDays(this)">December</option>
										</select>
									</div>
									<div
										style="display: flex; flex-direction: row; justify-content: center;">
										<label for="day">From Day</label> <select id="day"
											disabled="disabled"></select> <label for="tday">To
											Day</label> <select id="tday" disabled="disabled"></select>
									</div>
								</div>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pie-chart/1.0.0/pie-chart.js"></script>
							</div>
							<div></div>
						</div>
					</div>
				</div>
			</div>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You are not allowed to access this
				feature!!!
			</div>
		</div>
	</c:if>
</body>
</html>