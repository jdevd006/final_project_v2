<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<%-- <script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script> --%>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/cartPage.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/cartPage.css" />">
<script src="https://smtpjs.com/v3/smtp.js">
</script>
<script type="text/javascript"
	src="https://cdn.emailjs.com/sdk/2.3.2/email.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<title><spring:message code="cart.title"></spring:message></title>
</head>
<body style="background-color: #F4F4F4;"
	onload="initializeCheckOutContainer();showProductOfGuest(${currentUser.user_id});setUser(${currentUser.user_id});setNumItemCartGuest()">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i> <spring:message code="cart.home"></spring:message></a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
												code="adPro.cate"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<c:forEach items="${cartCategories}" var="category">
												<a class="dropdown-item" href="#">${category.cate_name}</a>
											</c:forEach>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i> <spring:message
											code="cart.contact"></spring:message></a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i> <spring:message
											code="cart.lang"></spring:message></a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">
										<spring:message code="cart.sign"></spring:message>
									</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="../cartController/cartPage?lang=vi"><spring:message
								code="cart.vi"></spring:message></a></li>
					<li class="nav-item"><a class="nav-link" href="../cartController/cartPage?lang=en"><spring:message
								code="cart.en"></spring:message></a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span><spring:message
							code="login.btn"></spring:message></span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a> <button class="dropdown-item"
							 onclick="logOut()"><spring:message
								code="cart.logout"></spring:message></button>
					</div>
				</div>

			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart-guest">0</span></i>
				</button>
			</c:if>


			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>
	<div id="contentContainer">
		<div id="content" class="container"
			style="display: flex; flex-direction: column; margin-top: 100px; border-bottom-color: #F7F7F7;">
			<div id="cart-title">
				<h3>
					<spring:message code="cart.h3"></spring:message>
				</h3>
				<c:if test="${currentUser != null}">
					<p style="color: #6581aa; float: left;" id="item-quantity-in-cart">${cartItems}</p>
					<p style="color: #6581aa; float: left;">
						<spring:message code="cart.d"></spring:message>
					</p>
				</c:if>
				<c:if test="${currentUser == null}">
					<p style="color: #6581aa;" id="item-quantity-in-cart"><spring:message code="cart.p"></spring:message></p>
				</c:if>
				<div id="alert-is-removing">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#exampleModal" id="open-alert-remove-button"
						style="visibility: hidden;">Launch demo modal</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1"
						role="dialog" aria-labelledby="exampleModalLabel"
						aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header" style="background-color: #FAFAFA;">
									<h5 class="modal-title" id="exampleModalLabel">
										<spring:message code="cart.warning"></spring:message>
									</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body" style="text-align: center;">
									<spring:message code="cart.quest"></spring:message>
								</div>
								<div class="modal-footer" style="background-color: #FAFAFA;">
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal" value="" id="disagreeRemoveButton">
										<spring:message code="cart.b1"></spring:message>
									</button>
									<c:if test="${currentUser != null}">
										<button type="button" class="btn btn-primary"
											onclick="agreeRemove(this)" value="" id="agreeRemoveButton"
											data-dismiss="modal">
											<spring:message code="cart.b2"></spring:message>
										</button>
									</c:if>
									<c:if test="${currentUser == null}">
										<button type="button" class="btn btn-primary" value=""
											id="agreeRemoveItemGuest" data-dismiss="modal">
											<spring:message code="cart.b2"></spring:message>
										</button>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="alrert-is-checkout">
					<!-- Modal -->
					<div class="modal fade" id="checkout" tabindex="-1" role="dialog"
						aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg"
							role="document" style="max-width: 1000px;">
							<div class="modal-content" style="height: 800px;">
								<div class="modal-header" style="background-color: #FAFAFA;">
									<h5 class="modal-title" id="exampleModalLabel"
										style="margin-left: auto;">
										<spring:message code="cart.h5"></spring:message>
									</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>


								<div class="modal-body"
									style="/* text-align: center; display: flex; flex-direction: row; */"
									id="form-checkout">
									<form action="" style="display: flex; flex-direction: row;">
										<div id="info-shipping-form"
											style="width: 310px; /*  background-color: blue;  */ height: 100%; display: flex; flex-direction: column;">
											<h3 style="margin-bottom: 20px; display: inherit;">
												<spring:message code="cart.h3"></spring:message>
											</h3>
											<div class="form-group">
												<label for="usr" hidden="hidden">:</label>
												<p class="text text-danger" id="fname"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message
														code="cart.empty"></spring:message></p>
												<input name="fname" placeholder="<spring:message
														code="cart.phd.fname"></spring:message>"
													style="width: 300px" class="form-control" id="txtFName"
													required="required">
											</div>
											<div class="form-group">
												<label for="usr" hidden="hidden">:</label>
												<p class="text text-danger" id="lname"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message
														code="cart.empty"></spring:message></p>
												<input name="lname" placeholder="<spring:message
														code="cart.phd.lname"></spring:message>"
													style="width: 300px" class="form-control" id="txtLName"
													required="required">
											</div>
											<div class="form-group">
												<label for="usr" hidden="hidden">:</label>
												<p class="text text-danger" id="email"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message
														code="cart.formatmail"></spring:message></p>
												<input name="email" placeholder="<spring:message
														code="cart.phd.email"></spring:message>" style="width: 300px"
													class="form-control" id="txtEmail" required="required">
											</div>
											<div class="form-group">
												<label for="usr" hidden="hidden">:</label>
												<p class="text text-danger" id="phone"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message
														code="cart.formatphone"></spring:message></p>
												<input name="phone" placeholder="<spring:message
														code="cart.phd.phone"></spring:message>" style="width: 300px"
													class="form-control" id="txtPhone" required="required">
											</div>

											<c:if test="${currentUser != null}">
												<div class="form-group" style="display: flex;">
													<label for="address" style="margin-top: 4px;"><spring:message
															code="cart.add"></spring:message> : </label> <input
														type="checkbox" name="address" placeholder="Address"
														style="width: 20px;" class="form-control" id="address"
														onclick="clickAddress(this)">
												</div>
											</c:if>

											<c:if test="${currentUser != null}">
												<div class="form-group" style="display: none;"
													id="available-address-container">
													<p class="text text-danger" id="available"
														style="visibility: hidden; margin-bottom: 0px;">
														<spring:message code="cart.chooseavailable"></spring:message>
													</p>
													<select id="available-address-select-tag"
														style="width: 300px" class="form-control">
														<option selected value="-1"><spring:message code="cart.sel"></spring:message></option>
														<c:forEach items="${addresses}" var="address">
															<option value="${address.city}-${address.district}">${address.city}-${address.district}</option>
														</c:forEach>
													</select>
												</div>
											</c:if>
											<div class="form-group">
												<p class="text text-danger" id="city"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message code="cart.choosecity"></spring:message></p>
												<select id="city-select-tag" style="width: 300px"
													class="form-control">
													<option selected value="-1"><spring:message
															code="cart.sel1"></spring:message></option>
												</select>
											</div>
											<div class="form-group">
												<p class="text text-danger" id="district"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message code="cart.choosedistrict"></spring:message></p>
												<select id="district-select-tag" style="width: 300px;"
													class="form-control">
													<option selected value="-1"><spring:message
															code="cart.sel2"></spring:message></option>
												</select>
											</div>
											<div class="form-group">

												<label for="usr" hidden="hidden">Name:</label>
												<p class="text text-danger" id="more"
													style="visibility: hidden; margin-bottom: 0px;"><spring:message
														code="cart.empty"></spring:message></p>
												<input name="moreDetail" placeholder="<spring:message
														code="cart.moredetail"></spring:message>"
													style="width: 300px" class="form-control"
													id="txtAddressDetail" required="required">
											</div>

										</div>
										<div style="width: 321.6px; height: 100%">
											<h3 style="text-align: center;">
												<spring:message code="cart.h31"></spring:message>
											</h3>
											<table style="margin-left: auto; margin-right: auto;">
												<tr>
													<td><img alt=""
														src="<c:url value='/resources/image/cash.png'/>"
														width="50px" height="50px"></td>
													<td>
														<div class="form-check">
															<label class="form-check-label" for="radio1"> <input
																type="radio" class="form-check-input" id="cash"
																name="optradio" value="Cash on delivery" checked>
																<spring:message code="cart.sel3"></spring:message>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td><img alt=""
														src="<c:url value='/resources/image/atm.jpg'/>"
														width="50px" height="50px"></td>
													<td>
														<div class="form-check">
															<label class="form-check-label" for="radio2"> <input
																type="radio" class="form-check-input" id="atm"
																name="optradio" value="ATM"> <spring:message
																	code="cart.atm"></spring:message>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td><img alt=""
														src="<c:url value='/resources/image/visa.png'/>"
														width="50px" height="50px"></td>
													<td>
														<div class="form-check">
															<label class="form-check-label" for="ratio3"> <input
																type="radio" class="form-check-input" id="master"
																name="optradio" value="MasterCart/Visa"> <spring:message
																	code="cart.sel4"></spring:message>
															</label>
														</div>
													</td>
												</tr>


											</table>
										</div>
										<div id="info-left" style="width: 366.167px; height: 100%;">
											<table id="info-cart-table"
												style="width: inherit; border: 1px solid #cacaca; margin-left: auto; margin-right: auto;">
											</table>
										</div>
									</form>
								</div>

								<div class="modal-body"
									style="text-align: center; display: none; flex-direction: column;"
									id="form-notify">
									<h3>
										<spring:message code="cart.h32"></spring:message>
									</h3>
								</div>

								<div class="modal-footer" style="background-color: #FAFAFA;">
									<button id="buy-button" type="button" class="btn btn-primary"
										onclick="buy(this, ${currentUser.user_id})" value=""
										id="agreeRemoveButton">
										<spring:message code="cart.b3"></spring:message>
									</button>
									<button style="display: none;" id="close-button" type="button"
										class="btn btn-primary" onclick="location.href='./cartPage'"
										value="" id="agreeRemoveButton">
										<spring:message code="cart.close">
										</spring:message>
									</button>
									<!-- data-dismiss="modal" -->
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div id="listCart">
				<table id="cart-table" style="width: 1110px;">
					<c:set var="count" value="0"></c:set>
					<c:set var="total" value="0"></c:set>
					<c:forEach items="${listItems}" var="item" begin="0">
						<tr
							style="border-bottom: 1px solid #efefef; border-top: 1px solid #efefef; height: 100px;">
							<td><img style="width: 50px; height: 50px;"
								src='<c:url value="${listImage[count].img}"/>'></td>
							<td
								style="display: flex; flex-direction: column; justify-content: space-around; height: 100px">
								<div
									style="display: flex; flex-direction: column; justify-content: space-around; height: 100px;">
									<div style="margin-top: 15px;">
										<a class="product-title"
											href="../viewProductController/product?proid=${item.product.pro_id}"
											style="text-transform: uppercase; text-decoration: none; font-weight: 700;">${item.product.pro_name}</a><br>
										<p class="product-cost" style="float: left;">${item.product.pro_cost}</p>
										<c:set var="total"
											value="${total+item.product.pro_cost*item.num}"></c:set>
										<span style="float: left;">&#8363</span>
									</div>
								</div>
							</td>
							<td>
								<div style="display: inline-flex;">
									<button class="btn btn-outline-secondary"
										onclick="descrease(${item.product.pro_id}, ${count})"
										value="${count}">
										<i class="fas fa-minus"></i>
									</button>
									<input value="${item.num}"
										style="width: 39.8333px; height: 37.8333px; background-color: #f4f4f4; text-align: center; border-radius: .2rem; border: 1px solid #6c757d;">
									<button class="btn btn-outline-secondary"
										onclick="inscrease(${item.product.pro_id}, ${count})">
										<i class="fas fa-plus"></i>
									</button>
								</div>
							</td>
							<td><button class="remove-item"
									style="border: none; background-color: #f4f4f4;"
									onclick="deleteItem(${item.product.pro_id}, ${count}, this)"
									value="${count}">
									<i class="far fa-trash-alt remove-item"
										style="cursor: pointer;"></i>
								</button></td>
						</tr>
						<c:set var="count" value="${count+1}"></c:set>
					</c:forEach>
				</table>
				<script type="text/javascript">
			function descrease(pro_id, order) {
				changeWhenDescrease(pro_id, order);
			}
			function inscrease(pro_id, order) {
				changeWhenInscrease(pro_id, order);
			}
			function deleteItem(pro_id, order, button){
				implementDeleteItem(pro_id, order, button);
				}
			</script>
			</div>
			<div id="check-out-container"
				style="display: flex; flex-direction: row; justify-content: space-between;">
				<p style="color: #6581aa; margin-top: 10px; visibility: hidden;"
					id="empty-cart-inform">
					<spring:message code="cart.p1"></spring:message>
				</p>
				<div style="display: flex; flex-direction: column;">
					<p style="color: #6581aa; margin-top: 10px; visibility: hidden;"
						id="total-container">
						<spring:message code="cart.total"></spring:message>
						<span id="total-price"
							style="font-size: 30px; font-weight: 700; color: black;">${total}</span><span
							style="font-size: 30px; font-weight: 700; color: black;">&#8363</span>
					</p>

					<button type="button" id="check-out-button" class="btn btn-dark"
						style="visibility: hidden;" data-toggle="modal"
						data-target="#checkout"
						onclick="getCities();getInfoCartForCheckOut()">
						<spring:message code="cart.check"></spring:message>
					</button>

					<button type="button" id="keep-shopping-button"
						class="btn btn-dark" style="visibility: hidden;"
						onclick="location.href='../homecontroller/homepage'">
						<spring:message code="cart.b4"></spring:message>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div id="searchResult"
		style="display: none; margin-top: 100px; margin-bottom: 80px;"
		class="container">
		<table id="searchResult-table" style="width: 100%;">
			<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
		</table>
	</div>

	<div class="container-fluid" id="footer-top" style="margin-top: auto;">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p3"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shipping"></spring:message> </a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a> <a href="#"><spring:message code="footer.term"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p4"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a> <a
						href="../contactcontroller/contact"><spring:message code="footer.contactus"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p5"></spring:message>
					</p>
					<a href="https://www.lazada.vn">Lazada</a> <a
						href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
					<a href="https://www.amazon.com/">Amazon</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p6"></spring:message>
					</p>
					<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
						href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message code="footer.cash"></spring:message></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="address"></spring:message>
								</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="infomation"></spring:message>
								</p>
							</div>
						</div>
					</div>
					<div style="background-color: gray;color: black;text-align: center;">
						<p style="margin-top: auto;margin-bottom: auto;">
							<spring:message code="footer.compyright"></spring:message>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<!-- localhost:8181/final_project/viewProductController/product?proid=1&userid=3 -->