<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Sign Up</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/formRegist.css"/>">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script async defer crossorigin="anonymous"
	src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=413826286108180&autoLogAppEvents=1"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
</head>
<body>
	<div class="container">
		<!-- Form dang nhap -->
		<h2 style="text-align: center;">
			<a style="text-decoration: none;" href="../homecontroller/homepage"><spring:message
					code="login.welcome" /></a>
		</h2>
		<div>
			<a style="text-decoration: none" href="./login?lang=vi"><img
				alt="" src="<c:url value='/resources/image
		/vn.png'></c:url>">
				VN</a> <a style="text-decoration: none" href="./login?lang=en"><img
				alt="" src="<c:url value='/resources/image
		/en.png'></c:url>">
				EN</a>
		</div>
		<fb:login-button scope="public_profile,email"
			onlogin="checkLoginState();">
		</fb:login-button>
		<div id="status"></div>
		<form action="./login" method="POST">
			<div class="row">
				<div class="col col-md-6">
					<%-- <a href="#" class="twitter btn"><spring:message
							code="login.twitter"></spring:message><i
						class="fab fa-twitter-square"></i></a> <a href="#" class="google btn"><spring:message
							code="login.google"></spring:message><i
						class="fab fa-google-plus-square"></i></a> --%>
				</div>
				<div class="col col col-md-6">
					<div class="hide-md-lg">
						<p>
							<spring:message code="login.ask"></spring:message>
						</p>
					</div>
					<input type="text" name="email" placeholder="Email" required>
					<input type="password" name="password" placeholder="Password"
						required> <input type="submit"
						value="<spring:message code="login.btn"></spring:message>">
					<ul class="other" style="list-style: none">
						<li><a href="" data-toggle="modal" data-target="#forgot"><spring:message
									code="login.ask1"></spring:message> </a></li>
						<li><a data-toggle="modal" data-target="#myModal" href=""><spring:message
									code="login.signup"></spring:message></a></li>
					</ul>
					<c:if test="${invalidMail != null}">
						<p class="text text-danger" id="dangerMail">${invalidMail}</p>
					</c:if>
					<c:if test="${emptyPass != null}">
						<p class="text text-danger" id="dangerEmptyPass">${emptyPass}</p>
					</c:if>
					<c:if test="${doesNotExist != null}">
						<p class="text text-danger" id="dangerNotExist">${doesNotExist}</p>
					</c:if>
					<c:if test="${incorrectPass != null}">
						<p class="text text-danger" id="dangerIncorrectPass">${incorrectPass}</p>
					</c:if>
					<c:if test="${isblocked != null}">
						<p class="text text-danger" id="dangerIsBlocked">${isblocked}</p>
					</c:if>
				</div>
			</div>
		</form>
		<!-- Form dang ky -->


		<!-- The Modal -->
		<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">
							<spring:message code="login.register"></spring:message>
						</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<%-- <form action="./regist" method="POST"> --%>
						<div class="container">
							<h1>
								<spring:message code="login.register"></spring:message>
							</h1>
							<p>
								<spring:message code="login.request"></spring:message>
							</p>
							<hr>

							<label for="username"><spring:message
									code="login.username"></spring:message></label>
							<p class="text text-danger"
								style="float: right; visibility: hidden;" id="invalidName"></p>
							<input type="text" placeholder="Enter username" name="username"
								required id="name"> <label for="phone"><spring:message
									code="login.phone"></spring:message></label>
							<p class="text text-danger"
								style="float: right; visibility: hidden;" id="invalidPhone">
								<spring:message code='regist.invalidPhone'></spring:message>
							</p>
							<input type="text" placeholder="Enter phone" name="phone"
								required id="phone"> <label for="email"><spring:message
									code="login.email"></spring:message></label>
							<p class="text text-danger"
								style="float: right; visibility: hidden;" id="invalidMail">
								<spring:message code='regist.invalidMail'></spring:message>
							</p>
							<input type="text" placeholder="Enter Email" name="email"
								required id="mail"> <label for="psw"><spring:message
									code="login.password"></spring:message></label>
							<p class="text text-danger"
								style="float: right; visibility: hidden;" id="invalidPass"></p>
							<input type="password" placeholder="Enter Password" name="psw"
								required id="password"> <label for="psw-repeat"><spring:message
									code="login.repassword"></spring:message></label>
							<p class="text text-danger"
								style="float: right; visibility: hidden;" id="invalidRepeatPass"></p>
							<input type="password" placeholder="Repeat Password"
								name="psw-repeat" required id="repeatPassword">
							<hr>

							<p>
								<spring:message code="login.request1"></spring:message>
							</p>
							<button type="button"
								class="registerbtn btn btn-outline-secondary"
								style="width: 100px;" id="regist-button" onclick="login()">
								<spring:message code="login.btn.regist"></spring:message>
							</button>
						</div>

						<div class="container signin">
							<p>
								<spring:message code="login.ask2"></spring:message>
								<a href="" data-toggle="modal" data-target="#myModal"><spring:message
										code="login.signin"></spring:message></a>.
							</p>
						</div>
						<%-- </form> --%>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">
							<spring:message code="login.btn.close"></spring:message>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="forgot">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h1 class="modal-title">Forget password form</h1>
						<button type="button" class="close" data-dismiss="modal">×</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<h3 style="text-align: center;">GET PASSWORD</h3>
						<div class="form-group" id="enterForgotMail">
							<div
								style="display: flex; flex-direction: row; justify-content: space-between;">
								<label for="txtForgotMail">Email :</label>
								<p class="text text-warning" id="noExistMail"
									style="visibility: hidden">Account does not exist</p>
							</div>
							<input type="text" class="form-control" id="txtForgotMail"
								placeholder="Enter your mail" name="" required>

							<div class="valid-feedback">Valid.</div>
							<div class="invalid-feedback">Please fill out this field.</div>
						</div>
						<div class="form-group" style="visibility: hidden" id="timer">
							<p class="text text-warning" id="pPassCode"></p>

							<div
								style="display: flex; flex-direction: row; justify-content: space-between;">
								<label for="txtPassCode">Passcode :</label>
								<p id="demo" class="text text-warning"></p>
							</div>
							<input type="text" class="form-control" id="txtPassCode"
								placeholder="Enter passcode" name="" required>
							<button onclick="checkPassCode()" class="btn btn-primary"
								id="btnVerify">Verify</button>
							<div class="valid-feedback">Valid.</div>
							<div class="invalid-feedback">Please fill out this field.</div>
						</div>

						<div class="form-group" id="changePass" style="visibility: hidden">
							<div
								style="display: flex; flex-direction: row; justify-content: space-between;">
								<label for="txtForgotMail">Enter new password :</label>
							</div>
							<p style="display: none" id="emptyNewPass" class="text text-danger">
								<spring:message code="regist.invalidName"></spring:message>
							</p>
							<input type="password" class="form-control" id="txtNewPass"
								placeholder="Enter new password" name="" required>

							<div
								style="display: flex; flex-direction: row; justify-content: space-between;">
								<label for="txtForgotMail">Enter password again :</label>
							</div>
							<p style="display: none" id="emptyConfirmPass" class="text text-danger">
								<spring:message code="regist.invalidName"></spring:message>
							</p>
							<input type="password" class="form-control" id="txtConfirmPass"
								placeholder="Enter password again" name="" required>
							<p style="display: none" id="match" class="text text-danger">
								<spring:message code="regist.invalidRePass"></spring:message>
							</p>
							<div class="valid-feedback">Valid.</div>
							<div class="invalid-feedback">Please fill out this field.</div>
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-primary"
							onclick="checkMail()" id="btnGet">Get</button>
						<button type="button" class="btn btn-primary"
							onclick="change()" id="btnChange" style="display: none;">Change</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var passcode;
		var custEmail;
		var email = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,5}(\.[a-z]{2,3}){1,2}$/;
		var phone = /^(03|09|08)[0-9]{8}$/;
		var empty = /\s+/g;

		function login() {
			//<spring:message code="login.ask2"></spring:message>
			var count = 0;
			//check
			if (document.getElementById("phone").value.match(phone) != null) {
				count++;
				document.getElementById("invalidPhone").style.visibility = "hidden";
			} else {
				document.getElementById("invalidPhone").style.visibility = "visible";
			}
			if (document.getElementById("mail").value.match(email) != null) {
				count++;
				document.getElementById("invalidMail").style.visibility = "hidden";
			} else {
				document.getElementById("invalidMail").style.visibility = "visible";
			}
			if (document.getElementById("password").value.match(empty) != null) {
				document.getElementById("invalidPass").innerHTML = "<spring:message code='regist.invalidRePass'></spring:message>";
				document.getElementById("invalidPass").style.visibility = "visible";
			} else {
				count++;
				document.getElementById("invalidPass").style.visibility = "hidden";
			}
			if (document.getElementById("repeatPassword").value == document
					.getElementById("password").value) {
				count++;
				document.getElementById("invalidRepeatPass").style.visibility = "hidden";
			} else {
				document.getElementById("invalidRepeatPass").innerHTML = "<spring:message code='regist.invalidRePass'></spring:message>";
				document.getElementById("invalidRepeatPass").style.visibility = "visible";
			}
			if (document.getElementById("name").value.split(empty).join("").lenght != 0) {
				count++;
				document.getElementById("invalidName").style.visibility = "hidden";
			} else {
				document.getElementById("invalidName").innerHTML = "<spring:message code='regist.invalidName'></spring:message>";
				document.getElementById("invalidName").style.visibility = "visible";
			}
			console.log(count);
			if (count == 5) {
				$
						.ajax({
							url : "./getuser?email="
									+ document.getElementById("mail").value
											.trim(),
							type : 'GET',
							data : '',
							contentType : "application/json",
							success : function(user) {
								console.log(user + "sdsdd");
								if (user != "") {
									document.getElementById("invalidMail").innerHTML = "<spring:message code='regist.invalidMail'></spring:message>";
									document.getElementById("invalidMail").style.visibility = "visible";
								} else {
									$
											.ajax({
												url : "./regist?phone="
														+ document
																.getElementById("phone").value
														+ "&email="
														+ document
																.getElementById("mail").value
														+ "&psw="
														+ document
																.getElementById("password").value
														+ "&username="
														+ document
																.getElementById("name").value,
												type : "POST",
												data : '',
												contentType : "application/json",
												success : function(data) {
													location.href = "../loginRegisterController/login";
												},
												async : false,
												error : function(e) {
													console.log(e);
												}
											});
								}
							}
						});

			}
		}

		function checkMail() {
			$
					.ajax({
						url : "./getuser?email="
								+ document.getElementById("txtForgotMail").value
										.trim(),
						type : 'GET',
						data : '',
						contentType : "application/json",
						success : function(user) {
							if (user != "") {
								custEmail = document
										.getElementById("txtForgotMail").value
										.trim();
								document.getElementById("noExistMail").style.visibility = "hidden";
								$
										.ajax({
											url : "./sendpasscode?email="
													+ document
															.getElementById("txtForgotMail").value
															.trim(),
											type : 'GET',
											data : '',
											contentType : "application/json",
											success : function(data) {
												passcode = data.str;

												document.getElementById("demo").innerHTML = "";
												document
														.getElementById("pPassCode").innerHTML = "We sent a passcode to your mail, this code will expire in about 4 minutes";
												document
														.getElementById("timer").style.visibility = "visible";

												var date = new Date();
												var y = date.getFullYear();
												var m = date.getMonth();
												var d = date.getDate();
												var h = date.getHours();
												var mi = date.getMinutes() + 4;
												var s = "0";

												switch (m) {
												case 0:
													m = "January";
													break;
												case 1:
													m = "February";
													break;
												case 2:
													m = "March";
													break;
												case 3:
													m = "April";
													break;
												case 4:
													m = "May";
													break;
												case 5:
													m = "June";
													break;
												case 6:
													m = "July";
													break;
												case 7:
													m = "August";
													break;
												case 8:
													m = "September";
													break;
												case 9:
													m = "October";
													break;
												case 10:
													m = "November";
													break;
												case 11:
													m = "December";
													break;
												}
												var countDownDate = new Date(m
														+ " " + d + ", " + y
														+ " " + h + ":" + mi
														+ ":" + s).getTime();

												// Update the count down every 1 second
												var x = setInterval(
														function() {

															var now = new Date()
																	.getTime();

															// Find the distance between now and the count down date
															var distance = countDownDate
																	- now;

															// Time calculations for days, hours, minutes and seconds

															var minutes = Math
																	.floor((distance % (1000 * 60 * 60))
																			/ (1000 * 60));
															var seconds = Math
																	.floor((distance % (1000 * 60)) / 1000);

															// Output the result in an element with id="demo"
															document
																	.getElementById("demo").innerHTML = minutes
																	+ "m "
																	+ seconds
																	+ "s ";

															// If the count down is over, write some text 
															if (distance < 0) {
																clearInterval(x);
																document
																.getElementById("demo").innerHTML = "PASSCODE IS EXPIRED, THIS PAGE WILL RELOAD IN THE NEXT 3 SECONDS";
																setTimeout(function(){
																	location.reload();
																	}, 3000);
															}
														}, 1000);
											}
										});
							} else {
								document.getElementById("noExistMail").style.visibility = "visible";
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
		}

		function checkPassCode() {
			if (document.getElementById("txtPassCode").value == passcode + "") {
				document.getElementById("timer").style.visibility = "hidden";
				document.getElementById("changePass").style.visibility = "visible";
				document.getElementById("btnVerify").style.display = "none";
				document.getElementById("btnGet").style.display = "none";
				document.getElementById("btnChange").style.display = "block";
			} else {
				location.reload();
			}
		}

		function change() {
			if (document.getElementById("txtNewPass").value.split(/\s+/)
					.join("").length != 0) {
				if (document.getElementById("txtConfirmPass").value.split(/\s+/)
						.join("").length != 0) {
					if (document.getElementById("txtNewPass").value == document
							.getElementById("txtConfirmPass").value) {
						$
								.ajax({
									url : './changepass?email='
											+ custEmail
											+ "&pass="
											+ document
													.getElementById("txtConfirmPass").value
													.trim(),
									type : 'POST',
									data : '',
									contenType : "application/json",
									success : function(data) {
										alert("Your password is changed!!!");
										location.reload();
									}
								});

					} else {
						document.getElementById("match").style.display = "block";
						document.getElementById("emptyConfirmPass").style.display = "none";
						document.getElementById("emptyNewPass").style.display = "none";
					}
				} else {
					document.getElementById("match").style.display = "none";
					document.getElementById("emptyConfirmPass").style.display = "block";
					document.getElementById("emptyNewPass").style.display = "none";
				}
			} else {
				document.getElementById("match").style.display = "none";
				document.getElementById("emptyConfirmPass").style.display = "none";
				document.getElementById("emptyNewPass").style.display = "block";
			}
		}
	</script>
</body>
</html>