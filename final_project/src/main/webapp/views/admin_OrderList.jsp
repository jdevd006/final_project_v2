<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title><spring:message code="adOr.title"></spring:message></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Account.css" />">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_OrderList.js" />"></script>
</head>
<body>
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header style="float: left;">
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<div style="width: 200px; display: flex;">
						<div style="margin: auto 0px;">
							<a style="font-size: 14px; text-decoration: none;"
								href="./orders?lang=vi"><img alt=""
								src="<c:url value="/resources/image/vn.png"/>"> VN</a> <a
								style="font-size: 14px; text-decoration: none;"
								href="./orders?lang=en"><img alt=""
								src="<c:url value="/resources/image/en.png"/>"> EN</a>
						</div>
						<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
						<div class="dropdown-menu" aria-labelledby="dropdownId">
							<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
						</div>
					</div>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout"><spring:message
								code="adPro.exit"></spring:message></a>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" style="display: flex;">
			<div class="wrapper d-flex justify content-between">
				<div id="side-bar">
					<div class="logo" style="text-align: center">
						<spring:message code="adPro.logo"></spring:message>
					</div>
					<ul class="list-group rounded-0">
						<li class="dashboard"><a
							href="../dashBoardController/dashBoard"><i
								class="fa fa-dashboard" style="font-size: 36px"></i> <spring:message
									code="adPro.db"></spring:message></a></li>
						<li><a href="../categoryController/categories"> <i
								class="fa fa-book mr-2"></i> <spring:message code="adPro.db1"></spring:message>
						</a></li>
						<li><a href="../productController/products"> <i
								class="fa fa-user mr-2"></i> <spring:message code="adPro.db2"></spring:message>
						</a></li>
						<li><a href="../userController/users"><i
								class="fa fa-cart-plus"></i> <spring:message code="adPro.db3"></spring:message></a></li>
						<li><a href="../messageController/messages"><i
								class="fa fa-tags"></i> <spring:message code="adPro.db4"></spring:message></a></li>
						<li style="background-color: steelblue;"><a
							href="../adminOrderListController/orders"> <i
								class="fa fa-cart-plus"></i> <spring:message code="adMe.db6"></spring:message></a></li>
						
					</ul>
				</div>
			</div>

			<div style="margin-top: 60px; margin-left: 1px; width: 100%">
				<div id="wrapper-content" style="width: inherit;">
					<section class="control">
						<div class="row" style="margin-left: 1px; padding: 0 5px;">
							<div class="d-flex align-items-center ">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Name of Order or order date" id="searchName"
										value="">
									<div class="input-group-prepend">
										<span class="input-group-text" id="btnTimNV"><i
											class="fa fa-search"></i></span>
									</div>
								</div>
							</div>
							<div class="align-items-center justify-content-end"
								style="display: flex; flex-direction: row; margin: auto; width: 400px;">
								<label style="width: 100px"><spring:message
										code="adPro.sort"></spring:message></label> <select id="followSelect"
									style="width: 300px;">
									<option value="finish" onclick="selectClick()"><%-- <spring:message
											code="adOr.paid"></spring:message> --%>Finish
									</option>
									<option value="processing" onclick="selectClick()">Processing</option>
									<option value="waiting" onclick="selectClick()"
										selected="selected">Waiting</option>
									<option value="canceled" onclick="selectClick()">Canceled</option>
									<option value="total" onclick="selectClick()"><spring:message
											code="adOr.total"></spring:message></option>
									<option value="id" onclick="selectClick()"><spring:message
											code="adOr.id"></spring:message></option>
								</select>
								<form id="myRadioBtn" class="d-flex align-items-center m-2"
									action="">
									<input class="m-2" type="radio" name="gender" value="asc"
										onclick="radioClick()" checked="checked" id="ascRd">
									<spring:message code="adPro.sortI"></spring:message>
									<br> <input class="m-2" type="radio" name="gender"
										value="dscRd" onclick="radioClick()" id="dscRd">
									<spring:message code="adPro.sortD"></spring:message>
									<br>
								</form>
							</div>
						</div>

						<!-- <div style="display: none;" id="test"> -->
						<h2 style="margin-left: 10px;">
							<spring:message code="adOr.orders"></spring:message>
						</h2>
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<form:form action="./order" method="post" modelAttribute="order">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">
												<spring:message code="adOr.upOr"></spring:message>
											</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<fieldset
												style="display: flex; flex-direction: column; justify-content: space-around;">
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtCustname"><spring:message
															code="adOr.cusName"></spring:message></label>

													<form:input type="text" path="name" class="form-control"
														id="txtCustname" placeholder="Customer name"
														style="width: 300px;" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtCustAddress"><spring:message
															code="adOr.address"></spring:message></label>
													<form:input type="text" placeholder="Address"
														style="width: 300px;" path="address" id="txtCustAddress"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtCustTotal"><spring:message
															code="adOr.total"></spring:message></label>
													<form:input type="text" placeholder="Total"
														style="width: 300px;" path="total" id="txtCustTotal"
														class="form-control" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<form:label class="control-label" path="status">
														<spring:message code="adOr.status"></spring:message>
													</form:label>
													<div style="width: 300px;">
														<form:radiobutton path="status" value="Waiting"
															label="Waiting" id="waitingRd" />
														<form:radiobutton path="status" value="Processing"
															label="Processing" id="processRd" />
														<form:radiobutton path="status" value="Finish"
															label="Finish" id="finishRd" />
														<form:radiobutton path="status" value="Canceled"
															label="Canceled" id="canceledRd" />

													</div>
												</div>
											</fieldset>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary"
												name="btnSaveInModal" value="" id="btnSaveInModal">
												<spring:message code="adPro.save"></spring:message>
											</button>
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">
												<spring:message code="adPro.close"></spring:message>
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
						<!-- </div> -->

						<div id="orders" class="carousel slide" data-ride="carousel"
							style="background: none;" data-interval="false">
							<!-- Indicators -->
							<c:set var="first" value="0"></c:set>
							<ul class="carousel-indicators carousel-indicators-numbers"
								style="margin-bottom: 0px;">
								<c:if test="${fn:length(orders) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#orders" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#orders" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(orders) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#orders" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#orders" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
							</ul>

							<div class="carousel-inner" style="">
								<c:set var="first" value="0"></c:set>
								<c:set var="count1" value="0"></c:set>
								<c:set var="count" value="1"></c:set>
								<c:if test="${fn:length(orders) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="orders-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adOr.code"></spring:message></th>
															<th><spring:message code="adOr.cusName"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="adOr.total"></spring:message></th>
															<th>Order date</th>
															<th><spring:message code="adOr.status"></spring:message></th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(orders)-1}">
																<tr>
																	<td>${orders[count1].bill_id}</td>
																	<td>${orders[count1].name}</td>
																	<td>${orders[count1].address}</td>
																	<td>${orders[count1].total}</td>
																	<td>${orders[count1].order_date}</td>
																	<c:if test="${orders[count1].status == 'Finish'}">
																		<td style="color: green;">${orders[count1].status}</td>
																	</c:if>
																	<c:if test="${orders[count1].status != 'Finish'}">
																		<td style="color: red;">${orders[count1].status}</td>
																	</c:if>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${orders[count1].name}"
																			onclick="update(this, ${orders[count1].bill_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${orders[count1].status == 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})"
																				disabled="disabled">Canceled</button>
																		</c:if> <c:if test="${orders[count1].status != 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})">Cancel</button>
																		</c:if>
																		<button type="button" class="btn btn-secondary"
																			onclick="more(${orders[count1].bill_id}, ${count}, this)"
																			data-toggle="modal" data-target="#moreDetail">More
																			detail</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="orders-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adOr.code"></spring:message></th>
															<th><spring:message code="adOr.cusName"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="adOr.total"></spring:message></th>
															<th>Order date</th>
															<th><spring:message code="adOr.status"></spring:message></th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(orders)-1}">
																<tr>
																	<td>${orders[count1].bill_id}</td>
																	<td>${orders[count1].name}</td>
																	<td>${orders[count1].address}</td>
																	<td>${orders[count1].total}</td>
																	<td>${orders[count1].order_date}</td>
																	<c:if test="${orders[count1].status == 'Finish'}">
																		<td style="color: green;">${orders[count1].status}</td>
																	</c:if>
																	<c:if test="${orders[count1].status != 'Finish'}">
																		<td style="color: red;">${orders[count1].status}</td>
																	</c:if>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${orders[count1].name}"
																			onclick="update(this, ${orders[count1].bill_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${orders[count1].status == 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})"
																				disabled="disabled">Canceled</button>
																		</c:if> <c:if test="${orders[count1].status != 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})">Cancel</button>
																		</c:if>
																		<button type="button" class="btn btn-secondary"
																			onclick="more(${orders[count1].bill_id}, ${count}, this)"
																			data-toggle="modal" data-target="#moreDetail">More
																			detail</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(orders) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="orders-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adOr.code"></spring:message></th>
															<th><spring:message code="adOr.cusName"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="adOr.total"></spring:message></th>
															<th>Order date</th>
															<th><spring:message code="adOr.status"></spring:message></th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(orders)-1}">
																<tr>
																	<td>${orders[count1].bill_id}</td>
																	<td>${orders[count1].name}</td>
																	<td>${orders[count1].address}</td>
																	<td>${orders[count1].total}</td>
																	<td>${orders[count1].order_date}</td>
																	<c:if test="${orders[count1].status == 'Finish'}">
																		<td style="color: green;">${orders[count1].status}</td>
																	</c:if>
																	<c:if test="${orders[count1].status != 'Finish'}">
																		<td style="color: red;">${orders[count1].status}</td>
																	</c:if>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${orders[count1].name}"
																			onclick="update(this, ${orders[count1].bill_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${orders[count1].status == 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})"
																				disabled="disabled">Canceled</button>
																		</c:if> <c:if test="${orders[count1].status != 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})">Cancel</button>
																		</c:if>
																		<button type="button" class="btn btn-secondary"
																			onclick="more(${orders[count1].bill_id}, ${count}, this)"
																			data-toggle="modal" data-target="#moreDetail">More
																			detail</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="orders-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adOr.code"></spring:message></th>
															<th><spring:message code="adOr.cusName"></spring:message></th>
															<th><spring:message code="adOr.address"></spring:message></th>
															<th><spring:message code="adOr.total"></spring:message></th>
															<th>Order date</th>
															<th><spring:message code="adOr.status"></spring:message></th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(orders)-1}">
																<tr>
																	<td>${orders[count1].bill_id}</td>
																	<td>${orders[count1].name}</td>
																	<td>${orders[count1].address}</td>
																	<td>${orders[count1].total}</td>
																	<td>${orders[count1].order_date}</td>
																	<c:if test="${orders[count1].status == 'Finish'}">
																		<td style="color: green;">${orders[count1].status}</td>
																	</c:if>
																	<c:if test="${orders[count1].status != 'Finish'}">
																		<td style="color: red;">${orders[count1].status}</td>
																	</c:if>
																	<td><button type="button" class="btn btn-primary"
																			data-toggle="modal" data-target="#myModal"
																			value="${orders[count1].name}"
																			onclick="update(this, ${orders[count1].bill_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if test="${orders[count1].status == 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})"
																				disabled="disabled">Canceled</button>
																		</c:if> <c:if test="${orders[count1].status != 'Canceled'}">
																			<button type="button" class="btn btn-warning"
																				onclick="cancel(${orders[count1].bill_id}, ${count}, this, ${first})">Cancel</button>
																		</c:if>
																		<button type="button" class="btn btn-secondary"
																			onclick="more(${orders[count1].bill_id}, ${count}, this)"
																			data-toggle="modal" data-target="#moreDetail">More
																			detail</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
							</div>
						</div>

						<div id="searchResult" style="display: none;">
							<table class="table table-bordered" style="width: 100%;"
								id="userSearchTable">
								<thead>
									<tr>
										<th><spring:message code="adOr.code"></spring:message></th>
										<th><spring:message code="adOr.cusName"></spring:message></th>
										<th><spring:message code="adOr.address"></spring:message></th>
										<th><spring:message code="adOr.total"></spring:message></th>
										<th>Order date</th>
										<th><spring:message code="adOr.status"></spring:message></th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		</div>

		<!-- Modal for show bill detail -->
		<div class="modal fade" id="moreDetail">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content" style="width: 1000px">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Bill Detail</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<table id="more" class="table table-dark">
							<thead>
								<tr>
									<th>Product name</th>
									<th>Product cost</th>
									<th>Product quantity</th>
									<th>Total</th>
								</tr>
							</thead>
						</table>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>


		<script>
		function deleteOrder(button, orderId, orderTable) {
			$.ajax({
				url : "./order/" + orderId,
				type : "DELETE",
				data : "",
				contentType : "application/json",
				success : function() {
					var table = document.getElementById("orders-table"+orderTable);
					table.deleteRow(parseInt(button.value));
					//
					var tr = table.getElementsByTagName("tr");
					for (var i = 1; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[1].value = i;
					}
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
		}
		function update(button, orderId) {
			$
					.ajax({
						url : "./order?orderId=" + orderId,
						type : "GET",
						data : "",
						contentType : "application/json",
						success : function(data) {
							document.getElementById("btnSaveInModal").value = orderId;

							document.getElementById("txtCustname").value = data.name;
							document.getElementById("txtCustAddress").value = data.address;
							document.getElementById("txtCustTotal").value = data.total;
							console.log(data.status);
							if (data.status == "Waiting")
								document.getElementById("waitingRd").checked = true;
							if (data.status == "Finish")
								document.getElementById("finishRd").checked = true;
							if (data.status == "Processing")
								document.getElementById("processRd").checked = true;
							if (data.status == "Canceled")
								document.getElementById("canceledRd").checked = true;

						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
		}

		function cancel(billId, order, button, orderTable) {
			$
					.ajax({
						url : './cancel/' + billId,
						type : 'POST',
						data : '',
						contentType : "application/json",
						success : function(data) {
							var cateTable = document
									.getElementById("orders-table"+orderTable);
							cateTable.getElementsByTagName("tr")[order]
									.getElementsByTagName("td")[5].innerHTML = "Canceled";
							cateTable.getElementsByTagName("tr")[order]
							.getElementsByTagName("td")[5].style.color = "red";
							button.innerHTML = "Canceled";
							button.disabled = true;
						}
					});
		}

		function more(billId, order, button) {
			var cost;
			$
					.ajax({
						url : "./more/" + billId,
						type : "GET",
						data : "",
						contentType : "application/json",
						success : function(data) {
							var detailTable = document.getElementById("more");
							for (var i = detailTable.getElementsByTagName("tr").length - 1; i > 0; i--)
								detailTable.deleteRow(i);
							var tr;
							var td;
							for (var i = 0; i < data.length; i++) {
								tr = document.createElement("tr");
								td = document.createElement("td");
								td.innerHTML = data[i].product.pro_name;
								tr.appendChild(td);
								td = document.createElement("td");
								td.innerHTML = data[i].product.pro_cost;
								tr.appendChild(td);
								td = document.createElement("td");
								td.innerHTML = data[i].detail.num;
								tr.appendChild(td);
								td = document.createElement("td");
								td.innerHTML = data[i].detail.num
										* data[i].product.pro_cost;
								tr.appendChild(td);
								detailTable.appendChild(tr);
							}
						}
					});
		}
	</script>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You are not allowed to access this
				feature!!!
			</div>
		</div>
	</c:if>
</body>
</html>