<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/cartPage.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/cartPage.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<script type="text/javascript">
</script>
<style type="text/css">
</style>
<title>Order List</title>
</head>
<body style="background-color: #F4F4F4;"
	onload=";setUser(${currentUser.user_id});setNumItemCartGuest()">
	<div id="header" style="">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
												code="adPro.cate"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<c:forEach items="${viewCategories}" var="category">
												<a class="dropdown-item" href="#">${category.cate_name}</a>
											</c:forEach>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i> <spring:message
											code="cart.contact"></spring:message></a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i> <spring:message
											code="cart.lang"></spring:message></a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">
										<spring:message code="cart.sign"></spring:message>
									</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link"
						href="../orderListController/orderList?lang=vi">VN</a></li>
					<li class="nav-item"><a class="nav-link"
						href="../orderListController/orderList?lang=en">EN</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">

					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>

			<c:if test="${currentUser == null}">
				<button onclick="location.href='../loginRegisterController/login';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${currentUser.user_name}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="../orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a>
						<button class="dropdown-item" onclick="logOut()">
							<spring:message code="cart.logout"></spring:message>
						</button>
					</div>
				</div>

			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart-guest">0</span></i>
				</button>
			</c:if>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="contentContainer">
		<div id="content" class="container"
			style="display: flex; flex-direction: column; margin-top: 100px; border-bottom-color: #F7F7F7;">

			<div id="accordion">
				<table class="table table-hover" align="center"
					style="width: 1068.17px;; text-align: center;">
					<tr>
						<th style="width: 134.017px;"><spring:message
								code="order.billcode"></spring:message></th>
						<th style="width: 349.67px;"><spring:message
								code="order.orderdate"></spring:message></th>
						<th style="width: 366.533px;"><spring:message
								code="order.item"></spring:message></th>
						<th style=""><spring:message code="order.total"></spring:message></th>
					</tr>
				</table>
				<c:if test="${fn:length(bills) > 0}">
					<c:set var="count" value="0"></c:set>
					<c:forEach items="${bills}" var="bill">
						<div class="card">
							<div class="card-header" id="header${bill.bill_id}">
								<table class="table table-hover" data-toggle="collapse"
									href="#collapseOne${bill.bill_id}">
									<tr style="text-align: center;">

										<th>${bill.bill_id}</th>
										<th>${bill.order_date}</th>
										<th>${fn:substring(items[count].product.pro_name, 0, 10)}</th>
										<th>${bill.total}</th>
									</tr>
								</table>
							</div>
							<div id="collapseOne${bill.bill_id}" class="collapse show"
								data-parent="#accordion">
								<div class="card-body">
									<table class="table table-hover" style="text-align: center;">
										<tr>
											<th>Product</th>
											<th>Price</th>
											<th>Quantity</th>
											<th>Price x Quantity</th>
										</tr>
										<c:forEach items="${billDetails}" var="billDetails">
											<c:set var="trOrder" value="1"></c:set>
											<c:if test="${bill.bill_id == billDetails.id.bill_id}">
												<tr>
													<td><img style="width: 50px; height: 50px;"
														src='<c:url value="${imgs[count].img}"/>'> | <a
														href="../viewProductController/product?proid=${items[count].product.pro_id}">${fn:substring(items[count].product.pro_name, 0, 10)}</a></td>
													<td>${items[count].product.pro_cost}</td>
													<td>${items[count].num}</td>
													<td>${items[count].product.pro_cost * items[count].num}</td>
												</tr>
												<c:set var="trOrder" value="${trOrder+1}"></c:set>
												<c:set var="count" value="${count+1}"></c:set>
											</c:if>
										</c:forEach>
									</table>
									<c:if test="${bill.status == 'Canceled'}">
										<button style="margin-left: 50%;" type="button"
											class="btn btn-danger" onclick="cancel(${bill.bill_id},this)"
											value="${bill.due_date}" disabled="disabled">Canceled</button>
									</c:if>
									<c:if test="${bill.status == 'Finish'}">
										<button style="margin-left: 50%;" type="button"
											class="btn btn-success" value="${bill.due_date}"
											disabled="disabled">${bill.status}</button>
									</c:if>
									<c:if test="${bill.status == 'Waiting'}">
										<button style="margin-left: 50%;" type="button"
											id="btnWaiting${bill.bill_id}" class="btn btn-danger"
											onclick="cancel(${bill.bill_id},this)"
											value="${bill.due_date}">${bill.status}</button>
									</c:if>
									<c:if test="${bill.status == 'Processing'}">
										<button style="margin-left: 50%;" type="button"
											class="btn btn-warning" data-toggle="modal"
											data-target="#myModalProcesing">${bill.status}</button>
									</c:if>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:if>
			</div>
			<button type="button" onclick="test()">Test</button>
			<script type="text/javascript">
			function test(){
				$("#myModalProccesing").modal('show');
				}
			</script>
		</div>

		<!-- Modal Time is expired -->
		<div class="modal fade" id="myModalExpired">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title" style="text-align: center;">Cancel
							order</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">Time expired</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">
							<spring:message code="login.btn.close" />
						</button>
					</div>

				</div>
			</div>
		</div>
		<!--  -->

		<!-- Modal confirm cancel -->
		<div class="modal fade" id="myModalCancel">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title" style="text-align: center;">Cancel
							order</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">Do you really want to cancel this
						order?</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							id="btnAgreeCancel" onclick="agreeCancel()" data-dismiss="modal">
							<spring:message code="cart.b2" />
						</button>
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">
							<spring:message code="login.btn.close" />
						</button>
					</div>

				</div>
			</div>
		</div>
		<!--  -->

		<!-- Modal cancel a processing order -->
		<div class="modal fade" id="myModalProcesing">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title" style="text-align: center;">Cancel
							order</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">You cannot cancel because this order
						is being delivering!!!</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">
							<spring:message code="login.btn.close" />
						</button>
					</div>

				</div>
			</div>
		</div>
		<!--  -->
	</div>
	<!-- ket thuc content -->
	<div id="searchResult"
		style="display: none; margin-top: 100px; margin-bottom: 80px;"
		class="container">
		<table id="searchResult-table" style="width: 100%;">
			<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
		</table>
	</div>

	<script type="text/javascript">
	function cancel(billId, button){
		var currentDate = new Date(new Date().getFullYear() + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()+"");
		var dueDate = new Date(button.value+"");
		console.log(dueDate);
		if (currentDate <= dueDate){
			$("#myModalCancel").modal('show');
			$("#btnAgreeCancel").val(billId);
			/* var isCancel = confirm("Do you really want to cancel this order?"); */
			/* if (isCancel)
				document.getElementById("header"+billId).style.display = "none";
				$.ajax({
					url : "./order/"+billId,
					type : "DELETE",
					data : '',
					contentType : "application/json",
					success : function (data){
						alert("Successed!!!");
						}
					});*/
			}
		else $("#myModalExpired").modal('show');
		}

	function agreeCancel(){
		$.ajax({
			url : "./order/"+$("#btnAgreeCancel").val(),
			type : "POST",
			data : '',
			contentType : "application/json",
			success : function (data){
				alert("Successed!!!");
				document.getElementById("btnWaiting"+$("#btnAgreeCancel").val()).innerHTML = "Canceled";
				document.getElementById("btnWaiting"+$("#btnAgreeCancel").val()).disabled = "true";
				}
			});
		}
	</script>
</body>
</html>