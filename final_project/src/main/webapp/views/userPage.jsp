<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css">
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/profile.js" />"></script>
</head>
<body
	onload="getAnswer();setUser(${currentUser.user_id});setNumItemCartGuest();getCities()">
	<c:if test="${currentUser != null}">
		<div id="header">
			<nav
				class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
				id="navbar">
				<div>
					<nav
						class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
						<a class="navbar-brand" href="../homecontroller/homepage"><i
							class="fa fa-store"></i>TATALO</a>
						<div class="collapse navbar-collapse" id="collapsibleNavbar">
							<div class="narbar-brand">
								<ul class="navbar-nav">
									<li class="nav-item"><a class="nav-link"
										href="../homecontroller/homepage"><i
											class="fa fa-fw fa-home"></i> <spring:message
												code="cart.home"></spring:message></a></li>
									<li class="nav-item">
										<div class="dropdown"
											style="background-color: #F8F9FA; border: none;">
											<a class="nav-link dropdown-toggle" data-toggle="dropdown"
												id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
													code="adPro.cate"></spring:message></a>
											<div class="dropdown-menu" style="background-color: inherit;">
												<c:forEach items="${cartCategories}" var="category">
													<a class="dropdown-item" href="#">${category.cate_name}</a>
												</c:forEach>
											</div>
										</div>
									</li>
									<li class="nav-item"><a class="nav-link"
										href="../contactcontroller/contact"><i
											class="fa fa-fw fa-envelope"></i> <spring:message
												code="cart.contact"></spring:message></a></li>
									<li class="nav-item"><a class="nav-link navbar-toggler"
										href="#" data-toggle="collapse" data-target="#languages"
										style="border: none;" id="lang"><i
											class="fas fa-fw fa-globe-europe"></i> <spring:message
												code="cart.lang"></spring:message></a></li>
									<li class="nav-item navbar-toggler" style="border: none;"><button
											class="btn" id="signIn" type="button"
											style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">
											<spring:message code="cart.sign"></spring:message>
										</button></li>
								</ul>
							</div>
						</div>
					</nav>
				</div>

				<div class="collapse navbar-collapse justify-content-end"
					id="languages">
					<ul class="navbar-nav ">
						<li class="nav-item"><a class="nav-link"
							href="../profileController/profile?lang=vi"><spring:message
									code="cart.vi"></spring:message></a></li>
						<li class="nav-item"><a class="nav-link"
							href="../profileController/profile?lang=en"><spring:message
									code="cart.en"></spring:message></a></li>
					</ul>
					<form class="form-inline" action="/action_page.php">

						<input class="form-control mr-sm-2" type="text"
							placeholder="Search"
							style="background-color: inherit; display: none;" id="searchBox">
						<button class="btn btn-success" type="submit"
							style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
							id="btnSearch">
							<i class="fas fa-search"></i>
						</button>
					</form>

				</div>

				<c:if test="${currentUser == null}">
					<button
						onclick="location.href='../viewProductController/movetologin';"
						class="btn" id="signInOut" type="button"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i> <span><spring:message
								code="login.btn"></spring:message></span>
					</button>
				</c:if>
				<c:if test="${currentUser != null}">
					<div class="dropdown">
						<button class="btn" id="signInOut" type="button"
							data-toggle="dropdown"
							style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
							<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
						</button>
						<div class="dropdown-menu">
							<a class="dropdown-item"
								href="/final_project/orderListController/orderList"><spring:message
									code="view.my"></spring:message></a> <a class="dropdown-item"
								href="../profileController/profile"><spring:message
									code="view.ac"></spring:message></a>
							<button class="dropdown-item" onclick="logOut()">
								<spring:message code="cart.logout"></spring:message>
							</button>
						</div>
					</div>

				</c:if>

				<c:if test="${currentUser != null}">
					<button class="btn" id="btnCart" type="button" value="${user_id}"
						onclick="location.href='../cartController/cartPage'"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
						<i class="fas fa-shopping-cart"><span
							class="badge badge-light"
							style="background-color: yellow; margin-left: 5px;"
							id="num-item-in-cart">${cartItems}</span></i>
					</button>
				</c:if>
				<c:if test="${currentUser == null}">
					<button class="btn" id="btnCart" type="button" value="${user_id}"
						onclick="location.href='../cartController/cartPage'"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
						<i class="fas fa-shopping-cart"><span
							class="badge badge-light"
							style="background-color: yellow; margin-left: 5px;"
							id="num-item-in-cart-guest">0</span></i>
					</button>
				</c:if>


				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#collapsibleNavbar">
					<span class="navbar-toggler-icon"></span>
				</button>
			</nav>
		</div>

		<div id="contentContainer">
			<div id="content"
				style="margin-bottom: 50px; margin-top: 80px; margin-left: 100px;">
				<h3>
					<spring:message code="profile.title"></spring:message>
				</h3>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link active"
						data-toggle="tab" href="#menu1"><spring:message
								code="profile.shipping"></spring:message></a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab"
						href="#menu2"><spring:message code="profile.detail"></spring:message></a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab"
						href="#menu3"><spring:message code="profile.message"></spring:message></a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane container active" id="menu1">
						<div
							style="width: inherit; display: flex; flex-direction: column;">
							<button type="button" class="btn btn-outline-primary"
								onclick="setTextForAdd()" data-toggle="modal"
								data-target="#myModal" style="margin-top: 10px;">
								<spring:message code="profile.addshipping"></spring:message>
							</button>
							<c:forEach items="${addresses}" var="address">
								<div
									style="width: 300px; height: 100px; border: 2px solid #25c5df; margin-top: 10px;"
									id="address${address.id}">
									<div style="margin: 20px 20px;">
										<div
											style="display: flex; flex-direction: row; justify-content: space-between;">
											<p id="name${address.id}" style="font-weight: 600">${address.name}</p>
											<i class="material-icons" id="edit" style="cursor: pointer;"
												onclick="setTextForEdit(${address.id})" data-toggle="modal"
												data-target="#myModal">mode_edit</i>
										</div>
										<div
											style="display: flex; flex-direction: row; justify-content: space-between;">
											<p style="font-weight: 400">
												<span id="district${address.id}">${address.district}</span>,
												<span id="city${address.id}">${address.city}</span>
											</p>
											<i class="material-icons" id="delete"
												style="cursor: pointer;"
												onclick="deleteAddress(${address.id})">delete</i>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>

					</div>
					<div class="tab-pane container fade" id="menu2">
						<div style="margin-top: 10px;">
							<form>
								<p class="text text-danger" style="display: none"
									id="invalidName">
									<spring:message code="regist.invalidName"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.name"></spring:message></span>
									</div>
									<input type="text" class="form-control" placeholder="Name"
										id="txtUsername" value="${currentUser.user_name}"
										required="required">
								</div>
								<p class="text text-danger" style="display: none" id="emptyMail">
									<spring:message code="regist.invalidName"></spring:message>
								</p>
								<p class="text text-danger" style="display: none"
									id="invalidMail">
									<spring:message code="cart.formatmail"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.mail"></spring:message></span>
									</div>
									<input type="text" class="form-control" placeholder="Name"
										id="txtEmail" value="${currentUser.email}" required="required">
								</div>
								<p class="text text-danger" style="display: none"
									id="emptyPhone">
									<spring:message code="regist.invalidName"></spring:message>
								</p>
								<p class="text text-danger" style="display: none"
									id="invalidPhone">
									<spring:message code="cart.formatphone"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.phone"></spring:message></span>
									</div>
									<input type="text" class="form-control" placeholder="Phone"
										id="txtPhone" value="${currentUser.phone}" required="required">
								</div>
								<p class="text text-danger" style="display: none"
									id="invalidCity">
									<spring:message code="cart.choosecity"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.city"></spring:message></span>
									</div>
									<div class="input-group-prepend">
										<span class="input-group-text" id="city">${currentUser.address.city}</span>
									</div>
									<select id="city-select-tag" style="width: 300px"
										class="form-control">
										<option selected value="-1"><spring:message
												code="cart.sel1"></spring:message></option>
									</select>
								</div>
								<p class="text text-danger" style="display: none"
									id="invalidDistrict">
									<spring:message code="cart.choosedistrict"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.district"></spring:message></span>
									</div>
									<div class="input-group-prepend">
										<span class="input-group-text" id="district">${currentUser.address.district}</span>
									</div>
									<select id="district-select-tag" style="width: 300px;"
										class="form-control">
										<option selected value="-1"><spring:message
												code="cart.sel2"></spring:message></option>
									</select>
								</div>
								<p class="text text-danger" style="display: none"
									id="invalidMore">
									<spring:message code="regist.invalidName"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.more"></spring:message></span>
									</div>
									<input type="text" class="form-control"
										placeholder="More detail" id="txtMore"
										value="${currentUser.address.street}" required="required">
								</div>
								<p class="text text-danger" style="display: none"
									id="invalidPass">
									<spring:message code="regist.invalidName"></spring:message>
								</p>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><spring:message
												code="profile.pass"></spring:message></span>
									</div>
									<input type="password" class="form-control" placeholder="Name"
										id="txtPassword" value="${pws}"
										required="required">
								</div>
								<input type="button" class="form-control" placeholder="Name"
									style="background-color: black; color: white; font-size: 30px"
									id="txtUpdate"
									value='<spring:message code="profile.update"></spring:message>'
									onmouseenter="this.style.backgroundColor = '#1cb5fc'"
									onmouseout="this.style.backgroundColor = 'black'"
									onclick="clickUpdateProfile()">
							</form>
						</div>
					</div>
					<div class="tab-pane container fade" id="menu3">
						<div class="container">
							<c:forEach items="${messages}" var="m">
								<div
									style="border: 2px solid #25c5df; width: 300px; height: 100px; margin-bottom: 5px; display: flex; flex-direction: column; justify-content: space-around;">
									<div style="display: flex; flex-direction: row">
										<span style="font-weight: 500">Question : </span>
										<p>${m.question}</p>
									</div>
									<div style="display: flex; flex-direction: row">
										<span style="font-weight: 500">Answer : </span>
										<p>${m.answer}</p>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>

				<!-- The Modal -->
				<div class="modal fade" id="myModal">
					<div class="modal-dialog modal-dialog-centered">
						<div class="modal-content">

							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title">
									<spring:message code="profile.shippingaddress"></spring:message>
								</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<!-- Modal body -->
							<div class="modal-body">
								<form>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><spring:message
													code="profile.nameadd"></spring:message></span>
										</div>
										<input type="text" class="form-control" placeholder="Name"
											id="txtName">
									</div>
								</form>
								<form>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><spring:message
													code="profile.addressadd"></spring:message></span>
										</div>
										<input type="text" class="form-control" placeholder="City"
											id="txtCity"> <input type="text" class="form-control"
											placeholder="District" id="txtDistrict">
									</div>
								</form>
							</div>

							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal" id="save" onclick="clickSaveButton()">Save</button>
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="searchResult"
			style="display: none; margin-top: 100px; margin-bottom: 80px;"
			class="container">
			<table id="searchResult-table" style="width: 100%;">
				<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
			</table>
		</div>

		<div class="container-fluid" id="footer-top" style="margin-top: auto;">
			<div class="row" style="display: flex; flex-direction: column;">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
					id="footer-links">
					<div class="links-in-footer">
						<p class="footer-title">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<a href="#"><spring:message code="footer.shipping"></spring:message>
						</a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a>
						<a href="#"><spring:message code="footer.term"></spring:message></a>
					</div>
					<div class="links-in-footer">
						<p class="footer-title">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a>
						<a href="../contactcontroller/contact"><spring:message
								code="footer.contactus"></spring:message></a>
					</div>
					<div class="links-in-footer">
						<p class="footer-title">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<a href="https://www.lazada.vn">Lazada</a> <a
							href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
						<a href="https://www.amazon.com/">Amazon</a>
					</div>
					<div class="links-in-footer">
						<p class="footer-title">
							<spring:message code="cart.p6"></spring:message>
						</p>
						<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
							href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message
								code="footer.cash"></spring:message></a>
					</div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
					id="footer-links-collapse">
					<div id="accordion">
						<div>
							<p class="footer-title" data-toggle="collapse"
								href="#collapseOne">
								<spring:message code="cart.p3"></spring:message>
							</p>
							<div id="collapseOne" class="collapse show"
								data-parent="#accordion">
								<div class="links-in-footer">
									<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
										href="#">d</a>
								</div>
							</div>
						</div>
						<div>
							<p class="footer-title" data-toggle="collapse"
								href="#collapseTwo">
								<spring:message code="cart.p4"></spring:message>
							</p>
							<div id="collapseTwo" class="collapse show"
								data-parent="#accordion">
								<hr>
								<div class="links-in-footer">
									<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
										href="#">d</a>
								</div>
							</div>
						</div>
						<div>
							<p class="footer-title" data-toggle="collapse"
								href="#collapseThree">
								<spring:message code="cart.p5"></spring:message>
							</p>
							<div id="collapseThree" class="collapse show"
								data-parent="#accordion">
								<hr>
								<div class="links-in-footer">
									<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
										href="#">d</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
					<div class="container" id="footer-bottom">
						<div class="footer-bottom-top">
							<div>
								<p>
									<i class="fa fa-store" style="font-size: 50px;"></i>
								</p>
								<br>
								<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
							</div>

							<div class="footer-bottom-top-right">
								<button type="button" id="btnFace">
									<i class="fab fa-facebook-f"></i>
								</button>
								<button type="button" id="btnIns">
									<i class="fab fa-instagram"></i>
								</button>
								<button type="button" id="btnYou">
									<i class="fab fa-youtube"></i>
								</button>
							</div>
						</div>
						<div class="footer-bottom-bottom">
							<div
								style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
								<div class="contact">
									<p style="margin-bottom: 2px;">Website:
										www.fashionstar.company</p>
									<p style="margin-bottom: 2px;">
										<spring:message code="address"></spring:message>
									</p>
									<p style="margin-bottom: 2px;">
										<spring:message code="infomation"></spring:message>
									</p>
								</div>
							</div>
						</div>
						<div
							style="background-color: gray; color: black; text-align: center;">
							<p style="margin-top: auto; margin-bottom: auto;">
								<spring:message code="footer.compyright"></spring:message>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
		function deleteAddress(addressId){
			$.ajax({
				url : "./address/"+addressId,
				type : "DELETE",
				data : '',
				contentType : "application/json",
				success : function (data){
					document.getElementById("address"+addressId).style.display = "none";
					}
				})
			}

		function setTextForEdit(addressId){
			document.getElementById("txtName").value = "";
			document.getElementById("txtCity").value = "";
			document.getElementById("txtDistrict").value = "";
			
			document.getElementById("txtName").value = document.getElementById("name"+addressId).innerHTML;
			document.getElementById("txtCity").value = document.getElementById("city"+addressId).innerHTML;
			document.getElementById("txtDistrict").value = document.getElementById("district"+addressId).innerHTML;
			document.getElementById("save").value = "task=update&data="+addressId;
			}

		function setTextForAdd(){
			document.getElementById("txtName").value = "";
			document.getElementById("txtCity").value = "";
			document.getElementById("txtDistrict").value = "";
			
			document.getElementById("save").value = "task=add&data=";
			}

		function clickSaveButton(){
			if (document.getElementById("save").value.substring(document.getElementById("save").value.length-1, document.getElementById("save").value.length) != "="){
				document.getElementById("save").value = document.getElementById("save").value + "," + document.getElementById("txtName").value +"," + document.getElementById("txtCity").value + "," + document.getElementById("txtDistrict").value;
				}else {
					document.getElementById("save").value = document.getElementById("save").value+document.getElementById("txtName").value +"," + document.getElementById("txtCity").value + "," + document.getElementById("txtDistrict").value;
					}
			console.log(document.getElementById("save").value);
			$.ajax({
				url : "./address?"+document.getElementById("save").value,
				type : 'POST',
				data : '',
				contentType : "application/json",
				success : function (data){
					location.reload();
					}
				}); 
			}



		function getAnswer(){
			$.ajax({
				url : "./messages",
				type : "GET",
				data : "",
				contentType : "application/json",
				success : function (data){
					var t = 0
						setInterval(
								function (){
									if (t <= data.length-1)
										$.toast({
										    text: "<strong style='font-size:20px;'>"+data[t].question+'</strong>'+'<br>'
										    +'<i>'+data[t++].answer+'</i>',
										    heading: 'New message', 
										    icon: 'info', 
										    showHideTransition: 'fade', 
										    allowToastClose: true, 
										    hideAfter: 5000,
										    stack: 5, 
										    position: 'bottom-left', 
										    textAlign: 'left',  
										    loader: true,  
										    loaderBg: '#1cb5fc',  
										    bgColor : '#80FFFF',
										    textColor : 'black',
										    beforeShow: function () {}, 
										    afterShown: function () {}, 
										    beforeHide: function () {}, 
										    afterHidden: function () {},
										    onclick : function(){
											    alert(1);
											    }
										});
									}
						, 1000);
					}
				});
			} 

		function assignDis(option){
			document.getElementById("district").innerHTML = option.innerHTML;
			}
		</script>
	</c:if>
	<c:if test="${currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You have not logged in yet!!!<br> <strong><a
					href="../loginRegisterController/login"
					style="text-decoration: none;">Log in now</a></strong>
			</div>
		</div>
	</c:if>
</body>
</html>