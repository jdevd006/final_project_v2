<%@page import="com.jdev006.dao.ProductDAO"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.jdev006.entitties.Product"%>
<%@page import="java.util.List"%>
<%@page import="com.jdev006.service.Product_service"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Product Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Product.css" />">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_Product.js" />"></script>
</head>
<body onload="">
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header style="float: left;">
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<div style="width: 200px; display: flex;">
						<div style="margin: auto 0px;">
							<a style="font-size: 14px; text-decoration: none;"
								href="./products?lang=vi"><img alt=""
								src="<c:url value="/resources/image/vn.png"/>"> VN</a> <a
								style="font-size: 14px; text-decoration: none;"
								href="./products?lang=en"><img alt=""
								src="<c:url value="/resources/image/en.png"/>"> EN</a>
						</div>
						<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
						<div class="dropdown-menu" aria-labelledby="dropdownId">
							<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
						</div>
					</div>

					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout"><spring:message
								code="adPro.exit"></spring:message></a>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" style="display: flex;">
			<div class="wrapper d-flex justify content-between">
				<div id="side-bar">
					<div class="logo" style="text-align: center">
						<spring:message code="adPro.logo"></spring:message>
					</div>
					<ul class="list-group rounded-0">
						<li class="dashboard"><a
							href="../dashBoardController/dashBoard"><i
								class="fa fa-dashboard" style="font-size: 36px"></i> <spring:message
									code="adPro.db"></spring:message></a></li>
						<li><a href="../categoryController/categories"> <i
								class="fa fa-book mr-2"></i> <spring:message code="adPro.db1"></spring:message>
						</a></li>
						<li style="background-color: steelblue;"><a
							href="../productController/products"> <i
								class="fa fa-user mr-2"></i> <spring:message code="adPro.db2"></spring:message>
						</a></li>
						<li><a href="../userController/users"><i
								class="fa fa-cart-plus"></i> <spring:message code="adPro.db3"></spring:message></a></li>
						<li><a href="../messageController/messages"><i
								class="fa fa-tags"></i> <spring:message code="adPro.db4"></spring:message></a></li>
						<li><a href="../adminOrderListController/orders"> <i
								class="fa fa-cart-plus"></i> <spring:message code="adMe.db6"></spring:message></a></li>
						
					</ul>
				</div>
			</div>

			<div style="margin-top: 60px; width: 100%; margin-left: 10px;">
				<div id="wrapper-content" style="margin-left: 10px;">
					<section class="control">
						<div class="row" style="margin-left: 1px; padding: 0 5px;">
							<div class="d-flex align-items-center ">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="<spring:message code ="adPro.phd.name" ></spring:message>"
										id="searchName" value="">
									<div class="input-group-prepend">
										<span class="input-group-text" id="btnTimNV"><i
											class="fa fa-search"></i></span>
									</div>
								</div>
							</div>
							<div class="align-items-center justify-content-end"
								style="display: flex; flex-direction: row; margin: auto; width: 400px;">
								<label style="width: 100px"><spring:message
										code="adPro.sort"></spring:message></label> <select
									style="width: 300px;" id="followSelect">
									<option value="name" onclick="selectClick()"><spring:message
											code="adPro.sortName"></spring:message></option>
									<option value="id" onclick="selectClick()" selected="selected"><spring:message
											code="adPro.sortID"></spring:message></option>
								</select>
								<form id="myRadioBtn" class="d-flex align-items-center m-2"
									action="">
									<input class="m-2" type="radio" name="gender" value="asc"
										onclick="radioClick()" checked="checked" id="ascRd">
									<spring:message code="adPro.sortI"></spring:message>
									<br> <input class="m-2" type="radio" name="gender"
										value="dsc" onclick="radioClick()" id="ascRd">
									<spring:message code="adPro.sortD"></spring:message>
									<br>
								</form>
							</div>
						</div>
						<button class="btn btn-primary" data-toggle="modal"
							data-target="#myModal" style="float: right;"
							onmouseenter="cleanAddModal()" onclick="">
							<spring:message code="adPro.add"></spring:message>
						</button>

						<!-- <div style="display: none;" id="test"> -->
						<h2>
							<spring:message code="adPro.h2"></spring:message>
						</h2>
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<form:form action="./product" method="post"
									modelAttribute="product" enctype="multipart/form-data">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title" style="text-align: center;">
												<spring:message code="adPro.h4"></spring:message>
											</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">

											<fieldset>
												<div class="control-group">
													<label class="control-label" for="txtProductName"><spring:message
															code="adPro.name"></spring:message></label>
													<form:input type="text" path="pro_name"
														class="form-control" id="txtProductName"
														placeholder="Product Name" />
												</div>
												<div class="control-group">
													<label class="control-label" for="txtProductCost"><spring:message
															code="adPro.cost"></spring:message></label>
													<form:input type="text" path="pro_cost"
														class="form-control" id="txtProductCost"
														placeholder="Cost" />
												</div>
												<div class="control-group">
													<label class="control-label" for="txtProductQuantity"><spring:message
															code="adPro.quan"></spring:message></label>
													<form:input type="text" path="pro_quantity"
														class="form-control" id="txtProductQuantity"
														placeholder="Quantity" />
												</div>
												<div class="control-group">
													<label class="control-label" for="categories"><spring:message
															code="adPro.cate"></spring:message></label>
													<form:select path="category.cate_id" id="category-select">
														<form:option value="-1" label="--- Select ---" />
														<form:options items="${categories}" itemValue="cate_id"
															itemLabel="cate_name" onclick="test(this)"
															id="cate_id${cate_id}" />
													</form:select>
												</div>
												<div class="control-group">
													<label class="control-label" for="txtStatus">Status</label>
													<form:input type="text" path="status" class="form-control"
														id="txtStatus" placeholder="status"
														style="text-align:center;" readonly="true" />
												</div>
												<div class="control-group">
													<label class="control-label" for="fileInput">File
														input</label>
													<div class="controls">
														<fieldset>
															Description: <br>
															<form:input path="upload.description"
																style="width:300px;" id="txtId" readonly="true" />
															<br /> <br /> File to upload (1):
															<form:input path="upload.fileDatas" type="file" />
															<br /> File to upload (2):
															<form:input path="upload.fileDatas" type="file" />
															<br /> File to upload (3):
															<form:input path="upload.fileDatas" type="file" />
															<br /> File to upload (4):
															<form:input path="upload.fileDatas" type="file" />
															<br /> File to upload (5):
															<form:input path="upload.fileDatas" type="file" />
														</fieldset>
													</div>
												</div>
												<div class="control-group">
													<form:label path="pro_content">
														<spring:message code="adPro.des"></spring:message>
													</form:label>
													<form:textarea path="pro_content" rows="5" cols="30"
														id="txtProductContent" />
												</div>
											</fieldset>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary"
												name="btnSaveInModal" value="" id="btnSaveInModal">
												<spring:message code="adPro.save"></spring:message>
											</button>
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">
												<spring:message code="adPro.close"></spring:message>
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>

						<div id="products" class="carousel slide" data-ride="carousel"
							style="background: none;" data-interval="false">
							<!-- Indicators -->
							<c:set var="first" value="0"></c:set>
							<ul class="carousel-indicators carousel-indicators-numbers"
								style="margin-bottom: 0px;">
								<c:if test="${fn:length(products) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#products" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#products" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(products) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#products" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#products" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
							</ul>

							<div class="carousel-inner" style="">
								<c:set var="first" value="0"></c:set>
								<c:set var="count1" value="0"></c:set>
								<c:set var="count" value="1"></c:set>
								<c:if test="${fn:length(products) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="products-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adPro.id"></spring:message></th>
															<th><spring:message code="adPro.name1"></spring:message></th>
															<th><spring:message code="adPro.quan1"></spring:message></th>
															<th><spring:message code="adPro.content"></spring:message></th>
															<th><spring:message code="adPro.star"></spring:message></th>
															<th><spring:message code="adPro.price"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adPro.action"></spring:message></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(products)-1}">
																<tr>
																	<td>${products[count1].pro_id}</td>
																	<td>${products[count1].pro_name}</td>
																	<td>${products[count1].pro_quantity}</td>
																	<td>${products[count1].pro_content}</td>
																	<td>${products[count1].pro_star}</td>
																	<td>${products[count1].pro_cost}</td>
																	<td>${products[count1].status}</td>
																	<td style="display: flex"><button type="button"
																			class="btn btn-primary" data-toggle="modal"
																			data-target="#myModal"
																			value="${products[count1].pro_name}"
																			onclick="update(this, ${products[count1].pro_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if
																			test="${products[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${products[count1].pro_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${products[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${products[count1].pro_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteProduct(this, ${products[count1].pro_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button> <%-- <button type="button" data-toggle="modal"
																	data-target="#myModal2" class="btn btn-primary"
																	onclick="upload(this, ${products[count1].pro_id})">
																	<spring:message code="adPro.upimg"></spring:message>
																</button> --%></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="products-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adPro.id"></spring:message></th>
															<th><spring:message code="adPro.name1"></spring:message></th>
															<th><spring:message code="adPro.quan1"></spring:message></th>
															<th><spring:message code="adPro.content"></spring:message></th>
															<th><spring:message code="adPro.star"></spring:message></th>
															<th><spring:message code="adPro.price"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adPro.action"></spring:message></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(products)-1}">
																<tr>
																	<td>${products[count1].pro_id}</td>
																	<td>${products[count1].pro_name}</td>
																	<td>${products[count1].pro_quantity}</td>
																	<td>${products[count1].pro_content}</td>
																	<td>${products[count1].pro_star}</td>
																	<td>${products[count1].pro_cost}</td>
																	<td>${products[count1].status}</td>
																	<td style="display: flex"><button type="button"
																			class="btn btn-primary" data-toggle="modal"
																			data-target="#myModal"
																			value="${products[count1].pro_name}"
																			onclick="update(this, ${products[count1].pro_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if
																			test="${products[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${products[count1].pro_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${products[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${products[count1].pro_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteProduct(this, ${products[count1].pro_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button> <%-- <button type="button" data-toggle="modal"
																	data-target="#myModal2" class="btn btn-primary"
																	onclick="upload(this, ${products[count1].pro_id}, ${first})">
																	<spring:message code="adPro.upimg"></spring:message>
																</button> --%></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(products) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="products-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adPro.id"></spring:message></th>
															<th><spring:message code="adPro.name1"></spring:message></th>
															<th><spring:message code="adPro.quan1"></spring:message></th>
															<th><spring:message code="adPro.content"></spring:message></th>
															<th><spring:message code="adPro.star"></spring:message></th>
															<th><spring:message code="adPro.price"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adPro.action"></spring:message></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(products)-1}">
																<tr>
																	<td>${products[count1].pro_id}</td>
																	<td>${products[count1].pro_name}</td>
																	<td>${products[count1].pro_quantity}</td>
																	<td>${products[count1].pro_content}</td>
																	<td>${products[count1].pro_star}</td>
																	<td>${products[count1].pro_cost}</td>
																	<td>${products[count1].status}</td>
																	<td style="display: flex"><button type="button"
																			class="btn btn-primary" data-toggle="modal"
																			data-target="#myModal"
																			value="${products[count1].pro_name}"
																			onclick="update(this, ${products[count1].pro_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if
																			test="${products[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${products[count1].pro_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${products[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${products[count1].pro_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteProduct(this, ${products[count1].pro_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button> <%-- <button type="button" data-toggle="modal"
																	data-target="#myModal2" class="btn btn-primary"
																	onclick="upload(this, ${products[count1].pro_id})">
																	<spring:message code="adPro.upimg"></spring:message>
																</button> --%></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="products-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adPro.id"></spring:message></th>
															<th><spring:message code="adPro.name1"></spring:message></th>
															<th><spring:message code="adPro.quan1"></spring:message></th>
															<th><spring:message code="adPro.content"></spring:message></th>
															<th><spring:message code="adPro.star"></spring:message></th>
															<th><spring:message code="adPro.price"></spring:message></th>
															<th>Status</th>
															<th><spring:message code="adPro.action"></spring:message></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(products)-1}">
																<tr>
																	<td>${products[count1].pro_id}</td>
																	<td>${products[count1].pro_name}</td>
																	<td>${products[count1].pro_quantity}</td>
																	<td>${products[count1].pro_content}</td>
																	<td>${products[count1].pro_star}</td>
																	<td>${products[count1].pro_cost}</td>
																	<td>${products[count1].status}</td>
																	<td style="display: flex"><button type="button"
																			class="btn btn-primary" data-toggle="modal"
																			data-target="#myModal"
																			value="${products[count1].pro_name}"
																			onclick="update(this, ${products[count1].pro_id})">
																			<spring:message code="adPro.update"></spring:message>
																		</button> <c:if
																			test="${products[count1].status == 'activated'}">
																			<button class="btn btn-warning"
																				onclick="block(${products[count1].pro_id}, ${count}, this, ${first})">Block</button>
																		</c:if> <c:if test="${products[count1].status == 'blocked'}">
																			<button class="btn btn-warning"
																				onclick="activate(${products[count1].pro_id}, ${count}, this, ${first})">Activate</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteProduct(this, ${products[count1].pro_id}, ${first})"
																			value="${count}">
																			<spring:message code="adPro.del"></spring:message>
																		</button> <%-- <button type="button" data-toggle="modal"
																	data-target="#myModal2" class="btn btn-primary"
																	onclick="upload(this, ${products[count1].pro_id}, ${first})">
																	<spring:message code="adPro.upimg"></spring:message>
																</button> --%></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
							</div>
						</div>

						<div id="searchResult" style="display: none;">
							<table class="table table-bordered" style="width: 100%;"
								id="userSearchTable">
								<thead>
									<tr>
										<th><spring:message code="adPro.id"></spring:message></th>
										<th><spring:message code="adPro.name1"></spring:message></th>
										<th>Status</th>
										<th><spring:message code="adPro.action"></spring:message></th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		</div>

		<script>
		function deleteProduct(button, pro_id, orderTable) {
			$.ajax({
				url : "./product/" + pro_id,
				type : "DELETE",
				data : "",
				contentType : "application/json",
				success : function() {
					var table = document.getElementById("products-table"+orderTable);
					table.deleteRow(button.value);
					//
					var tr = table.getElementsByTagName("tr");
					for (var i = 1; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[1].value = i;
					}
				}
			});
		}
		function update(button, pro_id) {
			$
					.ajax({
						url : "./aproduct?proId=" + pro_id,
						type : "GET",
						data : "",
						contentType : "application/json",
						success : function(data) {
							document.getElementById("btnSaveInModal").value = "update-"
									+ pro_id;

							document.getElementById("txtProductName").value = data[0].pro_name;
							document.getElementById("txtProductCost").value = data[0].pro_cost;
							document.getElementById("txtProductQuantity").value = data[0].pro_quantity;
							document.getElementById("txtProductContent").value = data[0].pro_content;
							document.getElementById("txtStatus").value = data[0].status;
							var select = document
									.getElementById("category-select");
							var option = select.getElementsByTagName("option");
							for (var i = 0; i < option.length; i++) {
								if (option[i].value == data[1].cate_id)
									option[i].selected = "true";
							}
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					})
		}

		function upload(button, proId) {
			document.getElementById("txtId").value = proId;
		}

		function block(proId, order, button, orderTable) {
			$
					.ajax({
						url : "./block/" + proId,
						type : "POST",
						data : '',
						contentType : "application/json",
						success : function(data) {
							var cateTable = document
									.getElementById("products-table"+orderTable);
							cateTable.getElementsByTagName("tr")[order]
									.getElementsByTagName("td")[6].innerHTML = "blocked";
							button.innerHTML = "Activate";
							var func = function(proId1, order1, button1, orderTable1) {
								return function() {
									activate(proId1, order1, button1, orderTable1);
								}
							}
							button.onclick = func(proId, order, button, orderTable);
						}
					});
		}

		function activate(proId, order, button, orderTable) {
			$
					.ajax({
						url : "./activate/" + proId,
						type : "POST",
						data : '',
						contentType : "application/json",
						success : function(data) {
							var cateTable = document
									.getElementById("products-table"+orderTable);
							cateTable.getElementsByTagName("tr")[order]
									.getElementsByTagName("td")[6].innerHTML = "activated";
							button.innerHTML = "Block";
							var func = function(proId1, order1, button1, orderTable1) {
								return function() {
									block(proId1, order1, button1, orderTable1);
								}
							}
							button.onclick = func(proId, order, button, orderTable);
						}
					});
		}
	</script>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You are not allowed to access this
				feature!!!
			</div>
		</div>
	</c:if>
</body>
</html>