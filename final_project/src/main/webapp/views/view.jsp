<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/view.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">
<%-- <script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script> --%>
<style>
/* Make the image fully responsive */
.carousel-inner img {
	width: 100%;
	height: 100%;
}
</style>
</head>
<body
	onload="productRating(${product.pro_star});setUser(${currentUser.user_id});setNumItemCartGuest()">
	<div id="header" style="">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
												code="adPro.cate"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<c:forEach items="${viewCategories}" var="category">
												<a class="dropdown-item" href="#">${category.cate_name}</a>
											</c:forEach>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i> <spring:message
											code="cart.contact"></spring:message></a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i> <spring:message
											code="cart.lang"></spring:message></a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">
										<spring:message code="cart.sign"></spring:message>
									</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link"
						href="../viewProductController/product?proid=${pro.pro_id}&lang=vi">VN</a></li>
					<li class="nav-item"><a class="nav-link"
						href="../viewProductController/product?proid=${pro.pro_id}&lang=en">EN</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">

					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>

			<c:if test="${currentUser == null}">
				<button onclick="location.href='./movetologin';" class="btn"
					id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${currentUser.user_name}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="../orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a>
						<button class="dropdown-item" onclick="logOut()">
							<spring:message code="cart.logout"></spring:message>
						</button>
					</div>
				</div>

			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart-guest">0</span></i>
				</button>
			</c:if>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>
	<div id="contentContainer">
		<div id="content" style="margin-bottom: 50px; margin-top: 80px;">
			<div class="container">
				<div class="row" style="height: 778px;">
					<div class="col-lg-6"
						style="text-align: center; width: 585px; height: 634px;">
						<div style="width: 565px; height: 634px;">
							<img src="<c:url value='${listImage[0].img}' />" width="565px"
								height="634px" id="img-lg">
						</div>
						<div
							style="width: 565px; height: 144px; display: flex; margin-top: 10px; flex-direction: row; justify-content: space-around;">
							<div style="width: 131.25px; height: 131.25px;">
								<img src="<c:url value='${listImage[0].img}'/>"
									width="131.25px;" height="131.25px" id="img-sm-1"
									onclick="clickImage(this)"
									style="border: 3px solid darkblue; cursor: pointer;">
							</div>
							<div style="width: 131.25px; height: 131.25px; float:">
								<img src="<c:url value='${listImage[1].img}'/>"
									width="131.25px;" height="131.25px" id="img-sm-2"
									onclick="clickImage(this)" style="cursor: pointer;">
							</div>
							<div style="width: 131.25px; height: 131.25px; float:">
								<img src="<c:url value='${listImage[2].img}'/>"
									width="131.25px;" height="131.25px" id="img-sm-3"
									onclick="clickImage(this)" style="cursor: pointer;">
							</div>
						</div>
					</div>
					<div class="col-lg-6"
						style="text-align: center; width: 585px; display: flex; flex-direction: column;">
						<h2
							style="font-size: 25px; line-height: 30px; color: #21293c; margin-bottom: .2em; text-align: left; font-weight: 600;">${product.pro_name}</h2>
						<div style="display: inherit;" style="">
							<table id="rating-table-content">
								<tr>
									<td><i class="far fa-star"></i></td>
									<td><i class="far fa-star"></i></td>
									<td><i class="far fa-star"></i></td>
									<td><i class="far fa-star"></i></td>
									<td><i class="far fa-star"></i></td>
								</tr>
							</table>
							<a href="#"
								style="margin-left: 10px; color: #888; text-decoration: none;"><spring:message
									code="view.re"></spring:message></a> <span
								style="margin-left: 10px;">|</span> <a data-toggle="tab"
								href="#menu3"
								style="margin-left: 10px; color: #888; text-decoration: none;"
								onclick="test(this)"><spring:message code="view.re1"></spring:message></a>
							<script type="text/javascript">
							function test(aTag){
								aTag.style.backgroundColor = "white";
								window.scroll(0,1000);

								for (var i = 0; i <= document.getElementById("item-info-tabs").getElementsByTagName("a").length-1; i++){
									document.getElementById("item-info-tabs").getElementsByTagName("a")[i].className = "nav-link";
									}
								
								document.getElementById("review-tab").className = "nav-link active";
								
								}
							</script>
						</div>
						<p id="price"
							style="font-size: 21px; line-height: 21px; font-weight: 700; color: #2b2b2b; text-align: left; line-height: 1.5; margin-top: 15px;">${product.pro_cost}
							<span>&#8363</span>
						</p>
						<div id="discription">
							<p
								style="color: #7b858a; font-size: 14px; line-height: 27px; text-align: left;">${fn:substring(product.pro_content, 0, 30)}</p>
						</div>
						<div id="reference-info"
							style="display: flex; flex-direction: column; text-align: left;">
							<span>CODE : ${product.pro_id}</span> <span><spring:message
									code="view.cate"></spring:message> <a href="#">${product.category.cate_name}</a></span>
							<span><spring:message code="view.tag"></spring:message> <a
								href="#">Fashion</a></span> <span><spring:message code="view.nu"></spring:message>
								<span id="quantity">${quantity}</span></span>
						</div>
						<hr style="width: inherit;">
						<div id="addCart" style="display: flex; flex-direction: row;">
							<div id="insc-desc" style="text-align: left; margin-right: 10px;">
								<button type="button"
									style="width: 30px; height: 43px; background: none; border: 1px solid #dae2e6; margin-right: -5px;"
									onclick="descrease(this)">-</button>
								<input type="text" value="1"
									style="text-align: center; width: 44px; height: 43px; margin: 0px 0px; border: 1px solid #dae2e6; margin-right: -6px;"
									id="txtQuantity">
								<button type="button"
									style="width: 30px; height: 43px; background: none; border: 1px solid #dae2e6;"
									onclick="inscrease(this)">+</button>
							</div>
							<c:if test="${currentUser != null}">
								<c:if test="${quantity > 0}">
									<button type="button"
										style="width: 127px; height: 43px; background-color: #1fc0a0; border: 1px solid #1fc0a0; font-size: 14px;"
										onclick="addCart(${product.pro_id}, 0)" id="add-cart-button">
										<spring:message code="view.add"></spring:message>
									</button>
								</c:if>
								<c:if test="${quantity <= 0}">
									<button type="button"
										style="width: 127px; height: 43px; background-color: #ff1a1a; border: 1px solid #ff1a1a; color: white; font-weight: 500; font-size: 14px;"
										onclick="addCart(${product.pro_id}, 0)" disabled="disabled">
										<spring:message code="view.add1"></spring:message>
									</button>
								</c:if>
								<div class="toast" role="alert" aria-live="polite"
									aria-atomic="true" data-delay="2000">
									<div class="toast-header" style="text-align: center">
										<spring:message code="view.info"></spring:message>
									</div>
									<div class="toast-body" style="color: red">
										<spring:message code="view.info1"></spring:message>
									</div>
								</div>
							</c:if>
							<c:if test="${currentUser == null}">
								<c:if test="${quantity > 0}">
									<button type="button"
										style="width: 127px; height: 43px; background-color: #1fc0a0; border: 1px solid #1fc0a0; font-size: 14px;"
										onclick="addCart(${product.pro_id}, 1)" id="add-cart-button">
										<spring:message code="view.info2"></spring:message>
									</button>
								</c:if>
								<c:if test="${quantity <= 0}">
									<button type="button"
										style="width: 127px; height: 43px; background-color: #ff1a1a; border: 1px solid #ff1a1a; color: white; font-weight: 500; font-size: 14px;"
										onclick="addCart(${product.pro_id}, 1)" disabled="disabled">
										<spring:message code="view.info3"></spring:message>
									</button>
								</c:if>
								<div class="toast" role="alert" aria-live="polite"
									aria-atomic="true" data-delay="2000">
									<div class="toast-header" style="text-align: center">Info</div>
									<div class="toast-body" style="color: red">
										<spring:message code="view.info4"></spring:message>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- </div> -->
	<!-- hang 2 -->
	<div class="container" style="">
		<div class="row">
			<div class="col-lg-12">
				<ul class="nav nav-tabs" role="tablist" id="item-info-tabs">
					<li class="nav-item"><a class="nav-link active"
						data-toggle="tab" href="#menu1"> <spring:message
								code="view.a1"></spring:message>
					</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab"
						href="#menu2"> <spring:message code="view.a2"></spring:message>
					</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab"
						href="#menu3" id="review-tab"> <spring:message code="view.a3"></spring:message>
					</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab"
						href="#menu4" id="review-tab"> <spring:message code="view.a4"></spring:message>
					</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div id="menu1" class="container tab-pane active"
						style="background: none;">
						<br>
						<p id="full-description">${product.pro_content}</p>
					</div>
					<div id="menu2" class="container tab-pane fade"
						style="background: none;">
						<br>
						<p>${product.pro_content}</p>
					</div>
					<div id="menu3" class="container tab-pane fade"
						style="background: none;">
						<br>
						<div id="review-container">
							<div class="container">
								<div id="reviews" class="carousel slide" data-ride="carousel"
									style="background: none;" data-interval="false">
									<!-- Indicators -->
									<c:set var="first" value="0"></c:set>
									<ul class="carousel-indicators carousel-indicators-numbers"
										style="margin-bottom: 0px;">
										<c:if test="${fn:length(listReview) % 6 != 0}">
											<c:forEach begin="0" end="${evenReview}">
												<c:if test="${first == 0}">
													<li data-target="#reviews" data-slide-to="${first}"
														class="active"
														style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
												</c:if>
												<c:if test="${first != 0}">
													<li data-target="#reviews" data-slide-to="${first}"
														style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
												</c:if>
												<c:set var="first" value="${first+1}"></c:set>
											</c:forEach>
										</c:if>
										<c:if test="${fn:length(listReview) % 6 == 0}">
											<c:if test="${evenReview > 0}">
												<c:forEach begin="0" end="${evenReview-1}">
													<c:if test="${first == 0}">
														<li data-target="#reviews" data-slide-to="${first}"
															class="active"
															style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
													</c:if>
													<c:if test="${first != 0}">
														<li data-target="#reviews" data-slide-to="${first}"
															style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
													</c:if>
													<c:set var="first" value="${first+1}"></c:set>
												</c:forEach>
											</c:if>
										</c:if>
									</ul>

									<div class="carousel-inner" style="">
										<h4>
											<spring:message code="view.h4"></spring:message>
										</h4>
										<c:set var="first" value="0"></c:set>
										<c:set var="count" value="0"></c:set>
										<c:if test="${fn:length(listReview) % 6 != 0}">
											<c:forEach begin="0" end="${evenReview}">
												<c:if test="${first == 0}">
													<div class="carousel-item active" style="background: none;">
														<table
															style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
															<c:forEach begin="0" end="5">
																<c:if test="${count <= fn:length(listReview)-1}">
																	<tr
																		style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																		<td
																			style="background-color: lavender; width: 80px; height: 80x;">
																			${fn:substring(listReview[count].name,0, 10)}</td>
																		<td
																			style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																			<div
																				style="display: flex; flex-direction: column; padding-left: 20px;">
																				<p>
																					<strong><i>${listReview[count].name}</i></strong>
																				</p>
																				<p>${listReview[count].content}</p>
																			</div>
																			<div style="margin-right: 5px;">
																				<c:set var="i" value="0"></c:set>
																				<table id="">
																					<tr>
																						<c:forEach begin="0" end="4">
																							<c:if test="${i < listReview[count].pro_star}">
																								<td style="color: yellow;" data-toggle="tooltip"
																									title="${listReview[count].pro_star}"><i
																									class="fas fa-star"></i></td>
																							</c:if>
																							<c:if test="${i >= listReview[count].pro_star}">
																								<td data-toggle="tooltip"
																									title="${listReview[count].pro_star}"><i
																									class="far fa-star"></i></td>
																							</c:if>
																							<c:set var="i" value="${i+1}"></c:set>
																						</c:forEach>
																					</tr>
																				</table>
																			</div>
																		</td>
																	</tr>
																</c:if>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</table>
													</div>
												</c:if>
												<c:if test="${first != 0}">
													<div class="carousel-item" style="background: none;">
														<table
															style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
															<c:forEach begin="0" end="5">
																<c:if test="${count <= fn:length(listReview)-1}">
																	<tr
																		style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																		<td
																			style="background-color: lavender; width: 80px; height: 80x;">
																			${fn:substring(listReview[count].name,0, 10)}</td>
																		<td
																			style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																			<div
																				style="display: flex; flex-direction: column; padding-left: 20px;">
																				<p>
																					<strong><i>${listReview[count].name}</i></strong>
																				</p>
																				<p>${listReview[count].content}</p>
																			</div>
																			<div>
																				<c:set var="i" value="0"></c:set>
																				<table id="">
																					<tr>
																						<c:forEach begin="0" end="4">
																							<c:if test="${i < listReview[count].pro_star}">
																								<td style="color: yellow;" data-toggle="tooltip"
																									title="${listReview[count].pro_star}"><i
																									class="fas fa-star"></i></td>
																							</c:if>
																							<c:if test="${i >= listReview[count].pro_star}">
																								<td data-toggle="tooltip"
																									title="${listReview[count].pro_star}"><i
																									class="far fa-star"></i></td>
																							</c:if>
																							<c:set var="i" value="${i+1}"></c:set>
																						</c:forEach>
																					</tr>
																				</table>
																			</div>
																		</td>
																	</tr>
																</c:if>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</table>
													</div>
												</c:if>
												<c:set var="first" value="${first+1}"></c:set>
											</c:forEach>
										</c:if>

										<c:if test="${fn:length(listReview) % 6 == 0}">
											<c:if test="${evenReview > 0}">
												<c:forEach begin="0" end="${evenRevie-1}">
													<c:if test="${first == 0}">
														<div class="carousel-item active"
															style="background: none;">
															<table
																style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
																<c:forEach begin="0" end="5">
																	<c:if test="${count <= fn:length(listReview)-1}">
																		<tr
																			style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																			<td
																				style="background-color: lavender; width: 80px; height: 80x;">
																				${fn:substring(listReview[count].name,0, 10)}</td>
																			<td
																				style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																				<div
																					style="display: flex; flex-direction: column; padding-left: 20px;">
																					<p>
																						<strong><i>${listReview[count].name}</i></strong>
																					</p>
																					<p>${listReview[count].content}</p>
																				</div>
																				<div>
																					<c:set var="i" value="0"></c:set>
																					<table id="">
																						<tr>
																							<c:forEach begin="0" end="4">
																								<c:if test="${i < listReview[count].pro_star}">
																									<td style="color: yellow;"
																										data-toggle="tooltip"
																										title="${listReview[count].pro_star}"><i
																										class="fas fa-star"></i></td>
																								</c:if>
																								<c:if test="${i >= listReview[count].pro_star}">
																									<td data-toggle="tooltip"
																										title="${listReview[count].pro_star}"><i
																										class="far fa-star"></i></td>
																								</c:if>
																								<c:set var="i" value="${i+1}"></c:set>
																							</c:forEach>
																						</tr>
																					</table>
																				</div>
																			</td>
																		</tr>
																	</c:if>
																	<c:set var="count" value="${count+1}"></c:set>
																</c:forEach>
															</table>
														</div>
													</c:if>
													<c:if test="${first != 0}">
														<div class="carousel-item" style="background: none;">
															<table
																style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
																<c:forEach begin="0" end="5">
																	<c:if test="${count <= fn:length(listReview)-1}">
																		<tr
																			style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																			<td
																				style="background-color: lavender; width: 80px; height: 80x;">
																				${fn:substring(listReview[count].name,0, 10)}</td>
																			<td
																				style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																				<div
																					style="display: flex; flex-direction: column; padding-left: 20px;">
																					<p>
																						<strong><i>${listReview[count].name}</i></strong>
																					</p>
																					<p>${listReview[count].content}</p>
																				</div>
																				<div>
																					<c:set var="i" value="0"></c:set>
																					<table id="">
																						<tr>
																							<c:forEach begin="0" end="4">
																								<c:if test="${i < listReview[count].pro_star}">
																									<td style="color: yellow;"
																										data-toggle="tooltip"
																										title="${listReview[count].pro_star}"><i
																										class="fas fa-star"></i></td>
																								</c:if>
																								<c:if test="${i >= listReview[count].pro_star}">
																									<td data-toggle="tooltip"
																										title="${listReview[count].pro_star}"><i
																										class="far fa-star"></i></td>
																								</c:if>
																								<c:set var="i" value="${i+1}"></c:set>
																							</c:forEach>
																						</tr>
																					</table>
																				</div>
																			</td>
																		</tr>
																	</c:if>
																	<c:set var="count" value="${count+1}"></c:set>
																</c:forEach>
															</table>
														</div>
													</c:if>
													<c:set var="first" value="${first+1}"></c:set>
												</c:forEach>
											</c:if>
										</c:if>
									</div>
								</div>
							</div>
						</div>
						<hr style="width: inherit;">
						<div id="review-form">
							<h4>
								<spring:message code="view.h41"></spring:message>
							</h4>
							<div
								style="width: inherit; height: 606px; background-color: #f7f7f7; padding: 10px 20px;">
								<div id="rating-container" style="display: flex;">
									<h5 style="margin-right: 10px;">
										<spring:message code="view.h5"></spring:message>
									</h5>
									<table id="rating-table-review">
										<tr>
											<td onclick="rate(this)" onmouseover="over(this)"
												onmouseout="leave(this)" style="cursor: pointer;"><i
												class="far fa-star"></i></td>
											<td onclick="rate(this)" onmouseover="over(this)"
												onmouseout="leave(this)" style="cursor: pointer;"><i
												class="far fa-star"></i></td>
											<td onclick="rate(this)" onmouseover="over(this)"
												onmouseout="leave(this)" style="cursor: pointer;"><i
												class="far fa-star"></i></td>
											<td onclick="rate(this)" onmouseover="over(this)"
												onmouseout="leave(this)" style="cursor: pointer;"><i
												class="far fa-star"></i></td>
											<td onclick="rate(this)" onmouseover="over(this)"
												onmouseout="leave(this)" style="cursor: pointer;"><i
												class="far fa-star"></i></td>
										</tr>
									</table>
									<button type="button" class="btn btn-danger"
										style="border-radius: 100%; margin-left: 10px; visibility: hidden;"
										value="" id="remove-rating-button" onclick="removeRate(this)">x</button>
								</div>
								<div id="writereview-container" style="background: none;">
									<form>
										<div class="form-group">
											<label for="review-content" style="font-size: 1.25rem;"><spring:message
													code="view.yourreview"></spring:message> <span
												style="color: red;">*</span> </label>
											<textarea class="form-control" rows="10" id="review-content"
												style="width: 1040px;"></textarea>
										</div>
									</form>
								</div>
								<div id="name-container">
									<div class="form-group">
										<label for="name" style="font-size: 1.25rem;"><spring:message
												code="adAc.name"></spring:message> <span style="color: red;">*</span></label>
										<c:if test="${currentUser != null}">
											<input type="checkbox" id="nameCheckBox"
												value="${currentUser.user_name}"
												onclick="clickNameCheckBox(this)">
											<label for="nameCheckBox" style="font-size: 1.25rem;"><spring:message
													code="view.a5"></spring:message></label>
										</c:if>
										<input type="text" class="form-control" id="name"
											style="width: 1040px;">
									</div>
								</div>
								<div id="email-container">
									<div class="form-group">
										<label for="email" style="font-size: 1.25rem;"><spring:message
												code="adMe.email"></spring:message><span style="color: red;">*</span>
										</label>
										<c:if test="${currentUser != null}">
											<input type="checkbox" id="emailCheckBox"
												value="${currentUser.email}"
												onclick="clickEmailCheckBox(this)">
											<label for="emailCheckBox" style="font-size: 1.25rem;"><spring:message
													code="view.a6"></spring:message></label>
										</c:if>
										<input type="text" class="form-control" id="email"
											style="width: 1040px;">
									</div>
								</div>
								<button type="button"
									style="background: none; border: 1px solid #1cab8e; background-color: #1cab8e; color: white; width: 68px; height: 38px;"
									onclick="sendReview(${product.pro_id})">
									<spring:message code="view.sub"></spring:message>
								</button>
							</div>
						</div>
					</div>

					<!-- Question and answer -->
					<div id="menu4" class="container tab-pane fade"
						style="background: none;">
						<br>
						<div id="qa-container">
							<div class="container">
								<div id="qas" class="carousel slide" data-ride="carousel"
									style="background: none;" data-interval="false">
									<!-- Indicators -->
									<c:set var="first" value="0"></c:set>
									<ul class="carousel-indicators carousel-indicators-numbers"
										style="margin-bottom: 0px;">
										<c:if test="${fn:length(listQa) % 6 != 0}">
											<c:forEach begin="0" end="${evenQa}">
												<c:if test="${first == 0}">
													<li data-target="#qas" data-slide-to="${first}"
														class="active"
														style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
												</c:if>
												<c:if test="${first != 0}">
													<li data-target="#qas" data-slide-to="${first}"
														style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
												</c:if>
												<c:set var="first" value="${first+1}"></c:set>
											</c:forEach>
										</c:if>
										<c:if test="${fn:length(listQa) % 6 == 0}">
											<c:if test="${evenQa > 0}">
												<c:forEach begin="0" end="${evenQa-1}">
													<c:if test="${first == 0}">
														<li data-target="#reviews" data-slide-to="${first}"
															class="active"
															style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
													</c:if>
													<c:if test="${first != 0}">
														<li data-target="#reviews" data-slide-to="${first}"
															style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
													</c:if>
													<c:set var="first" value="${first+1}"></c:set>
												</c:forEach>
											</c:if>
										</c:if>
									</ul>

									<div class="carousel-inner" style="">
										<h4>RECENTLY QUESTIONS</h4>
										<c:set var="first" value="0"></c:set>
										<c:set var="count" value="0"></c:set>
										<c:if test="${fn:length(listQa) % 6 != 0}">
											<c:forEach begin="0" end="${evenQa}">
												<c:if test="${first == 0}">
													<div class="carousel-item active" style="background: none;">
														<table
															style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
															<c:forEach begin="0" end="5">
																<c:if test="${count <= fn:length(listQa)-1}">
																	<tr
																		style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																		<td
																			style="background-color: lavender; width: 80px; height: 80x;">

																		</td>
																		<td
																			style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																			<div
																				style="display: flex; flex-direction: column; padding-left: 20px;">
																				<p>
																					<strong><i>${listQa[count].q_content}</i></strong>
																				</p>
																				<p>${listQa[count].a_content}</p>
																			</div>
																		</td>
																	</tr>
																</c:if>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</table>
													</div>
												</c:if>
												<c:if test="${first != 0}">
													<div class="carousel-item" style="background: none;">
														<table
															style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
															<c:forEach begin="0" end="5">
																<c:if test="${count <= fn:length(listQa)-1}">
																	<tr
																		style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																		<td
																			style="background-color: lavender; width: 80px; height: 80x;">

																		</td>
																		<td
																			style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																			<div
																				style="display: flex; flex-direction: column; padding-left: 20px;">
																				<p>
																					<strong><i>${listQa[count].q_content}</i></strong>
																				</p>
																				<p>${listQa[count].a_content}</p>
																			</div>
																		</td>
																	</tr>
																</c:if>
																<c:set var="count" value="${count+1}"></c:set>
															</c:forEach>
														</table>
													</div>
												</c:if>
												<c:set var="first" value="${first+1}"></c:set>
											</c:forEach>
										</c:if>

										<c:if test="${fn:length(listQa) % 6 == 0}">
											<c:if test="${evenQa > 0}">
												<c:forEach begin="0" end="${evenQa-1}">
													<c:if test="${first == 0}">
														<div class="carousel-item active"
															style="background: none;">
															<table
																style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
																<c:forEach begin="0" end="5">
																	<c:if test="${count <= fn:length(listQa)-1}">
																		<tr
																			style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																			<td
																				style="background-color: lavender; width: 80px; height: 80x;">

																			</td>
																			<td
																				style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																				<div
																					style="display: flex; flex-direction: column; padding-left: 20px;">
																					<p>
																						<strong><i>${listQa[count].q_content}</i></strong>
																					</p>
																					<p>${listQa[count].a_content}</p>
																				</div>
																			</td>
																		</tr>
																	</c:if>
																	<c:set var="count" value="${count+1}"></c:set>
																</c:forEach>
															</table>
														</div>
													</c:if>
													<c:if test="${first != 0}">
														<div class="carousel-item" style="background: none;">
															<table
																style="margin-left: auto; margin-right: auto; width: inherit; border-collapse: separate; border-spacing: 5px;">
																<c:forEach begin="0" end="5">
																	<c:if test="${count <= fn:length(listQa)-1}">
																		<tr
																			style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
																			<td
																				style="background-color: lavender; width: 80px; height: 80x;">
																			</td>
																			<td
																				style="display: flex; flex-direction: row; background-color: #f5f7f7; justify-content: space-between; margin-left: 20px;">
																				<div
																					style="display: flex; flex-direction: column; padding-left: 20px;">
																					<p>
																						<strong><i>${listQa[count].q_content}</i></strong>
																					</p>
																					<p>${listQa[count].a_content}</p>
																				</div>
																			</td>
																		</tr>
																	</c:if>
																	<c:set var="count" value="${count+1}"></c:set>
																</c:forEach>
															</table>
														</div>
													</c:if>
													<c:set var="first" value="${first+1}"></c:set>
												</c:forEach>
											</c:if>
										</c:if>
									</div>
								</div>
							</div>
						</div>
						<hr style="width: inherit;">
						<div id="review-form">
							<h4>Add a question</h4>
							<div
								style="width: inherit; background-color: #f7f7f7; padding: 10px 20px;">
								<div id="question-container" style="display: flex;">
									<div id="writequestion-container" style="background: none;">
										<form>
											<div class="form-group">
												<label for="review-content" style="font-size: 1.25rem;">Your
													question <span style="color: red;">*</span>
												</label>
												<textarea class="form-control" rows="10"
													id="question-content" style="width: 1040px;"></textarea>
											</div>
										</form>
									</div>
								</div>
								<button type="button"
									style="background: none; border: 1px solid #1cab8e; background-color: #1cab8e; color: white; width: 68px; height: 38px;"
									onclick="sendQuestion(${product.pro_id})">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- </div> -->

		<!-- VIEWED ITEMS -->
		<c:if test="${currentUser != null}">
			<!-- show -->
			<div class="container" style="background: none; margin-top: 50px;">
				<div id="viewed" class="carousel slide" data-ride="carousel"
					style="background: none;">

					<!-- Indicators -->
					<c:set var="first" value="0"></c:set>
					<ul class="carousel-indicators carousel-indicators-numbers"
						style="margin-bottom: 0px;">
						<c:if test="${fn:length(listViewed) % 5 != 0}">
							<c:forEach begin="0" end="${even}">
								<c:if test="${first == 0}">
									<li data-target="#viewed" data-slide-to="${first}"
										class="active"
										style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
								</c:if>
								<c:if test="${first != 0}">
									<li data-target="#viewed" data-slide-to="${first}"
										style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
								</c:if>
								<c:set var="first" value="${first+1}"></c:set>
							</c:forEach>
						</c:if>
						<c:if test="${fn:length(listViewed) % 5 == 0}">
							<c:if test="${even > 0}">
								<c:forEach begin="0" end="${even-1}">
									<c:if test="${first == 0}">
										<li data-target="#viewed" data-slide-to="${first}"
											class="active"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:if test="${first != 0}">
										<li data-target="#viewed" data-slide-to="${first}"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
								</c:forEach>
							</c:if>
						</c:if>
					</ul>

					<!-- VIEWED ITEMS -->
					<div class="carousel-inner" style="height: 460px;">
						<h4>
							<spring:message code="view.h43"></spring:message>
						</h4>
						<c:set var="first" value="0"></c:set>
						<c:set var="count" value="0"></c:set>
						<c:if test="${fn:length(listViewed) % 5 != 0}">
							<c:forEach begin="0" end="${even}">
								<c:if test="${first == 0}">
									<div class="carousel-item active" style="background: none;">
										<table
											style="margin-left: auto; margin-right: auto; width: inherit;">
											<tr style="margin-left: auto; margin-right: auto;">
												<c:forEach begin="0" end="4">
													<td><c:if test="${count <= fn:length(listViewed)-1}">
															<div style="width: 218px; height: 400px;">
																<div class="card" style="width: 218px; height: 390px;">
																	<img class="card-img-top"
																		src="<c:url value="${imgViewed[count].img}"/>"
																		alt="Card image" style="height: 200px;">
																	<div class="card-body"
																		style="display: flex; flex-direction: column; justify-content: space-between;">
																		<a class="card-title"
																			style="text-align: center; text-decoration: none;"
																			href="./product?proid=${listViewed[count].pro_id}">${listViewed[count].pro_name}</a>
																		<p class="card-text" style="text-align: center;">${listViewed[count].pro_cost}
																			&#8363</p>
																		<div
																			style="display: flex; flex-direction: row; justify-content: space-between;">
																			<a href="#" class="btn btn-primary"
																				style="font-size: 12px; width: 86px;"><spring:message
																					code="view.details"></spring:message></a> <a href="#"
																				class="btn btn-success"
																				style="font-size: 12px; width: 86px; background-color: #1fc0a0; border: 1px solid #1fc0a0;"
																				onclick="addCart(${product.pro_id}, 0)"><spring:message
																					code="view.add"></spring:message></a>
																		</div>
																	</div>
																</div>
															</div>
														</c:if></td>
													<c:set var="count" value="${count+1}"></c:set>
												</c:forEach>
											</tr>
										</table>
									</div>
								</c:if>
								<c:if test="${first != 1}">
									<div class="carousel-item" style="background: none;">
										<table
											style="margin-left: auto; margin-right: auto; width: inherit;">
											<tr style="margin-left: auto; margin-right: auto;">
												<c:forEach begin="0" end="4">
													<td><c:if test="${count <= fn:length(listViewed)-1}">
															<div style="width: 218px; height: 400px;">
																<div class="card" style="width: 218px; height: 390px;">
																	<img class="card-img-top"
																		src="<c:url value="${imgViewed[count].img}"/>"
																		alt="Card image" height="200px" style="height: 200px;">
																	<div class="card-body"
																		style="display: flex; flex-direction: column; justify-content: space-between;">
																		<a class="card-title" style="text-align: center;"
																			href="./product?proid=${listViewed[count].pro_id}">${listViewed[count].pro_name}</a>
																		<p class="card-text" style="text-align: center;">${listViewed[count].pro_cost}
																			&#8363</p>
																		<div
																			style="display: flex; flex-direction: row; justify-content: space-between;">
																			<a href="#" class="btn btn-primary"
																				style="font-size: 12px;"><spring:message
																					code="view.a7"></spring:message></a>
																			<button type="button" class="btn btn-success"
																				style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																				onclick="addCart(${product.pro_id}, 0)">
																				<spring:message code="view.add"></spring:message>
																			</button>
																		</div>
																	</div>
																</div>
															</div>
														</c:if></td>
													<c:set var="count" value="${count+1}"></c:set>
												</c:forEach>
											</tr>
										</table>
									</div>
								</c:if>
								<c:set var="first" value="${first+1}"></c:set>
							</c:forEach>
						</c:if>

						<c:if test="${fn:length(listViewed) % 5 == 0}">
							<c:if test="${even > 0}">
								<c:forEach begin="0" end="${even-1}">
									<c:if test="${first == 0}">
										<div class="carousel-item active" style="background: none;">
											<table
												style="margin-left: auto; margin-right: auto; width: inherit;">
												<tr style="margin-left: auto; margin-right: auto;">
													<c:forEach begin="0" end="4">
														<td><c:if test="${count <= fn:length(listViewed)-1}">
																<div style="width: 218px; height: 400px;">
																	<div class="card" style="width: 218px; height: 390px;">
																		<img class="card-img-top"
																			src="<c:url value="${imgViewed[count].img}"/>"
																			alt="Card image" style="height: 200px;">
																		<div class="card-body"
																			style="display: flex; flex-direction: column; justify-content: space-between;">
																			<a class="card-title"
																				style="text-align: center; text-decoration: none;"
																				href="./product?proid=${listViewed[count].pro_id}">${listViewed[count].pro_name}</a>
																			<p class="card-text" style="text-align: center;">${listViewed[count].pro_cost}
																				&#8363</p>
																			<div
																				style="display: flex; flex-direction: row; justify-content: space-between;">
																				<a href="#" class="btn btn-primary"
																					style="font-size: 12px; width: 86px;">Details</a> <a
																					href="#" class="btn btn-success"
																					style="font-size: 12px; width: 86px; background-color: #1fc0a0; border: 1px solid #1fc0a0;"
																					onclick="addCart(${product.pro_id}, 0)">Add to
																					cart</a>
																			</div>
																		</div>
																	</div>
																</div>
															</c:if></td>
														<c:set var="count" value="${count+1}"></c:set>
													</c:forEach>
												</tr>
											</table>
										</div>
									</c:if>
									<c:if test="${first != 1}">
										<div class="carousel-item" style="background: none;">
											<table
												style="margin-left: auto; margin-right: auto; width: inherit;">
												<tr style="margin-left: auto; margin-right: auto;">
													<c:forEach begin="0" end="4">
														<td><c:if test="${count <= fn:length(listViewed)-1}">
																<div style="width: 218px; height: 400px;">
																	<div class="card" style="width: 218px; height: 390px;">
																		<img class="card-img-top"
																			src="<c:url value="${imgViewed[count].img}"/>"
																			alt="Card image" height="200px"
																			style="height: 200px;">
																		<div class="card-body"
																			style="display: flex; flex-direction: column; justify-content: space-between;">
																			<a class="card-title" style="text-align: center;"
																				href="./product?proid=${listViewed[count].pro_id}">${listViewed[count].pro_name}</a>
																			<p class="card-text" style="text-align: center;">${listViewed[count].pro_cost}
																				&#8363</p>
																			<div
																				style="display: flex; flex-direction: row; justify-content: space-between;">
																				<a href="#" class="btn btn-primary"
																					style="font-size: 12px;">More detail</a>
																				<button type="button" class="btn btn-success"
																					style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																					onclick="addCart(${product.pro_id}, 0)">Add
																					to cart</button>
																			</div>
																		</div>
																	</div>
																</div>
															</c:if></td>
														<c:set var="count" value="${count+1}"></c:set>
													</c:forEach>
												</tr>
											</table>
										</div>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
								</c:forEach>
							</c:if>
						</c:if>
					</div>

					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#viewed" data-slide="prev"
						style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-left: 3px;">
						<span class="carousel-control-prev-icon"></span>

					</a> <a class="carousel-control-next" href="#viewed" data-slide="next"
						style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-right: 3px;">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>

			</div>
		</c:if>

		<!-- RELATE ITEMS -->
		<%-- <c:if test="${currentUser != null}"> --%>
		<!-- show -->
		<div class="container" style="background: none; margin-top: 50px;">
			<div id="relate" class="carousel slide" data-ride="carousel"
				style="background: none;">

				<!-- Indicators -->
				<c:set var="first" value="0"></c:set>
				<ul class="carousel-indicators carousel-indicators-numbers"
					style="margin-bottom: 0px;">
					<c:if test="${fn:length(listRelate) % 5 != 0}">
						<c:forEach begin="0" end="${evenRelate}">
							<c:if test="${first == 0}">
								<li data-target="#relate" data-slide-to="${first}"
									class="active"
									style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
							</c:if>
							<c:if test="${first != 0}">
								<li data-target="#relate" data-slide-to="${first}"
									style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
							</c:if>
							<c:set var="first" value="${first+1}"></c:set>
						</c:forEach>
					</c:if>
					<c:if test="${fn:length(listRelate) % 5 == 0}">
						<c:if test="${evenRelate > 0}">
							<c:forEach begin="0" end="${evenRelate-1}">
								<c:if test="${first == 0}">
									<li data-target="#relate" data-slide-to="${first}"
										class="active"
										style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
								</c:if>
								<c:if test="${first != 0}">
									<li data-target="#relate" data-slide-to="${first}"
										style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
								</c:if>
								<c:set var="first" value="${first+1}"></c:set>
							</c:forEach>
						</c:if>
					</c:if>
				</ul>

				<!-- RELATE ITEMS -->
				<div class="carousel-inner" style="height: 460px;">
					<h4>
						<spring:message code="view.h44"></spring:message>
					</h4>
					<c:set var="first" value="0"></c:set>
					<c:set var="count" value="0"></c:set>
					<c:if test="${fn:length(listRelate) % 5 != 0}">
						<c:forEach begin="0" end="${evenRelate}">
							<c:if test="${first == 0}">
								<div class="carousel-item active" style="background: none;">
									<table
										style="margin-left: auto; margin-right: auto; width: inherit;">
										<tr style="margin-left: auto; margin-right: auto;">
											<c:forEach begin="0" end="4">
												<td><c:if test="${count <= fn:length(listRelate)-1}">
														<div style="width: 218px; height: 400px;">
															<div class="card" style="width: 218px; height: 390px;">
																<img class="card-img-top"
																	src="<c:url value="${imgRelate[count].img}"/>"
																	alt="Card image" style="height: 200px;">
																<div class="card-body"
																	style="display: flex; flex-direction: column; justify-content: space-between;">
																	<a class="card-title"
																		style="text-align: center; text-decoration: none;"
																		href="./product?proid=${listRelate[count].pro_id}">${listRelate[count].pro_name}</a>
																	<p class="card-text" style="text-align: center;">${listRelate[count].pro_cost}
																		&#8363</p>
																	<div
																		style="display: flex; flex-direction: row; justify-content: space-between;">
																		<a href="#" class="btn btn-primary"
																			style="font-size: 12px; width: 86px;">Details</a>
																		<c:if test="${currentUser == null}">
																			<button type="button" class="btn btn-success"
																				style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																				onclick="addCart(${listRelate[count].pro_id}, 1)">Add
																				to cart</button>
																		</c:if>
																		<c:if test="${currentUser != null}">
																			<button type="button" class="btn btn-success"
																				style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																				onclick="addCart(${listRelate[count].pro_id}, 0)">Add
																				to cart</button>
																		</c:if>
																	</div>
																</div>
															</div>
														</div>
													</c:if></td>
												<c:set var="count" value="${count+1}"></c:set>
											</c:forEach>
										</tr>
									</table>
								</div>
							</c:if>
							<c:if test="${first != 1}">
								<div class="carousel-item" style="background: none;">
									<table
										style="margin-left: auto; margin-right: auto; width: inherit;">
										<tr style="margin-left: auto; margin-right: auto;">
											<c:forEach begin="0" end="4">
												<td><c:if test="${count <= fn:length(listRelate)-1}">
														<div style="width: 218px; height: 400px;">
															<div class="card" style="width: 218px; height: 390px;">
																<img class="card-img-top"
																	src="<c:url value="${imgRelate[count].img}"/>"
																	alt="Card image" height="200px" style="height: 200px;">
																<div class="card-body"
																	style="display: flex; flex-direction: column; justify-content: space-between;">
																	<a class="card-title" style="text-align: center;"
																		href="./product?proid=${listRelate[count].pro_id}">${listRelate[count].pro_name}</a>
																	<p class="card-text" style="text-align: center;">${listRelate[count].pro_cost}
																		&#8363</p>
																	<div
																		style="display: flex; flex-direction: row; justify-content: space-between;">
																		<a href="#" class="btn btn-primary"
																			style="font-size: 12px;">More detail</a>
																		<c:if test="${currentUser == null}">
																			<button type="button" class="btn btn-success"
																				style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																				onclick="addCart(${listRelate[count].pro_id}, 1)">Add
																				to cart</button>
																		</c:if>
																		<c:if test="${currentUser != null}">
																			<button type="button" class="btn btn-success"
																				style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																				onclick="addCart(${listRelate[count].pro_id}, 0)">Add
																				to cart</button>
																		</c:if>
																	</div>
																</div>
															</div>
														</div>
													</c:if></td>
												<c:set var="count" value="${count+1}"></c:set>
											</c:forEach>
										</tr>
									</table>
								</div>
							</c:if>
							<c:set var="first" value="${first+1}"></c:set>
						</c:forEach>
					</c:if>

					<c:if test="${fn:length(listRelate) % 5 == 0}">
						<c:if test="${evenRelate > 0}">
							<c:forEach begin="0" end="${evenRelate-1}">
								<c:if test="${first == 0}">
									<div class="carousel-item active" style="background: none;">
										<table
											style="margin-left: auto; margin-right: auto; width: inherit;">
											<tr style="margin-left: auto; margin-right: auto;">
												<c:forEach begin="0" end="4">
													<td><c:if test="${count <= fn:length(listRelate)-1}">
															<div style="width: 218px; height: 400px;">
																<div class="card" style="width: 218px; height: 390px;">
																	<img class="card-img-top"
																		src="<c:url value="${imgRelate[count].img}"/>"
																		alt="Card image" style="height: 200px;">
																	<div class="card-body"
																		style="display: flex; flex-direction: column; justify-content: space-between;">
																		<a class="card-title"
																			style="text-align: center; text-decoration: none;"
																			href="./product?proid=${listRelate[count].pro_id}">${listRelate[count].pro_name}</a>
																		<p class="card-text" style="text-align: center;">${listRelate[count].pro_cost}
																			&#8363</p>
																		<div
																			style="display: flex; flex-direction: row; justify-content: space-between;">
																			<a href="#" class="btn btn-primary"
																				style="font-size: 12px; width: 86px;">Details</a>
																			<c:if test="${currentUser == null}">
																				<button type="button" class="btn btn-success"
																					style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																					onclick="addCart(${listRelate[count].pro_id}, 1)">Add
																					to cart</button>
																			</c:if>
																			<c:if test="${currentUser != null}">
																				<button type="button" class="btn btn-success"
																					style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																					onclick="addCart(${listRelate[count].pro_id}, 0)">Add
																					to cart</button>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
														</c:if></td>
													<c:set var="count" value="${count+1}"></c:set>
												</c:forEach>
											</tr>
										</table>
									</div>
								</c:if>
								<c:if test="${first != 1}">
									<div class="carousel-item" style="background: none;">
										<table
											style="margin-left: auto; margin-right: auto; width: inherit;">
											<tr style="margin-left: auto; margin-right: auto;">
												<c:forEach begin="0" end="4">
													<td><c:if test="${count <= fn:length(listRelate)-1}">
															<div style="width: 218px; height: 400px;">
																<div class="card" style="width: 218px; height: 390px;">
																	<img class="card-img-top"
																		src="<c:url value="${imgRelate[count].img}"/>"
																		alt="Card image" height="200px" style="height: 200px;">
																	<div class="card-body"
																		style="display: flex; flex-direction: column; justify-content: space-between;">
																		<a class="card-title" style="text-align: center;"
																			href="./product?proid=${listRelate[count].pro_id}">${listRelate[count].pro_name}</a>
																		<p class="card-text" style="text-align: center;">${listRelate[count].pro_cost}
																			&#8363</p>
																		<div
																			style="display: flex; flex-direction: row; justify-content: space-between;">
																			<a href="#" class="btn btn-primary"
																				style="font-size: 12px;"><spring:message
																					code="view.a7"></spring:message></a>
																			<c:if test="${currentUser == null}">
																				<button type="button" class="btn btn-success"
																					style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																					onclick="addCart(${listRelate[count].pro_id}, 1)">Add
																					to cart</button>
																			</c:if>
																			<c:if test="${currentUser != null}">
																				<button type="button" class="btn btn-success"
																					style="font-size: 12px; width: 86px; border: 1px solid #1fc0a0;"
																					onclick="addCart(${listRelate[count].pro_id}, 0)">Add
																					to cart</button>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
														</c:if></td>
													<c:set var="count" value="${count+1}"></c:set>
												</c:forEach>
											</tr>
										</table>
									</div>
								</c:if>
								<c:set var="first" value="${first+1}"></c:set>
							</c:forEach>
						</c:if>
					</c:if>
				</div>

				<!-- Left and right controls -->
				<a class="carousel-control-prev" href="#relate" data-slide="prev"
					style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-left: 3px;">
					<span class="carousel-control-prev-icon"></span>

				</a> <a class="carousel-control-next" href="#relate" data-slide="next"
					style="width: 20px; height: 20px; margin-top: auto; margin-bottom: 225px; background-color: green; border-radius: 10px; margin-right: 3px;">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
		</div>
	</div>
	</div>
	<%-- </c:if> --%>
	<div id="searchResult"
		style="display: none; margin-top: 100px; margin-bottom: 80px;"
		class="container">
		<table id="searchResult-table" style="width: 100%;">
			<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
		</table>
	</div>

	<!-- Footer -->
	<div class="container-fluid" id="footer-top" style="margin-top: auto;">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p3"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shipping"></spring:message>
					</a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a>
					<a href="#"><spring:message code="footer.term"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p4"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a>
					<a href="../contactcontroller/contact"><spring:message
							code="footer.contactus"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p5"></spring:message>
					</p>
					<a href="https://www.lazada.vn">Lazada</a> <a
						href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
					<a href="https://www.amazon.com/">Amazon</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p6"></spring:message>
					</p>
					<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
						href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message
							code="footer.cash"></spring:message></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="address"></spring:message>
								</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="infomation"></spring:message>
								</p>
							</div>
						</div>
					</div>
					<div
						style="background-color: gray; color: black; text-align: center;">
						<p style="margin-top: auto; margin-bottom: auto;">
							<spring:message code="footer.compyright"></spring:message>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>