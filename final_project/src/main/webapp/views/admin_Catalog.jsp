<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Catalogue Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Catalog.css" />">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_Catalog.js"/>"></script>
<script src="https://code.jquery.com/jquery-3.4.0.js"
	integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo="
	crossorigin="anonymous"></script>
</head>
<body>
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header>
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<div style="width: 200px; display: flex;">
						<div style="margin: auto 0px;">
							<a style="font-size: 14px; text-decoration: none;"
								href="./categories?lang=vi"><img alt=""
								src="<c:url value="/resources/image/vn.png"/>"> VN</a> <a
								style="font-size: 14px; text-decoration: none;"
								href="./categories?lang=en"><img alt=""
								src="<c:url value="/resources/image/en.png"/>"> EN</a>
						</div>
						<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
						<div class="dropdown-menu" aria-labelledby="dropdownId">
							<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
						</div>
					</div>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout"><spring:message
								code="adPro.exit"></spring:message></a>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" style="display: flex;">
			<div class="wrapper d-flex justify content-between">
				<div id="side-bar">
					<div class="logo" style="text-align: center">
						<spring:message code="adPro.logo"></spring:message>
					</div>
					<ul class="list-group rounded-0">
						<li class="dashboard"><a
							href="../dashBoardController/dashBoard"><i
								class="fa fa-dashboard" style="font-size: 36px"></i> <spring:message
									code="adPro.db"></spring:message></a></li>
						<li style="background-color: steelblue;"><a
							href="../categoryController/categories"> <i
								class="fa fa-book mr-2"></i> <spring:message code="adPro.db1"></spring:message>
						</a></li>
						<li><a href="../productController/products"> <i
								class="fa fa-user mr-2"></i> <spring:message code="adPro.db2"></spring:message>
						</a></li>
						<li><a href="../userController/users"><i
								class="fa fa-cart-plus"></i> <spring:message code="adPro.db3"></spring:message></a></li>
						<li><a href="../messageController/messages"><i
								class="fa fa-tags"></i> <spring:message code="adPro.db4"></spring:message></a></li>
						<li><a href="../adminOrderListController/orders"> <i
								class="fa fa-cart-plus"></i> <spring:message code="adMe.db6"></spring:message></a></li>
						
					</ul>
				</div>
			</div>
			<div class="wrapper d-flex justify content-between"
				style="width: 100%;">
				<div id="wrapper-content" style="width: 100%;">
					<!-- Nút thêm và tìm kiếm -->
					<div style="margin-top: 60px;">
						<div id="wrapper-content" style="">
							<section class="control">
								<div class="row" style="margin-left: 1px; padding: 0 5px;">
									<div class="d-flex align-items-center ">
										<div class="input-group">
											<input type="text" class="form-control"
												placeholder="Name Catalog" id="searchName">
											<div class="input-group-prepend">
												<span class="input-group-text" id="btnTimNV"><i
													class="fa fa-search"></i></span>
											</div>
										</div>
									</div>
									<div class="align-items-center justify-content-end"
										style="display: flex; flex-direction: row; margin: auto; width: 400px;">
										<label style="width: 100px"><spring:message
												code="adPro.sort"></spring:message></label> <select
											style="width: 300px;" id="followSelect">
											<option value="name" onclick="selectClick()"><spring:message
													code="adPro.sortName"></spring:message></option>
											<option value="id" onclick="selectClick()"
												selected="selected"><spring:message
													code="adPro.sortID"></spring:message></option>
										</select>
										<form id="myRadioBtn" class="d-flex align-items-center m-2"
											action="">
											<input class="m-2" type="radio" name="gender" value="asc"
												onclick="radioClick()" checked="checked" id="ascRd">
											<spring:message code="adPro.sortI"></spring:message>
											<br> <input class="m-2" type="radio" name="gender"
												value="dsc" onclick="radioClick()" id="ascRd">
											<spring:message code="adPro.sortD"></spring:message>
											<br>
										</form>
									</div>
								</div>
							</section>
						</div>

						<!-- The Modal -->
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<div class="modal-content">

									<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title">
											<spring:message code="adCa.add"></spring:message>
										</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<!-- Modal body -->
									<div class="modal-body">
										<form:form action="./category" class="form-horizontal"
											method="post" modelAttribute="category">
											<form:input path="cate_id" type="hidden" />
											<div class="form-group">
												<label for="txtCateName" class="control-lable col-md-4"><spring:message
														code="adCa.name"></spring:message></label>
												<form:input type="text" path="cate_name"
													class="form-control" id="txtCateName"
													placeholder="Category name" />
											</div>
											<div class="form-group">
												<label for="txtStatus" class="control-lable col-md-4">Status</label>
												<form:input type="text" path="status" class="form-control"
													id="txtStatus" placeholder="Status" readonly="true" />
											</div>

											<div class="modal-footer">
												<div
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<button id="btnSaveInModal" type="submit"
														class="btn btn-primary" value="" name="btnSaveInModal">
														<spring:message code="adPro.save"></spring:message>
													</button>
												</div>
												<div
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<button type="button" class="btn btn-danger"
														data-dismiss="modal">
														<spring:message code="adPro.close"></spring:message>
													</button>
												</div>
											</div>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- </section> -->
					<%-- <button class="btn btn-primary" data-toggle="modal"
					data-target="#myModal" style="float: right;"
					onmouseenter="cleanAddModal()">
					<spring:message code="adPro.add"></spring:message>
				</button> --%>
					<button style="background-color: none; border: none"></button>
					<button class="btn btn-primary" data-toggle="modal"
						data-target="#myModal" style="float: right;"
						onmouseenter="cleanAddModal()">
						<spring:message code="adPro.add"></spring:message>
					</button>
					<!-- Tablle  -->


					<div id="categories" class="carousel slide" data-ride="carousel"
						style="background: none;" data-interval="false">
						<!-- Indicators -->
						<c:set var="first" value="0"></c:set>
						<ul class="carousel-indicators carousel-indicators-numbers"
							style="margin-bottom: 0px;">
							<c:if test="${fn:length(categories) % 6 != 0}">
								<c:forEach begin="0" end="${even}">
									<c:if test="${first == 0}">
										<li data-target="#categories" data-slide-to="${first}"
											class="active"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:if test="${first != 0}">
										<li data-target="#categories" data-slide-to="${first}"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
								</c:forEach>
							</c:if>
							<c:if test="${fn:length(categories) % 6 == 0}">
								<c:forEach begin="1" end="${even}">
									<c:if test="${first == 0}">
										<li data-target="#categories" data-slide-to="${first}"
											class="active"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:if test="${first != 0}">
										<li data-target="#categories" data-slide-to="${first}"
											style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
								</c:forEach>
							</c:if>
						</ul>

						<div class="carousel-inner" style="">
							<c:set var="first" value="0"></c:set>
							<c:set var="count1" value="0"></c:set>
							<c:set var="count" value="1"></c:set>
							<c:if test="${fn:length(categories) % 6 != 0}">
								<c:forEach begin="0" end="${even}">
									<c:if test="${first == 0}">
										<div class="carousel-item active" style="background: none;">
											<table class="table table-bordered" style="width: inherit;"
												id="categories-table${first}">
												<thead>
													<tr>
														<th><spring:message code="adPro.id"></spring:message></th>
														<th><spring:message code="adPro.name1"></spring:message></th>
														<th>Status</th>
														<th><spring:message code="adPro.action"></spring:message></th>
													</tr>
												</thead>
												<tbody id="tblDialog">
													<c:forEach begin="0" end="5">
														<c:if test="${count1 <= fn:length(categories)-1}">
															<tr>
																<td>${categories[count1].cate_id}</td>
																<td>${categories[count1].cate_name}</td>
																<td>${categories[count1].status}</td>
																<td><button data-toggle="modal"
																		data-target="#myModal" class="btn btn-primary"
																		type="button"
																		onclick="update(this, ${categories[count1].cate_id})"
																		value="${categories[count1].cate_name}">
																		<spring:message code="adPro.update"></spring:message>
																	</button> <c:if
																		test="${categories[count1].status == 'activated'}">
																		<button class="btn btn-warning"
																			onclick="block(${categories[count1].cate_id}, ${count}, this, ${first})">Block</button>
																	</c:if> <c:if test="${categories[count1].status == 'blocked'}">
																		<button class="btn btn-warning"
																			onclick="activate(${categories[count1].cate_id}, ${count}, this, ${first})">Activate</button>
																	</c:if>
																	<button class="btn btn-danger" type="button"
																		onclick="deleteCategory(this, ${categories[count1].cate_id}, ${first})"
																		value="${count}">
																		<spring:message code="adPro.del"></spring:message>
																	</button></td>
															</tr>
															<c:set var="count1" value="${count1+1}"></c:set>
															<c:set var="count" value="${count+1}"></c:set>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:if>
									<c:if test="${first != 0}">
										<div class="carousel-item" style="background: none;">
											<table class="table table-bordered" style="width: inherit;"
												id="categories-table${first}">
												<thead>
													<tr>
														<th><spring:message code="adPro.id"></spring:message></th>
														<th><spring:message code="adPro.name1"></spring:message></th>
														<th>Status</th>
														<th><spring:message code="adPro.action"></spring:message></th>
													</tr>
												</thead>
												<tbody id="tblDialog">
													<c:forEach begin="0" end="5">
														<c:if test="${count1 <= fn:length(categories)-1}">
															<tr>
																<td>${categories[count1].cate_id}</td>
																<td>${categories[count1].cate_name}</td>
																<td>${categories[count1].status}</td>
																<td><button data-toggle="modal"
																		data-target="#myModal" class="btn btn-primary"
																		type="button"
																		onclick="update(this, ${categories[count1].cate_id})"
																		value="${categories[count1].cate_name}">
																		<spring:message code="adPro.update"></spring:message>
																	</button> <c:if
																		test="${categories[count1].status == 'activated'}">
																		<button class="btn btn-warning"
																			onclick="block(${categories[count1].cate_id}, ${count}, this, ${first})">Block</button>
																	</c:if> <c:if test="${categories[count1].status == 'blocked'}">
																		<button class="btn btn-warning"
																			onclick="activate(${categories[count1].cate_id}, ${count}, this, ${first})">Activate</button>
																	</c:if>
																	<button class="btn btn-danger" type="button"
																		onclick="deleteCategory(this, ${categories[count1].cate_id}, ${first})"
																		value="${count}">
																		<spring:message code="adPro.del"></spring:message>
																	</button></td>
															</tr>
															<c:set var="count1" value="${count1+1}"></c:set>
															<c:set var="count" value="${count+1}"></c:set>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
									<c:set var="count" value="1"></c:set>
								</c:forEach>
							</c:if>
							<c:if test="${fn:length(categories) % 6 == 0}">
								<c:forEach begin="1" end="${even}">
									<c:if test="${first == 0}">
										<div class="carousel-item active" style="background: none;">
											<table class="table table-bordered" style="width: inherit;"
												id="categories-table${first}">
												<thead>
													<tr>
														<th><spring:message code="adPro.id"></spring:message></th>
														<th><spring:message code="adPro.name1"></spring:message></th>
														<th>Status</th>
														<th><spring:message code="adPro.action"></spring:message></th>
													</tr>
												</thead>
												<tbody id="tblDialog">
													<c:forEach begin="0" end="5">
														<c:if test="${count1 <= fn:length(categories)-1}">
															<tr>
																<td>${categories[count1].cate_id}</td>
																<td>${categories[count1].cate_name}</td>
																<td>${categories[count1].status}</td>
																<td><button data-toggle="modal"
																		data-target="#myModal" class="btn btn-primary"
																		type="button"
																		onclick="update(this, ${categories[count1].cate_id})"
																		value="${categories[count1].cate_name}">
																		<spring:message code="adPro.update"></spring:message>
																	</button> <c:if
																		test="${categories[count1].status == 'activated'}">
																		<button class="btn btn-warning"
																			onclick="block(${categories[count1].cate_id}, ${count}, this, ${first})">Block</button>
																	</c:if> <c:if test="${categories[count1].status == 'blocked'}">
																		<button class="btn btn-warning"
																			onclick="activate(${categories[count1].cate_id}, ${count}, this, ${first})">Activate</button>
																	</c:if>
																	<button class="btn btn-danger" type="button"
																		onclick="deleteCategory(this, ${categories[count1].cate_id}, ${first})"
																		value="${count}">
																		<spring:message code="adPro.del"></spring:message>
																	</button></td>
															</tr>
															<c:set var="count1" value="${count1+1}"></c:set>
															<c:set var="count" value="${count+1}"></c:set>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:if>
									<c:if test="${first != 0}">
										<div class="carousel-item" style="background: none;">
											<table class="table table-bordered" style="width: inherit;"
												id="categories-table${first}">
												<thead>
													<tr>
														<th><spring:message code="adPro.id"></spring:message></th>
														<th><spring:message code="adPro.name1"></spring:message></th>
														<th>Status</th>
														<th><spring:message code="adPro.action"></spring:message></th>
													</tr>
												</thead>
												<tbody id="tblDialog">
													<c:forEach begin="0" end="5">
														<c:if test="${count1 <= fn:length(categories)-1}">
															<tr>
																<td>${categories[count1].cate_id}</td>
																<td>${categories[count1].cate_name}</td>
																<td>${categories[count1].status}</td>
																<td><button data-toggle="modal"
																		data-target="#myModal" class="btn btn-primary"
																		type="button"
																		onclick="update(this, ${categories[count1].cate_id})"
																		value="${categories[count1].cate_name}">
																		<spring:message code="adPro.update"></spring:message>
																	</button> <c:if
																		test="${categories[count1].status == 'activated'}">
																		<button class="btn btn-warning"
																			onclick="block(${categories[count1].cate_id}, ${count}, this, ${first})">Block</button>
																	</c:if> <c:if test="${categories[count1].status == 'blocked'}">
																		<button class="btn btn-warning"
																			onclick="activate(${categories[count1].cate_id}, ${count}, this, ${first})">Activate</button>
																	</c:if>
																	<button class="btn btn-danger" type="button"
																		onclick="deleteCategory(this, ${categories[count1].cate_id}, ${first})"
																		value="${count}">
																		<spring:message code="adPro.del"></spring:message>
																	</button></td>
															</tr>
															<c:set var="count1" value="${count1+1}"></c:set>
															<c:set var="count" value="${count+1}"></c:set>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:if>
									<c:set var="first" value="${first+1}"></c:set>
									<c:set var="count" value="1"></c:set>
								</c:forEach>
							</c:if>
						</div>
					</div>

					<div id="searchResult" style="display: none;">
						<table class="table table-bordered" style="width: 100%;"
							id="userSearchTable">
							<thead>
								<tr>
									<th><spring:message code="adPro.id"></spring:message></th>
									<th><spring:message code="adPro.name1"></spring:message></th>
									<th>Status</th>
									<th><spring:message code="adPro.action"></spring:message></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>

					<script type="text/javascript">
					function deleteCategory(button, cate_id, orderTable) {
						$
								.ajax({
									url : "./category/" + cate_id,
									type : "DELETE",
									data : "",
									contentType : "application/json",
									success : function() {
										var table = document
												.getElementById("categories-table"+orderTable);
										table.deleteRow(button.value);
										//
										var tr = table
												.getElementsByTagName("tr");
										for (var i = 1; i < tr.length; i++) {
											tr[i]
													.getElementsByTagName("button")[1].value = i;
										}
									}
								});
					}
					function update(button, cate_id) {
						$.ajax({
							url : "./getcategory/"+cate_id,
							type : "GET",
							data : '',
							contentType : "application/json",
							success : function (data) {
								document.getElementById("btnSaveInModal").value = "update-"
									+ cate_id;
							document.getElementById("txtCateName").value = data.cate_name;
							document.getElementById("txtStatus").value = data.status;
								},
						async : false,
						error : function(e) {
							console.log(e);
						}
							});
					}
					function block(cateId, order, button, orderTable) {
						$
								.ajax({
									url : "./block/" + cateId,
									type : "POST",
									data : '',
									contentType : "application/json",
									success : function(data) {
										var cateTable = document
												.getElementById("categories-table"+orderTable);
										cateTable.getElementsByTagName("tr")[order]
												.getElementsByTagName("td")[2].innerHTML = "blocked";
										button.innerHTML = "Activate";
										var func = function(cateId1, order1,
												button1, orderTable1) {
											return function() {
												activate(cateId1, order1,
														button1, orderTable1);
											}
										}
										button.onclick = func(cateId, order,
												button, orderTable);
									}
								});
					}

					function activate(cateId, order, button, orderTable) {
						$
								.ajax({
									url : "./activate/" + cateId,
									type : "POST",
									data : '',
									contentType : "application/json",
									success : function(data) {
										var cateTable = document
												.getElementById("categories-table"+orderTable);
										cateTable.getElementsByTagName("tr")[order]
												.getElementsByTagName("td")[2].innerHTML = "activated";
										button.innerHTML = "Block";
										var func = function(cateId1, order1,
												button1, orderTable1) {
											return function() {
												block(cateId1, order1, button1, orderTable1);
											}
										}
										button.onclick = func(cateId, order,
												button, orderTable);
									}
								});
					}
				</script>
				</div>
			</div>
		</div>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You are not allowed to access this
				feature!!!
			</div>
		</div>
	</c:if>
</body>
</html>