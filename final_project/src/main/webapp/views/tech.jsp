<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">
</head>
<script type="text/javascript"
	src="<c:url value="/resources/js/homepagelistproduct.js"/>"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/homepage.css"/>">
<script type="text/javascript"
	src="<c:url value="/resources/js/tech.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<body onload="pagination(${fn:length(num)},${startpoint})">
	<div id="header" style="">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i> <spring:message
												code="adPro.cate"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<c:forEach items="${viewCategories}" var="category">
												<a class="dropdown-item" href="#">${category.cate_name}</a>
											</c:forEach>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i> <spring:message
											code="cart.contact"></spring:message></a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i> <spring:message
											code="cart.lang"></spring:message></a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">
										<spring:message code="cart.sign"></spring:message>
									</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link"
						href="../viewProductController/product?proid=${pro.pro_id}&lang=vi">VN</a></li>
					<li class="nav-item"><a class="nav-link"
						href="../viewProductController/product?proid=${pro.pro_id}&lang=en">EN</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">

					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>

			<c:if test="${currentUser == null}">
				<button onclick="location.href='../loginRegisterController/login'" class="btn"
					id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${currentUser.user_name}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="../orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a>
						<button class="dropdown-item" onclick="logOut()">
							<spring:message code="cart.logout"></spring:message>
						</button>
					</div>
				</div>

			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart-guest">0</span></i>
				</button>
			</c:if>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>
	<div id="content" style="margin-top: 100px">
		<c:set value="0" var="count"></c:set>
		<div class="container">
			<div class="col-lg-12">
				<h1 class="text-center">
					<spring:message code="tech.h1"></spring:message>
				</h1>
				<table id="techs-table">
					<c:if test="${fn:length(listtech) % 4 != 0}">
						<c:forEach begin="0" end="${evenTr}">
							<tr>
								<c:forEach begin="0" end="3">
									<c:if test="${listtech[count] != null}">
										<th>
											<div id="infortechif1" class="card"
												style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a
														href="/final_project/viewProductController/product?proid=${listtech[count].pro_id}">
														<img class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap" />
													</a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="tech.name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="tech.price"></spring:message>
														: ${listtech[count].pro_cost}
													</p>
												</div>
											</div>
										</th>
									</c:if>
									<c:set value="${count+1}" var="count"></c:set>
								</c:forEach>
							</tr>

						</c:forEach>
					</c:if>

					<c:if test="${fn:length(listtech) % 4 == 0}">
						<c:forEach begin="0" end="${evenTr-1}">
							<tr>
								<c:forEach begin="0" end="3">
									<c:if test="${listtech[count] != null}">
										<th>
											<div id="infortechif1" class="card"
												style="width: 280px; height: 350px">
												<div style="width: 200x; height: 200px">
													<a
														href="/final_project/viewProductController/product?proid=${listtech[count].pro_id }">
														<img class="card-img-top"
														src="<c:url value="${listimg_tech[count].img }"/>"
														width="270" height="200" alt="Card image cap" />
													</a>
												</div>
												<div class="card-body">
													<p class="card-text">
														<spring:message code="tech.name"></spring:message>
														: ${listtech[count].pro_name }
													</p>
													<p class="card-text">
														<spring:message code="tech.price"></spring:message>
														: ${listtech[count].pro_cost}
													</p>
												</div>
											</div>
										</th>
									</c:if>
									<c:set value="${count+1}" var="count"></c:set>
								</c:forEach>
							</tr>
						</c:forEach>
					</c:if>


				</table>
				<div>
					<ul class="pagination" id="tech-pagination">


					</ul>
				</div>

			</div>

		</div>
	</div>


	<div class="container-fluid" id="footer-top" style="margin-top: auto;">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p3"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shipping"></spring:message>
					</a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a>
					<a href="#"><spring:message code="footer.term"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p4"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a>
					<a href="../contactcontroller/contact"><spring:message
							code="footer.contactus"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p5"></spring:message>
					</p>
					<a href="https://www.lazada.vn">Lazada</a> <a
						href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
					<a href="https://www.amazon.com/">Amazon</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p6"></spring:message>
					</p>
					<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
						href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message
							code="footer.cash"></spring:message></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="address"></spring:message>
								</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="infomation"></spring:message>
								</p>
							</div>
						</div>
					</div>
					<div
						style="background-color: gray; color: black; text-align: center;">
						<p style="margin-top: auto; margin-bottom: auto;">
							<spring:message code="footer.compyright"></spring:message>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>