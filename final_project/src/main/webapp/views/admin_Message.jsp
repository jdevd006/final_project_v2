<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Message Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Account.css" />">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/admin_Message.js" />"></script>
</head>
<body>
	<c:if test="${currentUser.account_role.role == 'admin'}">
		<header style="float: left;">
			<nav class="navbar navbar-expand-sm">
				<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
				<button class="navbar-toggler d-lg-none" type="button"
					data-toggle="collapse" data-target="#collapsibleNavId"
					aria-controls="collapsibleNavId" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavId">
					<ul class="navbar-nav ">
						<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
						<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
					</ul>
				</div>
				<div class="nav-item dropdown">
					<div style="width: 200px; display: flex;">
						<div style="margin: auto 0px;">
							<a style="font-size: 14px; text-decoration: none;"
								href="./messages?lang=vi"><img alt=""
								src="<c:url value="/resources/image/vn.png"/>"> VN</a> <a
								style="font-size: 14px; text-decoration: none;"
								href="./messages?lang=en"><img alt=""
								src="<c:url value="/resources/image/en.png"/>"> EN</a>
						</div>
						<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
						<div class="dropdown-menu" aria-labelledby="dropdownId">
							<a class="dropdown-item" href="../loginRegisterController/logout">Exit</a>
						</div>
					</div>
					<div class="dropdown-menu" aria-labelledby="dropdownId">
						<a class="dropdown-item" href="../loginRegisterController/logout"><spring:message
								code="adPro.exit"></spring:message></a>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" style="display: flex;">
			<div class="wrapper d-flex justify content-between">
				<div id="side-bar">
					<div class="logo" style="text-align: center">
						<spring:message code="adPro.logo"></spring:message>
					</div>
					<ul class="list-group rounded-0">
						<li class="dashboard"><a
							href="../dashBoardController/dashBoard"><i
								class="fa fa-dashboard" style="font-size: 36px"></i> <spring:message
									code="adPro.db"></spring:message></a></li>
						<li><a href="../categoryController/categories"> <i
								class="fa fa-book mr-2"></i> <spring:message code="adPro.db1"></spring:message>
						</a></li>
						<li><a href="../productController/products"> <i
								class="fa fa-user mr-2"></i> <spring:message code="adPro.db2"></spring:message>
						</a></li>
						<li><a href="../userController/users"><i
								class="fa fa-cart-plus"></i> <spring:message code="adPro.db3"></spring:message></a></li>
						<li style="background-color: steelblue;"><a
							href="../messageController/messages"><i class="fa fa-tags"></i>
								<spring:message code="adPro.db4"></spring:message></a></li>
						<li><a href="../adminOrderListController/orders"> <i
								class="fa fa-cart-plus"></i> <spring:message code="adMe.db6"></spring:message></a></li>
						
					</ul>
				</div>
			</div>

			<div style="margin-top: 60px; margin-left: 1px; width: 100%;">
				<div id="wrapper-content" style="width: inherit;">
					<section class="control">
						<div class="row" style="margin-left: 1px; padding: 0 5px;">
							<div class="d-flex align-items-center ">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="<spring:message code ="adMe.phd.mess"></spring:message>"
										id="searchName" value="">
									<div class="input-group-prepend">
										<span class="input-group-text" id="btnTimNV"><i
											class="fa fa-search"></i></span>
									</div>
								</div>
							</div>
							<div class="align-items-center justify-content-end"
								style="display: flex; flex-direction: row; margin: auto; width: 400px;">
								<label style="width: 100px"><spring:message
										code="adPro.sort"></spring:message></label> <select id="followSelect"
									style="width: 300px;">
									<option value="name" onclick="selectClick()"><spring:message
											code="adPro.sortName"></spring:message></option>
									<option value="id" onclick="selectClick()"><spring:message
											code="adPro.sortID"></spring:message></option>
									<option value="unread" onclick="selectClick()"
										selected="selected">Follow Unread</option>
									<option value="read" onclick="selectClick()">Follow
										Read</option>
								</select>
								<%-- <form id="myRadioBtn" class="d-flex align-items-center m-2"
								action="">
								<input class="m-2" type="radio" name="gender" value="asc"
									onclick="radioClick()" checked="checked" id="ascRd">
								<spring:message code="adPro.sortI"></spring:message>
								<br> <input class="m-2" type="radio" name="gender"
									value="dscRd" onclick="radioClick()" id="dscRd">
								<spring:message code="adPro.sortD"></spring:message>
								<br>
							</form> --%>
							</div>
						</div>

						<!-- <div style="display: none;" id="test"> -->
						<h2 style="margin-left: 10px;">
							<spring:message code="adMe.h2"></spring:message>
						</h2>
						<div class="modal fade" id="myModal">
							<div class="modal-dialog">
								<form:form action="./message" method="post"
									modelAttribute="message">
									<div class="modal-content">

										<!-- Modal Header -->
										<div class="modal-header">
											<h4 class="modal-title">
												<spring:message code="adMe.h4"></spring:message>
											</h4>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<fieldset
												style="display: flex; flex-direction: column; justify-content: space-around;">
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtMessageName"><spring:message
															code="adMe.name"></spring:message></label>
													<form:input type="text" path="name" class="form-control"
														id="txtMessageName" placeholder="Name"
														style="width: 300px;" readonly="true" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtMessageEmail"><spring:message
															code="adMe.email"></spring:message></label>
													<form:input type="text" path="email" class="form-control"
														id="txtMessageEmail" placeholder="Email"
														style="width: 300px;" readonly="true" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtMessgeAsk"><spring:message
															code="adMe.ques"></spring:message></label>
													<form:input type="text" placeholder="Question"
														style="width: 300px;" path="question" id="txtMessgeAsk"
														class="form-control" readonly="true" />
												</div>
												<div class="control-group"
													style="display: flex; flex-direction: row; justify-content: space-between;">
													<label class="control-label" for="txtMessageAns"><spring:message
															code="adMe.ans"></spring:message></label>
													<form:input type="text" placeholder="Answer"
														style="width: 300px;" path="answer" id="txtMessageAns"
														class="form-control" />
												</div>
											</fieldset>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary"
												name="btnSaveInModal" value="" id="btnSaveInModal">
												<spring:message code="adPro.save"></spring:message>
											</button>
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">
												<spring:message code="adPro.close"></spring:message>
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
						<!-- </div> -->

						<div id="messages" class="carousel slide" data-ride="carousel"
							style="background: none;" data-interval="false">
							<!-- Indicators -->
							<c:set var="first" value="0"></c:set>
							<ul class="carousel-indicators carousel-indicators-numbers"
								style="margin-bottom: 0px;">
								<c:if test="${fn:length(messages) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#messages" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#messages" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(messages) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<li data-target="#messages" data-slide-to="${first}"
												class="active"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:if test="${first != 0}">
											<li data-target="#messages" data-slide-to="${first}"
												style="text-align: center; text-indent: 0; width: 30px; height: 30px; border: none; border-radius: 100%; line-height: 30px; background-color: grey; color: white;">${first}</li>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
									</c:forEach>
								</c:if>
							</ul>

							<div class="carousel-inner" style="">
								<c:set var="first" value="0"></c:set>
								<c:set var="count1" value="0"></c:set>
								<c:set var="count" value="1"></c:set>
								<c:if test="${fn:length(messages) % 6 != 0}">
									<c:forEach begin="0" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="messages-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adMe.mess"></spring:message></th>
															<th><spring:message code="adMe.id"></spring:message></th>
															<th><spring:message code="adMe.sender"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adMe.ques"></spring:message></th>
															<th><spring:message code="adMe.ans"></spring:message></th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(messages)-1}">
																<tr>
																	<td>${messages[count1].id}</td>
																	<c:if
																		test="${messages[count1].account_detail.user_id == null}">
																		<td>GUEST</td>
																	</c:if>
																	<c:if
																		test="${messages[count1].account_detail.user_id != null}">
																		<td>${messages[count1].account_detail.user_id}</td>
																	</c:if>
																	<td>${messages[count1].name}</td>
																	<td>${messages[count1].email}</td>
																	<td>${messages[count1].question}</td>
																	<td>${messages[count1].answer}</td>
																	<td><c:if
																			test="${messages[count1].status == 'unread'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style="">New</span>
																			</button>
																		</c:if> <c:if test="${messages[count1].status == 'read'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style=""></span>
																			</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteMessage(this, ${messages[count1].id}, ${first})"
																			value="${count}">
																			<spring:message code="adMe.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="messages-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adMe.mess"></spring:message></th>
															<th><spring:message code="adMe.id"></spring:message></th>
															<th><spring:message code="adMe.sender"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adMe.ques"></spring:message></th>
															<th><spring:message code="adMe.ans"></spring:message></th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(messages)-1}">
																<tr>
																	<td>${messages[count1].id}</td>
																	<c:if
																		test="${messages[count1].account_detail.user_id == null}">
																		<td>GUEST</td>
																	</c:if>
																	<c:if
																		test="${messages[count1].account_detail.user_id != null}">
																		<td>${messages[count1].account_detail.user_id}</td>
																	</c:if>
																	<td>${messages[count1].name}</td>
																	<td>${messages[count1].email}</td>
																	<td>${messages[count1].question}</td>
																	<td>${messages[count1].answer}</td>
																	<td><c:if
																			test="${messages[count1].status == 'unread'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style="">New</span>
																			</button>
																		</c:if> <c:if test="${messages[count1].status == 'read'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style=""></span>
																			</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteMessage(this, ${messages[count1].id}, ${first})"
																			value="${count}">
																			<spring:message code="adMe.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(messages) % 6 == 0}">
									<c:forEach begin="1" end="${even}">
										<c:if test="${first == 0}">
											<div class="carousel-item active" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="messages-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adMe.mess"></spring:message></th>
															<th><spring:message code="adMe.id"></spring:message></th>
															<th><spring:message code="adMe.sender"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adMe.ques"></spring:message></th>
															<th><spring:message code="adMe.ans"></spring:message></th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(messages)-1}">
																<tr>
																	<td>${messages[count1].id}</td>
																	<c:if
																		test="${messages[count1].account_detail.user_id == null}">
																		<td>GUEST</td>
																	</c:if>
																	<c:if test="${message.account_detail.user_id != null}">
																		<td>${messages[count1].account_detail.user_id}</td>
																	</c:if>
																	<td>${messages[count1].name}</td>
																	<td>${messages[count1].email}</td>
																	<td>${messages[count1].question}</td>
																	<td>${messages[count1].answer}</td>
																	<td><c:if test="${message.status == 'unread'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style="">New</span>
																			</button>
																		</c:if> <c:if test="${messages[count1].status == 'read'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style=""></span>
																			</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteMessage(this, ${messages[count1].id}, ${first})"
																			value="${count}">
																			<spring:message code="adMe.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${first != 0}">
											<div class="carousel-item" style="background: none;">
												<table class="table table-bordered" style="width: inherit;"
													id="messages-table${first}">
													<thead>
														<tr>
															<th><spring:message code="adMe.mess"></spring:message></th>
															<th><spring:message code="adMe.id"></spring:message></th>
															<th><spring:message code="adMe.sender"></spring:message></th>
															<th><spring:message code="adMe.email"></spring:message></th>
															<th><spring:message code="adMe.ques"></spring:message></th>
															<th><spring:message code="adMe.ans"></spring:message></th>
															<th><spring:message code="adMe.action"></spring:message></th>
														</tr>
													</thead>
													<tbody id="tblDialog">
														<c:forEach begin="0" end="5">
															<c:if test="${count1 <= fn:length(messages)-1}">
																<tr>
																	<td>${messages[count1].id}</td>
																	<c:if
																		test="${messages[count1].account_detail.user_id == null}">
																		<td>GUEST</td>
																	</c:if>
																	<c:if
																		test="${messages[count1].account_detail.user_id != null}">
																		<td>${message.account_detail.user_id}</td>
																	</c:if>
																	<td>${messages[count1].name}</td>
																	<td>${messages[count1].email}</td>
																	<td>${messages[count1].question}</td>
																	<td>${messages[count1].answer}</td>
																	<td><c:if
																			test="${messages[count1].status == 'unread'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${messages[count1].id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style="">New</span>
																			</button>
																		</c:if> <c:if test="${messages[count1].status == 'read'}">
																			<button class="btn btn-success" type="button"
																				onclick="answer(this, ${message.id})"
																				data-toggle="modal" data-target="#myModal">
																				<spring:message code="adMe.ans"></spring:message>
																				<span class="text text-warning" style=""></span>
																			</button>
																		</c:if>
																		<button class="btn btn-danger" type="button"
																			onclick="deleteMessage(this, ${messages[count1].id}, ${first})"
																			value="${count}">
																			<spring:message code="adMe.del"></spring:message>
																		</button></td>
																</tr>
																<c:set var="count1" value="${count1+1}"></c:set>
																<c:set var="count" value="${count+1}"></c:set>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:set var="first" value="${first+1}"></c:set>
										<c:set var="count" value="1"></c:set>
									</c:forEach>
								</c:if>
							</div>
						</div>

						<div id="searchResult" style="display: none;">
							<table class="table table-bordered" style="width: 100%;"
								id="userSearchTable">
								<thead>
									<tr>
										<th><spring:message code="adMe.mess"></spring:message></th>
										<th><spring:message code="adMe.id"></spring:message></th>
										<th><spring:message code="adMe.sender"></spring:message></th>
										<th><spring:message code="adMe.email"></spring:message></th>
										<th><spring:message code="adMe.ques"></spring:message></th>
										<th><spring:message code="adMe.ans"></spring:message></th>
										<th><spring:message code="adMe.action"></spring:message></th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		</div>
		<script>
		function deleteMessage(button, messageId, orderTable) {
			$.ajax({
				url : "./message/" + messageId,
				type : "DELETE",
				data : "",
				contentType : "application/json",
				success : function() {
					var table = document.getElementById("messages-table"+orderTable);
					table.deleteRow(button.value);
					//
					var tr = table.getElementsByTagName("tr");
					for (var i = 1; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[1].value = i;
					}
				}
			});
		}
		function answer(button, messageId) {
			$
					.ajax({
						url : "./message?messageId=" + messageId,
						type : "GET",
						data : "",
						contentType : "application/json",
						success : function(data) {
							console.log(data.name);
							document.getElementById("btnSaveInModal").value = messageId;

							document.getElementById("txtMessageName").value = data.name;
							document.getElementById("txtMessageEmail").value = data.email;
							document.getElementById("txtMessgeAsk").value = data.question;
							document.getElementById("txtMessageAns").value = data.answer;

							button.innerHTML = "Answer";
						},
						async : false,
						error : function(e) {
							console.log(e);
						}
					});
		}
	</script>
	</c:if>

	<c:if
		test="${currentUser.account_role.role == 'user' || currentUser == null}">
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert"
				onclick="location.href='../homecontroller/homepage'"
				style="margin-top: 9px;">Come back TATALO</button>
			<div style="text-align: center; font-size: 30px;">
				<strong>Danger!</strong> You are not allowed to access this
				feature!!!
			</div>
		</div>
	</c:if>
</body>
</html>