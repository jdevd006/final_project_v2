﻿
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/searchFunction.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/contactPage.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/contact.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/logout.js" />"></script>
<title>Contact Page</title>
</head>
<body style="background-color: #F4F4F4;"
	onload="setNumItemCartGuest();setUser(${currentUser.user_id});setNumItemCartGuest()">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>
									<spring:message code="header.Home"></spring:message></a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>
										<spring:message code="header.Category"></spring:message></a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="../techcontroller/techs"><spring:message
													code="header.CateTech"></spring:message></a> <a
												class="dropdown-item" href="../fashioncontroller/fashions"><spring:message
													code="header.CateFashion"></spring:message></a> <a
												class="dropdown-item" href="../bookcontroller/books"><spring:message
													code="header.CateBook"></spring:message></a><a
												class="dropdown-item"
												href="../homeandfurniturecontroller/hafs"><spring:message
													code="header.CateHAF"></spring:message></a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link"
									href="../contactcontroller/contact"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link"
						href="../contactcontroller/contact?lang=vi">VN</a></li>
					<li class="nav-item"><a class="nav-link"
						href="../contactcontroller/contact?lang=en">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span><spring:message
							code="header.Login"></spring:message></span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList"><spring:message
								code="view.my"></spring:message></a> <a class="dropdown-item"
							href="../profileController/profile"><spring:message
								code="view.ac"></spring:message></a> <button class="dropdown-item"
							 onclick="logOut()"><spring:message
								code="cart.logout"></spring:message></button>
					</div>
				</div>
			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="location.href='../cartController/cartPage'"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart-guest">${cartItems}</span></i>
				</button>
			</c:if>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="contentContainer">
		<div id=content class="container" style="margin-top: 100px;">
			<div class="jumbotron text-center" style="margin-bottom: 0">
				<h1 class="text-center">TATALO</h1>
				<p>
					<spring:message code="contact.sologan"></spring:message>
				</p>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<form name="submitcontact" action="./contact" method="POST"
						onsubmit="submitinfo()">
						<div class="form-group">
							<label><spring:message code="contact.namefill"></spring:message></label>
							<input type="text" class="form-control" id="name" name="name">
						</div>

						<div class="form-group">
							<label><spring:message code="contact.emailfill"></spring:message></label>
							<input type="text" class="form-control" id="email" name="email">
						</div>
						<div class="form-group">
							<label for="comment"><spring:message
									code="contact.commentfill"></spring:message>:</label>
							<textarea class="form-control" rows="5" id="comment"
								name="comment"></textarea>
						</div>
						<button type="submit"
							class="registerbtn btn btn-outline-secondary"
							style="width: 100px;">
							<spring:message code="contact.submit"></spring:message>
						</button>
					</form>

				</div>
				<div class="col-lg-4 ">
					<p></p>
					<br>
					<h5>
						<spring:message code="contact.h5"></spring:message>
					</h5>
					<p>
						<i class='fas fa-address-card'></i>
						<spring:message code="contact.companyaddress"></spring:message>
					</p>
					<p>
						<i class='fas fa-phone'></i>
						<spring:message code="contact.companyphone"></spring:message>

					</p>
					<p>
						<i class='fas fa-mail-bulk'></i>
						<spring:message code="contact.companymail"></spring:message>

					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="searchResult"
		style="display: none; margin-top: 100px; margin-bottom: 80px;"
		class="container">
		<table id="searchResult-table" style="width: 100%;">
			<!-- <tr>
		<td>asdsd</td>
		</tr>
		<tr style="display: none">
		<td>asdsd</td>
		</tr> -->
		</table>
	</div>

	<div class="container-fluid" id="footer-top" style="margin-top: auto;">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p3"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shipping"></spring:message> </a> <a href="#"><spring:message code="footer.sitemap"></spring:message></a> <a href="#"><spring:message code="footer.term"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p4"></spring:message>
					</p>
					<a href="#"><spring:message code="footer.shippinginfo"></spring:message></a> <a
						href="../contactcontroller/contact"><spring:message code="footer.contactus"></spring:message></a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p5"></spring:message>
					</p>
					<a href="https://www.lazada.vn">Lazada</a> <a
						href="https://tiki.vn/">Tiki</a> <a href="https://shopee.vn/">Shoppee</a>
					<a href="https://www.amazon.com/">Amazon</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">
						<spring:message code="cart.p6"></spring:message>
					</p>
					<a href="https://www.mastercard.com.vn/">MasterCard</a> <a
						href="https://atmonline.vn/">ATM</a> <a href="www.vnpost.vn"><spring:message code="footer.cash"></spring:message></a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">
							<spring:message code="cart.p3"></spring:message>
						</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">
							<spring:message code="cart.p4"></spring:message>
						</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">
							<spring:message code="cart.p5"></spring:message>
						</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">TATALO</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="address"></spring:message>
								</p>
								<p style="margin-bottom: 2px;">
									<spring:message code="infomation"></spring:message>
								</p>
							</div>
						</div>
					</div>
					<div style="background-color: gray;color: black;text-align: center;">
						<p style="margin-top: auto;margin-bottom: auto;">
							<spring:message code="footer.compyright"></spring:message>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>